# VR Client version 2

## A custom made private client designed for the game "Runescape". 

### Design Goal

The client is designed to assist users with in-game features using GUIs and UIs that the orginial client lacks.

### Usage/How it was developed

To use the client it required managing "hooks" from the orginial client. By hooks we refer to the functions that are being called or any other precise information required by the client.
This required the use of Reflection and the use of Injection libraries to add our own customization. To simply explain on how the client worked, we took the orginial game JAR file, decompiled the
classes in the JAR file, and any custom features we had were inserted into the proper class files. After this step, the client would recompile the JAR file and reload the JAR file with our custom features.

### Pictures/Images of the client

https://imgur.com/a/joiJBuo

https://imgur.com/a/VMZykn7

https://imgur.com/a/68W8Brg

https://imgur.com/a/QTB4V9H

https://imgur.com/a/rVfvaiG

https://imgur.com/a/XsflxIS
