package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.Map;

import javax.imageio.ImageIO;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.NPC;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.Utilities;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class OpponentInfo extends Plugins<Player>{
	public static boolean opponentInfoEnabled;
	private Map<String, Integer> oppInfoHealth = loadNpcHealth();
	private Integer exactHP;
	private String hp;
	private Font font;
	
	public OpponentInfo(){
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.BOLD, 11);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public Player[] elements() {
		return Players.getAll(filter);
	} 

	@Override
	public boolean activate() {
		
		return config.drawOpponentInfo() && Game.isLoggedIn() && Players.getLocal().getInteracting() != null
				&& Players.getLocal().getInteracting().isValid() && Players.getLocal().getInteracting().getHealth() != -1;
	}
	
	@Override
	public void render(Graphics2D graphics) {
		opponentInfoEnabled = true;
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		
		//Background
		graphics.setColor(new Color(75, 67, 54, config.overlayOpacity));
		Utilities.fillRect(graphics, 10, 30, 130, 40);
        
		
        
        //Strings
        String opponent = Players.getLocal().getInteracting().getName();
       
        exactHP = oppInfoHealth.get(Players.getLocal().getInteracting().getName() + "_" + Players.getLocal().getInteracting().getCombatLevel());
        if (exactHP != null)
		{
        	int currHealth = Utilities.round(((Players.getLocal().getInteracting().getHealth()*.01) * exactHP));
        	hp = currHealth + "/" + exactHP;
			
		} else {
			 hp = Players.getLocal().getInteracting().getHealth()+"%";
		}
       
        
        
        //int
        int opponentx = ((130/2) - (fm.stringWidth(opponent)/2)) + 11;
        int opponenty = ((20/2) / 2 + fm.getAscent()) + 30;
        int hpx = ((130/2) - (fm.stringWidth(hp)/2)) + 12;
        int hpwidth = Players.getLocal().getInteracting().getHealth()* 127 / Players.getLocal().getInteracting().getMaxHealth();
        int hpxdead = ((130/2) - (fm.stringWidth("DEAD")/2)) + 12;
        int hpy = ((8/2) / 2 + fm.getAscent()) + 51;
        
        //Name
        graphics.setColor(Color.BLACK);
        graphics.drawString(Players.getLocal().getInteracting().getName(), opponentx+1, opponenty+1); //Name   
        graphics.setColor(Color.WHITE);
        Utilities.drawString2(graphics, Players.getLocal().getInteracting().getName(), opponentx, opponenty);
         
        //Hp Bar
        if(Players.getLocal().getInteracting().getMaxHealth() != 0) {
        	if(Players.getLocal().getInteracting().getHealth() == 0) {
        		//Bar
        		graphics.setColor(new Color(135, 50, 43));
	            graphics.fillRect(12, 52, 127, 16);
	            
	            //Text
	            graphics.setColor(Color.BLACK);
                graphics.drawString("DEAD", hpxdead, hpy+1); 
                graphics.setColor(Color.WHITE);
                graphics.drawString("DEAD", hpxdead-1, hpy); 
        	} else {
        		//Bar
        		graphics.setColor(new Color(135, 50, 43));
        		graphics.fillRect(12, 52, 127, 16);
	            graphics.setColor(new Color(53, 128, 42));
	            graphics.fillRect(12, 52, hpwidth, 16);
	            
	            //Text
	            graphics.setColor(Color.WHITE);
	            Utilities.drawString2(graphics, hp, hpx, hpy);
	            
        	}
        }
        
        if(config.drawoverhead() && Players.getLocal().getInteracting().isValid() && Players.getLocal().getInteracting().isOnScreen()) {
        	if(Players.getLocal().getInteractingIndex() >= 32768) {
        	int Overhead = Players.getLocal().getInteracting().getOverheadPray();
	        	if(Overhead != -1) {
	        		try {
	        		BufferedImage overheadpray = ImageIO.read(getClass().getResource("/resources/pray"+"_"+Overhead+".png"));
	        		graphics.drawImage(overheadpray, 128, 17, null);
	        		} catch (IOException e) {
	        			e.printStackTrace();
	        		}	
	        	}
        	}
        }
        
        
        graphics.setFont(null);
    }
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid();
        }
    };
	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		opponentInfoEnabled = false;
	}
	public static boolean opponentInfoEnabled() {
		return opponentInfoEnabled;
	}
	
	public static Map<String, Integer> loadNpcHealth()
	{
		Gson gson = new Gson();
		Type type = new TypeToken<Map<String, Integer>>(){}.getType();

		InputStream healthFile = OpponentInfo.class.getResourceAsStream("/resources/npc_health.json");
		return gson.fromJson(new InputStreamReader(healthFile), type);
	}

}