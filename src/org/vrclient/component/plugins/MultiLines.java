package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Tile;

public class MultiLines extends Plugins<Object>{

	@Override
	public void render(Graphics2D graphics) {
		for (int x = 3008; x <= 3550; x++) {
			for (int y = 3600; y <= 3800; y++) {
				if(Calculations.isOnMap(new Tile(x, y, Players.getLocal().getLocation().getZ()))) {
					if(!new Tile(x,y).isOnScreen()) continue;
					graphics.draw3DRect((int)Calculations.tileToScreen(new Tile(x,y)).getX(), (int)Calculations.tileToScreen(new Tile(x,y)).getY(), 4, 4, false);
				}
			}
		}
		
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && Calculations.drawMultiLines();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

}
