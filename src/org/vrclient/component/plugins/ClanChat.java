package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Player;

public class ClanChat extends Plugins<Player>{

	@Override
	public void render(Graphics2D graphics) {
		config.getBotFrame().getSidePanel().getCCPanel().clearNames();
		config.getBotFrame().getSidePanel().getCCPanel().timer = new Timer(30000);
			config.getBotFrame().getSidePanel().getCCPanel().setTotal(refresh().size()+"/"+Menu.clanList().size());
			for(String clanMembers: Menu.clanList()) {
				Player player = Players.getPlayerByName(clanMembers);
				if(player == null) {
					config.getBotFrame().getSidePanel().getCCPanel().addMissing(clanMembers);
				}
			}
			for (Player player: elements()) {
				if(Equipment.hasBindGear(player) || Equipment.hasMageGear(player) || Equipment.hasStaff(player)) {
					config.getBotFrame().getSidePanel().getCCPanel().addMage(player.getName());
				} else if(Equipment.isRanging(player)) {
					config.getBotFrame().getSidePanel().getCCPanel().addRanger(player.getName());
				} else{
					config.getBotFrame().getSidePanel().getCCPanel().addMelee(player.getName());
				}		
			}
			config.getBotFrame().getSidePanel().getCCPanel().updateText();
			
	}

	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && Menu.clanList().contains(player.getName());
        }
    };
	public Player[] elements() {
		return Players.getAll(filter);
	}
	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawCCInfo() && !Menu.clanList().isEmpty() && !config.getBotFrame().getSidePanel().getCCPanel().timer.isRunning();
	}

	@Override
	public void onStart() {
		}

	@Override
	public void onStop() {
	}

}
