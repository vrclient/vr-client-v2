package org.vrclient.component.plugins;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.Utilities;

import java.awt.*;


/**
 * Created by VR on 7/30/2014.
 */
public class PlayerNames extends Plugins<Player> {

    @Override
    public Player[] elements() {
        return Players.getAll(filter);
    }

    @Override
    public boolean activate() {
        return config.drawPlayers() && Game.isLoggedIn();
    }

    @Override
    public void render(Graphics2D graphics) {
        for (Player player : refresh()) {
        	Point point2 = Calculations.tileToMap(player.getLocation());
        	Point point = Calculations.groundToViewport(player.getLocalX(), player.getLocalY(), player.getHeight()/2);//player.getPointOnScreen();
        	graphics.setFont(new Font("Tahoma", Font.BOLD, 10));
        	graphics.setColor(Color.WHITE);
            Utilities.drawString(graphics,player.getName() + ":" + player.getSpellAnimation(),point.x,point.y);
            
        }
    }

    private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && player.isOnScreen();
        }
    };

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
}
