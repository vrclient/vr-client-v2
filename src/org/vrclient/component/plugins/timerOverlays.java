package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.utils.Utilities;

public class timerOverlays extends Plugins<Object> {
	public final static List<GameTimers> timers = new ArrayList<>();
	BufferedImage stam,af,saf,tb,frozen,death,veng,imbheart,magicimbue,hometele,overload,bandos,zanny,arma,sara,callisto,fanatic,crazyarch,kbd,scorp,vanenat,vetion,prime,rex,supreme,corp,mole;
	Font font;
	int y,x,width;
	int boxWidth = 30;
	int wY;
	public timerOverlays() {
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.PLAIN, 8);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		if(Game.resizable() && !Widgets.get(162).getChild(29).isVisible()) {
			wY = Widgets.get(162).getChild(1).getY();
		} else {
			wY = Widgets.get(162).getChild(0).getY();
		}
		graphics.setColor(new Color(75, 67, 54, config.overlayOpacity));
		Utilities.fillRect(graphics, Widgets.get(162).getChild(0).getWidth()-(timers.size()*boxWidth)-2, wY-boxWidth-2, (timers.size()*boxWidth), boxWidth);
		Instant now = Instant.now();
		int x = boxWidth;
		for (GameTimers timer : new ArrayList<>(timers)) {
			if (timer.getTimerTime().isBefore(now))
			{
				timers.remove(timer);
				continue;
			}
			
		
			// calculate time left
			Duration timeLeft = Duration.between(Instant.now(), timer.getTimerTime());
			try {
				graphics.drawImage(ImageIO.read(getClass().getResource("/resources/timerOverlays/"+timer.getTimer()+".png")), Widgets.get(162).getChild(0).getWidth()-(x + ((ImageIO.read(getClass().getResource("/resources/timerOverlays/"+timer.getTimer()+".png")).getWidth()-boxWidth)/2))-2, wY-boxWidth-1, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(timeLeft.getSeconds() < 10) {
				graphics.setColor(new Color(255,100,100,255));
			} else {
				graphics.setColor(Color.white);
			}
			Utilities.drawString(graphics, Utilities.formatTime(timeLeft.toMillis()), Widgets.get(162).getChild(0).getWidth()-(x+((fm.stringWidth(Utilities.formatTime(timeLeft.toMillis()))-boxWidth)/2))-2, wY-3);
            x += boxWidth;
        }
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && timers.size() > 0 && !Bank.isOpen();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
	
	public static GameTimers findTimerFor(String name)
	{
		for (GameTimers timer : timers)
		{
			if (timer.getTimer().equals(name))
			{
				return timer;
			}
		}
		return null;
	}
	
	public static class GameTimers
	{
		private final String str;
		private final Instant timerTime;

		public GameTimers(String str, Instant timerTime)
		{
			this.str = str;
			this.timerTime = timerTime;
		}

		public String getTimer()
		{
			return str;
		}

		public Instant getTimerTime()
		{
			return timerTime;
		}

	
	}

}
