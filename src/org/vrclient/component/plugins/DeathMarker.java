package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Tile;
import org.vrclient.main.utils.Utilities;

public class DeathMarker extends Plugins<Object>{
	public static Timer deathTimer;
	public static Tile deathTile;
	public static int deathWorld,deathPlane;
	Font font;
	int addY;
	int width = 130;
	public DeathMarker(){
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.BOLD, 11);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		if(OpponentInfo.opponentInfoEnabled()){
			if(Boosts.boostEnabled()){
				addY = 75 + Boosts.height() + 5;
			} else {
				addY = 75;
			}
		} else if(Boosts.boostEnabled()){
			addY = 30 + Boosts.height() + 5;
		} else {
			addY = 30;
		}
		graphics.setColor(new Color(75, 67, 54, config.overlayOpacity));
		Utilities.fillRect(graphics, 10, addY, width, fm.getHeight()+4);
		String str1 = "Died on world: ";
		String str2 = deathWorld+"";
		graphics.setColor(Color.white);
		Utilities.drawString2(graphics, str1, 12, addY + fm.getHeight());
		graphics.setColor(Color.green);
		Utilities.drawString2(graphics, str2, width+3-fm.stringWidth(str2), addY + fm.getHeight());
		if(deathTile.isOnScreen() && deathPlane == Game.getPlane()){
			if(deathWorld != Game.getCurrentWorld()){
				graphics.setColor(Color.RED);
				graphics.drawPolygon(deathTile.getBounds());
			} else {
				Point mm = deathTile.getPointOnMap();
				graphics.setColor(Color.RED);
				graphics.drawPolygon(deathTile.getBounds());
				
				if(deathTile.isOnMap()){
					graphics.setFont(new Font("Tahoma", Font.BOLD, 9)); 
					graphics.drawOval(mm.x, mm.y, 2, 2);
					Utilities.drawString(graphics, "You died here", mm.x+2, mm.y);
				}
			}
		}
		if(deathTile.getLocation().distanceTo(Players.getLocal().getLocation()) < 1 && deathPlane == Game.getPlane()){
			if(timerOverlays.findTimerFor("death") != null)
        		timerOverlays.timers.remove(timerOverlays.findTimerFor("death"));
			deathTimer = null;
			
		}
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && deathTimer != null && deathTimer.isRunning();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

	public static void deathWorld(int currentWorld) {
		deathWorld = currentWorld;
	}

	public static void deathTile(Tile location) {
		deathTile = location;
	}
	public static void deathPlane(int plane) {
		deathPlane = plane;
	}

}
