package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.utils.Utilities;

public class FPS extends Plugins<String> {
	
	int fpswidth = 0;
	
	@Override
	public void render(Graphics2D graphics) {
		if(Game.resizable()) {
        	fpswidth = Widgets.get(161).getChild(0).getWidth()-20;
        } else {
        	fpswidth = 745;
        }
        graphics.setFont(new Font("Tahoma", Font.BOLD, 12));
        graphics.setColor(Color.WHITE);
        Utilities.drawString2(graphics, Game.getFPS()+"", fpswidth, 15);
        
	}

	

	@Override
	public boolean activate() {
		return config.drawFPS() && Game.isLoggedIn();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}



	@Override
	public String[] elements() {
		return null;
	}

}