package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Settings;
import org.vrclient.main.utils.Utilities;

public class SpecBar extends Plugins<Object> {
	BufferedImage p100,p75,p50,p25,p0,rp100,rp75,rp50,rp25,rp0;
	private static int x,y,specTimer;
	private Font font;
	Double specTimerBar;
	DateTime start;
	DateTime now;
	String time;
	
	public SpecBar() {
		try {
			//Loads images only once on startup
			p100 = ImageIO.read(getClass().getResource("/resources/p100.png"));
			p75 = ImageIO.read(getClass().getResource("/resources/p75.png"));
			p50 = ImageIO.read(getClass().getResource("/resources/p50.png"));
			p25 = ImageIO.read(getClass().getResource("/resources/p25.png"));
			p0 = ImageIO.read(getClass().getResource("/resources/p0.png"));
			rp100 = ImageIO.read(getClass().getResource("/resources/rp100.png"));
			rp75 = ImageIO.read(getClass().getResource("/resources/rp75.png"));
			rp50 = ImageIO.read(getClass().getResource("/resources/rp50.png"));
			rp25 = ImageIO.read(getClass().getResource("/resources/rp25.png"));
			rp0 = ImageIO.read(getClass().getResource("/resources/rp0.png"));
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.PLAIN, 11);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (IOException | FontFormatException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		if(Game.resizable()){
			x = Game.getWidth()-145;
			y = 157; 
		} else {
			x = 708;
			y = 85; 
		}
		
		//Spec Timer
		specTimerBar = (Double.parseDouble(timer())*0.46666666666666);
		if(specTimer != getSpecialAmount() && specTimer != 1000){
			start = new DateTime();
			specTimer = getSpecialAmount();
		}
		
		//Spec image/percent
		
		if(Game.resizable()) {
			if(getSpecialAmount() == 0) {
				graphics.drawImage(rp0,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+4, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(Color.RED);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 25){
				graphics.drawImage(rp25,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+4, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(255, 36, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 49){
				graphics.drawImage(rp25,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+4, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(Color.orange);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 64){
				graphics.drawImage(rp50,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+4, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(146, 255, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 99){
				graphics.drawImage(rp75,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+4, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(146, 255, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() == 100){
				graphics.drawImage(rp100,x,y,null);
				graphics.setColor(Color.GREEN);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+3+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} 
		} else {
			if(getSpecialAmount() == 0) {
				graphics.drawImage(p0,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+32, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(Color.RED);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 25){
				graphics.drawImage(p25,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+32, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(255, 36, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 49){
				graphics.drawImage(p25,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+32, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(Color.orange);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 64){
				graphics.drawImage(p50,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+32, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(146, 255, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() <= 99){
				graphics.drawImage(p75,x,y,null);
				if(specTimerBar <= 14) {
					graphics.setColor(new Color(255,255,255,90));
					graphics.fill(new Rectangle2D.Double(x+32, y+28-specTimerBar, 21, specTimerBar));
				}
				graphics.setColor(new Color(146, 255, 0,255));
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} else if(getSpecialAmount() == 100){
				graphics.drawImage(p100,x,y,null);
				graphics.setColor(Color.GREEN);
				Utilities.drawString2(graphics, getSpecialAmount()+"", ((22-fm.stringWidth(getSpecialAmount()+""))/2)+31+x, ((14-fm.getHeight())/2)+14+y+fm.getAscent());
			} 
		
		}
		
		
		
		
			
	}
	public int getSpecialAmount() {
		return Settings.get(300)/10;
	}
	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
	return Game.isLoggedIn() && config.drawSpecBar();
	}

	@Override
	public void onStart() {
	
	}

	@Override
	public void onStop() {
	}
	
	public String timer() {
		DateTime now = new DateTime();
    	Period period = new Period(start, now, PeriodType.seconds());
    	PeriodFormatter HHMMSSFormater = new PeriodFormatterBuilder()
		        .printZeroAlways()
		        .minimumPrintedDigits(2)
		        .appendSeconds()
		        .toFormatter(); 
		
        return time = (String.valueOf(HHMMSSFormater.print(period)));
	}

}