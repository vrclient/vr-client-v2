package org.vrclient.component.plugins;

import java.awt.Graphics2D;
import java.util.ArrayList;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.Friends;
import org.vrclient.main.script.api.wrappers.Player;

public class FriendsList extends Plugins<Player>{

	@Override
	public void render(Graphics2D graphics) {
		Widgets.get(429).getChild(1).customText("Friends ("+Friends.getFriendCount()+"/400) - W"+Game.getCurrentWorld());
	}

	@Override
	public Player[] elements() {
		return null;
	}
	
	@Override
	public boolean activate() {
		if(Game.resizable())
			return Game.isLoggedIn() && Widgets.get(161, 76).isVisible();
		return Game.isLoggedIn() && Widgets.get(548,73).isVisible();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		//Widgets.get(429).getChild(1).customText("Friends ("+Friends.friendList().size()+"/400) - W"+Game.getCurrentWorld());
	}

}
