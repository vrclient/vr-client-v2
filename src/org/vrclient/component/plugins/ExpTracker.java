package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Map.Entry;

import javax.swing.SwingUtilities;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.listeners.ExperienceMonitor;
import org.vrclient.main.script.api.listeners.SoundMonitor;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Skills;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.utils.Utilities;

public class ExpTracker extends Plugins<String>{
	long attackttl,defensettl,strengthttl,hpttl,prayerttl,rangettl,magicttl,cookttl,wcttl,fletchttl,fishttl,fmttl,craftttl,smithttl,miningttl,herbttl,agilityttl,thievettl,slayerttl,farmttl,rcttl,hunterttl,conttl;
	int attackleft,defenseleft,strengthleft,hpleft,prayerleft,rangeleft,magicleft,cookleft,wcleft,fletchleft,fishleft,fmleft,craftleft,smithleft,miningleft,herbleft,agilityleft,thieveleft,slayerleft,farmleft,rcleft,hunterleft,conleft;
	@Override
	public String[] elements() {
		ArrayList<String> element = new ArrayList<String>();
		element.clear();
		for (Entry<Integer, Integer> entry : ExperienceMonitor.updatedExp.entrySet()) {
			element.add(Skills.getSkill(entry.getKey())+": "+Utilities.perHour(entry.getValue(), ExperienceMonitor.start));
		}
		return element.toArray(new String[element.size()]);
	}

	@Override
	public boolean activate() {
		return config.drawExperienceTracker() && Game.isLoggedIn();
	}
	@Override
	public void render(Graphics2D g) {
		if(!ExperienceMonitor.needsUpdate())
			return;
		ExperienceMonitor.run();
		for (final Entry<Integer, Integer> entry : ExperienceMonitor.updatedExp.entrySet()) {
			if(entry.getValue() > 0) {
				if(entry.getKey() == 0) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						attackleft = 0;
						attackttl = 0;
					} else {
						attackleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						attackttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().attacklvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().attackprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().attackgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().attackleft = attackleft;
					config.getBotFrame().getSidePanel().getSkillPanel().attackxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().attackttl = attackttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel0.setVisible(true);
				}
				if(entry.getKey() == 1) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						defenseleft = 0;
						defensettl = 0;
					} else {
						defenseleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						defensettl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().defenselvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().defenseprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().defensegained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().defenseleft = defenseleft;
					config.getBotFrame().getSidePanel().getSkillPanel().defensexphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().defensettl = defensettl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel1.setVisible(true);
				}
				if(entry.getKey() == 2) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						strengthleft = 0;
						strengthttl = 0;
					} else {
						strengthleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						strengthttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().strengthlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().strengthprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().strengthgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().strengthleft = strengthleft;
					config.getBotFrame().getSidePanel().getSkillPanel().strengthxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().strengthttl = strengthttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel2.setVisible(true);
				}
				if(entry.getKey() == 3) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						hpleft = 0;
						hpttl = 0;
					} else {
						hpleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						hpttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().hplvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().hpprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().hpgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().hpleft = hpleft;
					config.getBotFrame().getSidePanel().getSkillPanel().hpxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().hpttl = hpttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel3.setVisible(true);
				}
				if(entry.getKey() == 4) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						rangeleft = 0;
						rangettl = 0;
					} else {
						rangeleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						rangettl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().rangelvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().rangeprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().rangegained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().rangeleft = rangeleft;
					config.getBotFrame().getSidePanel().getSkillPanel().rangexphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().rangettl = rangettl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel4.setVisible(true);
				}
				if(entry.getKey() == 5) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						prayerleft = 0;
						prayerttl = 0;
					} else {
						prayerleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						prayerttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().prayerlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().prayerprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().prayergained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().prayerleft = prayerleft;
					config.getBotFrame().getSidePanel().getSkillPanel().prayerxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().prayerttl = prayerttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel5.setVisible(true);
				}
				if(entry.getKey() == 6) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						magicleft = 0;
						magicttl = 0;
					} else {
						magicleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						magicttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().magiclvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().magicprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().magicgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().magicleft = magicleft;
					config.getBotFrame().getSidePanel().getSkillPanel().magicxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().magicttl = magicttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel6.setVisible(true);
				}
				if(entry.getKey() == 7) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						cookleft = 0;
						cookttl = 0;
					} else {
						cookleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						cookttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().cooklvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().cookprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().cookgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().cookleft = cookleft;
					config.getBotFrame().getSidePanel().getSkillPanel().cookxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().cookttl = cookttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel7.setVisible(true);
				}
				if(entry.getKey() == 8) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						wcleft = 0;
						wcttl = 0;
					} else {
						wcleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						wcttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().wclvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().wcprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().wcgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().wcleft = wcleft;
					config.getBotFrame().getSidePanel().getSkillPanel().wcxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().wcttl = wcttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel8.setVisible(true);
				}
				if(entry.getKey() == 9) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						fletchleft = 0;
						fletchttl = 0;
					} else {
						fletchleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						fletchttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().fletchlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fletchprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fletchgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().fletchleft = fletchleft;
					config.getBotFrame().getSidePanel().getSkillPanel().fletchxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().fletchttl = fletchttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel9.setVisible(true);
				}
				if(entry.getKey() == 10) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						fishleft = 0;
						fishttl = 0;
					} else {
						fishleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						fishttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().fishlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fishprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fishgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().fishleft = fishleft;
					config.getBotFrame().getSidePanel().getSkillPanel().fishxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().fishttl = fishttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel10.setVisible(true);
				}
				if(entry.getKey() == 11) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						fmleft = 0;
						fmttl = 0;
					} else {
						fmleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						fmttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().fmlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fmprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().fmgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().fmleft = fmleft;
					config.getBotFrame().getSidePanel().getSkillPanel().fmxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().fmttl = fmttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel11.setVisible(true);
				}
				if(entry.getKey() == 12) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						craftleft = 0;
						craftttl = 0;
					} else {
						craftleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						craftttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().craftlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().craftprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().craftgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().craftleft = craftleft;
					config.getBotFrame().getSidePanel().getSkillPanel().craftxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().craftttl = craftttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel12.setVisible(true);
				}
				if(entry.getKey() == 13) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						smithleft = 0;
						smithttl = 0;
					} else {
						smithleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						smithttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().smithlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().smithprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().smithgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().smithleft = smithleft;
					config.getBotFrame().getSidePanel().getSkillPanel().smithxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().smithttl = smithttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel13.setVisible(true);
				}
				if(entry.getKey() == 14) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						miningleft = 0;
						miningttl = 0;
					} else {
						miningleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						miningttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().mininglvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().miningprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().mininggained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().miningleft = miningleft;
					config.getBotFrame().getSidePanel().getSkillPanel().miningxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().miningttl = miningttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel14.setVisible(true);
				}
				if(entry.getKey() == 15) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						herbleft = 0;
						herbttl = 0;
					} else {
						herbleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						herbttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().herblvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().herbprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().herbgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().herbleft = herbleft;
					config.getBotFrame().getSidePanel().getSkillPanel().herbxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().herbttl = herbttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel15.setVisible(true);
				}
				if(entry.getKey() == 16) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						agilityleft = 0;
						agilityttl = 0;
					} else {
						agilityleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						agilityttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().agilitylvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().agilityprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().agilitygained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().agilityleft = agilityleft;
					config.getBotFrame().getSidePanel().getSkillPanel().agilityxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().agilityttl = agilityttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel16.setVisible(true);
				}
				if(entry.getKey() == 17) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						thieveleft = 0;
						thievettl = 0;
					} else {
						thieveleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						thievettl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().thievelvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().thieveprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().thievegained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().thieveleft = thieveleft;
					config.getBotFrame().getSidePanel().getSkillPanel().thievexphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().thievettl = thievettl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel17.setVisible(true);
				}
				if(entry.getKey() == 18) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						slayerleft = 0;
						slayerttl = 0;
					} else {
						slayerleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						slayerttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().slayerlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().slayerprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().slayergained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().slayerleft = slayerleft;
					config.getBotFrame().getSidePanel().getSkillPanel().slayerxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().slayerttl = slayerttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel18.setVisible(true);
				}
				if(entry.getKey() == 19) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						farmleft = 0;
						farmttl = 0;
					} else {
						farmleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						farmttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().farmlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().farmprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().farmgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().farmleft = farmleft;
					config.getBotFrame().getSidePanel().getSkillPanel().farmxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().farmttl = farmttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel19.setVisible(true);
				}
				if(entry.getKey() == 20) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						rcleft = 0;
						rcttl = 0;
					} else {
						rcleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						rcttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().rclvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().rcprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().rcgained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().rcleft = rcleft;
					config.getBotFrame().getSidePanel().getSkillPanel().rcxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().rcttl = rcttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel20.setVisible(true);
				}
				if(entry.getKey() == 21) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						hunterleft = 0;
						hunterttl = 0;
					} else {
						hunterleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						hunterttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().hunterlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().hunterprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().huntergained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().hunterleft = hunterleft;
					config.getBotFrame().getSidePanel().getSkillPanel().hunterxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().hunterttl = hunterttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel21.setVisible(true);
				}
				if(entry.getKey() == 22) {
					if(Skills.getRealLevel(entry.getKey()) == 99) {
						conleft = 0;
						conttl = 0;
					} else {
						conleft = Skills.getExpAtLevel(Skills.getRealLevel(entry.getKey())+1)-Skills.getExperience(entry.getKey());
						conttl = Utilities.timetolevel((int) entry.getValue(), ExperienceMonitor.start1, entry.getKey());
					}
					config.getBotFrame().getSidePanel().getSkillPanel().conlvl = Skills.getRealLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().conprogress = Skills.getPercentToNextLevel(entry.getKey());
					config.getBotFrame().getSidePanel().getSkillPanel().congained = entry.getValue();
					config.getBotFrame().getSidePanel().getSkillPanel().conleft = conleft;
					config.getBotFrame().getSidePanel().getSkillPanel().conxphr = Utilities.perHour((int) entry.getValue(),ExperienceMonitor.start1);
					config.getBotFrame().getSidePanel().getSkillPanel().conttl = conttl;
					config.getBotFrame().getSidePanel().getSkillPanel().repaint();
					config.getBotFrame().getSidePanel().getSkillPanel().panel22.setVisible(true);
				}
					
					
					
					
			} 
				
			}
		}
	
		
		@Override
		public void onStart() {
			
			
		}
	
		@Override
		public void onStop() {		
			
		}
		
		
}