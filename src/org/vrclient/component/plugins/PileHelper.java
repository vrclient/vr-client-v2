package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;




import org.apache.commons.lang3.math.NumberUtils;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.script.api.enums.Tab;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.Player;


public class PileHelper extends Plugins<Player> {
	BufferedImage styles;
	
	public PileHelper() {
		try {
			styles = ImageIO.read(getClass().getResource("/resources/styles.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void render(Graphics2D graphics) {
		if(config.pilehelpercombat() && refresh().size() <= 0)
			return;
		Color colormage;
		Color colorrange;
		Color colormelee;
		int totalmage = 0, totalrange = 0, totalmelee = 0,totalpile = 0;
		
		 for (Player player : refresh()) {
			 if(Equipment.hasBindGear(player) || Equipment.hasStaff(player) || Equipment.hasMageGear(player)) {
					totalmage +=1;
				} else if(Equipment.isRanging(player)) {
					totalrange += 1;
				} else{
					totalmelee +=1;
				}		
		 }
		
		totalpile =  totalmage + totalrange + totalmelee;
		if(totalmage == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
			colormage = Color.red;
		} else {
			colormage = Color.white;
		}
		if(totalrange == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
			colorrange = Color.red;
		} else {
			colorrange = Color.white;
		}
		if(totalmelee == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
			colormelee = Color.red;
		} else {
			colormelee = Color.white;
		}
		 
		
		graphics.setFont(graphics.getFont().deriveFont(14.0f));
		
		//MELEERS
		if(Game.resizable() && Widgets.get(161).getChild(0).isVisible()) {
			if(Widgets.get(122).getChild(7).isVisible()){
				graphics.drawImage(styles, Widgets.get(161).getChild(0).getWidth() - 371, 37, null);
				graphics.setColor(Color.black);
				graphics.drawString(String.format("%02d", totalmage),Widgets.get(161).getChild(0).getWidth() - 360+1, 78);
				graphics.drawString(String.format("%02d", totalmelee),Widgets.get(161).getChild(0).getWidth() - 320+1, 78);
				graphics.drawString(String.format("%02d", totalrange),Widgets.get(161).getChild(0).getWidth() - 280+1, 78);
				graphics.setColor(colormage);
				graphics.drawString(String.format("%02d", totalmage),Widgets.get(161).getChild(0).getWidth() - 360, 78);
				graphics.setColor(colormelee);
				graphics.drawString(String.format("%02d", totalmelee),Widgets.get(161).getChild(0).getWidth() - 320, 78);
				graphics.setColor(colorrange);
				graphics.drawString(String.format("%02d", totalrange),Widgets.get(161).getChild(0).getWidth() - 280, 78);
			} else {
				graphics.drawImage(styles, Widgets.get(161).getChild(0).getWidth() - 371, 6, null);
				graphics.setColor(Color.black);
				graphics.drawString(String.format("%02d", totalmage),Widgets.get(161).getChild(0).getWidth() - 360+1, 47);
				graphics.drawString(String.format("%02d", totalmelee),Widgets.get(161).getChild(0).getWidth() - 320+1, 47);
				graphics.drawString(String.format("%02d", totalrange),Widgets.get(161).getChild(0).getWidth() - 280+1, 47);
				graphics.setColor(colormage);
				graphics.drawString(String.format("%02d", totalmage),Widgets.get(161).getChild(0).getWidth() - 360, 47);
				graphics.setColor(colormelee);
				graphics.drawString(String.format("%02d", totalmelee),Widgets.get(161).getChild(0).getWidth() - 320, 47);
				graphics.setColor(colorrange);
				graphics.drawString(String.format("%02d", totalrange),Widgets.get(161).getChild(0).getWidth() - 280, 47);
				
			}
		 
		} else {
			if(Widgets.get(122).getChild(7).isVisible()){
				graphics.drawImage(styles, 395, 37, null);
				graphics.setColor(Color.black);
				graphics.drawString(String.format("%02d", totalmage),406+1, 78);
				graphics.drawString(String.format("%02d", totalmelee),446+1, 78);
				graphics.drawString(String.format("%02d", totalrange),486+1, 78);
				graphics.setColor(colormage);
				graphics.drawString(String.format("%02d", totalmage),406, 78);
				graphics.setColor(colormelee);
				graphics.drawString(String.format("%02d", totalmelee),446, 78);
				graphics.setColor(colorrange);
				graphics.drawString(String.format("%02d", totalrange),486, 78);
			} else {
				graphics.setColor(Color.black);
				graphics.drawImage(styles, 395, 6, null);
				graphics.drawString(String.format("%02d", totalmage),406+1, 47);
				graphics.drawString(String.format("%02d", totalmelee),446+1, 47);
				graphics.drawString(String.format("%02d", totalrange),486+1, 47);
				graphics.setColor(colormage);
				graphics.drawString(String.format("%02d", totalmage),406, 47);
				graphics.setColor(colormelee);
				graphics.drawString(String.format("%02d", totalmelee),446, 47);
				graphics.setColor(colorrange);
				graphics.drawString(String.format("%02d", totalrange),486, 47);
			}
		
		}
		
		
		if(config.drawpilehelperprays()) {
			if(Widgets.get(548,68).isVisible() || Widgets.get(161, 66).isVisible() ) {
				if(totalpile > 5){
					if(totalmage == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
						graphics.setColor(new Color(50, 130, 40, 120));
						if(Widgets.get(271).getChild(16).isVisible())
						graphics.fillRect(Widgets.get(271).getChild(16).getX(),Widgets.get(271).getChild(16).getY(),Widgets.get(271).getChild(16).getWidth(),Widgets.get(271).getChild(16).getHeight());
					 } 
					if(totalrange == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
						graphics.setColor(new Color(50, 130, 40, 120));
						if(Widgets.get(271).getChild(17).isVisible())
						graphics.fillRect(Widgets.get(271).getChild(17).getX(),Widgets.get(271).getChild(17).getY(),Widgets.get(271).getChild(17).getWidth(),Widgets.get(271).getChild(17).getHeight());
					 } 
					if(totalmelee == NumberUtils.max(totalmage,totalrange,totalmelee) && totalpile > 5){
						graphics.setColor(new Color(50, 130, 40, 120));
						if(Widgets.get(271).getChild(18).isVisible())
						graphics.fillRect(Widgets.get(271).getChild(18).getX(),Widgets.get(271).getChild(18).getY(),Widgets.get(271).getChild(18).getWidth(),Widgets.get(271).getChild(18).getHeight());
					 }
				}
			}
		}
		
	}
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid()      
            		&& player.getInteracting().isValid()
            		&& player.getInteracting() instanceof Player
            		&& player.getInteracting().equals(Players.getLocal())
            		&& !Menu.clanList().contains(player.getName());
        }
    };
	@Override
	public Player[] elements() {
	return Players.getAll(filter);
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.enablepilehelper() && Widgets.get(160) != null && Widgets.get(160).isValid() && !Bank.isOpen();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}

}