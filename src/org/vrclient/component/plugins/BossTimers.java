package org.vrclient.component.plugins;

import java.awt.Graphics2D;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.List;

import org.vrclient.main.script.api.events.Event;
import org.vrclient.main.script.api.methods.data.Boss;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Actor;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class BossTimers extends Plugins<Object> {

	@Override
	public void render(Graphics2D graphics) {
		checkDead();
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawBossTimer() && Players.getLocal().getInteracting().isValid();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
	private final List<Boss> bosses = loadBossData();

	public Boss findBoss(String name)
	{
		for (Boss boss : bosses)
		{
			if (boss.getName().equals(name))
			{
				return boss;
			}
		}
		return null;
	}

	private static List<Boss> loadBossData()
	{
		Gson gson = new Gson();
		Type type = new TypeToken<List<Boss>>(){}.getType();

		InputStream in = BossTimers.class.getResourceAsStream("/resources/boss_timers.json");
		return gson.fromJson(new InputStreamReader(in), type);
	}
	private void checkDead()
	{
		Actor actor = Players.getLocal().getInteracting();

		if (actor == null || actor.getHealthRatio() != 0)
		{
			return;
		}

		Boss boss = findBoss(actor.getName());
		
		if (boss == null)
		{
			return;
		}

		if (timerOverlays.findTimerFor(boss.getType()) != null)
		{
			System.out.println(boss.getName()+ ":" + boss.getType() + ":" + boss.getSpawnTime());
			return;
		}

		Event.createTimer(boss.getSpawnTime(), boss.getType());

	}
}
