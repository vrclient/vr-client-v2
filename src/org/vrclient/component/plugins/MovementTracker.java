package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Graphics2D;



import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Actor;
import org.vrclient.main.utils.Utilities;

public class MovementTracker extends Plugins<Object>{
	static Timer timer;
	int x, y;
	Actor lastTarget;
	@Override
	public void render(Graphics2D graphics) {
		if(lastTarget.getHealthRatio() == 0 || lastTarget == null)
			return;
		graphics.setColor(Color.GREEN);
		try {
			BufferedImage img = ImageIO.read(getClass().getResource("/resources/bind2.png"));
			graphics.drawImage(img, x,y, null);
			graphics.drawString(Utilities.formatSeconds(timer.getRemaining()),x+8, y);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//graphics.drawString(Utilities.formatSeconds(timer.getRemaining()),Widgets.get(161).getChild(0).getWidth() - 280+1, 47);
		
	}

	@Override
	public Object[] elements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean activate() {
		if(Game.isLoggedIn() && config.drawBinderTimer() && Players.getLocal().getInteracting().getSpellAnimation() == 181) {
			if(Players.getLocal().getInteractingIndex() >= 32768 && Players.getLocal().getInteracting().getOverheadPray() == 2)
				timer = new Timer(2500);
			else
			timer = new Timer(5000);
			Point point2 = Calculations.groundToViewport(Players.getLocal().getInteracting().getLocalX(), Players.getLocal().getInteracting().getLocalY(), 150);
			x = point2.x+16;
			y= point2.y;
			lastTarget = Players.getLocal().getInteracting();
			return true;
		} else if (Game.isLoggedIn() && config.drawBinderTimer() && timer != null && timer.isRunning() && lastTarget != null) {
			Point point2 = Calculations.groundToViewport(lastTarget.getLocalX(), lastTarget.getLocalY(), 150);
			x = point2.x+16;
			y= point2.y;
			return true;
		}
	return false;
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		lastTarget = null;
		x = 0;
		y= 0;
	}

}