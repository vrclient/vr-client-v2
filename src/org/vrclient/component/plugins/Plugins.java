package org.vrclient.component.plugins;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.interfaces.PaintListener;
import org.vrclient.main.script.api.util.Timer;

/**
 * Created by VR on 7/30/2014.
 */
public abstract class Plugins<E> implements PaintListener {

    public Configuration config = Configuration.getInstance();
    protected List<E> list = new ArrayList<E>();
    public boolean startMethod = true;
    protected boolean endMethod = true;
    private Timer refreshRate = new Timer(1000);

    public abstract E[] elements();

    public abstract boolean activate();

    public abstract void onStart();
    
    public abstract void onStop();
    
    public boolean shouldEnd() {
    	return endMethod;
    }
    
    
    public void start() {
    	if(startMethod){
    		onStart();
    		startMethod = false;
    	}
    }
    public void stop() {
    }
    public List<E> refresh() {
        if (!refreshRate.isRunning()) {
            list = Arrays.asList(elements());
        }
        return list;
    }
    
    
}
