package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Map;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

public class CallerList extends Plugins<Player>{

	

	@Override
	public Player[] elements() {
		return Players.getAll(filter);
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawCallerInfo() && elements().length > 0;
	}
	public String getDisplayName(String name) {
		for (Map.Entry<String, String> entry : Configuration.getInstance().getBotFrame().getCallerPanel().rsns.entries())
		{
			if(entry.getValue().equalsIgnoreCase(name)) {
		    return entry.getKey();
			}
		}
			return name;
	}
	@Override
	public void render(Graphics2D graphics) {
			for(Player player: refresh()) {
				//Show who he/she is attacking
			           if(!Configuration.getInstance().getBotFrame().getCallerPanel().callers().contains(player.getInteracting().getName()) 
			            		&& player.getInteracting() != null 
			            		&& player.getInteracting().isValid() && player.getInteracting() instanceof Player && !Menu.clanList().contains(player.getInteracting().getName())) {
			            	Point point2 = Calculations.groundToViewport(player.getInteracting().getLocalX(), player.getInteracting().getLocalY(), 150);
			            	graphics.setFont(new Font("SansSerif", Font.BOLD, 12));
				        	graphics.setColor(Color.black);
				            graphics.drawString(player.getInteracting().getName(),point2.x+1,point2.y+6);
				            graphics.drawString(player.getInteracting().getName(),point2.x+1,point2.y+4);
				            graphics.drawString(player.getInteracting().getName(),point2.x-1,point2.y+6);
				            graphics.drawString(player.getInteracting().getName(),point2.x-1,point2.y+4);
				            
				           // Point mapPoint = Calculations.tileToMap(player.getLocation());
				           if (FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") != null){
				            	graphics.setColor(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color")));
					    		 
					    	} else {
				            	graphics.setColor(Color.cyan);
				            }
				           //graphics.drawString(player.getInteracting().getName(),mapPoint.x,mapPoint.y+5);
				           	//graphics.fillOval(mapPoint.x,mapPoint.y, 2, 2);
				            graphics.drawString(player.getInteracting().getName(),point2.x,point2.y+5);
				            
				            if(config.drawTilecaller1() && player.getInteracting().getLocation().getBounds() != null) {
					            if (FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") != null){
					            	Color lol1 = Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color")).darker();
					            	graphics.setColor(new Color(lol1.getRed(),lol1.getGreen(),lol1.getBlue(),100));
						    	} else {
					            	graphics.setColor(new Color(Color.cyan.getRed(),Color.cyan.getGreen(),Color.cyan.getBlue(),100));
					            }
								graphics.fillPolygon(player.getInteracting().getLocation().getBounds());
								graphics.setColor(Color.black);
								graphics.drawPolygon(player.getInteracting().getLocation().getBounds());
				            }
				            
			            }
			}
			
				            
				         
			            
	}

	
	
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && Configuration.getInstance().getBotFrame().getCallerPanel().callers().contains(player.getName())&& player.isOnScreen();
        }
    };

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		
	}

}