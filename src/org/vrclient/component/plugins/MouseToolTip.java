package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.utils.Utilities;

public class MouseToolTip extends Plugins<Object>{
	private final Pattern p = Pattern.compile("^<col=([^>]+)>([^<]*)");
	private Configuration config = Configuration.getInstance();
	private Font font;
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(new Font("Tahoma", Font.BOLD, 10));
		String[] targets = Menu.getOptions2();
		String[] options = Menu.getActions2();
		int count = Menu.getMenuSize() - 1;
		String target = targets[count];
		String option = options[count];
		Matcher m = p.matcher(target);
		if (!m.find())
		{
			return;
		}
		String colour = m.group(1);
		String matchedTarget = m.group(2);
		
		
		
		Point mouse = config.getBotFrame().getMousePosition();
		int x = mouse.x-1;
		int y = mouse.y-30;
		
		
		
		
		FontMetrics fm = graphics.getFontMetrics();
		// Gets the widths of the various strings we will be displaying
		int option_width = fm.stringWidth(option + " ");
		int total_width = option_width + fm.stringWidth(matchedTarget);
		int height = fm.getHeight();
		x -= total_width + 6; // Draw to the left of the mouse
		y -= height / 2;
		if (x < 0)
		{
			x = 0;
		}
		if (y < 0)
		{
			y = 0;
		}
		
		graphics.setColor(new Color(58, 50, 41, 120));
		graphics.fillRect(x, y - (height / 2), total_width + 6, height);
		
		graphics.setColor(new Color(58, 50, 41, 220));
		graphics.drawRect(x, y - (height / 2), total_width + 6, height);
		x += 3;
		y += 5;
		graphics.setColor(Color.black);
		graphics.drawString(option + " ", x+1, y+1);
		graphics.setColor(Color.white);
		// Draws the option (Use, Walk here, Wield)
		graphics.drawString(option + " ", x, y);
		// Sets the string colour to the colour the game uses.
		graphics.setColor(Color.black);
		graphics.drawString(matchedTarget, x + option_width+1, y+1);
		graphics.setColor(hex2rgb(colour));
		// Draws the target (Player, item)
		graphics.drawString(matchedTarget, x + option_width, y);

		
		
	}

	@Override
	public Object[] elements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean activate() {
		return config.drawToolTip() && Game.isLoggedIn() && !Bank.isOpen() && !Menu.isOpen() && !Menu.getActions().isEmpty() && (Menu.getActions().get(0) != "Walk here" || Menu.getActions().get(0) != "Cancel");
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
	
	private enum FoodItem {

        CABBAGE("Cabbage", 1),
        CAKE("Cake|2/3 cake|Slice of cake", 4),
        SARDINE("Sardine", 4),
        SHRIMPS("Shrimps", 3),
        ANCHOVIES("Anchovies", 1),
        COOKED_CHICKEN("Cooked chicken", 4),
        COOKED_MEAT("Cooked meat", 4),
        CHOCOLATE_CAKE("Chocolate cake|2/3 chocolate cake|Chocolate slice", 5),
        BREAD("Bread", 5),
        HERRING("Herring", 5),
        TROUT("Trout", 7),
        SALMON("Salmon", 9),
        TUNA("Tuna", 10),
        LOBSTER("Lobster", 12),
        SWORDFISH("Swordfish", 14),
        MONKFISH("Monkfish", 16),
        SHARK("Shark", 20),
        MANTA_RAY("Manta Ray", 22),
        TUNA_POTATO("Tuna potato", 22),
        DARK_CRAB("Dark crab", 22),
        PLAIN_PIZZA("Plain pizza", 7),
        MEAT_PIZZA("Meat pizza", 8),
        ANCHOVY_PIZZA("Anchovy pizza", 9),
        PINEAPPLE_PIZZA("Pineapple pizza", 11),
        REDBERRY_PIE("Redberry pie", 5),
        MEAT_PIE("Meat pie", 6),
        APPLE_PIE("Apple pie", 7),
        GARDEN_PIE("Garden pie", 6),
        FISH_PIE("Fish pie", 6),
        ADMIRAL_PIE("Admiral pie", 8),
        WILD_PIE("Wild pie", 11),
        SUMMER_PIE("Summer pie", 11),
        COOKED_KARAMBWAN("Cooked karambwan", 18),
        PEACH("Peach", 8),
        POTATO_WITH_CHEESE("Potato with cheese", 16),
        BASS("Bass", 13),
        TRIANGLE_SANDWICH("Triangle sandwich", 6),
        PINEAPPLE_RING("Pineapple ring", 2),
        UGHTHANKI_KEBAB("Ughthanki kebab", 19),
        FRIED_MUSHROOMS("Fried mushrooms", 5),
        PIKE("Pike", 8),
        CURRY("Curry", 19),
        EDIBLE_SEAWEED("Edible seaweed", 4);

        String name;
        int healthBoost;

        FoodItem(String name, int healthBoost) {
            this.name = name;
            this.healthBoost = healthBoost;
        }
        
        public static FoodItem forName(String name) {
            for (FoodItem item : FoodItem.values()) {
                if (name.startsWith(item.name)) {
                    return item;
                }
            }
            return null;
        }
    }
	private static Color hex2rgb(String col)
	{
		if (col.length() < 6)
		{
			return new Color(0, 255, 255);
		}

		return new Color(
			Integer.valueOf(col.substring(0, 2), 16),
			Integer.valueOf(col.substring(2, 4), 16),
			Integer.valueOf(col.substring(4, 6), 16)
		);
	}
	
}
