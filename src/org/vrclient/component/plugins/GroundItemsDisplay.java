package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.script.api.wrappers.GroundItem;
import org.vrclient.main.script.api.wrappers.threads.GroundItemThread;


/**
 * Created by Kenneth on 7/30/2014.
 */
public class GroundItemsDisplay extends Plugins<GroundItem> {
	GroundItemThread thread;

    @Override
    public GroundItem[] elements() {
        return GroundItems.getAll(filter);
    }

    @Override
    public boolean activate() {
        return Game.isLoggedIn() && config.drawGroundItems() && elements().length > 0;
    }
    @Override
    public void render(Graphics2D graphics) {
     thread = new GroundItemThread(elements(), graphics);
     thread.run();
    }

    private Filter<GroundItem> filter = new Filter<GroundItem>() {
        @Override
        public boolean accept(GroundItem groundItem) {
            return groundItem.isValid() && groundItem.isOnScreen() && groundItem.getPrice() > 10000
            		|| groundItem.isValid() && groundItem.isOnScreen() && groundItem.isFood()
            		|| groundItem.isValid() && groundItem.isOnScreen() && groundItem.getId() == 995 && groundItem.getStackSize() >= 50000;
        }
    };

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}
}