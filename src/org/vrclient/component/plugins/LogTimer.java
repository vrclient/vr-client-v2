package org.vrclient.component.plugins;

import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.vrclient.main.script.api.events.Event;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.methods.data.Settings;



public class LogTimer extends Plugins<Object>{
	DateTime start = new DateTime();
	DateTime now;
	String time;
	
	@Override
	public void render(Graphics2D graphics) {
		if(Widgets.get(162).getChild(28).isVisible()){
			Widgets.get(162).getChild(28).customText(timer());
		}
	}

	@Override
	public Object[] elements() {
		
		return null;
	}

	@Override
	public boolean activate() {
		return Game.getGameState() >= 25;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		start = new DateTime();
		
	}
	public String timer() {
		DateTime now = new DateTime();
    	Period period = new Period(start, now);
    	PeriodFormatter HHMMSSFormater = new PeriodFormatterBuilder()
		        .printZeroAlways()
		        .minimumPrintedDigits(2)
		        .appendHours().appendSeparator(":")
		        .appendMinutes().appendSeparator(":")
		        .appendSeconds()
		        .toFormatter(); 
		
        return time = (String.valueOf(HHMMSSFormater.print(period)));
	}
}
