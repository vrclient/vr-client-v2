package org.vrclient.component.plugins;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.methods.data.Settings;
import org.vrclient.main.utils.Utilities;

import java.awt.*;
import java.util.ArrayList;


public class PlayerSettings extends Plugins<String> {

    private int[] cache;

    private ArrayList<String> debugger = new ArrayList<>();

    @Override
    public String[] elements() {
        return new String[0];  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean activate() {
        return Configuration.getInstance().drawSettings();
    }

    @Override
    public void render(Graphics2D graphics) {
    	graphics.setFont(new Font("SansSerif", Font.PLAIN, 11));
    	graphics.setColor(Color.white);
        int[] settings = Settings.getAll();
        if (cache == null)
            cache = settings;
        for (int i = 0; i < 2000; i++) {
            if (cache[i] != settings[i]) {
                debugger.add(0, "Setting " + i + " : " + settings[i] + "-->" + cache[i]);
            }
        }
        while (debugger.size() > 25) {
            debugger.remove(debugger.size() - 1);
        }
        int y = 15;
        for (String s : debugger) {
        	Utilities.drawString2(graphics,s, 25, y);
            y += 13;
        }
        cache = Settings.getAll();
    }

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
}
