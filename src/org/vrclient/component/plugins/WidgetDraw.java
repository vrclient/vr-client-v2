package org.vrclient.component.plugins;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.ui.WidgetViewer;

import java.awt.*;


public class WidgetDraw extends Plugins<Object> {

	public static int x,y,height,width = -1;

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.getGameState() == Game.STATE_LOGGED_IN && Configuration.getInstance().drawWidgets() && WidgetViewer.getbounds() != null;
	}

	@Override
	public void render(Graphics2D graphics) {
		Rectangle rect = WidgetViewer.getbounds();
		if(rect.width >= 0 && rect.height >= 0){
			graphics.setColor(Color.GREEN);
			graphics.drawRect(rect.x,rect.y,rect.width,rect.height);
		}
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
}
