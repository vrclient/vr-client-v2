package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import javax.imageio.ImageIO;

import org.vrclient.component.plugins.timerOverlays.GameTimers;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.utils.Utilities;

public class FrozenOverlay extends Plugins<Object>{
	int size = 35;
	int x = 100;
	int y = 100;
	Image spell;
	boolean drawSpell = false;
	static Timer timer;
	BufferedImage bind,snare,entangle,rush,burst,blitz,barrage;
	Font font;
	
	public FrozenOverlay(){
		try {
			bind = ImageIO.read(getClass().getResource("/resources/bind.png"));
			snare = ImageIO.read(getClass().getResource("/resources/snare.png"));
			entangle = ImageIO.read(getClass().getResource("/resources/entangle.png"));
			barrage = ImageIO.read(getClass().getResource("/resources/barrage.png"));
			blitz = ImageIO.read(getClass().getResource("/resources/blitz.png"));
			burst = ImageIO.read(getClass().getResource("/resources/burst.png"));
			rush = ImageIO.read(getClass().getResource("/resources/rush.png"));
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.BOLD, 10);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (IOException | FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		
		
        
        if(Players.getLocal().getSpellAnimation() == 181 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(3000);
        	} else {
        		timer = new Timer(5500);
        	}
        	spell = bind;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 180 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(5500);
        	} else {
        		timer = new Timer(10500);
        	}
        	spell = snare;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 179 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(8000);
        	} else {
        		timer = new Timer(15500);
        	}
        	spell = entangle;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 361 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		Instant gameTime = Instant.now().plus(360, ChronoUnit.SECONDS);
	        	GameTimers GameTimers = new GameTimers("af", gameTime);
	        	timerOverlays.timers.add(GameTimers);
        		timer = new Timer(3000);
        	} else {
        		timer = new Timer(5500);
        	}
        	spell = rush;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 363 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(5500);
        	} else {
        		timer = new Timer(10500);
        	}
        	spell = burst;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 367 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(8000);
        	} else {
        		timer = new Timer(15500);
        	}
        	spell = blitz;
        	drawSpell = true;
        }
        if(Players.getLocal().getSpellAnimation() == 369 && !drawSpell) {
        	if(Players.getLocal().getOverheadPray() == 2){
        		timer = new Timer(10500);
        	} else {
        		timer = new Timer(20500);
        	}
        	spell = barrage;
        	drawSpell = true;
        }
        if(timer != null && timer.isRunning()) {
        	int y = Widgets.get(162).getChild(0).getY() - size;
    		int x = Widgets.get(162).getChild(0).getWidth() - size;
        	//Background
    		graphics.setColor(new Color(75, 67, 54, 200));
            graphics.fillRect(x, y, size, size);
            graphics.setColor(new Color(56, 48, 35, 255));
            graphics.drawRect(x, y, size, size);
        	graphics.drawImage(spell, x+((size-spell.getWidth(null))/2),y+1, null);
        	String time = Utilities.formatSeconds(timer.getRemaining());
        	graphics.setColor(Color.GREEN);
            Utilities.drawString(graphics, time, x+((size-fm.stringWidth(time))/2), y+size-fm.getHeight()+fm.getAscent());
        } else if(timer != null && !timer.isRunning()) {
        	drawSpell = false;
        }
        
        Utilities.drawString(graphics, Players.getLocal().getSpellAnimation()+"", 100, 100);
		
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
	
	
	
}
