package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Inventory;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.Item;
import org.vrclient.main.script.api.wrappers.WidgetChild;
import org.vrclient.main.utils.Utilities;

public class JewelryCharges extends Plugins<Item>{
	
	WidgetChild inv,gear;
	private String[] jewelry = new String[] {"dueling(", "glory(", "Games"};
	
	
	
	@Override
	public void render(Graphics2D graphics) {
		graphics.setFont(new Font("Tahoma", Font.PLAIN, 10));
		graphics.setColor(new Color(255,200,75,255));
		
		if(inv.isVisible()|| Bank.isOpen()){
			for(Item item : refresh()) {
				String str = item.getName();
				Utilities.drawString2(graphics, "(" + str.substring(str.indexOf("(") + 1, str.indexOf(")")) + ")", item.getCentralPoint().x,item.getCentralPoint().y);
			}
		}
		
		if(gear.isVisible() && !Bank.isOpen()){
			if(Equipment.getEquipmentNames().size() != 0 ){
				if(Equipment.getEquipmentNames().size() > 12 && stringContainsItemFromList(Equipment.getEquipmentNames().get(12),jewelry)){
					WidgetChild ring = Widgets.get(387).getChild(15);
					String str = Equipment.getEquipmentNames().get(12);
					Utilities.drawString2(graphics, "(" + str.substring(str.indexOf("(") + 1, str.indexOf(")")) + ")", ring.getX()+21, ring.getY()+30);
				}
				if(Equipment.getEquipmentNames().get(2) != null && stringContainsItemFromList(Equipment.getEquipmentNames().get(2),jewelry)){
					WidgetChild amulet = Widgets.get(387).getChild(8);
					String str = Equipment.getEquipmentNames().get(2);
					Utilities.drawString2(graphics, "(" + str.substring(str.indexOf("(") + 1, str.indexOf(")")) + ")", amulet.getX()+21, amulet.getY()+30);
				}
				
			}
		}
	}

	@Override
	public Item[] elements() {
		
		Item[] rod = Inventory.getAllItems(new Filter<Item>() {
            @Override
            public boolean accept(Item item) {
                return item.getName().contains("dueling(") || item.getName().contains("glory(") ||  item.getName().contains("Games");
            }
        });
		return rod;
	}

	@Override
	public boolean activate() {
		if(Game.resizable()) {
    		inv = Widgets.get(161).getChild(71);
    		gear = Widgets.get(161).getChild(72);
    	} else {
    		inv = Widgets.get(548).getChild(68);
    		gear = Widgets.get(548).getChild(69);
    	}
		return Game.isLoggedIn() && config.drawJewelry() && inv.isVisible() || Bank.isOpen() || gear.isVisible();
	}

	@Override
	public void onStart() {
		
		
	}

	@Override
	public void onStop() {
		
		
	}
	
	public static boolean stringContainsItemFromList(String inputStr, String[] items)
	{
	    for(int i =0; i < items.length; i++)
	    {
	        if(inputStr.contains(items[i]))
	        {
	            return true;
	        }
	    }
	    return false;
	}
}

