package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;

public class MenuDisplay extends Plugins<Object>{
	@Override
	public void render(Graphics2D graphics) {
           // Menu.colors = Menu.finalizedColoredActions();
            //System.out.println("Size: "+Menu.colors.size());
		//Menu.finalizedColoredActionNames();
		new Thread() {
			public void run() {
		Menu.finalizedColoredActions();
		Menu.updated = true;
			}
		}.start();
	}

	@Override
	public Object[] elements() {
		return new Object[0];
	}

	public boolean inClan() {
		Object[] clanMembers = (Object[]) Reflection.value("client_clanMembers", null);
		if(clanMembers == null)
			return false;
		return true;
	}
	@Override
	public boolean activate() {
		 return Game.isLoggedIn() && Game.inWilderness() && Menu.isOpen() && inClan() && !Menu.updated;
	}

	@Override
	public void onStart() {
		
	}

	@Override
	public void onStop() {
		if(!Menu.isOpen() && Menu.updated)
			Menu.updated = false;
	}

}