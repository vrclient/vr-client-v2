package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import javax.swing.SwingUtilities;

import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.input.AutoTyper;
import org.vrclient.main.script.api.methods.input.AutoTyper.AutoTyperRunnable;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.NPC;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.Utilities;


public class Spammer extends Plugins<Player>{
	@Override
	public void render(Graphics2D graphics) {
		if(Players.getLocal().getInteracting() == null || Menu.clanList().contains(Players.getLocal().getInteracting().getName()))
			return;
	
		if(config.getBotFrame().getSidePanel().getSpammerPanel().comboBox3.getSelectedItem().toString().equalsIgnoreCase("flash2")) {
		if(Players.getLocal().getInteracting().getName().contains(" ")) { //
			String name = Players.getLocal().getInteracting().getName().split(" ")[0];
			config.getAutoTyper().start("flash2:vr "+name, true);
		}else{
			config.getAutoTyper().start("flash2:vr "+fixPartialName(Players.getLocal().getInteracting().getName()), true);
		}
	} else {
		if(Players.getLocal().getInteracting().getName().contains(" ")) { //
			String name = Players.getLocal().getInteracting().getName().split(" ")[0];
			config.getAutoTyper().start(" vr "+name, true);
		} else {
			config.getAutoTyper().start("vr "+fixPartialName(Players.getLocal().getInteracting().getName()), true);
				   }
		}
	
		Game.keyBoardTimer = new Timer(500);
	}
public String fixPartialName(String str) {
	StringBuilder name = new StringBuilder();
	char[] array = str.toCharArray();
	if(array.length < 5)
		return str;
	for(int i = 0; i < 3; i++) {
		/*if(array[i] == '0')
			array[i] = 'o';
		if(array[i] == '1')
			array[i] = 'l';*/
		name.append(array[i]);
	}
	return name.toString();
}
	@Override
	public Player[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return config.autoSpammer() && Game.isLoggedIn() && Players.getLocal().getInteractingIndex() != -1
				&& Players.getLocal().getInteracting() != null
				&& Players.getLocal().getInteracting() instanceof Player
				&& !Menu.clanList().contains(Players.getLocal().getInteracting().getName())
				&& Menu.getPiledNames().contains(Players.getLocal().getInteracting().getName())
				&& !Game.keyBoardTimer.isRunning();
	}

	@Override
	public void onStart() {
		
	}

	@Override
	public void onStop() {
		
	}

}
