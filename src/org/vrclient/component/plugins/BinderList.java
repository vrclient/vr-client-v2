package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;

import org.vrclient.main.Constants;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

public class BinderList extends Plugins<Player>{
	Timer timer = new Timer(60000);
	@Override
	public Player[] elements() {
		return Players.getAll(filter);
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawBinder() && elements().length > 0;
	}
	public int offSetY(Player player) {
		for(Player p2 : refresh()) {
			if(player.getLocation() == p2.getLocation()) {
				
			}
		}
		return 1;
	}
	@Override
	public void render(Graphics2D graphics) {
		if(timer.isRunning()) {
		for(Player player: refresh()) {
			if(Menu.clanList() != null && Menu.clanList().contains(player.getName())) continue;
			//BinderList display
					Point point = Calculations.groundToViewport(player.getLocalX(), player.getLocalY(), 185);//player.getPointOnScreen();
		        	graphics.setFont(new Font("Tahoma", Font.BOLD, 10));
		            graphics.setColor(Color.BLACK);
		            if(config.writeBinderNames())
		            graphics.drawString(player.getName(),point.x+1,point.y+1);
		            if (FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") != null){
		            	graphics.setColor(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color")));
			    		 
			    	} else {
		            	graphics.setColor(Color.green);
		            }
		            if(config.writeBinderNames())
		            graphics.drawString(player.getName(),point.x,point.y);
		            config.getBotFrame().getBinderPanel().addBinder(player.getName());
			}
		} else {
			timer = new Timer(30000);
			config.getBotFrame().getBinderPanel().clearBinders();		
		}
	}
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
        	int[] animationids = {710, 1161, 1162};
        	for(int id : animationids) {
            return player.isValid() && player.getAnimation() == id && player.getCombatLevel() >= 110
            		|| player.isValid() && player.getCombatLevel() >= 110 && Equipment.hasBindGear(player);
        	}
        	return false;
        }
    };
	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		config.getBotFrame().getBinderPanel().clearBinders();
		
	}
}