package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

public class SniperList extends Plugins<Player>{
	@Override
	public Player[] elements() {
		return Players.getAll(filter);
	}

	@Override
	public boolean activate() {
		return config.drawEnemyLeaders() && Game.isLoggedIn();
	}
	
	@Override
	public void render(Graphics2D graphics) {
			for(Player player: refresh()) {
				
				//Caller display
					
						Point point = Calculations.groundToViewport(player.getLocalX(), player.getLocalY(), 150);//player.getPointOnScreen();
						graphics.setFont(new Font("SansSerif", Font.BOLD, 12));
			        	
						
			        	graphics.setColor(Color.BLACK);
			            graphics.drawString(player.getName(),point.x+1,point.y+6);
			            graphics.drawString(player.getName(),point.x+1,point.y+4);
			            graphics.drawString(player.getName(),point.x-1,point.y+6);
			            graphics.drawString(player.getName(),point.x-1,point.y+4);
			            if (FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") != null){
			            	graphics.setColor(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color")));
				    	} else {
			            	graphics.setColor(Color.red);
			            }
			            
			            graphics.drawString(player.getName(),point.x,point.y+5);
			            if (FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") != null){
			            	Color lol1 = Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color")).darker();
			            	graphics.setColor(new Color(lol1.getRed(),lol1.getGreen(),lol1.getBlue(),100));
				    	} else {
			            	graphics.setColor(new Color(Color.red.getRed(),Color.red.getGreen(),Color.red.getBlue(),100));
			            }
			            if(player.getLocation().getBounds() != null) {
			            graphics.fillPolygon(player.getLocation().getBounds());
			            graphics.setColor(Color.black);
			            graphics.drawPolygon(player.getLocation().getBounds());
			            }
			            }
			            
					}
	
	
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && Configuration.getInstance().getBotFrame().getSniperPanel().enemyCallers().contains(player.getName()) && player.isOnScreen();
        }
    };

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
}
