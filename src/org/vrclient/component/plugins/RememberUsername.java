package org.vrclient.component.plugins;

import java.awt.Graphics2D;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.client.security.encryption.AES;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.utils.FileUtils;

public class RememberUsername extends Plugins<Object> {
	
	private final Configuration config = Configuration.getInstance();
	private final AES encryption = new AES("Nf6JBwAn\\YGX,J,g", "dh'Tv3X(PX;fx`+u");
	
	@Override
	public void render(Graphics2D graphics) {
			Game.getUsername = (String) Reflection.value("client_username", null);
			Game.password = (String) Reflection.value("client_password", null);
			FileUtils.save(Constants.RS_FILE_NAME, "rsn", encryption.encrypt(Game.getUsername) + "");
			FileUtils.save(Constants.RS_FILE_NAME, "rspass", encryption.encrypt(Game.password) + "");
			Game.updatedRSN = false;
			
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return config.rememberme() && Game.getGameState() == 25;
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

}