package org.vrclient.component.plugins;

import java.awt.Graphics2D;
import java.lang.reflect.Field;

import org.vrclient.main.Configuration;
import org.vrclient.main.client.parser.FieldHook;
import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.client.security.encryption.AES;
import org.vrclient.main.script.api.methods.data.Game;

public class AutoLogin extends Plugins<Object> {	
		private final Configuration config = Configuration.getInstance();
		private final AES encryption = new AES("Nf6JBwAn\\YGX,J,g", "dh'Tv3X(PX;fx`+u");
		@Override
		public void render(Graphics2D graphics) {
			try {
			  	FieldHook fieldHook = HookReader.fields.get("client_username");
	    		Class<?> clazz = Configuration.getInstance().getBotFrame().loader().loadClass(fieldHook.getClassName());
	    		Field field = clazz.getDeclaredField(fieldHook.getFieldName());
	    		field.setAccessible(true);
	    		field.set(null, Game.getUsername);
	    		Reflection.value(field, null);
	    		
	    		fieldHook = HookReader.fields.get("client_password");
	    		clazz = Configuration.getInstance().getBotFrame().loader().loadClass(fieldHook.getClassName());
	    		field = clazz.getDeclaredField(fieldHook.getFieldName());
	    		field.setAccessible(true);
	    		field.set(null, Game.password);
	    		Reflection.value(field, null);
	    		Game.updatedRSN = true;
	        	} catch (Exception e) {
	        	}
			
		}

		@Override
		public Object[] elements() {
			return null;
		}

		@Override
		public boolean activate() {
		return config.rememberme() && Game.getGameState() == Game.STATE_LOG_IN_SCREEN && Game.getUsername != null && !Game.updatedRSN;
		}

		@Override
		public void onStart() {
		}

		@Override
		public void onStop() {
			
		}
		
	
}