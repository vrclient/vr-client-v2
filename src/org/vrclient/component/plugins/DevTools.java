package org.vrclient.component.plugins;

import org.vrclient.main.Configuration;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.data.movement.Camera;
import org.vrclient.main.script.api.methods.interactive.Npcs;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.NPC;
import org.vrclient.main.script.api.wrappers.definitions.ItemDefinition;

import java.awt.*;
import java.util.ArrayList;


/**
 * Created by Kenneth on 7/30/2014.
 */
public class DevTools extends Plugins<String> {

    private final ArrayList<String> debuggedList = new ArrayList<>();
    private final Configuration config = Configuration.getInstance();

  
    @Override
    public String[] elements() {
        debuggedList.clear();
        int mapScale = (int) Reflection.value("client_minimapScale", null);
		int mapOffset = (int) Reflection.value("client_minimapOffset", null);
		Point point2 = Calculations.tileToMap(Players.getLocal().getLocation());
		int angle = Camera.getMapAngle();
       // drawText(config.drawMouseLocation(), "Mouse Location -^^> " + Mouse.getLocation().toString());
	    drawText("Game State -^^> " + Game.getGameState());
	    drawText("Location - ^^> " + Players.getLocal().getLocation());
	    drawText("RAW Location -^^> " + Players.getLocal().getRawLocation());
	    drawText("Camera -^^> " + Camera.getX() + " " + Camera.getY());
	    drawText("Base -^^> " + Game.getBaseX() + " " + Game.getBaseY());
	    drawText("Local -^^> " + Players.getLocal().getLocalX() + " " + Players.getLocal().getLocalY());
	    drawText("Minimap Angle -^^> " + angle);
	    if(Players.getLocal().getInteractingIndex() != -1)
	    	drawText("Opponent's animation: "+Players.getLocal().getInteracting().getSpellAnimation());
		/**
	    if(Equipment.getBankGp() != null)
	    drawText(Equipment.getBankGp());
	    for(String name: Equipment.getEquipmentNames()) {
	    	drawText(name);
	    }
	    **/
	    //drawText("RING - ^^> " + Equipment.getRing(Players.getLocal()));
	    if(Players.getLocal().getInteractingIndex() != -1)
	    	drawText("Spell Animation -^^> " + Players.getLocal().getInteracting().getSpellAnimation());
	   // graphics.drawString("SOUTH", point2.x, point2.y+40);
	  //drawText(true, "Player health -^^> " + Players.getLocal().getInteracting().getMaxHealth());
	  //System.out.println("Player health -^^> " + Players.getLocal().getInteracting().getMaxHealth());
	  //  drawText(config.drawFloor(), "Floor -^^> " + Game.getPlane());
	   // drawText(config.drawMapBase(), "Map Base -^^> [" + Game.getBaseX() + " , " + Game.getBaseY() + "]");
	   // drawText(true, "Camera -^^> [" + Camera.getX() + " , " + Camera.getY() + " , " + Camera.getZ() + "] Pitch: " + Camera.getPitch() + " Yaw: " + Camera.getYaw() + " Map Angle: " + Camera.getMapAngle() + "]");
	   // drawText(true, "Camera Angle -^^> " + Players.getLocal().getLocation().getZ());
	   // drawText(config.drawMenu(), "Menu Rectangle -^^> " + Menu.getArea().toString());
	    //drawText(true, "Menu Open -^^> " + Menu.isOpen());

	    java.util.List<String> actions = Menu.getActions();
	    java.util.List<String> options = Menu.getOptions();
	    for (int i = 0; i < actions.size(); i++) {
		    if (options.size() > i) {
			    drawText("-^^> " + actions.get(i)+" "+options.get(i));
		    }
	    }
        
        return debuggedList.toArray(new String[debuggedList.size()]);
    }

    @Override
    public boolean activate() {
    	return config.drawTextDebugger();
    }

    @Override
    public void render(Graphics2D graphics) {
        int yOff = 30;
        for(String str : refresh()) {
            graphics.drawString(str, 15, yOff);
            yOff += 15;
        }
        if(Game.isLoggedIn()) {
    	    for(NPC npc : Npcs.getAll()) {
    	    	if(npc != null) {
    	    		Point point = Calculations.groundToViewport(npc.getLocalX(), npc.getLocalY(), npc.getHeight());
    	    		graphics.drawString(npc.getName() + ":" + npc.getId(), point.x, point.y);
    	    		}
    	    	}
    	    }
    }

    private void drawText(String debug){
            debuggedList.add(debug);
    }

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}

}