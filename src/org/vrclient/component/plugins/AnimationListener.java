package org.vrclient.component.plugins;

import java.awt.Graphics2D;

import org.vrclient.main.script.api.events.Event;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;

public class AnimationListener extends Plugins<Object> {
	int animationID = 0;
	@Override
	public void render(Graphics2D graphics) {
		Event.submitAnimationEvent(Players.getLocal().getSpellAnimation());
		animationID = Players.getLocal().getSpellAnimation();
		
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && animationID != Players.getLocal().getSpellAnimation();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

}
