package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;

public class BindTimer extends Plugins<Player> {

	@Override
	public void render(Graphics2D graphics) {
		for(Player player: refresh()) {
	Point point = Calculations.groundToViewport(player.getLocalX(), player.getLocalY(), 195);
	graphics.setFont(new Font("Tahoma", Font.BOLD, 10));
	graphics.setColor(Color.black);
   graphics.drawString("BINDED: "+player.getName(),point.x+1,point.y+1);
    
    graphics.setColor(Color.green);
   graphics.drawString("BINDED: "+player.getName(), point.x,point.y);
		}
	}

	@Override
	public Player[] elements() {
		return Players.getAll(filter);
	}
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && !Menu.clanList().contains(player.getName()) && player.getSpellAnimation() == 181;
        }
    };
	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawBinderTimer();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}

}
