package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Point;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;



public class MiniMap extends Plugins<Player> {
	@Override
    public Player[] elements() {
        return Players.getAll(filter);
    }

    @Override
    public boolean activate() {
        return config.drawMiniMap() && Game.isLoggedIn();
    }

    public String replaceName(String name) {
    	/*String[] names = {"Smoke battlestaff", "Bandos godsword", "Armadyl godsword", "Dragon warhammer", "Saradomin godsword", "Zamorakian hasta", 
						"Abyssal bludgeon", "Zamorakian spear", "Zamorak godsword", };*/
    	if(name.equalsIgnoreCase("armadyl godsword"))
    		return "AGS";
    	if(name.equalsIgnoreCase("bandos godsword"))
		return "BGS";
    	if(name.equalsIgnoreCase("saradomin godsword"))
    		return "SGS";
    	if(name.equalsIgnoreCase("Zamorak godsword"))
    		return "ZGS";
    	if(name.equalsIgnoreCase("dragon warhammer"))
    		return "DWH";
    	if(name.equalsIgnoreCase("dragon claws"))
    		return "Dragon Claws";
    	if(name.equalsIgnoreCase("Zamorakian hasta"))
    		return "HASTA";
    	if(name.equalsIgnoreCase("Toxic staff of the dead"))
    		return "Toxic staff";
    	if(name.equalsIgnoreCase("Abyssal bludgeon"))
    		return "BLUDGEON";
    	if(name.equalsIgnoreCase("Zamorakian spear"))
    		return "Z SPEAR";
    	if(name.equalsIgnoreCase("Armadyl crossbow"))
    		return "ACB";
    	if(name.equalsIgnoreCase("Dragonfire shield"))
    		return "DFS";
    	if(name.equalsIgnoreCase("Elysian spirit shield"))
    		return "ELYSIAN";
    	if(name.equalsIgnoreCase("Arcane spirit shield"))
    		return "ARCANE";
    	if(name.equalsIgnoreCase("Heavy ballista"))
    		return "HEAVY BALLISTA";
    	if(name.equalsIgnoreCase("Armadyl crossbow"))
    		return "ACB";
    	return name;
    }

	@Override
    public void render(Graphics2D graphics) {
        for (Player player : refresh()) {
        	if(Equipment.getWeapon2(player) != null || Equipment.getShield(player) != null) {
        	if(!Equipment.isValueable(player)) continue;
        	Point point = Calculations.tileToMap(player.getLocation());//player.getPointOnScreen();
        	graphics.setFont(new Font("Tahoma", Font.BOLD, 9));       
            graphics.setColor(Color.green);
           //String format = player.getName() + "\n [Combat: " + player.getCombatLevel() + "\n Weapon: " + Equipment.getWeapon(player) + "]\n";
        	graphics.setColor(Color.green);
            graphics.drawString(player.getName(),point.x,point.y);
            
        	graphics.setColor(Color.green);
            graphics.drawString("Cmb: "+player.getCombatLevel(),point.x,point.y+10);   
            
        	graphics.setColor(Color.green);
        	if(Equipment.getWeapon2(player) != null && Equipment.isWeaponValueable(player))
        		graphics.drawString("Weapon: "+replaceName(Equipment.getWeapon2(player)),point.x,point.y+20);
        	else if(Equipment.getShield(player) != null && Equipment.isShieldValueable(player))
        	graphics.drawString("Shield: "+replaceName(Equipment.getShield(player)),point.x,point.y+20);
        	else
        		graphics.drawString("Weapon: "+replaceName(Equipment.getWeapon2(player)),point.x,point.y+20);
        	}
        }
    }

    private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid();
        }
    };

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
	}
}