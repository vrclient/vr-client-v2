package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.Player;

public class PublicCC extends Plugins {
	
	@Override
	public void render(Graphics2D graphics) {
		Widgets.get(7).getChild(1).customText("Clan Chat ("+Menu.clanList().size()+"/100)");
	}

	@Override
	public Player[] elements() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && Menu.clanList().size() > 0 && Widgets.get(548).getChild(72).isVisible()
				|| Game.isLoggedIn() && Menu.clanList().size() > 0 && Widgets.get(161).getChild(75).isVisible();
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		if(Widgets.get(548).getChild(72).isVisible()
				|| Widgets.get(161).getChild(75).isVisible())
			Widgets.get(7).getChild(1).customText("Clan Chat");
		
	}

}