package org.vrclient.component.plugins;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Friends;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.Utilities;

import java.awt.*;


/**
 * Created by VR on 7/30/2014.
 */
public class FriendsDisplay extends Plugins<Player> {

    @Override
    public Player[] elements() {
        return Players.getAll(filter);
    }

    @Override
    public boolean activate() {
        return config.drawfriends() && Game.isLoggedIn() && !Bank.isOpen();
    }

    @Override
    public void render(Graphics2D graphics) {
    	final FontMetrics metrics = graphics.getFontMetrics();
        for (Player player : refresh()) {
        	Point point = Calculations.groundToViewport(player.getLocalX(), player.getLocalY(), 100);//player.getPointOnScreen();
        	graphics.setFont(new Font("Tahoma", Font.BOLD, 10));
        	graphics.setColor(Color.GREEN);
        	Utilities.drawString(graphics, player.getName(),point.x,point.y);
        }
    }

    private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && player.isOnScreen() && Friends.friendList().contains(player.getName());
        }
    };

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		// TODO Auto-generated method stub
		
	}
}
