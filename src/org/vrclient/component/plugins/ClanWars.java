package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.GameEntities;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.GameObject;
import org.vrclient.main.script.api.wrappers.Tile;

public class ClanWars extends Plugins<GameObject>{

	final int NORTH_PORTAL = 26728;
	final int SOUTH_PORTAL = 26727;
	
	GameObject northPortal = GameEntities.getNearest(26728);
	GameObject southPortal = GameEntities.getNearest(26727);
	public boolean inClanWars() {
		if(GameEntities.getNearest(26742).isValid() || Players.getLocal().getRawX() >= Game.getBaseX()+3648-400
				&& Players.getLocal().getRawX() <= Game.getBaseX()+9920)
			return true;
		return false;
	}
	@Override
	public void render(Graphics2D graphics) {
		if(Game.savedTankLocation == 1) { //SPAWN AT NORTH PORTAL
			Tile tile = new Tile(Players.getLocal().getX(), Players.getLocal().getY()+12);
			Tile tile2 = new Tile(Players.getLocal().getX(), Players.getLocal().getY()-12);
			Point point = Calculations.tileToMap(tile);
			Point point2 = Calculations.tileToMap(tile2);
			graphics.setFont(new Font("SansSerif", Font.BOLD, 12));
	    	graphics.setColor(Color.black);
	    	
	    	graphics.setColor(Color.RED);
         	graphics.drawString("TANK HERE", point.x, point.y);
		} else if(Game.savedTankLocation == 2) {
			Tile tile3 = new Tile(Players.getLocal().getX(), Players.getLocal().getY()+12);
			Tile tile4 = new Tile(Players.getLocal().getX(), Players.getLocal().getY()-12);
			Point point3 = Calculations.tileToMap(tile3);
			Point point4 = Calculations.tileToMap(tile4);
			graphics.setFont(new Font("SansSerif", Font.BOLD, 12));
	    	graphics.setColor(Color.black);
	    	
	    	 graphics.setColor(Color.RED);
         	graphics.drawString("TANK HERE", point4.x, point4.y);
		} else if(Game.savedTankLocation == 0) {
			 for (GameObject gameObject : refresh()) {
		            if (gameObject.isValid()) {
		            	if(gameObject.getId() == NORTH_PORTAL) {
	                	Game.savedTankLocation = 1;
	                } else if(gameObject.getId() == SOUTH_PORTAL) {
	                	Game.savedTankLocation = 2;
	                	}
	                }
			 }
		}
		
	}

	@Override
    public GameObject[] elements() {
        return GameEntities.getAll(new Filter<GameObject>() {
            @Override
            public boolean accept(GameObject gameObject) {
            	if(gameObject.getId() == NORTH_PORTAL || gameObject.getId() == SOUTH_PORTAL)
            		return true;
                return false;
            }
        });
    }

	@Override
	public boolean activate() {
		if(Game.isLoggedIn() && !GameEntities.getNearest(1326,3182).isValid() && Game.savedTankLocation != 0 
				|| Game.isLoggedIn() && inClanWars() && !GameEntities.getNearest(1326,3182).isValid())
			return true;
		return false;
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStop() {
		Game.savedTankLocation = 0;
	}

}
