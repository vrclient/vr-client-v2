package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;
import java.awt.RenderingHints;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.vrclient.main.script.api.methods.data.Bank;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Skills;
import org.vrclient.main.utils.Utilities;

public class Boosts extends Plugins<Object>{
	private static final int[] SHOW = new int[] { Skills.ATTACK, Skills.STRENGTH, Skills.DEFENSE, Skills.RANGE, Skills.MAGIC };
	public static boolean boostEnabled;
	private static final int TOP_BORDER = 3;
	private static final int LEFT_BORDER = 3;
	private static final int RIGHT_BORDER = 3;
	private static final int SEPARATOR = 2;
	private static int height;
	private int addY;
	private int width = 130;
	private Font font;
	
	
	public Boosts() {
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, getClass().getResourceAsStream("/resources/fonts/latobold.ttf"));
			font = font.deriveFont(Font.BOLD, 11);
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(font);
		} catch (FontFormatException | IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void render(Graphics2D graphics) {
		boostEnabled = true;
		if(OpponentInfo.opponentInfoEnabled()){
			addY = 75;
		} else {
			addY = 30;
		}
		graphics.setFont(font);
		graphics.setRenderingHint(
		        RenderingHints.KEY_TEXT_ANTIALIASING,
		        RenderingHints.VALUE_TEXT_ANTIALIAS_GASP);
		FontMetrics fm = graphics.getFontMetrics();
		
		height = SEPARATOR;
		for (int skill : SHOW)
		{
			int boosted = Skills.getCurrentLevel(skill),
				base = Skills.getRealLevel(skill);
			
			if(base != boosted) {
				height += fm.getHeight() + SEPARATOR;
			}
		}
		if(height != SEPARATOR){
			graphics.setColor(new Color(75, 67, 54, config.overlayOpacity));
			Utilities.fillRect(graphics, 10, addY, width, height);
			
			int y = 0;
			for (int skill : SHOW)
			{
				int boosted = Skills.getCurrentLevel(skill),
						base = Skills.getRealLevel(skill);
				if(base != boosted) {
					graphics.setColor(Color.white);
					Utilities.drawString2(graphics,Skills.getSkill(skill) + ":", 10+LEFT_BORDER, addY+y + fm.getHeight());
		
					String str1 = boosted+"";
					String str2 = "/" + base;
					if(base < boosted){
						graphics.setColor(Color.GREEN);
					} else {
						graphics.setColor(Color.RED);
					}
					Utilities.drawString2(graphics,str1, 10+width - RIGHT_BORDER - (fm.stringWidth(str1)+fm.stringWidth(str2)), addY+y + fm.getHeight());
					graphics.setColor(Color.WHITE);
					Utilities.drawString2(graphics,str2, 10+width - RIGHT_BORDER - (fm.stringWidth(str2)), addY+y + fm.getHeight());
		
					y += fm.getHeight() + 2;		
				}
			}
		}
		
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.drawBoosts() && !Bank.isOpen() && !Game.isMapOpen();
	}

	@Override
	public void onStart() {
	}

	@Override
	public void onStop() {
		boostEnabled = false;
	}
	public static boolean boostEnabled() {
		return boostEnabled;
	}
	public static int height(){
		return height;
	}
}
