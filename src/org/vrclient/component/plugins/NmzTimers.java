package org.vrclient.component.plugins;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.io.IOException;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Settings;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.utils.Utilities;

public class NmzTimers extends Plugins<Object>{
	
	DateTime start;
	DateTime now;
	String time;
	
	@Override
	public void render(Graphics2D graphics) {
		//Overload
		if(config.overloadAlert() && Game.getPlane() == 3 && Players.getLocal().getCurrentLevels()[0] <= Players.getLocal().getRealLevels()[0] && !config.getSoundMonitor().timer.isRunning()){
			new Thread() {
				public void run() {
					try {
						
						if(!config.getSoundMonitor().isRunning()) {
						config.getSoundMonitor().playClip(getClass().getResource("/resources/overload.wav"));
						config.getSoundMonitor().timer = new Timer(4000);
						}
					} catch (IOException | UnsupportedAudioFileException
							| LineUnavailableException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}.start();
		}
		
		//Absorb
		if(config.absorbAlert() && Game.getPlane() == 3 && config.nightmareZone() && Widgets.get(202) != null && Widgets.get(202).isValid() && Widgets.get(202).getChild(1).getChild(9).isVisible() && getAmount() < 100 && !config.getSoundMonitor().timer.isRunning()){
			new Thread() {
				public void run() {
					try {
						if(!config.getSoundMonitor().isRunning()) {
						config.getSoundMonitor().playClip(getClass().getResource("/resources/absorb.wav"));
						config.getSoundMonitor().timer = new Timer(4000);
						}
					} catch (IOException | UnsupportedAudioFileException
							| LineUnavailableException | InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}.start();
		}
		
		//Rapid Heal
		if(config.drawRapidHeal()) {
			if(Widgets.get(541).getChild(0).isVisible()) {
				if(Widgets.get(541).getChild(11).getChild(0).isVisible()){
					start = new DateTime();
				}
				
				
				if(Game.resizable() && Widgets.get(161).getChild(73).isVisible()) {
					if(Integer.parseInt(timer()) <= 40){
						graphics.setColor(new Color(0, 225, 25, 170));
					} else if(Integer.parseInt(timer()) <= 50) {
						graphics.setColor(new Color(255, 255, 0, 170));
					} else {
						graphics.setColor(new Color(255, 50, 50, 170));
					}
					
					
					graphics.fillRect(Widgets.get(541).getChild(11).getX(),Widgets.get(541).getChild(11).getY(),Widgets.get(541).getChild(11).getWidth(),Widgets.get(541).getChild(11).getHeight());
					
					FontMetrics fm = graphics.getFontMetrics();
					graphics.setColor(Color.BLACK);
					graphics.drawString(timer(), 1+Widgets.get(541).getChild(11).getX() + ((Widgets.get(541).getChild(11).getWidth()/2) - (fm.stringWidth(timer())/2)), 1+Widgets.get(541).getChild(11).getY()+((Widgets.get(541).getChild(11).getHeight()+fm.getAscent())/2));
					graphics.setColor(Color.WHITE);
					graphics.drawString(timer(), Widgets.get(541).getChild(11).getX() + ((Widgets.get(541).getChild(11).getWidth()/2) - (fm.stringWidth(timer())/2)), Widgets.get(541).getChild(11).getY()+((Widgets.get(541).getChild(11).getHeight()+fm.getAscent())/2));
				} else if(Widgets.get(548).getChild(70).isVisible()){
					if(Integer.parseInt(timer()) <= 40){
						graphics.setColor(new Color(0, 225, 25, 170));
					} else if(Integer.parseInt(timer()) <= 50) {
						graphics.setColor(new Color(255, 255, 0, 170));
					} else {
						graphics.setColor(new Color(255, 50, 50, 170));
					}
					graphics.fillRect(Widgets.get(541).getChild(11).getX(),Widgets.get(541).getChild(11).getY(),Widgets.get(541).getChild(11).getWidth(),Widgets.get(541).getChild(11).getHeight());
					
					FontMetrics fm = graphics.getFontMetrics();
					graphics.setColor(Color.BLACK);
					graphics.drawString(timer(), 1+Widgets.get(541).getChild(11).getX() + ((Widgets.get(541).getChild(11).getWidth()/2) - (fm.stringWidth(timer())/2)), 1+Widgets.get(541).getChild(11).getY()+((Widgets.get(541).getChild(11).getHeight()+fm.getAscent())/2));
					graphics.setColor(Color.WHITE);
					graphics.drawString(timer(), Widgets.get(541).getChild(11).getX() + ((Widgets.get(541).getChild(11).getWidth()/2) - (fm.stringWidth(timer())/2)), Widgets.get(541).getChild(11).getY()+((Widgets.get(541).getChild(11).getHeight()+fm.getAscent())/2));
				}
			}
		}
		
		
	}

	@Override
	public Object[] elements() {
		return null;
	}

	@Override
	public boolean activate() {
		return Game.isLoggedIn() && config.nightmareZone() && Game.getPlane() == 3;
	}

	@Override
	public void onStart() {
		
		
	}

	@Override
	public void onStop() {
	}

	public String timer() {
		DateTime now = new DateTime();
    	Period period = new Period(start, now, PeriodType.seconds());
    	PeriodFormatter HHMMSSFormater = new PeriodFormatterBuilder()
		        .printZeroAlways()
		        .minimumPrintedDigits(2)
		        .appendSeconds()
		        .toFormatter(); 
		
        return time = (String.valueOf(HHMMSSFormater.print(period)));
	}
	
	public int getAmount() {
		if(Widgets.get(202) != null 
				&& Widgets.get(202).isValid() 
				&& Widgets.get(202).getChild(1).getChild(9).isVisible() 
				&& !Widgets.get(202).getChild(1).getChild(9).getText().equalsIgnoreCase("1,000"))
			return Integer.parseInt(Widgets.get(202).getChild(1).getChild(9).getText());
		return 100;
	}
	
	
	
}
