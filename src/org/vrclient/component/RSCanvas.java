package org.vrclient.component;

import org.vrclient.component.plugins.*;
import org.vrclient.main.Configuration;
import org.vrclient.main.client.injection.Injected;
import org.vrclient.main.client.parser.FieldHook;
import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.script.api.interfaces.PaintListener;
import org.vrclient.main.script.api.listeners.ExperienceMonitor;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.SwingUtilities;

/**
 * Created by VR on 7/29/2014.
 */
public class RSCanvas extends java.awt.Canvas {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1866865517983194665L;
	private static Configuration config = Configuration.getInstance();
	public BufferedImage botBuffer = new BufferedImage(763, 503, BufferedImage.TYPE_INT_RGB);
	public BufferedImage gameBuffer = new BufferedImage(763, 503, BufferedImage.TYPE_INT_RGB);
	public volatile BufferedImage a = null;
	public volatile BufferedImage b = null;
	
	

	private static List<PaintListener> listeners = new ArrayList<>();

	
	public RSCanvas() {
		Configuration.getInstance().setCanvas(this);
	}

	public void loadAll() {
		final Plugins<?>[] debuggers = {
		            new MovementTracker(),
		            //new ClanWars(),
		            new GameObjects(),
		            new MenuDisplay(),
		            new GroundItemsDisplay(),
		            new FPS(),
		            new SpecBar(),
		            new PileHelper(),
		            new RememberUsername(),
		            new AutoLogin(),
		            new FriendsList(),
		            new PublicCC(),
		            new Spammer(),
		            new ClanChat(),
		            new SniperList(),
		            new BinderList(),
		            new OpponentInfo(),
		            new PlayerNames(),
		            new DevTools(),
		            new PlayerSettings(),
		            new WidgetDraw(),
		            new MiniMap(),
		            new CallerList(),
		            new NmzTimers(),
		            new ExpTracker(),
		            new MouseToolTip(),
		            new LogTimer(),
		            new Boosts(),
		            new timerOverlays(),
		            new DeathMarker(),
		            new AnimationListener(),
		            new BossTimers(),
		            //new JewelryCharges(),
		        };
	 Collections.addAll(listeners, debuggers);
	}
	 
	/**
	 * Doubling Canvas Buffering & drawing PaintListeners
	 *
	 * @return final Graphics
	 */
	@Override
	public Graphics getGraphics() {
		
		if(getWidth() != botBuffer.getWidth() || getHeight() != botBuffer.getHeight()){
			botBuffer = resize(botBuffer, getWidth(), getHeight());
			gameBuffer = resize(gameBuffer, getWidth(), getHeight());
	    }
		
		
			Graphics graphics = botBuffer.getGraphics();
			if (config.drawCanvas()) {
				graphics.drawImage(gameBuffer, 0, 0, null);
				for (PaintListener listener : getPaintListeners()) {
					if (listener instanceof Plugins) {
						final Plugins<?> debug = (Plugins<?>) listener;
						if (debug.activate()) {
							debug.render((Graphics2D) graphics);
						} else {
							debug.onStop();
						}
					} else {
						listener.render((Graphics2D) graphics);
					}
				}
			}				
		
			graphics.dispose();
			final Graphics2D rend = (Graphics2D) super.getGraphics();
			if(rend != null)
			rend.drawImage(botBuffer, 0, 0, null);
			
		
			
	 	
		return gameBuffer.getGraphics();	
	
	} 

	/**
	 * @return Original Canvas hashCode
	 */
	@Override
	public int hashCode() {
		return hashCode();
	}

	/**
	 * set Original field to return to this Canvas
	 */

	public void set() {
		try {
			FieldHook fieldHook = HookReader.fields.get("Client#getCanvas()");
			if (fieldHook == null) {
				System.out.println("Error in Canvas Can't find Hook info");
				return;
			}
			Class<?> clazz = config.getBotFrame().loader().loadClass(fieldHook.getClassName());
			Field field = clazz.getDeclaredField(fieldHook.getFieldName());
			field.setAccessible(true);
			field.set(null, this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<PaintListener> getPaintListeners() {
		return listeners;
	}

	public java.awt.Canvas getCanvas() {
		return this;
	}
	
	public BufferedImage getGameBuffer() {
		return gameBuffer;
	}
	
	public BufferedImage getBotBuffer() {
		return botBuffer;
	}
	public BufferedImage a(final BufferedImage bufferedImage) {
		final BufferedImage bufferedImage2;
		final Graphics graphics;
		(graphics = (bufferedImage2 = new BufferedImage(bufferedImage.getWidth(), 
				bufferedImage.getHeight(), bufferedImage.getType())).getGraphics()).drawImage(bufferedImage, 0, 0, null);
		graphics.dispose();
		return bufferedImage2;
	}
	
	private BufferedImage resize(BufferedImage img, int newWidth, int newHeight)
	{
		Image tmp = img.getScaledInstance(newWidth, newHeight, Image.SCALE_FAST);
		BufferedImage bufferedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics = bufferedImage.createGraphics();
		graphics.drawImage(tmp, 0, 0, null);
		graphics.dispose();

		return bufferedImage;
	}
	

}