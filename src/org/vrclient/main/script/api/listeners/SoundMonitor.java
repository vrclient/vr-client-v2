package org.vrclient.main.script.api.listeners;


import javax.sound.sampled.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.LineEvent.Type;

import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.util.Timer;

public class SoundMonitor implements Runnable{
	public boolean running = false;
	public URL file;
	public Timer timer = new Timer(0);

	public SoundMonitor() {
		
	}
	public void playClip(final URL url) throws IOException, 
	  UnsupportedAudioFileException, LineUnavailableException, InterruptedException {
		running = true;
	  class AudioListener implements LineListener {
	    private boolean done = false;
	    @Override public synchronized void update(LineEvent event) {
	    	
	      Type eventType = event.getType();
	      if (eventType == Type.STOP || eventType == Type.CLOSE) {
	        done = true;
	        notifyAll();
	      }
	    }
	    public synchronized void waitUntilDone() throws InterruptedException {
	      while (!done) { wait(); }
	    }
	  }
	  AudioListener listener = new AudioListener();
	  AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(url);
	  try {
	    Clip clip = AudioSystem.getClip();
	    clip.addLineListener(listener);
	    clip.open(audioInputStream);
	    try {
	      clip.start();
	      listener.waitUntilDone();
	    } finally {
	      clip.close();
	    }
	  } finally {
	    audioInputStream.close();
	  }
	  running = false;
	}
	@Override
	public void run() {
		if(!running) {
			running = true;
		try {
			playClip(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
			running = false;
			} catch (InterruptedException e) {
			}
	}
	}
	public boolean isRunning() {
		return running;
	}
}