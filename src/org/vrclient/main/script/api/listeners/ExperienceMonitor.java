package org.vrclient.main.script.api.listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.util.SkillData;

/**
 * Created by VR on 8/10/2014.
 */
public class ExperienceMonitor {

    private final Configuration config = Configuration.getInstance();
    private final static List<ExperienceListener> listeners = new ArrayList<>();
   
    private static SkillData sd = new SkillData();
    public static long start = System.currentTimeMillis();
    public static Map<Integer, Integer> experiences = new HashMap<>();
    public static Map<Integer, Integer> updatedExp = new HashMap<>();
    public static long start1;
    static boolean started = false;

    public ExperienceMonitor() {
       
    }

    public void addListener(ExperienceListener... listeners) {
        Collections.addAll(this.listeners, listeners);
    }
	public static void stop() {
		experiences.clear();
		updatedExp.clear();
		start();
		start = System.currentTimeMillis();
	}
	public static boolean needsUpdate() {
		 for(int i = 0; i < 24; i++) {
	            final int currXp = sd.experience2(i);
	            final int lastXp = experiences.get(i) == null ? currXp : experiences.get(i);
	            if(currXp > lastXp) {
	            	return true;
	            }
		 }
		return false;
	}
	public static void run() {
		if(!started){
			start();
		}
			
	        for(int i = 0; i < 24; i++) {
	            final int currXp = sd.experience2(i);
	            final int lastXp = experiences.get(i) == null ? currXp : experiences.get(i);
	            if(currXp > lastXp) {
	            	for(ExperienceListener listener : listeners) {
                        listener.onExperience(i, currXp - lastXp);
                    }
	                experiences.put(i, currXp);
	                if(!updatedExp.containsKey(i))
	                	updatedExp.put(i, currXp-lastXp);
	                else
	                updatedExp.put(i, updatedExp.get(i)+(currXp-lastXp));
	            }
	        }
	}
	
	public static void start() {
		if(experiences.isEmpty()) {
		sd = new SkillData();
		start1 = System.currentTimeMillis();
		started = true;
		  for(int i = 0; i < 24; i++) {
			  experiences.put(i, sd.experience2(i));
		  }
		  }
	}
	
	

}