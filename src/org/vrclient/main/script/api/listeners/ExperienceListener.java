package org.vrclient.main.script.api.listeners;

/**
 * Created by VR on 8/10/2014.
 */
public interface ExperienceListener {

    void onExperience(int skillId, int difference);

}
