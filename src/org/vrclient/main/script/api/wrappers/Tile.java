package org.vrclient.main.script.api.wrappers;/*
 * Created by VR on 7/30/14
 */

import org.vrclient.main.Constants;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.movement.Camera;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Time;

import java.awt.*;

public class Tile implements Locatable {
	public int x;
	public int y;
	int z;

	public Tile(int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Tile(int x, int y) {
		this.x = x;
		this.y = y;
		this.z = Game.getPlane();
	}

	/**
	 * @return Integer : X Coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return Integer : Y Coordinate
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return Integer : Z Coordinate
	 */
	public int getZ() {
		return z;
	}



	

	public Polygon getBounds() {
		if (!isOnScreen())
			return null;
		Polygon polygon = new Polygon();
		Point pn = Calculations.tileToScreen(new Tile(x, y, z), 0, 0, 0);
		Point px = Calculations.tileToScreen(new Tile(x + 1, y, z), 0, 0, 0);
		Point py = Calculations.tileToScreen(new Tile(x, y + 1, z), 0, 0, 0);
		Point pxy = Calculations.tileToScreen(new Tile(x + 1, y + 1, z), 0, 0, 0);
		polygon.addPoint(py.x, py.y);
		polygon.addPoint(pxy.x, pxy.y);
		polygon.addPoint(px.x, px.y);
		polygon.addPoint(pn.x, pn.y);
		return polygon;
	}

	/**
	 * @return boolean : true if this on viewPort else false
	 */
	@Override
	public boolean isOnScreen() {
		Rectangle gamescreen2 = new Rectangle(0, 0, RSLoader.getApplet().getWidth(), RSLoader.getApplet().getHeight());
		return gamescreen2.contains(getPointOnScreen());
	}

	/**
	 * @return Point: Point converted from WorldToScreen depend on X/Y
	 */
	@Override
	public Point getPointOnScreen() {
		return Calculations.tileToScreen(this);
	}


	@Override
	public int distanceTo() {
		return Calculations.distanceTo(this);
	}

	/**
	 * @param locatable
	 * @return Integer : distance from this to locatable
	 */
	@Override
	public int distanceTo(Locatable locatable) {
		return Calculations.distanceBetween(getLocation(), locatable.getLocation());
	}

	/**
	 * @param tile
	 * @return Integer : distance from Player to this
	 */
	@Override
	public int distanceTo(Tile tile) {
		return (int) Calculations.distanceBetween(getLocation(), tile);
	}


	@Override
	public String toString() {
		return "[" + this.x + "," + this.y + "," + this.z + "]";
	}

	@Override
	public boolean equals(Object a) {
		if (a != null && a instanceof Tile) {
			Tile t = (Tile) a;
			return t.x == this.x && t.y == this.y;
		}
		return false;
	}

	/**
	 * @return Tile : currentTile
	 */
	@Override
	public Tile getLocation() {
		return this;
	}


	public Point getPointOnMap() {
		return Calculations.tileToMap(this);
	}
	

	public boolean isOnMap() {
		return Calculations.isOnMap(this);
	}

	public Tile derive(int x, int y) {
		return this.derive(x, y, 0);
	}

	public Tile derive(int x, int y, int plane) {
		return new Tile(this.getX() + x, this.getY() + y, this.getZ() + plane);
	}

	public static Tile derive(Tile t, int x, int y) {
		return derive(t, x, y, 0);
	}

	public static Tile derive(Tile t, int x, int y, int plane) {
		return new Tile(t.getX() + x, t.getY() + y, t.getZ() + plane);
	}

	public void draw(final Graphics g) {
		draw(g, Color.WHITE);
	}

	public void draw(final Graphics g, final Color color) {
		if (Game.isLoggedIn() && isOnScreen()) {
			Point pn = Calculations.tileToScreen(new Tile(x, y, z), 0, 0, 0);
			Point px = Calculations.tileToScreen(new Tile(x + 1, y, z), 0, 0, 0);
			Point py = Calculations.tileToScreen(new Tile(x, y + 1, z), 0, 0, 0);
			Point pxy = Calculations.tileToScreen(new Tile(x + 1, y + 1, z), 0, 0, 0);
			if (Constants.VIEWPORT.contains(py) && Constants.VIEWPORT.contains(pxy) && Constants.VIEWPORT.contains(px) && Constants.VIEWPORT.contains(pn)) {
				g.setColor(color.darker());
				g.drawPolygon(new int[]{py.x, pxy.x, px.x, pn.x}, new int[]{py.y, pxy.y, px.y, pn.y}, 4);
				//g.setColor(color);
				g.setColor(new Color(color.getRed(), color.getGreen(), color.getBlue(), 100));
				g.fillPolygon(new int[]{py.x, pxy.x, px.x, pn.x}, new int[]{py.y, pxy.y, px.y, pn.y}, 4);
			}
		}
	}



	
}
