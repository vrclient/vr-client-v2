package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.client.parser.FieldHook;
import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.script.api.util.Time;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/*
 * Created by VR on 8/2/14
 */
public class WidgetChild {

    private Object raw;
    private int index;
    private int widgetX;
    private int widgetY;
    public static Rectangle gamescreen2;
    public WidgetChild(Object raw, int index) {
        this.raw = raw;
        this.index = index;
    }

    	public int getX() {
            if (raw == null)
                return -1;
            if (widgetX > 0)
                return widgetX;
            int staticPosition = getStaticPosition();
            int relX = (int) Reflection.value("Widget_X", raw);
            int[] posX = (int[]) Reflection.value("Client#getWidgetPositionsX()", null);
            WidgetChild parent = getParent();
            int x = 0;
            if (parent != null) {
                x = parent.getX() - parent.getScrollX();
            } else {
                if (staticPosition != -1 && posX[staticPosition] > 0) {
                    x = (posX[staticPosition] + (getType() > 0 ? relX : 0));
                    gamescreen2 = new Rectangle(0, 0, RSLoader.getApplet().getWidth(), RSLoader.getApplet().getHeight());
                    widgetX = gamescreen2.contains(new Point(x, 0)) ? x : relX;
                    return widgetX;
                }
            }
            widgetX = (relX + x);
            return widgetX;
        }


        public int getY() {
            if (raw == null) {
                return -1;
            }
            if (widgetY > 0)
                return widgetY;
            int staticPosition = getStaticPosition();
            int relY = (int) Reflection.value("Widget_Y", raw);
            int[] posY = (int[]) Reflection.value("Client#getWidgetPositionsY()", null);
            WidgetChild parent = getParent();
            int y = 0;
            if (parent != null) {
                y = parent.getY() - parent.getScrollY();
            } else {
                if (staticPosition != -1 && posY[staticPosition] > 0) {
                    y = (posY[staticPosition] + (getType() > 0 ? relY : 0));
                    gamescreen2 = new Rectangle(0, 0, RSLoader.getApplet().getWidth(), RSLoader.getApplet().getHeight());
                    widgetY = gamescreen2.contains(new Point(0, y)) ? y : relY;
                    return widgetY;
                }
            }
            widgetY = (y + relY);
            return widgetY;
        }

    public WidgetChild getParent() {
        if (raw == null) {
            return null;
        }
        Field widgetNodes = Reflection.field("Client#getWidgetNodes()");
        Field id = Reflection.field("WidgetNode#getId()");
        Field nodeUid = Reflection.field("Node_uid");
        int uid = getParentId();
        if (uid == -1) {
            int groupIdx = getId() >>> 16;
            final HashTableIterator hti = new HashTableIterator(Reflection.value(widgetNodes, null));
            for (Object n = hti.getFirst(); n != null; n = hti.getNext()) {
                if ((Reflection.value("WidgetNode#getId()", id, n)+"").equalsIgnoreCase(groupIdx+"")) {
                    uid = Integer.parseInt(((Long) Reflection.value(nodeUid, n)) + "");
                }
            }
        }

        if (uid == -1) {
            return null;
        }
        int parent = uid >> 16;
        int child = uid & 0xffff;

        return Widgets.get(parent, child);
    }

    public int getId() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getId()", raw);
    }

    public int getModelType() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getModelType()", raw);
    }

    public String[] getActions() {
        return (String[]) Reflection.value("client_menuActions", raw);
    }
    public String customText(String text) {
    	try {
        	FieldHook fieldHook = HookReader.fields.get("Widget_text");
    		Class<?> clazz = Configuration.getInstance().getBotFrame().loader().loadClass(fieldHook.getClassName());
    		Field field = clazz.getDeclaredField(fieldHook.getFieldName());
    		field.setAccessible(true);
    		field.set(raw, text);
    		return (String) Reflection.value(field, raw);
        	} catch (Exception e) {
        		e.printStackTrace();
        		return (String) Reflection.value("Widget_text", raw);
        	}
    }
    public int customLoc(int x) {
    	try {
        	FieldHook fieldHook = HookReader.fields.get("Widget_X");
    		Class<?> clazz = Configuration.getInstance().getBotFrame().loader().loadClass(fieldHook.getClassName());
    		Field field = clazz.getDeclaredField(fieldHook.getFieldName());
    		field.setAccessible(true);
    		field.set(raw, x);
    		return (int) Reflection.value(field, raw);
        	} catch (Exception e) {
        		e.printStackTrace();
        		return (int) Reflection.value("Widget_X", raw);
        	}
    }
    public String getText() {
    	 return (String) Reflection.value("Widget_text", raw);
    }

    public String getName() {
        return ((String) Reflection.value("Widget_name", raw)).replaceAll("<col=(.*?)>", "");
    }

    public int getTextColor() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getTextColor()", raw);
    }

    public int getRelativeX() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_ScrollX", raw);
    }

    public int getRelativeY() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_ScrollY", raw);
    }

    public int getWidth() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_Width", raw);
    }

    public int getHeight() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_Height", raw);
    }

	/**
	 * @Lorex
	 *
	 * @return Boolean : if widget is visible
	 */
	public boolean isVisible() {
		if(raw == null){
			return false;
		}
		if((boolean) Reflection.value("Widget_hidden", raw)){
			//System.out.println((boolean) Reflection.value("Widget_hidden", raw));
			return false;
		}
		int parentId = this.getParentId();
		if(parentId == -1){
			return true;
		}
		if(parentId == 0){
			return false;
		}
		final WidgetChild parent =  Widgets.get(parentId >> 16, parentId & 65535);
		if(!parent.isVisible()){
			return false;
		}
		return true;
	}

    public int getRotationX() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getRotationX()", raw);
    }

    public int getX2() {
    	 if(raw == null)
 		    return -1;
         return (int) Reflection.value("Widget_X", raw);
    }
    public int getY2() {
   	 if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_Y", raw);
   }
    public int getRotationY() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getRotationY()", raw);
    }

    public int getRotationZ() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getRotationZ()", raw);
    }

    /*public int getContentType() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getContentType()", raw);
    }*/

    public int getScrollX() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_ScrollX", raw);
    }

    public int getScrollY() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget_ScrollY", raw);
    }

    public int getTextureId() {
	    if(raw == null)
		    return -1;
        return (int) Reflection.value("Widget#getTextureId()", raw);
    }

    public int getModelId() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget#getModelId()", raw);
    }

    public int getBorderThickness() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget#getBorderThickness()", raw);
    }


    public int getType() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget#getType()", raw);
    }

    public int getParentId() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget#getParentId()", raw);
    }


    public int getItemStack() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget_itemQuantity", raw);
    }

    public int getStaticPosition() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget_boundsIndex", raw);
    }

    public int getParentIndex() {
        return getId() >> 16;
    }

    public int getItemId() {
        if(raw == null)
            return -1;
        return (int) Reflection.value("Widget_itemId", raw);
    }

    public int[] getSlotContentIds() {
        return (int[]) Reflection.value("Widget_itemIds", raw);
    }

    public int[] getStackSizes() {
        return (int[]) Reflection.value("Widget_itemQuantities", raw);
    }

    public int getIndex() {
        return index;
    }


    public WidgetChild[] getChildren() {
        List<WidgetChild> list = new ArrayList<>();
        Object[] children = (Object[]) Reflection.value("Widget_children", raw);
        if (children == null)
            return list.toArray(new WidgetChild[list.size()]);
        for (int i = 0; i < children.length; i++) {
            list.add(new WidgetChild(children[i], i));
        }
        return list.toArray(new WidgetChild[list.size()]);
    }

    public WidgetChild getChild(int index) {
        Object[] children = (Object[]) Reflection.value("Widget_children", raw);
        if (children == null || children.length <= index)
            return new WidgetChild(null, index);
        return new WidgetChild(children[index], index);
    }

  

    public Point getLocation() {
        return new Point(getX(), getY());
    }

    public Rectangle getArea() {
        return new Rectangle(getX(), getY(), getWidth(), getHeight());
    }


    class HashTableIterator {

        private Object hashTable;
        private int currindex;
        private Object curr;

        HashTableIterator(Object hashTable) {
            this.hashTable = hashTable;
        }

        final Object getFirst() {
            currindex = 0;
            return getNext();
        }

        final Object getNext() {
            if (hashTable == null)
                return null;
            final Object[] buckets = (Object[]) Reflection.value("NodeHashTable#getBuckets()", hashTable);
            if (buckets == null)
                return null;
            if (currindex > 0 && curr != buckets[currindex - 1]) {
                final Object node = curr;
                if (node == null) {
                    return null;
                }
                curr = Reflection.value("Node_previous", node);
                return node;
            }
            while (currindex < buckets.length) {
                final Object node1 = Reflection.value("Node_previous", buckets[currindex++]);
                if (node1 != null) {
                    if (buckets[currindex - 1] != node1) {
                        curr = Reflection.value("Node_previous", node1);
                        return node1;
                    }
                }
            }
            return null;
        }
    }
   
	public Point centerPoint() {
		final Point p = new Point(getX(), getY());
		p.translate(getWidth() / 2, getHeight() / 2);
		return p;
	}
	
}