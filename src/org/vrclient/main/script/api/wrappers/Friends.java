package org.vrclient.main.script.api.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Nameable;

public class Friends implements Nameable {
	private Object raw;

    public Friends(Object raw) {
    	this.raw = raw;
    }

   public String getName() {
	   if(raw == null)
		   return null;
	   return (String) Reflection.value("Friend_name", raw);
   }
   public String getPreviousName() {
	   if(raw == null)
		   return null;
	   return (String) Reflection.value("Friend_prevName", raw);
   }
    public boolean isValid() {
        return raw != null;
    }
    
   
    
	public static ArrayList<String> friendList() {
		ArrayList<String> friendsList = new ArrayList<String>();
		Object[] friends = (Object[]) Reflection.value("client_friends", null);
		if(friends == null)
			return friendsList;
		for(Object member: friends) {
			if(member != null) {
				Friends afriend = new Friends(member);
				friendsList.add(afriend.getName().toLowerCase().replace("\u00a0", " "));
			}
		}
		
		return friendsList;
	}

	public static int getFriendCount() {
		return (int) Reflection.value("friendCount", null);
	}
}
