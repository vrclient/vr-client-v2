package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.injection.callback.ModelCallBack;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Identifiable;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.interfaces.Nameable;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.data.movement.Camera;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.wrappers.definitions.ObjectDefinition;

import java.awt.*;

/*
 * Created by VR on 8/1/14
 */
public class GameObject implements Identifiable, Nameable, Locatable {

	public enum Type {
		INTERACTIVE("GameObject"), BOUNDARY("Boundary"), FLOOR_DECORATION("FloorDecoration"), WALL_OBJECT("WallObject");

		String cato;

		Type(String cato) {
			this.cato = cato;
		}
	}


	private Object raw;
	private Type type;
	private Tile tile;
	private int id;
	private ObjectDefinition objectDefinition;

	public GameObject(Object raw, Type type, int x, int y, int z) {
		this.raw = raw;
		this.type = type;
		this.tile = new Tile(x, y, z);
	}

	@Override
	public String getName() {
		if (objectDefinition == null) {
			objectDefinition = new ObjectDefinition(getId());
		}
		return objectDefinition.getName();
	}

	public String[] getActions() {
		if (objectDefinition == null) {
			objectDefinition = new ObjectDefinition(getId());
		}
		return objectDefinition.getActions();
	}

	public Type getType() {
		return type;
	}

	@Override
	public int getId() {
		if (raw == null)
			return 0;
		if (id == 0) {
			id = ((int) Reflection.value(type.cato + "#getId()", raw) >> 14) & 0x7FFF;
		}
		return id;
	}

	public int getX() {
		return tile.getX();
	}

	public int getY() {
		return tile.getY();
	}

	public boolean isValid() {
		return raw != null;
	}

	public Tile getLocation() {
		return tile;
	}


	public int getHeight() {
		if (raw == null)
			return 20;
		Object renderable = Reflection.value(type.cato + "#getRenderable()", raw);
		if (renderable == null)
			return 20;
		return (int) Reflection.value("Renderable#getModelHeight()", renderable);
	}


	@Override
	public boolean isOnScreen() {
		return Constants.VIEWPORT.contains(Calculations.tileToScreen(getLocation(), 0.5, 0.5, getHeight()));
	}

	@Override
	public Point getPointOnScreen() {
		return getLocation().getPointOnScreen();
	}

	public Point getInteractPoint() {
		Model bounds = getModel();
		if (bounds != null)
			return bounds.getRandomPoint();
		return Calculations.tileToScreen(getLocation(), 0.5, 0.5, getHeight());
	}

	@Override
	public int distanceTo() {
		return Calculations.distanceTo(this);
	}

	@Override
	public int distanceTo(Locatable locatable) {
		return Calculations.distanceBetween(tile, locatable.getLocation());
	}

	@Override
	public int distanceTo(Tile tile) {
		return Calculations.distanceBetween(tile, getLocation());
	}
	public void draw(Graphics2D g, Color color) {
		Model model = getModel();
		if (model == null)
			return;
		model.draw(g, color);
	}

	public void draw(Graphics2D g) {
		draw(g, Color.WHITE);
	}
	public Model getModel() {
		int gridX = (int) Reflection.value(type.cato + "#getX()", raw);
		int gridY = (int) Reflection.value(type.cato + "#getY()", raw);
		//int tileByte = Walking.getTileFlags()[Game.getPlane()][getLocation().x - Game.getBaseX()][getLocation().y - Game.getBaseY()];
		
		//int z = tileByte == 1 ? 210 : 0;
		Object[] renderable = new Object[]{Reflection.value(type.cato + "#getRenderable()", raw), null};
		if (instanceOf(renderable[0])) {
			return new Model(new Model(renderable[0]), 0, gridX, gridY, 0);
		}
		if (instanceOf(renderable[1])) {
			return new Model(new Model(renderable[1]), 0, gridX, gridY, 0);
		}

		return renderable[0] != null && ModelCallBack.get(renderable[0]) != null ? new Model(ModelCallBack.get(renderable[0]), 0, gridX, gridY, 0): null;
	}
	
	public boolean instanceOf(Object first) {
		if (first == null)
			return false;
		try {
			Reflection.value("Model#getVerticesX()", first);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}

}
