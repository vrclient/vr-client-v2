package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.client.reflection.Reflection;

public class Node {
	private Object raw;
	public Node(Object raw) {
		this.raw = raw;	
	}
	/**
	 * Returns the raw reflection object
	 */
	protected Object getRaw() {
		return raw;
	}
	public Node getNext() {
		if(raw == null)
			return null;
		return (Node) Reflection.value("Node_next", raw);
	}
	public Node getPrevious() {
		if(raw == null)
			return null;
		return (Node) Reflection.value("Node_previous", raw);
	}
	public long getID() {
		if(raw == null)
			return -1;
		return (Long) Reflection.value("Node_uid", raw);
	}
	public String getName() {
        if (getRaw() == null)
            return null;
        return (String) Reflection.value("ClanMember_name", getRaw()).toString();
	}
}
