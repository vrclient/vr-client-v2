package org.vrclient.main.script.api.wrappers.definitions;

import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.utils.PriceChecker;

public class Pricing extends Thread {

	public int price, id;
	public Pricing(int id) {
		this.id = id;
	}
	@Override
	public void run() {
		try {
	price = PriceChecker.getOSbuddyPrice(id);
	GroundItems.priceDefinitions.put(id, price);
		} catch (Exception e) {
			System.out.println("Error getting pricing!");
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
