package org.vrclient.main.script.api.wrappers.definitions;

import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;

import java.util.Hashtable;

/*
 * Created by VR on 7/31/14
 */
public class ItemDefinition {

    private static Hashtable<Integer, String> cache = new Hashtable<>();

    private String name;

    public ItemDefinition(int Id) {
        if (cache.get(Id) == null) {
        	if(HookReader.methods.get("Client#getItemComposite()").getDataType().equalsIgnoreCase("short")) {
            Object raw = Reflection.invoke("Client#getItemComposite()", null, Id, (short)HookReader.methods.get("Client#getItemComposite()").getCorrectParam());
            cache.put(Id, (String) Reflection.value("ItemDefinition_name", raw));
        	} else if(HookReader.methods.get("Client#getItemComposite()").getDataType().equalsIgnoreCase("byte")) {
        		 Object raw = Reflection.invoke("Client#getItemComposite()", null, Id, (byte)HookReader.methods.get("Client#getItemComposite()").getCorrectParam());
                 cache.put(Id, (String) Reflection.value("ItemDefinition_name", raw));
        	} else {
        		 Object raw = Reflection.invoke("Client#getItemComposite()", null, Id, HookReader.methods.get("Client#getItemComposite()").getCorrectParam());
                 cache.put(Id, (String) Reflection.value("ItemDefinition_name", raw));
        	}
        }
        name = cache.get(Id);
    }

    /**
     * Item Name
     *
     * @return String: Item Name
     */
    public String getName() {
        return name;
    }

    /**
     * check if item composite is null or not
     *
     * @return Boolean : return true if not null else false
     */
    public boolean isValid() {
        return name != null;
    }
}
