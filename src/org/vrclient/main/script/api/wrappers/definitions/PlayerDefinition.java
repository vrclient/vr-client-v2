package org.vrclient.main.script.api.wrappers.definitions;

import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;

/*
 * Created by VR on 7/31/14
 */
public class PlayerDefinition {

    private Object raw;
    private Object transformedComposite;

    public PlayerDefinition(Object raw) {
    	this.raw = raw;   	
    }

    public boolean isFemale() {
        if (raw == null)
            return false;
        return false;//return (Boolean) Reflection.value("PlayerComposite#isFemale()", raw);
    }

    public int[] getEquipment() {
        if (raw == null)
            return null;
        return (int[]) Reflection.value("PlayerDefinition_equipmentIds", raw);
    }

    public boolean isValid() {
        return raw != null;
    }
}
