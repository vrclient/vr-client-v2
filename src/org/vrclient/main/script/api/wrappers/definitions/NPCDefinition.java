package org.vrclient.main.script.api.wrappers.definitions;

import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;

/*
 * Created by VR on 7/31/14
 */
public class NPCDefinition {

	private Object raw;
	private Object transformedComposite;

	public NPCDefinition(Object raw) {
		this.raw = raw;
		if (raw != null) {
			String name = (String) Reflection.value("NpcDefinition_name", raw);
			if (name == null || name.equalsIgnoreCase("null")) {
				//byte correctParam = (byte) HookReader.methods.get("NpcDefinition#getChildComposite()").getCorrectParam();
				if(HookReader.methods.get("NpcDefinition#getChildComposite()").getDataType().equalsIgnoreCase("short")) {
					transformedComposite = Reflection.invoke("NpcDefinition#getChildComposite()", raw, getId(), (short)HookReader.methods.get("NpcDefinition#getChildComposite()").getCorrectParam());
		        	} else if(HookReader.methods.get("NpcDefinition#getChildComposite()").getDataType().equalsIgnoreCase("byte")) {
		        		transformedComposite = Reflection.invoke("NpcDefinition#getChildComposite()", raw, getId(), (byte)HookReader.methods.get("NpcDefinition#getChildComposite()").getCorrectParam());
		        	} else {
		        		transformedComposite = Reflection.invoke("NpcDefinition#getChildComposite()", raw, getId(), HookReader.methods.get("NpcDefinition#getChildComposite()").getCorrectParam());
		        	}
				//transformedComposite = Reflection.invoke("NpcDefinition#getChildComposite()", raw, getId(), (byte) HookReader.methods.get("NpcDefinition#getChildComposite()").getCorrectParam());
			}
		}
	}

	public int getId() {
		if (raw == null)
			return -1;
		return (int) Reflection.value("NpcDefinition_id", raw);
	}

	public String getName() {
		if (raw == null)
			return null;
		return (String) Reflection.value("NpcDefinition_name", transformedComposite != null ? transformedComposite : raw);
	}

	public String[] getActions() {
		if (raw == null)
			return null;
		return (String[]) Reflection.value("NPCComposite#getActions()", transformedComposite != null ? transformedComposite : raw);
	}

	public int getCombatLevel() {
		if (raw == null)
			return -1;
		return (int) Reflection.value("NpcDefinition#level", raw);
	}

	public int[] getModelIds() {
		if (raw == null)
			return null;
		return null;//return (int[]) Reflection.value("NPCComposite#getModelIds()", transformedComposite != null ? transformedComposite : raw);
	}

	public boolean isValid() {
		return raw != null;
	}
}
