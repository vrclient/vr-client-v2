package org.vrclient.main.script.api.wrappers.definitions;

import java.util.Hashtable;

import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;

public class ItemContainer {
	
	public String var1, var2, gameMessage;


private static Hashtable<Integer, Object> cache = new Hashtable<>();

public ItemContainer(int id) {
	 if (cache.get(id) == null) {
     	if(HookReader.methods.get("Client#getGameMessage()").getDataType().equalsIgnoreCase("short")) {
         Object raw = Reflection.invoke("Client#getGameMessage()", null, id, var1, var2, (short)HookReader.methods.get("Client#getGameMessage()").getCorrectParam());
         cache.put(id, raw);
     	} else if(HookReader.methods.get("Client#getGameMessage()").getDataType().equalsIgnoreCase("byte")) {
     		 Object raw = Reflection.invoke("Client#getGameMessage()", null, id, (byte)HookReader.methods.get("Client#getGameMessage()").getCorrectParam());
              cache.put(id, raw);
     	} else {
     		 Object raw = Reflection.invoke("Client#getGameMessage()", null, id, HookReader.methods.get("Client#getGameMessage()").getCorrectParam());
              cache.put(id, raw);
     	}
     }
     gameMessage = (String) cache.get(id);
	}
public Object getMessage() {
	return gameMessage;
}
}
