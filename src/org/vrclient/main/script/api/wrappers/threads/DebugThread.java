package org.vrclient.main.script.api.wrappers.threads;

import java.awt.Graphics;
import java.awt.Graphics2D;

import org.vrclient.component.plugins.Plugins;

public class DebugThread implements Runnable{

	Plugins<?> debug;
	Graphics graphics;
	public DebugThread(Plugins<?> debug, Graphics graphics) {
		this.debug = debug;
		this.graphics = graphics;
	}
	@Override
	public void run() {
		if (debug.activate()) {
			//debug.startup();
			debug.render((Graphics2D) graphics);				
		} else {
			//if(debug.shouldEnd())
				//debug.end();
			debug.onStop();
		}
		
	}

}
