package org.vrclient.main.script.api.wrappers.threads;

import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.utils.PriceLookup;

public class PriceThread implements Runnable{

	public int id;
	public PriceThread(int id) {
		this.id = id;
	}
	@Override
	public void run() {
		if(!GroundItems.priceDefinitions.containsKey(id)) {
		int price = PriceLookup.getPrice(id);
		if(price > 0)
		GroundItems.priceDefinitions.put(id, price);
		}
	}

}
