package org.vrclient.main.script.api.wrappers.threads;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.util.Arrays;
import java.util.List;

import org.vrclient.main.script.api.wrappers.GroundItem;
import org.vrclient.main.utils.Utilities;

public class GroundItemThread implements Runnable{
public GroundItem[] groundItems;
public Graphics graphics;

public GroundItemThread(GroundItem[] groundItems, Graphics graphics) {
	this.groundItems = groundItems;
	this.graphics = graphics;
}
public List<GroundItem> refresh() {
       return Arrays.asList(groundItems);
}
@Override
public void run() {
	graphics.setFont(new Font("Tahoma", Font.PLAIN, 10));
	 FontMetrics metrics = graphics.getFontMetrics();
	 for (GroundItem groundItem : groundItems) {
         if (groundItem.isValid()) {
             Point point = groundItem.fixedPoint(groundItem.sameLocationList(refresh()));
             String format;
             if(groundItem.getStackSize() > 1) {
             	format = groundItem.getName() + " (" + Utilities.formatNumber(groundItem.getStackSize()) + ") ("+groundItem.formattedPrice()+" gp)";
             } else {
             	int multiple = groundItem.occurences(refresh());
             	if(multiple > 1)
             		format = groundItem.getName()+ " (" +  Utilities.formatNumber(multiple) + ") ("+groundItem.formattedPrice(multiple)+" gp)";
             	else
             	format = groundItem.getName() + " ("+groundItem.formattedPrice()+" gp)";
             }
             graphics.setColor(Color.black);                         
             graphics.drawString(format, point.x - (metrics.stringWidth(format) / 2)+1, point.y+1);
            
             if(groundItem.getPrice() > 50000)
            	 graphics.setColor(Color.red);
             else
             graphics.setColor(Color.white);
             graphics.drawString(format, point.x - (metrics.stringWidth(format) / 2), point.y);
            // graphics.drawString(format, point.x - (metrics.stringWidth(format) / 2), point.y);
             //Point point2 = groundItem.fixedLastPoint(groundItem.sameLocationList(refresh()));
        	 //graphics.drawString("Total Loot: "+groundItem.totalLoot(groundItem.sameLocationList(refresh())), point2.x, point2.y-(10*groundItem.sameLocationList(refresh()).size()));
         }
     }
}

}
