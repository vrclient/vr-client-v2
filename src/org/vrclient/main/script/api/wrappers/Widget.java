package org.vrclient.main.script.api.wrappers;

import java.util.ArrayList;
import java.util.List;

import org.vrclient.main.client.reflection.Reflection;

/*
 * Created by VR on 8/2/14
 */
public class Widget {

    private Object[] raw;
    private int index;

    public Widget(Object[] raw, int index) {
        this.raw = raw;
        this.index = index;
    }

    public WidgetChild[] getChildren() {
        List<WidgetChild> list = new ArrayList<>();
        if (raw == null)
            return list.toArray(new WidgetChild[list.size()]);
        for (int i = 0; i < raw.length; i++) {
            list.add(new WidgetChild(raw[i], i));
        }
        return list.toArray(new WidgetChild[list.size()]);
    }

    public WidgetChild getChild(int index) {
        if (raw == null || raw.length <= index) {
            return new WidgetChild(null, index);
        }
        return new WidgetChild(raw[index], index);
    }

    public boolean isValid() {
        return raw != null;
    }

    public int getIndex() {
        return index;
    }
    
    public static int[] getSettings() {
        return (int[]) Reflection.value("Widget#getSettings()", null);
    }
    public static int getSettings(int a) {
        int[] settings = getSettings();
        if (settings.length <= a)
            return 0;
        return settings[a];
    }
}
