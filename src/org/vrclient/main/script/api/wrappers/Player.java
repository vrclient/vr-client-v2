package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Nameable;
import org.vrclient.main.script.api.methods.data.Equipment;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.definitions.PlayerDefinition;

/**
 * Created by VR on 7/30/2014.
 */
public class Player extends Actor implements Nameable {

    private PlayerDefinition playerDefinition;
    public Player(Object raw) {
        super(raw);
        if (raw != null) {
            this.playerDefinition = new PlayerDefinition(Reflection.value("Player_playerDefinition", raw));
        }
    }
    
    /**
     * Player name
     *
     * @return String: return the name of player
     */
    @Override
    public String getName() {
    	 try {
             return ((String) Reflection.value("Player_name", getRaw())).toLowerCase().replace("\u00a0", " ");
            } catch (NullPointerException e) {
            	return null;
            }
    }

    /**
     * Combat Level
     *
     * @return Integer: combat level of player
     */
    public int getCombatLevel() {
        if (getRaw() == null)
            return 0;
        return (int) Reflection.value("Player_level", getRaw());
    }

    /**
     * Player's equipments
     *
     * @return Integer[]: player's equipments
     */
    public int[] getEquipment() {
        return playerDefinition.getEquipment();
    }

    /**
     * Check for player's if female or not
     *
     * @return boolean: player's female check
     */
    public boolean isFemale() {
        return playerDefinition.isFemale();
    }
    public boolean isAttackable() {
    if(Game.inWilderness()) {
    	int Level = Integer.parseInt(Widgets.get(90).getChild(42).getText());
    	if(Players.getLocal().getCombatLevel()-getCombatLevel() > Level) 
			return false;
    	else
    		return true;
    	}
    return true;
    }
    public int[] getCurrentLevels() {
    	return (int[]) Reflection.value("client_current_levels", null);
    }
    public int[] getRealLevels() {
    	return (int[]) Reflection.value("client_real_levels", null);
    }

	
}
