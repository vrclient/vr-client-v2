package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Nameable;

public class ClanMember implements Nameable {
	private Object raw;

    public ClanMember(Object raw) {
    	this.raw = raw;
    }

   public String getName() {
	   if(raw == null)
		   return null;
	   return (String) Reflection.value("ClanMember_name", raw);
   }

    public boolean isValid() {
        return raw != null;
    }
}
