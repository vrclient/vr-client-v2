package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.enums.Tab;
import org.vrclient.main.script.api.interfaces.Identifiable;
import org.vrclient.main.script.api.interfaces.Nameable;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.wrappers.definitions.ItemDefinition;

import java.awt.*;

/*
 * Created by VR on 8/3/14
 */
public class Item implements Identifiable, Nameable {

	private int index;
	private int id;
	private int stackSize;
	private Rectangle area;
	private ItemDefinition itemDefinition;
	private Type type;

	public enum Type {
		INVENTORY, BANK, DEPOSIT_BOX
	}

	public Item(int id, int stackSize, int index, Type type, Rectangle area) {
		this.type = type;
		this.id = id;
		this.stackSize = stackSize;
		this.area = area;
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public Type getType() {
		return type;
	}

	public int getStackSize() {
		return stackSize;
	}

	public Rectangle getArea() {
		return area;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		if (itemDefinition == null)
			itemDefinition = new ItemDefinition(id);
		return itemDefinition.getName();
	}

	public boolean isValid() {
		return id > 0 && stackSize > 0;
	}

	public Point getCentralPoint() {
		return new Point((int) getArea().getCenterX(), (int) getArea().getCenterY());
	}

}
