package org.vrclient.main.script.api.wrappers;/*
 * Created by VR on 7/30/14
 */

import org.apache.commons.lang3.text.WordUtils;
import org.vrclient.main.Configuration;
import org.vrclient.main.client.injection.callback.ModelCallBack;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.data.movement.Camera;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Time;

import java.awt.*;
import java.lang.reflect.Field;


public class Actor implements Locatable {

	private final Object raw;

	public Actor(Object raw) {
		this.raw = raw;
	}


	/**
	 * Returns the raw reflection object
	 */
	protected Object getRaw() {
		return raw;
	}

	/**
	 * This get you text Message that appear upon Actor Head
	 *
	 * @return Spoken Message Text that Actor Speak
	 */
	public String getSpokenMessage() {
		return (String) Reflection.value("Actor#getSpokenMessage()", raw);
	}

	/**
	 * This you get animation Id of Actor
	 *
	 * @return animation Id
	 */
	public int getAnimation() {
		if (raw == null)
			return -1;
		return (int) Reflection.value("Actor#animationId", raw);
	}
	/**
	 * This you get spell animation Id of Actor
	 *
	 * @return spell animation Id
	 */
	public int getSpellAnimation() {
		if (raw == null)
			return -1;
		return (int) Reflection.value("Actor_spellAnimationId", raw);
	}
	public int[] getDamage() {
		if (raw == null)
			return null;
		return new int[]{1};//(int[]) Reflection.value("Actor#getDamage", raw);
	}
	public int getOrientation() {
		if (raw == null)
			return -1;
		return (int) Reflection.value("Actor#getOrientation()", raw);
	}

	/**
	 * Max health for Actor only show when health bar visible
	 *
	 * @return Integer Max Health
	 */
	public int getMaxHealth() {
		if (raw == null)
			return -1;
		return 100;//((int) Reflection.value("Actor_maxHealth", raw) == 0 ? 1 : (int) Reflection.value("Actor_maxHealth", raw));
	}

	/**
	 * Current health for Actor only show when health bar visible
	 *
	 * @return Integer current Health
	 */
	public int getHealth() {
        if (raw == null)
            return -1;
        return getHealthRatio();//(int) Reflection.value("CombatInfo_healthRatio", raw);
	}
	
	public int getHealthRatio() {
		Object combatInfoList = Reflection.value("Actor_combatInfoList", raw);//actor.getCombatInfoList();
		if (combatInfoList != null) {
			Object node = Reflection.value("CombatInfoList_node", combatInfoList);//combatInfoList.getNode();
			Object next = Reflection.value("Node_next", node);//((Node) node).getNext();
			Field hp = Reflection.field("CombatInfoListHolder_combatInfo2");
			//Object holder = Reflection.value("CombatInfoListHolder_combatInfoList", next);
			//System.out.println("holder: "+holder);
				try {
					if (hp.get(next) != null) {
						Object hpbar = Reflection.value("CombatInfoListHolder_combatInfo2", next);
						int maxHealth = (int) Reflection.value("CombatInfo2_healthScale", hpbar);
						//Object combatInfoListWrapper = next;
						Object combatInfoList2 = Reflection.value("CombatInfoListHolder_combatInfo1", next);//combatInfoListWrapper.getCombatInfoList();
						if(combatInfoList2 != null) {
						//Object scale = Reflection.value("CombatInfoListHolder_combatInfo2", next);
						Object node2 = Reflection.value("CombatInfoList_node", combatInfoList2);//combatInfoList2.getNode();
						Object next2 = Reflection.value("Node_next", node2);//node2.getNext();
						Object holder2 = Reflection.value("CombatInfo1_healthRatio", next2);
						
						if (holder2 != null) {
						return (int)holder2 * 100 / maxHealth;
						}
					}
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
	}
		return -1;
	}
	
	
	/**
	 * @return int : 0-100% if health bar visible else 0%
	 */
	public int getHealthPercent() {
		if (getHealth() == 0) return isInCombat() ? 0 : 100;
		return (int) ((double) getHealth() / getMaxHealth() * 100);
	}
	public int getHitCycle() {
		return (int) Reflection.value("Actor#getHitCycles()", raw);
	}
	public int getLoopCycle() {
		return (int) Reflection.value("Client#getLoopCycle()", raw);
	}
	/**
	 * @return boolean : if in combat return true else return false
	 */
	public boolean isInCombat() {
		if (raw == null)
			return false;
		int LoopCycleStatus = ((int) Reflection.value("Client#getLoopCycle()", null)) - 130;
		int hitCycles = (int) Reflection.value("Actor#getHitCycles()", raw);
		//System.out.println(hitCycles+"> "+LoopCycleStatus);
			if (hitCycles > LoopCycleStatus)
				return true;
		return false;
	}
	/**
	 * This you get overhead pray of Actor
	 *
		 * @return Overhead Prayer
		 */
		public int getOverheadPray() {
			if (raw == null)
				return -1;
			return (int) Reflection.value("Player_overheadIcon", raw);
		}
	/**
	 * the Height of Actor
	 *
	 * @return Integer Height
	 */
	public int getHeight() {
		if (raw == null)
			return 0;
		return (int) Reflection.value("Renderable#getModelHeight()", raw);
	}


	/**
	 * check if actor moving or running
	 *
	 * @return true if so , else false if not
	 */
	public boolean isMoving() {
		return getQueueSize() > 0;
	}

	/**
	 * @return Integer: QueueSize how many tiles queue there in map
	 */
	public int getQueueSize() {
		if (raw == null)
			return 0;
		return (int) Reflection.value("Actor#getQueueSize()", raw);
	}

	/**
	 * returns local X location
	 * 
	 * @return Integer X location
	 */
	public int getLocalX() {
		if (!isValid())
			return -1;
		return (int) Reflection.value("Actor_localX", raw);
	}
	
	/**
	 * returns local X location
	 * 
	 * @return Integer X location
	 */
	public int getLocalY() {
		if (!isValid())
			return -1;
		return (int) Reflection.value("Actor_localY", raw);
	}
	/**
	 * return real X location
	 *
	 * @return Integer X Location
	 */
	public int getX() {
		if (!isValid())
			return -1;
		return ((((int) Reflection.value("Actor_localX", raw)) >> 7) + Game.getBaseX());
	}
	
	/**
	 * return real Y location
	 *
	 * @return Integer Y Location
	 */
	public int getY() {
		if (!isValid())
			return -1;
		return ((((int) Reflection.value("Actor_localY", raw)) >> 7) + Game.getBaseY());
	}

	public int getRawX() {
		return ((int) Reflection.value("Actor_localX", raw) + Game.getBaseX());
	}
	public int getRawY() {
		return ((int) Reflection.value("Actor_localY", raw) + Game.getBaseY());
	}
	/**
	 * Check if Actor is on screen
	 *
	 * @return Boolean: true if it's on Viewport else false
	 */
	@Override
	public boolean isOnScreen() {
		if(raw == null)
		return false;
		return getLocation().isOnScreen();
	}

	/**
	 * @return Point: Point converted from WorldToScreen depend on X/Y
	 */
	@Override
	public Point getPointOnScreen() {
		return getLocation().getPointOnScreen();
	}

	/**
	 * @return Point: Point used to interact
	 */
	

	/**
	 * distance from local player
	 *
	 * @return Integer
	 */
	@Override
	public int distanceTo() {
		return Calculations.distanceTo(getLocation());
	}

	/**
	 * distance to specific Locatable
	 *
	 * @return Integer
	 */
	@Override
	public int distanceTo(Locatable locatable) {
		return (int) Calculations.distanceBetween(getLocation(), locatable.getLocation());
	}

	/**
	 * distance to specific tile
	 *
	 * @return Integer
	 */
	@Override
	public int distanceTo(Tile tile) {
		return (int) Calculations.distanceBetween(getLocation(), tile);
	}

	public Tile getRawLocation() {
		return new Tile(getRawX(), getRawY(), Game.getPlane());
	}

	/**
	 * current Tile of this Actor
	 *
	 * @return Tile
	 */
	@Override
	public Tile getLocation() {
		return new Tile(getX(), getY());  //To change body of implemented methods use File | Settings | File Templates.
	}
	
	



	/**
	 * Checks if the object is null
	 *
	 * @return true if the object is not null
	 */
	public boolean isValid() {
		return getRaw() != null;
	}
	public int getInteractingIndex() {
		return (int) Reflection.value("Actor_interacting", raw);
	}
	
	public String getInteracting2() {
		if (raw == null)
			return null;
		int interactingIndex = (int) Reflection.value("Actor_interacting", raw);
		if (interactingIndex == -1)
			return null;
		if (interactingIndex < 32768) {
			return "NPC";
		} else {
			return "PLAYER";
		}
	}
	
	/**
	 * @return NPC/Player: return the actor that this actor interacting with
	 */
	
	
	public Actor getInteracting() {
		if (raw == null)
			return null;
		int interactingIndex = (int) Reflection.value("Actor_interacting", raw);
		if (interactingIndex == -1)
			return new Actor(null);
		if (interactingIndex < 32768) {
			Object[] localNpcs = (Object[]) Reflection.value("client_npcs", null);
			if (localNpcs.length > interactingIndex)
				return new NPC(localNpcs[interactingIndex]);
		} else {
			interactingIndex -= 32768;
			int playerIndex = (int) Reflection.value("Client#getPlayerIndex()", null);
			if (interactingIndex == playerIndex) {
				return Players.getLocal();
			}
			Object[] localPlayers = (Object[]) Reflection.value("client_players", null);
			if (localPlayers.length > interactingIndex)
				return new Player(localPlayers[interactingIndex]);
		}
		return new Actor(null);
	}

	

	@Override
	public boolean equals(Object a) {
		if (a != null && a instanceof Actor) {
			Actor t = (Actor) a;
			boolean x = this.getLocation().equals(t.getLocation()) && this.getAnimation() == t.getAnimation() && this.getHealthRatio() == this.getHealthRatio();
			if (t instanceof Player && this instanceof Player) {
				Player j = (Player) t;
				return x & j.getName().equals(((Player) this).getName());
			} else if (t instanceof NPC && this instanceof NPC) {
				NPC j = (NPC) t;
				return x & j.getId() == (((NPC) this).getId());
			}
			return false;
		}
		return false;
	}
	

	
    public String getName() {
        if (getRaw() == null)
            return null;
        return WordUtils.capitalize((String) Reflection.value("Player_name", getRaw()).toString().replace("\u00a0", " "));  //To change body of implemented methods use File | Settings | File Templates.
    }
    
   

    


	public int getCombatLevel() {
		int interactingIndex = (int) Reflection.value("Actor_interacting", raw);
		if (interactingIndex == -1)
			return new Actor(null).getCombatLevel();
		if (interactingIndex < 32768) {
			Object[] localNpcs = (Object[]) Reflection.value("client_npcs", null);
			if (localNpcs.length > interactingIndex)
				return new NPC(localNpcs[interactingIndex]).getCombatLevel();
		} else {
			interactingIndex -= 32768;
			int playerIndex = (int) Reflection.value("Client#getPlayerIndex()", null);
			Object[] localPlayers = (Object[]) Reflection.value("client_players", null);
			if (localPlayers.length > interactingIndex)
				return new Player(localPlayers[interactingIndex]).getCombatLevel();
		}
		return new Actor(null).getCombatLevel();
	}


	

}