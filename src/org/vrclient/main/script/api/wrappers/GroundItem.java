package org.vrclient.main.script.api.wrappers;

import org.vrclient.main.Constants;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Identifiable;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.interfaces.Nameable;
import org.vrclient.main.script.api.methods.data.Calculations;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.script.api.query.GroundItemQuery;
import org.vrclient.main.script.api.wrappers.definitions.ItemDefinition;
import org.vrclient.main.script.api.wrappers.definitions.Pricing;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.NetUtils;
import org.vrclient.main.utils.PriceChecker;
import org.vrclient.main.utils.Utilities;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Created by Hiasat on 7/31/14
 */
public class GroundItem implements Locatable, Identifiable, Nameable {

    private int id;
    private int amount;
    private int price;
    //private int value;
    private Tile location;
    private ItemDefinition itemDefinition;
    //private static HashMap<Integer, Integer> priceDefinitions = new HashMap<Integer, Integer>();
    //TODO: FIX CONSTANT HTTP REQUESTS FOR PRICING EACH ITEM FOR GROUNDITEMS
   // String price = NetUtils.readPage("http://api.rsbuddy.com/grandExchange?a=guidePrice&i="+id)[0];
    public GroundItem(Object raw, Tile tile) {
        if (raw != null) {
            location = tile;
            id = (int) Reflection.value("GroundItem_id", raw);
            amount = (int) Reflection.value("GroundItem_quantity", raw);
            //value = Integer.parseInt(price.substring(price.indexOf(":"), price.indexOf(",")).split(":")[1]);
            itemDefinition = new ItemDefinition(id);     
        } else {
            id = -1;
            amount = -1;
            price = 0;
        }
    }
   
   
    @Override
    public int getId() {
        return id;
    }

/*public int getValue() {
	return value * amount;
}*/
   /*public int getPrice() {
	   if(GroundItemsDisplay.priceDefinitions.containsKey(id)) {
		   System.out.println("map contains "+getName());
		   return GroundItemsDisplay.priceDefinitions.get(id)*amount;
	   }
	   ExecutorService executor = Executors.newFixedThreadPool(30);
	Pricing pricing = new Pricing(id);
	executor.submit(pricing);//pricing.start();
	//executor.shutdown();
	return pricing.price*amount;
   }*/
    public int getPrice() {
    	if(GroundItems.priceDefinitions.containsKey(id))
    	return GroundItems.priceDefinitions.get(id) * amount;
    	return 1 * amount;
    }
    public String formattedPrice() {
    	return Utilities.withSuffix(getPrice());
    }
    public String formattedPrice(int amount) {
    	return Utilities.withSuffix(getPrice()*amount);
    }
    public Polygon getBounds() {
        Polygon polygon = new Polygon();
        if (!isOnScreen())
            return null;
        int x = getLocation().x;
        int y = getLocation().y;
        int h = 20;
        int z = Game.getPlane();
        
        double a = -0.25;
        double r = 0.25;
        Point pn = Calculations.tileToScreen(new Tile(x, y, z), r, r, 0);
        Point px = Calculations.tileToScreen(new Tile(x + 1, y, z), a, r, 0);
        Point py = Calculations.tileToScreen(new Tile(x, y + 1, z), r, a, 0);
        Point pxy = Calculations.tileToScreen(new Tile(x + 1, y + 1, z), a, a, 0);

        Point pnh = Calculations.tileToScreen(new Tile(x, y, z), r, r,  h);
        Point pxh = Calculations.tileToScreen(new Tile(x + 1, y, z), a, r,  h);
        Point pyh = Calculations.tileToScreen(new Tile(x, y + 1, z), r, a,  h);
        Point pxyh = Calculations.tileToScreen(new Tile(x + 1, y + 1, z), a, a,  h);
        if (Constants.VIEWPORT.contains(py)
                && Constants.VIEWPORT.contains(pyh)
                && Constants.VIEWPORT.contains(px)
                && Constants.VIEWPORT.contains(pxh)
                && Constants.VIEWPORT.contains(pxy)
                && Constants.VIEWPORT.contains(pxyh)
                && Constants.VIEWPORT.contains(pn)
                && Constants.VIEWPORT.contains(pnh)) {
            polygon.addPoint(py.x, py.y);
            polygon.addPoint(pyh.x, pyh.y);

            polygon.addPoint(px.x, px.y);
            polygon.addPoint(pxh.x, pxh.y);

            polygon.addPoint(pxy.x, pxy.y);
            polygon.addPoint(pxyh.x, pxyh.y);

            polygon.addPoint(pn.x, pn.y);
            polygon.addPoint(pnh.x, pnh.y);
        } else {
            return null;
        }
        return polygon;
    }

    @Override
    public boolean isOnScreen() {
        return location.isOnScreen();
    }

    @Override
    public Point getPointOnScreen() {
        int realX = (int) ((getLocation().getX() - Game.getBaseX() + 0.5) * 128);//(getLocation().getX() - Game.getBaseX()) * 128;
        int realY = (int) ((getLocation().getY() - Game.getBaseY() + 0.5) * 128);//(getLocation().getY() - Game.getBaseY()) * 128;
        return Calculations.groundToViewport(realX, realY, Game.resizable() ? 0 : 100);
    }


    @Override
    public int distanceTo() {
        return Calculations.distanceTo(this);
    }

    @Override
    public int distanceTo(Locatable locatable) {
        return Calculations.distanceBetween(getLocation(), locatable.getLocation());
    }

    @Override
    public int distanceTo(Tile tile) {
        return Calculations.distanceBetween(getLocation(), tile);
    }

   

    @Override
    public Tile getLocation() {
        return location;
    }


    @Override
    public String getName() {
        return itemDefinition.getName();
    }

    public boolean isValid() {
        return (id | amount) != -1;
    }

    public int getStackSize() {
        return amount;
    }
  
    /*
     * This stores items in a list that are at the same Location
     */
    public List<GroundItem> sameLocationList(List<GroundItem> refresh) {
    	ArrayList<GroundItem> newList = new ArrayList<GroundItem>();
    	for(GroundItem groundItem: refresh) {
    		if(isValid() && groundItem.isValid() && groundItem.getLocation().equals(getLocation())) {
    			newList.add(groundItem);
    		}
    	}
    	Collections.reverse(newList);
    	return newList;
    }
    /*
     * This only counts how many items at the same location
     */
    public int sameLocation(List<GroundItem> refresh) {
    	int count = 0;
    	for(GroundItem groundItem: refresh) {
    		if(groundItem.getLocation().equals(getLocation())) {
    			count++;
    		}
    	}
    	return count;
    }
    /*
     * this is a boolean to check for multiple/duplicate items at the same location
     */
	public int duplicates(GroundItem item, List<GroundItem> list) {
		int count = 0;
		for(GroundItem groundItem: list) {
			if(item.getLocation().equals(groundItem.getLocation()) && item.getId() == groundItem.getId()) {
				count++;
			}
		}
		return count;
	}

/*
 * The purpose of this to check 
 * if there are multiple same kind 
 * of items at the same location, we 
 * just update the total amount instead 
 * of drawing it multiple times.
 */
	public int occurences(List<GroundItem> list) {
		int occurs = 0;
		for(GroundItem groundItem: list) {
			if(isValid() && groundItem.isValid() && groundItem.getId() == this.getId() && this.getLocation().equals(groundItem.getLocation())) {
				if(occurs == 12)
					break;
				occurs++;
			}
		}
		return occurs;
	}
	
	public Point fixedLastPoint(List<GroundItem> sameLocationList) {
		 GroundItem item = sameLocationList.get(sameLocationList.size() - 1);
		 Point point = item.getPointOnScreen();
		 return new Point(point.x, point.y-15);
	}

	public Point fixedPoint(List<GroundItem> refresh) {
		  Point point = getPointOnScreen();
		  ArrayList<Integer> duplicateItems = new ArrayList<Integer>();
		  int offSet = 0;
		for (GroundItem item : refresh) {
			if(isValid() && item.isValid() && item.getId() == id && item.getLocation().equals(getLocation())) {
				return new Point(point.x, point.y-offSet);
				} else {
					if(!duplicateItems.contains(item.getId())) {
					offSet += 15;
					duplicateItems.add(item.getId());
					} else {					
					continue;
					}
				}
			}
		return point;
	}


	public boolean isGoodItem() {
	String[] valuable = {"Toxic", "Serpentine", "Zulrah's scales", "Elysian spirit shield", "Arcane spirit shield",
			"Spectral spirit shield", "Amulet of fury", "Dragon", "Abyssal", "Dharok's", "Ahrim's",
			"Torag's", "Verac's", "Karil's", "Armadyl", "Bandos", "Zamorak", "Rune", "Mystic robe",
			"Helm of neitiznot", "Amulet of glory", "Ancient", "Saradomin", "Cooked karambwan", "Shark", "Anchovy pizza", "Swordfish", "Lobster"};
	for(String aname: valuable) {
	if(getName().startsWith(aname) && !getName().endsWith("arrow") && !getName().endsWith("robe") || getName().endsWith("arrow") && amount >= 50 || getName().endsWith("rune") && amount >= 50
			|| getName().contains("bolts (e)") && amount >= 25){
		return true;
		}
	}
		return false;
	}


	public boolean isFood() {
		String[] foods = {"1/2 anchovy pizza","Lobster", "Shark", "Anchovy pizza", "Swordfish", "Monkfish", "Cooked karambwan", "Anglerfish", "Potato with cheese", "Tuna potato"};
		for(String food : foods) {
			if(getName().equalsIgnoreCase(food))
				return true;
		}
		return false;
	}


	public String totalLoot(List<GroundItem> refresh) {
		int total = 0;
		for(GroundItem item: refresh) {
			total+= item.getPrice();
		}
	return Utilities.withSuffix(total);
	}


	

    
   
}