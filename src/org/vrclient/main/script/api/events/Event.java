package org.vrclient.main.script.api.events;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.vrclient.component.plugins.DeathMarker;
import org.vrclient.component.plugins.timerOverlays;
import org.vrclient.component.plugins.timerOverlays.GameTimers;
import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.ui.Screenshotmanager;

public class Event {
	private static Configuration config = Configuration.getInstance();
	public static void submitAnimationEvent(int id) {
		System.out.println("AnimationEvent: - " + "[" + id + "]");
		if(id == 726 && config.drawVengTimer()) {
			if(timerOverlays.findTimerFor("veng") != null)
        		timerOverlays.timers.remove(timerOverlays.findTimerFor("veng"));
        	createTimer(30,"veng");
		} else if(id == 1316 && config.drawImbheartTimer()) {
			if(timerOverlays.findTimerFor("imbheart") != null)
	        	timerOverlays.timers.remove(timerOverlays.findTimerFor("imbheart"));
	        createTimer(420,"imbheart");
		} else if(id == 804 && config.drawHomeTimer()) {
			if(timerOverlays.findTimerFor("hometele") != null)
	        	timerOverlays.timers.remove(timerOverlays.findTimerFor("hometele"));
	        createTimer(1800,"hometele");
		}
	}
	
	
	public static void submitMessageEvent(int type, String sender, String message) {
		System.out.println("MessageEvent: - ["+type+"] "+sender+": " + message);
		switch(type) {
			case 0:
				if(message.contains("Congratulations, you've just advanced")) {
					Screenshotmanager.takeScreeny();
				} else if(message.equals("Oh dear, you are dead!") && config.drawDeath()){
					DeathMarker.deathTile(Players.getLocal().getLocation());
					DeathMarker.deathWorld(Game.getCurrentWorld());
					DeathMarker.deathPlane(Game.getPlane());
					
					if(config.drawDeathTimer()){
						if(timerOverlays.findTimerFor("death") != null)
			        		timerOverlays.timers.remove(timerOverlays.findTimerFor("death"));
						if(Game.inWilderness()){
							DeathMarker.deathTimer = new Timer(120000);
							createTimer(120,"death");
						} else {
							DeathMarker.deathTimer = new Timer(3600000);
							createTimer(3599,"death");
						}
					}
				} else if(message.equals("<col=4f006f>A teleblock spell has been cast on you. It will expire in 5 minutes, 0 seconds.</col>") && config.drawTbTimer()) {
					createTimer(300,"tb");
		        } else if(message.equals("<col=4f006f>A teleblock spell has been cast on you. It will expire in 2 minutes, 30 seconds.</col>") && config.drawTbTimer()) {
		        	createTimer(150,"tb");
		        } else if(message.equals("<col=ef1020>Your imbued heart has regained its magical power.</col>")) {
		        	if(timerOverlays.findTimerFor("imbheart") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("imbheart"));
		        } else if(message.equals("Your Magic Imbue charge has ended.")){
		        	if(timerOverlays.findTimerFor("magicimbue") != null)
			        	timerOverlays.timers.remove(timerOverlays.findTimerFor("magicimbue"));
		        } else if(message.contains("You need to wait another") && config.drawHomeTimer()) {
		        	Pattern p = Pattern.compile("-?\\d+");
		        	Matcher m = p.matcher(message);
		        	while (m.find()) {
		        		  if(timerOverlays.findTimerFor("hometele") != null)
		      	        	timerOverlays.timers.remove(timerOverlays.findTimerFor("hometele"));
		      	        createTimer((Integer.parseInt(m.group())*60),"hometele");
		        	}
		        }

			case 105:
		        if(message.equals("You drink some of your stamina potion.") && config.drawStaminaTimer()) {
		        	if(timerOverlays.findTimerFor("stam") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("stam"));
		        	createTimer(120,"stam");
		        } else if(message.equals("You drink some of your antifire potion.") && config.drawAfTimer()) {
		        	if(timerOverlays.findTimerFor("af") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("af"));
		        	createTimer(360,"af");
		        } else if(message.equals("You drink some of your extended antifire potion.") && config.drawSafTimer()) {
		        	if(timerOverlays.findTimerFor("saf") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("saf"));
		        	createTimer(720,"saf");
			    } else if(message.equals("<col=8f4808>Your stamina potion has expired.</col>")) {
			    	if(timerOverlays.findTimerFor("stam") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("stam"));
			    } else if(message.equals("<col=7f007f>Your antifire potion has expired.</col>")) {
			    	if(timerOverlays.findTimerFor("af") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("af"));
			    	if(timerOverlays.findTimerFor("saf") != null)
		        		timerOverlays.timers.remove(timerOverlays.findTimerFor("saf"));
			    } else if(message.equals("You are charged to combine runes!") && config.drawImbueTimer()) {
			    	if(timerOverlays.findTimerFor("magicimbue") != null)
			        	timerOverlays.timers.remove(timerOverlays.findTimerFor("magicimbue"));
			        createTimer(13,"magicimbue");
			    } else if(message.contains("You drink some of your overload potion.<br>") && config.drawOverloadTimer()) {
			    	if(timerOverlays.findTimerFor("overload") != null)
			        	timerOverlays.timers.remove(timerOverlays.findTimerFor("overload"));
			        createTimer(300,"overload");
			    }
		}
	}

	public static void createTimer(int time, String name){
		Instant gameTime = Instant.now().plus(time+1, ChronoUnit.SECONDS);
    	GameTimers GameTimers = new GameTimers(name, gameTime);
    	timerOverlays.timers.add(GameTimers);
	}

    
}
