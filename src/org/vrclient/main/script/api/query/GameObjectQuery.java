package org.vrclient.main.script.api.query;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.interactive.GameEntities;
import org.vrclient.main.script.api.wrappers.GameObject;

/**
 * Created by Kenneth on 8/4/2014.
 */
public class GameObjectQuery extends BasicQuery<GameObjectQuery, GameObject> {
    @Override
    protected GameObject[] elements() {
        return GameEntities.getAll();
    }

    @Override
    public GameObject nil() {
        return GameEntities.nil();
    }
    
    
    public GameObjectQuery id(final int... ids) {
       new Filter<GameObject>() {
            @Override
            public boolean accept(GameObject acceptable) {
                for (int id : ids) {
                    if (acceptable.getId() == id) {
                        return true;
                    }
                }
                return false;
            }
        };
        return this;
    }
}