package org.vrclient.main.script.api.query;

import java.util.Arrays;
import java.util.Comparator;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.interfaces.Nameable;
import org.vrclient.main.script.api.methods.interactive.Npcs;
import org.vrclient.main.script.api.wrappers.NPC;
import org.vrclient.main.script.api.wrappers.Tile;

public class NpcQuery extends AbstractQuery<NpcQuery, NPC> implements Locatable.Query<NpcQuery>, Nameable.Query<NpcQuery> {


    @Override
    protected NPC[] elements() {
        return Npcs.getAll();
    }

    @Override
    public NpcQuery within(final int radius) {
        return filter(new Filter<NPC>() {
            @Override
            public boolean accept(NPC e) {
                return e != null && e.distanceTo() <= radius;
            }
        });
    }

    @Override
    public NpcQuery at(final Tile tile) {
        return filter(new Filter<NPC>() {
            @Override
            public boolean accept(NPC e) {
                return e != null && e.getLocation().equals(tile);
            }
        });
    }

    @Override
    public NpcQuery nearest() {
        return sort(DISTANCE_SORT);
    }

    @Override
    public NpcQuery name(final String... names) {
        return filter(new Filter<NPC>() {
            @Override
            public boolean accept(NPC e) {
                return e != null && Arrays.asList(names).contains(e.getName());
            }
        });
    }

    private final Comparator<NPC> DISTANCE_SORT = new Comparator<NPC>() {
        @Override
        public int compare(NPC o1, NPC o2) {
            return o1.distanceTo() - o2.distanceTo();
        }
    };

    @Override
    public NPC nil() {
        return Npcs.nil();
    }

}
