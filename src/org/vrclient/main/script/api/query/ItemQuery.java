package org.vrclient.main.script.api.query;

import java.util.Arrays;

import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Inventory;
import org.vrclient.main.script.api.wrappers.Item;

public class ItemQuery extends AbstractQuery<ItemQuery, Item> {

    @Override
    protected Item[] elements() {
        return Inventory.getAllItems();
    }

    public ItemQuery name(final String... names) {
        return filter(new Filter<Item>() {
            @Override
            public boolean accept(Item item) {
                return Arrays.asList(names).contains(item.getName());
            }
        });
    }

    public ItemQuery id(final int... ids) {
        return filter(new Filter<Item>() {
            @Override
            public boolean accept(Item item) {
                return Arrays.asList(ids).contains(item.getId());
            }
        });
    }

    @Override
    public Item nil() {
        return Inventory.nil();
    }
}
