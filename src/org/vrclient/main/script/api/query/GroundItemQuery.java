package org.vrclient.main.script.api.query;

import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.script.api.wrappers.GroundItem;

/**
 * Created by VR on 8/4/2014.
 */
public class GroundItemQuery extends BasicQuery<GroundItemQuery, GroundItem> {
    @Override
    protected GroundItem[] elements() {
        return GroundItems.getAll();
    }
   

	@Override
    public GroundItem nil() {
        return GroundItems.nil();
    }

}
