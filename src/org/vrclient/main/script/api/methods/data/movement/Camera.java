package org.vrclient.main.script.api.methods.data.movement;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Player;

import java.awt.event.KeyEvent;

/*
 * Created by VR on 7/31/14
 */
public class Camera {

	/**
	 * X coordinate of Camera
	 *
	 * @return Integer
	 */
	public static int getX() {
		return (int) Reflection.value("client_cameraX", null);
	}

	/**
	 * Y coordinate of Camera
	 *
	 * @return Integer
	 */
	public static int getY() {
		return (int) Reflection.value("client_cameraY", null);
	}

	/**
	 * Z coordinate of Camera
	 *
	 * @return Integer
	 */
	public static int getZ() {
		return (int) Reflection.value("client_cameraZ", null);
	}

	/**
	 * Minimap angel
	 *
	 * @return Integer
	 */
	public static int getMapAngle() {
		return (int) Reflection.value("client_minimapAngle", null);
	}
	
	/**
	 * Minimap scale
	 *
	 * @return Integer
	 */
	public static int getMapScale() {
		return (int) Reflection.value("client_minimapScale", null);
	}
	
	/**
	 * Minimap offset
	 *
	 * @return Integer
	 */
	public static int getMapOffset() {
		return (int) Reflection.value("client_minimapOffset", null);
	}

	/**
	 * Client Pitch View Angle Integer
	 *
	 * @return Integer 0-90
	 */
	public static int getPitch() {
		return (int) (Reflection.value("client_cameraPitch", null));
	}

	/**
	 * Client Yaw View Angle Integer
	 *
	 * @return Integer 0-2048
	 */
	public static int getYaw() {
		return (int) Reflection.value("client_cameraYaw", null);
	}

	/**
	 * Camera Angel
	 *
	 * @return Integer 0-360
	 */
	public static int getAngle() {
		return Math.abs((int) (getYaw() / 5.68) - 360);
	}

	/**
	 * angel to specific degrees
	 *
	 * @param degrees
	 * @return Integer of angel
	 */
	public static int getAngleTo(int degrees) {
		int ca = getAngle();
		if (ca < degrees) {
			ca += 360;
		}
		int da = ca - degrees;
		if (da > 180) {
			da -= 360;
		}
		return da;
	}

	/**
	 * View angel to specific Locatable
	 *
	 * @param locatable such as NPC/GameObject/Tile
	 * @return Integer 0-360
	 */
	public static int getAngleTo(final Locatable locatable) {
		final Player local = Players.getLocal();
		double degree = 360 - Math.toDegrees(Math.atan2(locatable.getLocation().getX()
				- local.getLocation().getX(), local.getLocation().getY()
				- locatable.getLocation().getY()));
		if (degree >= 360) {
			degree -= 360;
		}
		return (int) degree;
	}

	

	

}
