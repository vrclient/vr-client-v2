package org.vrclient.main.script.api.methods.data;

public class Boss
{
	private String name;
	private int spawnTime;
	private String type;

	public String getName()
	{
		return name;
	}
	public String getType()
	{
		return type;
	}

	public int getSpawnTime()
	{
		return spawnTime;
	}
}