package org.vrclient.main.script.api.methods.data;

import org.vrclient.main.Configuration;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Locatable;
import org.vrclient.main.script.api.methods.data.movement.Camera;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.script.api.wrappers.Tile;
import org.vrclient.main.script.api.wrappers.Widget;
import org.vrclient.main.script.api.wrappers.WidgetChild;

import com.sun.security.ntlm.Client;

import java.applet.Applet;
import java.awt.*;

/*
 * Created by VR on 7/30/14
 */
public class Calculations {
	
	/**
	 * The SIN values of a given set of angles.
	 */
	public static int[] SINE = new int[2048];
	/**
	 * The COS values of a given set of angles.
	 */
	public static int[] COSINE = new int[2048];


	static {
		for (int i = 0; i < SINE.length; i++) {
			SINE[i] = (int) (65536.0D * Math.sin((double) i * 0.0030679615D));
			COSINE[i] = (int) (65536.0D * Math.cos((double) i * 0.0030679615D));
		}
	}

	/**
	 * @param x
	 * @param y
	 * @param x1
	 * @param y1
	 * @return distance between two Coordinate
	 */
	public static int distanceBetween(int x, int y, int x1, int y1) {
		return (int) Math.sqrt(Math.pow(x1 - x, 2) + Math.pow(y1 - y, 2));
	}

	/**
	 * @param a first point
	 * @param b second point
	 * @return distance between two Coordinate
	 */
	public static int distanceBetween(Point a, Point b) {
		return (int) distanceBetween(a.x, a.y, b.x, b.y);
	}

	/**
	 * @param a first tile
	 * @param b second tile
	 * @return distance between two Coordinate
	 */
	public static int distanceBetween(Tile a, Tile b) {
		return (int) distanceBetween(a.getX(), a.getY(), b.getX(), b.getY());
	}

	/**
	 * @param a tile
	 * @return current distance between player and specific tile
	 */
	public static int distanceTo(Tile a) {
		final Tile loc = Players.getLocal().getLocation();
		return (int) distanceBetween(a.getX(), a.getY(), loc.getX(), loc.getY());
	}

	/**
	 * @param a locatable
	 * @return current distance between player and specific Actor/Object/Tile
	 */
	public static int distanceTo(Locatable a) {
		final Tile loc = Players.getLocal().getLocation();
		return distanceBetween(a.getLocation().getX(), a.getLocation().getY(), loc.getX(), loc.getY());
	}

	/**
	 * @param tile
	 * @param dX     value to increase X for tile
	 * @param dY     value to increase Y for tile
	 * @param height
	 * @return Point: return Point on screen if it's valid else return new Point(-1,-1)
	 */
	public static Point tileToScreen(Tile tile, double dX, double dY, int height) {
		 return groundToViewport((int) ((tile.getX() - Game.getBaseX() + dX) * 128), (int) ((tile.getY() - Game.getBaseY() + dY) * 128), height);
	}
	
	

	/**
	 * @param tile
	 * @return boolean : true if distance to tile less than 17 else false
	 */
	public static boolean isOnMap(Tile tile) {
		return Calculations.distanceTo(tile) < 16;
	}

	/**
	 * @param tile
	 * @return Point: return tile point on screen
	 */
	public static Point tileToScreen(Tile tile) {
		return groundToViewport((int) ((tile.getX() - Game.getBaseX() + 0.5) * 128), (int) ((tile.getY() - Game.getBaseY() + 0.5) * 128), 0);
	}

	public static boolean drawMultiLines() {
		//(absX >= 2256 && abs X <= 2287 && absY >= 4680 && absY <=4711)
		for (int x = 3008; x <= 3550; x++) {
			for (int y = 3600; y <= 3800; y++) {
				if(isOnMap(new Tile(x, y, Players.getLocal().getLocation().getZ()))) {
					return true;
				}
			}
		}
		return false;
	}
	/**
	 * @param plane
	 * @param x
	 * @param y
	 * @return Integer: check tile height
	 */
	public static int getTileHeight(int plane, int x, int y) {
		int xx = x >> 7;
		int yy = y >> 7;
		if (xx < 0 || yy < 0 || xx > 103 || yy > 103) {
			return 0;
		}
		int[][][] tileHeights = (int[][][]) Reflection.value("client_tileHeights", null);
		int aa = tileHeights[plane][xx][yy] * (128 - (x & 0x7F))
				+ tileHeights[plane][xx + 1][yy] * (x & 0x7F) >> 7;

		int ab = tileHeights[plane][xx][yy + 1]
				* (128 - (x & 0x7F))
				+ tileHeights[plane][xx + 1][yy + 1]
				* (x & 0x7F) >> 7;
		return aa * (128 - (y & 0x7F)) + ab * (y & 0x7F) >> 7;
	}

	public static Dimension dimensions() {
		final Applet applet = RSLoader.getApplet();
		return applet != null ? new Dimension(applet.getWidth(), applet.getHeight()) : new Dimension(-1, -1);
	}
	public static Point groundToViewport(int x, int y, int Height) {
		if ((x < 128) || (y < 128) || (x > 13056) || (y > 13056)) {
			return new Point(-1, -1);
			}
			 
			int z = getGroundHeight(x, y) - Height;
			x -= Camera.getX();
			z -= Camera.getZ();
			y -= Camera.getY();
			//System.out.println(x+"-"+y+"-"+z);
			int sinCurveY = SINE[Camera.getPitch()];
			int cosCurveY = COSINE[Camera.getPitch()];
			int sinCurveX = SINE[Camera.getYaw()];
			int cosCurveX = COSINE[Camera.getYaw()];
			 
			int calculation = sinCurveX * y + cosCurveX * x >> 16;
			y = y * cosCurveX - x * sinCurveX >> 16;
			x = calculation;
			calculation = cosCurveY * z - sinCurveY * y >> 16;
			y = sinCurveY * z + cosCurveY * y >> 16;
			z = calculation;
			//System.out.println();
			if (y >= 50) {
				int screenX = x * Game.getScale() / y + Game.getWidth() / 2;
				int screenY = z * Game.getScale() / y + Game.getHeight() / 2;
				return new Point(screenX, screenY);
			}
			return new Point(-1, -1);
    }
	
	public static int getGroundHeight(int x, int y) {
        int x1 = x >> 7;
        int y1 = y >> 7;
        if (x1 < 0 || x1 > 103 || y1 < 0 || y1 > 103)
            return 0;
        byte[][][] rules = (byte[][][]) Reflection.value("client_tileSettings", null);
        if (rules == null)
            return 0;
        int[][][] heights = (int[][][]) Reflection.value("client_tileHeights", null);
        if (heights == null)
            return 0;
        int plane = (int) Reflection.value("client_plane", null);
        if (plane < 3 && (rules[1][x1][y1] & 0x2) == 2)
            plane++;
        int x2 = x & 0x7F;
        int y2 = y & 0x7F;
        int h1 = heights[plane][x1][y1] * (128 - x2) + heights[plane][x1 + 1][y1] * x2 >> 7;
        int h2 = heights[plane][x1][y1 + 1] * (128 - x2) + heights[plane][x1 + 1][y1 + 1] * x2 >> 7;
        return h1 * (128 - y2) + h2 * y2 >> 7;
    }
    public static WidgetChild mapComponent() {
		return Widgets.get(161).getChild(25);
	}
   
	
	
	
	
	/**
	 * @param regionX
	 * @param regionY
	 * @return Point : Convert from tile to point on map
	 */
	public static Point worldToMap(int regionX, int regionY) {
		int mapScale = (int) Reflection.value("client_minimapScale", null);
		int mapOffset = (int) Reflection.value("client_minimapOffset", null);
		int angle = Camera.getMapAngle() + mapScale & 0x7FF;
		int j = regionX * regionX + regionY * regionY;
		if (j > 6400)
			return new Point(-1, -1);

		int sin = Calculations.SINE[angle] * 256 / (mapOffset + 256);
		int cos = Calculations.COSINE[angle] * 256 / (mapOffset + 256);

		int xMap = regionY * sin + regionX * cos >> 16;
		int yMap = regionY * cos - regionX * sin >> 16;

		final Point centre = mapComponent().centerPoint();
		if(Game.resizable())
			return new Point(centre.x + xMap, centre.y - yMap);
		else
		return new Point(644 + xMap, 80 - yMap);
	}

	/**
	 * @param tile
	 * @return Point : convert Local player coordinates to point on map
	 */
	public static Point tileToMap(Tile tile) {
		int xMapTile = tile.getX() - Game.getBaseX();
		int yMapTile = tile.getY() - Game.getBaseY();
		Object myPlayer = Reflection.value("client_player", null);
		return worldToMap((xMapTile * 4 + 2) - (int) Reflection.value("Actor_localX", myPlayer) / 32, (yMapTile * 4 + 2) - (int) Reflection.value("Actor_localY", myPlayer) / 32);
	}
}
