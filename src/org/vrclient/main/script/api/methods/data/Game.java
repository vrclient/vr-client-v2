package org.vrclient.main.script.api.methods.data;

import java.awt.Canvas;
import java.util.ArrayList;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.client.security.encryption.AES;
import org.vrclient.main.script.api.enums.Tab;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.script.api.wrappers.Friends;
import org.vrclient.main.script.api.wrappers.Widget;
import org.vrclient.main.script.api.wrappers.WidgetChild;


/*
 * Created by VR on 7/31/14
 */
public class Game {

	public static final int STATE_LOGGED_IN = 30;
	public static final int STATE_LOG_IN_SCREEN = 10;
	public static String getUsername;
	public static String password;

	private static final int IN_GAME_WIDGET = 378;
	public static boolean updatedRSN = false;
	public static int savedTankLocation;
	public static Timer keyBoardTimer = new Timer(0);

	/**
	 * current Game State E.g Logged , Lobby , Loading
	 *
	 * @return Integer of state
	 */
	public static int getGameState() {
		return (int) Reflection.value("client_game_state", null);
	}

	/**
	 * current X of current Region
	 *
	 * @return Integer Region X
	 */
	public static int getBaseX() {
		return (int) Reflection.value("client_baseX", null);
	}

	/**
	 * current Y of current Region
	 *
	 * @return Integer Region Y
	 */
	public static int getBaseY() {
		return (int) Reflection.value("client_baseY", null);
	}

	/**
	 * @return Integer : Current floor player is on
	 */
	public static int getPlane() {
		return (int) Reflection.value("client_plane", null);
	}
	/**
	 * @return Integer : Current Scale
	 */
	public static int getScale() {
		return (int) Reflection.value("client_scale", null);
	}
	
	/**
	 * @return Integer : Game Width
	 */
	public static int getWidth() {
		return (int) Reflection.value("Client#ViewportWidth()", null);
	}
	/**
	 * @return Integer : Game Height
	 */
	public static int getHeight() {
		return (int) Reflection.value("Client#ViewportHeight()", null);
	}
	
	
	/**
	 * @return true if logged in else false
	 */
	public static boolean isLoggedIn() {
		return getGameState() == STATE_LOGGED_IN
				&& (Widgets.get(IN_GAME_WIDGET) == null || !Widgets.get(IN_GAME_WIDGET).isValid());
	}

	public static boolean inWilderness() {
		 if(Widgets.get(90).isValid()) {
			 return !Widgets.get(90).getChild(46).getText().isEmpty();
		 }
		 return false;
	}
	
	public static boolean isMapOpen() {
		 return Widgets.get(595).isValid();
	}

	/**
	 * @return Tab: depend on which tab is open
	 */
	public static Tab getCurrentTab() {
		final int WIDGET_PARENT = 548;
		final WidgetChild[] children = Widgets.get(WIDGET_PARENT).getChildren();
		if (children == null || children.length == 0)
			return null;
		for (WidgetChild p : children) {
			if (p.getTextureId() != -1 && p.getActions() != null) {
				String[] actions = p.getActions();
				for (Tab tab : Tab.values()) {
					for (String action : actions) {
						if (tab.name().equalsIgnoreCase(action)) {
							return tab;
						}
					}
				}
			}
		}
		return null;
	}
	public static boolean resizable() {
		//return Widgets.get(161).isValid() && Widgets.get(161).getChild(0).getHeight() != 503 && Widgets.get(161).getChild(0).getWidth() != 765;
		return (Boolean) Reflection.value("resizableMode", null);
	}
	
	public static int getFPS() {
		return (int) Reflection.value("client_fps", null);
	}
	
	public static int getCurrentWorld() {
		return (int) Reflection.value("client_world", null);
	}
	
	public static String[] getMessages() {
		return (String[]) Reflection.value("Chat#getMessages()", null);
	}
}