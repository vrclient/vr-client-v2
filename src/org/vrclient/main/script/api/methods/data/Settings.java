package org.vrclient.main.script.api.methods.data;

import org.vrclient.main.client.reflection.Reflection;

/*
 * Created by VR on 8/2/14
 */
public class Settings {
	
	public static int[] getPlayerSettings() {
		return (int[]) Reflection.value("client_varps", null);
	}
	
	public static int[] getWidgetSettings() {
		return (int[]) Reflection.value("client_settings", null);
	}

    public static int[] getAll() {
        int[] playerSettings = (int[]) Reflection.value("client_varps", null);
        int[] widgetSettings = (int[]) Reflection.value("client_settings", null);
        int[] allSettings = new int[playerSettings.length + widgetSettings.length];
        System.arraycopy(playerSettings, 0, allSettings, 0, playerSettings.length);
        System.arraycopy(widgetSettings, 0, allSettings, widgetSettings.length, widgetSettings.length);
        return allSettings;
    }

    public static int get(int a) {
        int[] settings = getAll();
        if (settings.length <= a)
            return 0;
        return settings[a];
    }
    
    
}
