package org.vrclient.main.script.api.methods.data;

public class Spell
{
	private String name;
	private int spellTime;

	public String getName()
	{
		return name;
	}

	public int getSpellTime()
	{
		return spellTime;
	}
}
