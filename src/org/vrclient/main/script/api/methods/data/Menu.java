package org.vrclient.main.script.api.methods.data;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.methods.interactive.Widgets;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.script.api.util.Time;
import org.vrclient.main.script.api.wrappers.Actor;
import org.vrclient.main.script.api.wrappers.ClanMember;
import org.vrclient.main.script.api.wrappers.GroundItem;
import org.vrclient.main.script.api.wrappers.Node;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;

/*
 * Created by Hiasat on 8/2/14
 */
public class Menu {
	ArrayList<String> piledPeople = new ArrayList<String>();
	//public static List<String>colors = Menu.finalizedColoredActions();
	public static boolean updated = false;
	public static boolean updatedSpec = false;
	public static int specAmount = 100;
	private static final Pattern htmlTags = Pattern.compile("\\<.+?\\>");

	public static int getX() {
		return (int) Reflection.value("client_menuX", null);
	}

	public static int getY() {
		return (int) Reflection.value("client_menuY", null);
	}

	public static int getWidth() {
		return (int) Reflection.value("client_menuWidth", null);
	}

	public static int getHeight() {
		return (int) Reflection.value("client_menuHeight", null);
	}

	public static int getMenuSize() {
		return (int) Reflection.value("client_menuCount", null);
	}

	public static boolean isOpen() {
		return (Boolean) Reflection.value("client_menuVisible", null);
	}

	public static Rectangle getArea() {
		return new Rectangle(getX(), getY(), getWidth(), getHeight());
	}
	public static ArrayList<String> clanList() {
		ArrayList<String> members = new ArrayList<String>();
		Object[] clanMembers = (Object[]) Reflection.value("client_clanMembers", null);
		if(clanMembers == null)
			return members;
		for(Object member: clanMembers) {
			if(member != null) {
			ClanMember clanmember = new ClanMember(member);
			members.add(clanmember.getName().toLowerCase().replace("\u00a0", " "));
			}
		}
		return members;
	}
	public static ArrayList<String> clanListSplitter() {
		ArrayList<String> members = new ArrayList<String>();
		Object[] clanMembers = (Object[]) Reflection.value("client_clanMembers", null);
		if(clanMembers == null)
			return members;
		for(Object member: clanMembers) {
			if(member != null) {
			ClanMember clanmember = new ClanMember(member);
			members.add(clanmember.getName().replace("\u00a0", " "));
			}
		}
		return members;
	}
	public static Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player.isValid() && Configuration.getInstance().getBotFrame().getCallerPanel().callers().contains(player.getName());
        }
    };
	public static ArrayList<String> getPiledNames() {
		ArrayList<String> piledPeople = new ArrayList<String>();
		for(Player player: Players.getAll(filter)) {
		if(player != null && player.getInteracting() != null && !piledPeople.contains(player.getInteracting().getName()) 
				&& !clanList().contains(player.getInteracting().getName())) {
			piledPeople.add(player.getInteracting().getName());
						}
			}
		return piledPeople;
	}
	
	public static java.util.List<String> finalizedColoredActions() {
		ArrayList<String> actions = new ArrayList<>();
		String[] menuActions = reColorActions();
		for (int i = Menu.getMenuSize() - 1; i >= 0; i--) {
			if (menuActions[i] != null) {
				actions.add(menuActions[i]);
			}
		}
		return actions;
	}
		private static Filter<GroundItem> filter2 = new Filter<GroundItem>() {
		        @Override
		        public boolean accept(GroundItem groundItem) {
		            return groundItem.isValid() && groundItem.isOnScreen() && groundItem.getPrice() > 30000;
		        }
		    };
			public static Boolean valueableItem(String s) {
				for(GroundItem groundItem: GroundItems.getAll(filter2)) {
				if(s.equalsIgnoreCase(groundItem.getName())) {
					return true;
					}
				}
				return false;
			}
	public static String[] reColorActions() {
		final String[] optionNames = (String[])Reflection.value("client_menuActionNames", null);
        final String[] actions = (String[]) Reflection.value("client_menuActions", null);
        for (int j = Menu.getMenuSize() - 1; j >= 0; j--) {
			String str1 = actions[j];
			Object localObject2 = optionNames[j].replace("\u00a0", " ");
            if(str1.equals("Attack")) {
           str1 = ((String) localObject2).replace("<col=ffffff>", "").split("<")[0].replace("\u00a0", " ");	
           if(!clanList().contains(str1.toLowerCase())) {
        	   if(getPiledNames().contains(str1.toLowerCase())) {
        		   if (FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") != null){
		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") + "><img=5>Attack";
		            	localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") + ">");
		            	optionNames[j] = (String) localObject2;
			    	} else {
		            	actions[j] = "<col=00eaff><img=5>Attack";
		            	localObject2 = optionNames[j].replace("<col=ffffff>", "<col=00eaff>");
		            	optionNames[j] = (String) localObject2;
		            }
           			
        	   }
        	   else if(!getPiledNames().contains(str1.toLowerCase())
        			   && !Configuration.getInstance().getBotFrame().getBinderPanel().listModel.contains(str1.toLowerCase())
        			   && Configuration.getInstance().getBotFrame().getSniperPanel().enemyCallers().contains(str1.toLowerCase())) {
        		   if (FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") != null){
		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") + "><img=4>Attack"; 
		            	 localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") + ">");
			            	optionNames[j] = (String) localObject2; 
			    	} else {
		            	actions[j] = "<col=FF0000><img=4>Attack";
		            	localObject2 = optionNames[j].replace("<col=ffffff>", "<col=FF0000>");
		            	optionNames[j] = (String) localObject2;
		            }
        		}
        	   else if(!getPiledNames().contains(str1.toLowerCase())
        			   && Configuration.getInstance().getBotFrame().getBinderPanel().listModel.contains(str1.toLowerCase()))
        	   {
        		   if (FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") != null){
		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") + ">Attack"; 
		            	localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") + ">");
		            	optionNames[j] = (String) localObject2;
			    	} else {
			    		localObject2 = optionNames[j].replace("<col=ffffff>", "<col=29F731>");
		            	optionNames[j] = (String) localObject2; 
			    	}
        	   }
        	   else
        		   actions[j] = "<col=ffc000>Attack"; 
        	   localObject2 = optionNames[j].replace("<col=ffffff>", "<col=ffc000>");
           	optionNames[j] = (String) localObject2;
           	}
            } else if(str1.equals("Take") && Configuration.getInstance().drawGroundItems() && valueableItem(optionNames[j].split("<col=ff9040>")[1])) {
            	actions[j] = "<col=ff0000>Take";
            	//if(Configuration.getInstance().drawGroundItems() && getAmount(optionNames[j].split("<col=ff9040>")[1]) > 1)
            	//	localObject2 = optionNames[j].replace("<col=ff9040>", "<col=ff0000>").replace(optionNames[j].split("<col=ff9040>")[1], optionNames[j].split("<col=ff9040>")[1]+" ("+getAmount(optionNames[j].split("<col=ff9040>")[1])+")");
            	//else
            		localObject2 = optionNames[j].replace("<col=ff9040>", "<col=ff0000>");
            	optionNames[j] = (String) localObject2;
            } else if(str1.equals("Cast") && ((String) localObject2).contains("->")) {
            str1 = ((String) localObject2).split(" -> ")[1].replace("<col=ffffff>", "").split("<")[0].replace("\u00a0", " ");
            if(!clanList().contains(str1.toLowerCase())) {
            	if(getPiledNames().contains(str1.toLowerCase())) {
            		if (FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") != null){
		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") + "><img=5>Cast";
		            	localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") + ">");
		            	optionNames[j] = (String) localObject2;
			    	} else {
			    		actions[j] = "<col=00eaff><img=5>Cast";
			    		localObject2 = optionNames[j].replace("<col=ffffff>", "<col=00eaff>");
		            	optionNames[j] = (String) localObject2;
		            }
            	}
            	else if(!getPiledNames().contains(str1.toLowerCase())
         			   && !Configuration.getInstance().getBotFrame().getBinderPanel().listModel.contains(str1.toLowerCase())
         			   && Configuration.getInstance().getBotFrame().getSniperPanel().enemyCallers().contains(str1.toLowerCase())) {
         		   if (FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") != null){
 		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") + ">Cast";
 		            	 localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") + ">");
			            	optionNames[j] = (String) localObject2; 
 			    	} else {
 		            	actions[j] = "<col=FF0000>Cast";
 		            	localObject2 = optionNames[j].replace("<col=ffffff>", "<col=FF0000>");
		            	optionNames[j] = (String) localObject2;
 		            	
 		            }
         		}
         	   else if(!getPiledNames().contains(str1.toLowerCase())
         			   && Configuration.getInstance().getBotFrame().getBinderPanel().listModel.contains(str1.toLowerCase()))
         	   {
         		   if (FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") != null){
 		            	actions[j] = "<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") + ">Cast";
 		            	localObject2 = optionNames[j].replace("<col=ffffff>","<col="+FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") + ">");
		            	optionNames[j] = (String) localObject2;
 			    	} else {
 			    		actions[j] = "<col=29F731>Cast";
 			    		localObject2 = optionNames[j].replace("<col=ffffff>", "<col=29F731>");
		            	optionNames[j] = (String) localObject2; 
 			    	}
         	   }
            	else
            		actions[j] = "<col=ffc000>Cast";
            	  localObject2 = optionNames[j].replace("<col=ffffff>", "<col=ffc000>");
                 	optionNames[j] = (String) localObject2;
            	}
            }
		}
			
        String[] localObject1 = actions;
			
		
		
        
		return (String[]) Reflection.value("client_menuActions", localObject1);
		
	}
	public static long getAmount(String item) {
				for (GroundItem groundItem: GroundItems.getAll(filter2)) {
					if(groundItem.getName().equalsIgnoreCase(item)) {
						return groundItem.getStackSize();
					}
				}
				return 1;
			}
	public static java.util.List<String> getActions() {
		ArrayList<String> actions = new ArrayList<>();
		String[] menuActions = (String[]) Reflection.value("client_menuActions", null);
		for (int i = Menu.getMenuSize() - 1; i >= 0; i--) {
			if (menuActions[i] != null) {
				actions.add(menuActions[i]);
			}
		}
		return actions;
	}
	public static String[] getActions2() {
		return (String[]) Reflection.value("client_menuActions", null);
	}
	public static java.util.List<String> getOptions() {
		ArrayList<String> options = new ArrayList<>();
		String[] menuOptions = (String[]) Reflection.value("client_menuActionNames", null);
		for (int i = Menu.getMenuSize() - 1; i >= 0; i--) {
			if (menuOptions[i] != null) {
				options.add(menuOptions[i]);
			}
		}
		return options;
	}
	public static String[] getOptions2() {
		return (String[]) Reflection.value("client_menuActionNames", null);
	}

	public static int index(String action) {
		return index(action, null);
	}

	public static int index(String action, String option) {
		int index = -1;
		int i = 0;
		for (String a : Menu.getActions()) {
			try {
				if (a.equalsIgnoreCase(action) && (option == null || Menu.getOptions().get(i).toLowerCase().contains(option.toLowerCase()))) {
					index = i;
					break;
				}
				i++;
			} catch (IndexOutOfBoundsException e) {

			}
		}
		return index;
	}

	public static boolean contains(String action, String option) {
		return index(action, option) != -1;
	}

	public static boolean contains(String action) {
		return index(action, null) != -1;
	}

}