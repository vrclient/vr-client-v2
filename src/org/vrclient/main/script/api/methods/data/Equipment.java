package org.vrclient.main.script.api.methods.data;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.script.api.wrappers.definitions.ItemDefinition;
import org.vrclient.main.utils.Utilities;

public class Equipment {
	private Player player;

	public static final int playerHat = 0;
	public static final int playerCape = 1;
	public static final int playerAmulet = 2;
	public static final int playerWeapon = 3;
	public static final int playerChest = 4;
	public static final int playerShield = 5;
	public static final int playerLegs = 7;
	public static final int playerHands = 9;
	public static final int playerFeet = 10;
	public static final int playerRing = 12;
	public static final int playerArrows = 13;
	
	public static String getWeapon(Player player, int id) {
		//ItemDefinition def = new ItemDefinition(player.getEquipment()[playerWeapon]-512);
		//return def == null ? null : def.getName();
		if(id == 12314) //ags
			return "AGS";
		if(id == 13775) //blung
			return "BLUNGEON";
		if(id == 12401) //hasta
			return "HASTA";
		if(id == 14088) //d warhammer
			return "D Warhammer";
		if(id == 12318) //SGS
			return "SGS";
		if(id == 12336) //SPEAR
			return "Z SPEAR";
		if(id == 12297) //SPEAR
			return "ACB";
		return null;
	}
	public static String getBankGp() {
		Field itemContainer = Reflection.field("Client#getItemContainers()");
		HashTableIterator hti = new HashTableIterator(Reflection.value(itemContainer, null));
		for (Object n = hti.getFirst(); n != null; n = hti.getNext()) {
		long id = (long) Reflection.value("Node_uid", n);
			if(id == 95) {
				int[] itemIds = (int[]) Reflection.value("ItemContainer#getIds()", n);
				int[] itemAmounts = (int[]) Reflection.value("ItemContainer#getAmounts()", n);
				for(int i = 0; i < itemAmounts.length; i++) {
					if(itemIds[i] == 995) {
					return "You have "+Utilities.withSuffix(itemAmounts[i])+" gp!";
						}
					}
				}
			}
		return null;
	}
	/*TODO:
	 * varrock general store = 4
	aubury's rune shop = 5
	varrock staff shop = 51
	ect.. 
	price checker = 90
	inventory = 93
	equipment = 94
	bank = 95
	looting bag = 516
	GE collectable items = 518
	 */
	public static ArrayList<String> getEquipmentNames() {
		ArrayList<String> names = new ArrayList<String>();
		Field itemContainer = Reflection.field("Client#getItemContainers()");
		HashTableIterator hti = new HashTableIterator(Reflection.value(itemContainer, null));
		for (Object n = hti.getFirst(); n != null; n = hti.getNext()) {
		long id = (long) Reflection.value("Node_uid", n);
			if(id == 94) {
				int[] itemIds = (int[]) Reflection.value("ItemContainer#getIds()", n);
				// Pointless unless you want to grab stackable Items
				//int[] itemAmounts = (int[]) Reflection.value("ItemContainer#getAmounts()", n);
				for (int i: itemIds) {
					ItemDefinition item = new ItemDefinition(i);
					if(item != null)
					names.add(item.getName());
				}
			}
		}
		return names;
	}
	public static String getWeapon2(Player player) {
		ItemDefinition def = new ItemDefinition(player.getEquipment()[playerWeapon]-512);
		return def == null ? null : def.getName().replace("\u00a0", " ");
	}
	public static boolean isRanging(Player player) {
		ItemDefinition itemDef = new ItemDefinition(player.getEquipment()[playerWeapon]-512);
		if(itemDef.getName().endsWith("bow") 
				|| itemDef.getName().endsWith("shortbow")
				|| itemDef.getName().endsWith("crossbow")
				|| itemDef.getName().endsWith("Ballista")
				|| itemDef.getName().endsWith("dart")
				|| itemDef.getName().endsWith("knife")
				|| itemDef.getName().endsWith("knife (p)")
				|| itemDef.getName().endsWith("javelin")
				|| itemDef.getName().endsWith("chinchompa")
				|| itemDef.getName().endsWith("Toktz-xil-ul")
				|| itemDef.getName().equalsIgnoreCase("Toxic blowpipe")
				|| itemDef.getName().endsWith("longbow")) { 
			return true;
		}
		return false;
	}
	public static boolean hasStaff(Player player) {
		ItemDefinition itemDef = new ItemDefinition(player.getEquipment()[playerWeapon]-512);
		if(itemDef.getName().endsWith("staff") 
				|| itemDef.getName().startsWith("Staff")
				|| itemDef.getName().contains("staff")
				|| itemDef.getName().endsWith("battlestaff")
				|| itemDef.getName().startsWith("Mystic") && itemDef.getName().endsWith("staff")
				|| itemDef.getName().endsWith("crozier")
				|| itemDef.getName().endsWith("knife (p)")
				|| itemDef.getName().endsWith("javelin")
				|| itemDef.getName().endsWith("chinchompa")
				|| itemDef.getName().endsWith("Toktz-xil-ul")
				|| itemDef.getName().equalsIgnoreCase("Toxic blowpipe")
				|| itemDef.getName().endsWith("longbow")) { //maple shortbow
			return true;
		}
		return false;
	}
	public static boolean hasMageGear(Player player) {
		ItemDefinition body = new ItemDefinition(player.getEquipment()[playerChest]-512);
		if(body.getName().startsWith("Mystic") 
				|| body.getName().startsWith("Xerician")
				|| body.getName().startsWith("Enchanted")
				|| body.getName().startsWith("Infinity")
				|| body.getName().startsWith("Ahrim's")
				|| body.getName().startsWith("Splitbark")
				|| body.getName().startsWith("Lunar")
				|| body.getName().startsWith("Ghostly")
				|| body.getName().equalsIgnoreCase("Skeletal top")) {
			return true;
		}
		return false;
	}
	public static boolean hasBindGear(Player player) {
		/*if(512-player.getEquipment()[playerChest] == 200 
				|| 512-player.getEquipment()[playerHat] == 200
				|| 512-player.getEquipment()[playerLegs] == 200) //nothing/naked
			return true;*/
		if(player.getEquipment()[playerChest]-512 == 577
				|| player.getEquipment()[playerChest]-512 == 581
				|| player.getEquipment()[playerLegs]-512 == 1095
				|| player.getEquipment()[playerLegs]-512 == 1097
				|| player.getEquipment()[playerHat]-512 == 579
				|| player.getEquipment()[playerAmulet]-512 == 1727)
			return true;
		return false;
	}
	public static String getShield(Player player) {
		ItemDefinition def = new ItemDefinition(player.getEquipment()[playerShield]-512);
		return def == null ? null : def.getName();
	}
	public static String getRing(Player player) {
		ItemDefinition def = new ItemDefinition(player.getEquipment()[playerRing]-512);
		return def == null ? null : def.getName();
	}
	public static String getAmulet(Player player) {
		ItemDefinition def = new ItemDefinition(player.getEquipment()[playerAmulet]-512);
		return def == null ? null : def.getName();
	}
	public static boolean isValueable(Player player) {
		String[] names = {"Dragon Claws", "Heavy ballista", "Dragonfire shield", "Armadyl crossbow",
				"Toxic Staff of the dead", "Armadyl godsword", 
				"Dragon warhammer", "Saradomin godsword", 
				"Abyssal bludgeon", "Elysian spirit shield", "Arcane spirit shield", "Spectral spirit shield"
				};	
		for(String name: names) {
			if(getWeapon2(player).equalsIgnoreCase(name) || getShield(player).equalsIgnoreCase(name))
				return true;
		}
			return false;
	}
	public static boolean isShieldValueable(Player player) {
		String[] names = {"Dragonfire shield", "Elysian spirit shield", "Arcane spirit shield", "Spectral spirit shield"};
		for(String name: names) {
		if(getShield(player).equalsIgnoreCase(name))
			return true;
	}
		return false;
 }
		public static boolean isWeaponValueable(Player player) {
				String[] names = {"Dragon claws", "Armadyl crossbow", "Heavy ballista","Toxic Staff of the dead", "Armadyl godsword", "Dragon warhammer", "Saradomin godsword", 
						"Abyssal bludgeon"};
				for(String name: names) {
				if(getWeapon2(player).equalsIgnoreCase(name))
					return true;
			}
				return false;
		 }
		static class HashTableIterator {

	        private Object hashTable;
	        private int currindex;
	        private Object curr;

	        HashTableIterator(Object hashTable) {
	            this.hashTable = hashTable;
	        }

	        final Object getFirst() {
	            currindex = 0;
	            return getNext();
	        }
	        final Object getNext() {
	            if (hashTable == null)
	                return null;
	            final Object[] buckets = (Object[]) Reflection.value("NodeHashTable#getBuckets()", hashTable);
	            if (buckets == null)
	                return null;
	            if (currindex > 0 && curr != buckets[currindex - 1]) {
	                final Object node = curr;
	                if (node == null) {
	                    return null;
	                }
	                curr = Reflection.value("Node_previous", node);
	                return node;
	            }
	            while (currindex < buckets.length) {
	                final Object node1 = Reflection.value("Node_previous", buckets[currindex++]);
	                if (node1 != null) {
	                    if (buckets[currindex - 1] != node1) {
	                        curr = Reflection.value("Node_previous", node1);
	                        return node1;
	                    }
	                }
	            }
	            return null;
	        }
		}
	
}
