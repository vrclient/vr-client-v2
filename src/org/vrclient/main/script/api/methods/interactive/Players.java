package org.vrclient.main.script.api.methods.interactive;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.query.PlayerQuery;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.script.api.wrappers.Tile;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by VR on 7/30/14
 */
public class Players {

    private static PlayerQuery query = new PlayerQuery();

    public static PlayerQuery query() {
        return query;
    }
    public static Player getPlayerByName(String name)
    {
        for(Player p : getAll())
            if(p.getName().equalsIgnoreCase(name))
                return p;
        return null;
    }
    /**
     * @return Player : Your Player
     */
    public static Player getLocal() {
        return new Player(Reflection.value("client_player", null));
    }

    /**
     * @return Player[] : all players
     */
    public static Player[] getAll() {
        return getAll(null);
    }

    /**
     * Get all Players that apply to that filter
     *
     * @param filter
     * @return Player[]
     */
    public static Player[] getAll(Filter<Player> filter) {
    	if(!Game.isLoggedIn())
    	return null;
        List<Player> list = new ArrayList<Player>();
        final Object[] objects = (Object[]) Reflection.value("client_players", null);
        for (Object player : objects) {
            if (player != null) {
                Player wrapper = new Player(player);
                if ((filter == null || filter.accept(wrapper))) {
                    list.add(wrapper);
                }
            }
        }
        return list.toArray(new Player[list.size()]);
    }

    /**
     * get Nearest Specific Player to Specific Location
     *
     * @param location startLocation
     * @param filter   Player Filter
     * @return Player
     */
    public static Player getNearest(Tile location, Filter<Player> filter) {
        Player closet = new Player(null);
        int distance = 9999;
        for (Player player : getAll(filter)) {
            if (player.isValid() && distance > player.distanceTo(location)) {
                closet = player;
            }
        }
        return closet;
    }

    /**
     * get Nearest Specific Player to Specific Location
     *
     * @param filter Player Filter
     * @return Player
     */
    public static Player getNearest(Filter<Player> filter) {
        return getNearest(Players.getLocal().getLocation(), filter);
    }

	/**

	 * @return wrapper that have null in structure to avoid Null Pointer Exception and able to use Player#isValid instead
	 */
	public static Player nil(){
		return new Player(null);
	}

}
