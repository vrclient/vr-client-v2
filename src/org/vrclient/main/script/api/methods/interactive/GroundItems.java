package org.vrclient.main.script.api.methods.interactive;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.query.GroundItemQuery;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.script.api.wrappers.GameObject;
import org.vrclient.main.script.api.wrappers.GroundItem;
import org.vrclient.main.script.api.wrappers.Tile;
import org.vrclient.main.script.api.wrappers.threads.GroundItemThread;
import org.vrclient.main.script.api.wrappers.threads.PriceThread;
import org.vrclient.main.utils.Utilities;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/*
 * Created by VR on 7/31/14
 */
public class GroundItems {

    private static final GroundItemQuery query = new GroundItemQuery();
    public static HashMap<Integer, Integer> priceDefinitions = new HashMap<Integer, Integer>();

    /**
     * Gets the query instance
     *
     * @return the query instance
     */
    public static GroundItemQuery query() {
        return query;
    }

    /*
     * public static GroundItem[] getAll() {
		ArrayList<GroundItem> items = new ArrayList<GroundItem>();
		HashTable itemTable = Client.getLoadedItems();
		for(Node n : itemTable.getBuckets()){
			for(Node in = n.getNext();in!=null && !in.currentObject.equals(n.currentObject);in=in.getNext()){
				DequeNode cache = new DequeNode(in.currentObject);
				Deque nodeList = cache.getDeque();
				Node tail = nodeList.getSentinel();
				for(Node curr = tail.getPrevious();curr!=null && !curr.currentObject.equals(tail.currentObject);curr=curr.getPrevious()){
					if(ItemNode.isInstance(curr.currentObject)){
						long id = in.getID();
						int x = (int) (id % 16384);
						int y = (int)((int)(id / 16384) % 16384);
						int plane = (int)((id - (x | y << 14))/268435456);
						items.add(new GroundItem(x, y, plane, new ItemNode(curr.currentObject)));
					}
				}
			}
		}
		return items.toArray(new GroundItem[]{});
	}
     */
  
 
    /**
     * @param filter
     * @return GroundItem[] : return all groundsitems in your region
     * that applied to that filter
     */
    public static GroundItem[] getAll(Filter<GroundItem> filter) {
        //ArrayList<GroundItem> groundItems = new ArrayList<GroundItem>();
        Set<GroundItem> groundItems = new LinkedHashSet<GroundItem>();
        Field groundArray = Reflection.field("client_groundItems");
        Object[][][] groundArrayObjects = (Object[][][]) Reflection.value(groundArray, null);
        Field head = Reflection.field("LinkedList_head");
        Field next = Reflection.field("Node_next");
        int z = Game.getPlane();
        for (int x = (Players.getLocal().getLocalX() >> 7)-10; x < (Players.getLocal().getLocalX() >> 7)+10; x++) {
            for (int y = (Players.getLocal().getLocalY() >> 7)-10; y < (Players.getLocal().getLocalY() >> 7)+10; y++) {
                Object nl = groundArrayObjects[z][x][y];
                if (nl != null) {
                    Object holder = Reflection.value(head, nl);
                    Object curNode = Reflection.value(next, holder);
                    while (curNode != null && curNode != holder && curNode != Reflection.value(head, nl)) {
                        GroundItem groundItem = new GroundItem(curNode, new Tile(Game.getBaseX() + x, Game.getBaseY() + y, Game.getPlane()));
                        if(!priceDefinitions.containsKey(groundItem.getId())) {
                        PriceThread pricing = new PriceThread(groundItem.getId());
                        Thread thread = new Thread(pricing);
                        thread.start();
                        thread.interrupt();
                        }
                        if (filter == null || filter.accept(groundItem)) {
                            groundItems.add(groundItem);
                        }
                        curNode = Reflection.value(next, curNode);
                    }
                }
            }
        }
        return groundItems.toArray(new GroundItem[groundItems.size()]);
    }

    /**
     * Return all groundItems that have one of these names
     *
     * @param names
     * @return GroundItem[] : Get all gorunditems with any of these names
     */
    public static GroundItem[] getAll(final String... names) {
        return getAll(new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && groundItem.getName() != null && Utilities.inArray(groundItem.getName(), names);
            }
        });
    }

    /**
     * Return all groundItems that have one of these ids
     *
     * @param ids
     * @return GroundItem[] : Get all gorunditems with any of these ids
     */
    public static GroundItem[] getAll(final int... ids) {
        return getAll(new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && Utilities.inArray(groundItem.getId(), ids);
            }
        });
    }

    /**
     * @return GroundItem[]: return all grounditems in your region 104x104
     */
    public static GroundItem[] getAll() {
        return getAll(new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return true;
            }
        });
    }

    /**
     * @param filter
     * @return GroundItem : nearest groundItem to Local Player
     * that apply to that filter
     */
    public static GroundItem getNearest(Filter<GroundItem> filter) {
        return getNearest(Players.getLocal().getLocation(), filter);
    }

    /**
     * @param filter
     * @return GroundItem : nearest groundItem to start tile
     * that apply to that filter
     */
    public static GroundItem getNearest(Tile start, Filter<GroundItem> filter) {
        GroundItem closet = new GroundItem(null, null);
        int distance = 255;
        for (GroundItem groundItem : getAll(filter)) {
            if (groundItem.isValid() && distance > groundItem.distanceTo(start)) {
                closet = groundItem;
                distance = groundItem.distanceTo(start);
            }
        }
        return closet;
    }

    /**
     * Get closet GroundItem that has that Id or one of ids
     *
     * @param ids target GroundItem Id or Ids
     * @return GroundItem
     */
    public static GroundItem getNearest(final int... ids) {
        return getNearest(Players.getLocal().getLocation(), new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && Utilities.inArray(groundItem.getId(), ids);
            }
        });
    }

    /**
     * Get closet GroundItem that has that name or one of names
     *
     * @param names target GroundItem name or names
     * @return GroundItem
     */
    public static GroundItem getNearest(final String... names) {
        return getNearest(Players.getLocal().getLocation(), new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && Utilities.inArray(groundItem.getName(), names);
            }
        });
    }

    /**
     * @param tile
     * @return GroundItem : ground item that is in specific tile
     * if there isn't it will return null
     */
    public static GroundItem getAt(final Tile tile) {
        return getNearest(Players.getLocal().getLocation(), new Filter<GroundItem>() {

            @Override
            public boolean accept(GroundItem obj) {
                return obj != null && tile.equals(obj.getLocation());
            }
        });
    }
    /**
   	 * Returns all the ground items at a tile on the current plane.
   	 *
   	 * @param x The x position of the tile in the world.
   	 * @param y The y position of the tile in the world.
   	 * @return An array of the ground items on the specified tile.
   	 */
   	public static GroundItem[] getAllAt(int x, int y) {
   		if (!Game.isLoggedIn()) {
   			return new GroundItem[0];
   		}
   		ArrayList<GroundItem> list = new ArrayList<GroundItem>();

   		HashTableIterator itemNC = new HashTableIterator(Reflection.value("getGroundItemTable()", null));//(HashTableIterator) Reflection.value("ItemHashTable", null);
   		int id = x | y << 14 | (int) Reflection.value("client_plane", null) << 28;
   		
   		if (itemNC.getFirst() == null) {
   			return new GroundItem[0];
   		}
   		 for (Object n = itemNC.getFirst(); n != null; n = itemNC.getNext()) {
   			 list.add(new GroundItem(n, new Tile(x, y)));
            }
   		return list.toArray(new GroundItem[list.size()]);
   	}
   	/**
	 * Returns all the ground items at a tile on the current plane.
	 *
	 * @param t The tile.
	 * @return An array of the ground items on the specified tile.
	 */
	public static GroundItem[] getAllAt(Tile t) {
		return getAllAt(t.getX(), t.getY());
	}
    public static GroundItem getNext(Filter<GroundItem> filter) {
        GroundItem[] groundItems = getAll(filter);
	    if (groundItems == null || groundItems.length < 1)
            return nil();
        return groundItems[Random.nextInt(0, groundItems.length)];
    }

    public static GroundItem getNext(final String... names) {
        return getNext(new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && groundItem.getName() != null && Utilities.inArray(groundItem.getName(), names);
            }
        });
    }

    public static GroundItem getNext(final int... ids) {
        return getNext(new Filter<GroundItem>() {
            @Override
            public boolean accept(GroundItem groundItem) {
                return groundItem.isValid() && Utilities.inArray(groundItem.getId(), ids);
            }
        });
    }

    /**
     * @return wrapper that have null in structure to avoid Null Pointer Exception and able to use GroundItem#isValid instead
     */
    public static GroundItem nil() {
        return new GroundItem(null, null);
    }

   static class HashTableIterator {

        private Object hashTable;
        private int currindex;
        private Object curr;

        HashTableIterator(Object hashTable) {
            this.hashTable = hashTable;
        }

        final Object getFirst() {
            currindex = 0;
            return getNext();
        }
       
        final Object getNext() {
            if (hashTable == null)
                return null;
            final Object[] buckets = (Object[]) Reflection.value("NodeHashTable#getBuckets()", hashTable);
            if (buckets == null)
                return null;
            if (currindex > 0 && curr != buckets[currindex - 1]) {
                final Object node = curr;
                if (node == null) {
                    return null;
                }
                curr = Reflection.value("Node_previous", node);
                return node;
            }
            while (currindex < buckets.length) {
                final Object node1 = Reflection.value("Node_previous", buckets[currindex++]);
                if (node1 != null) {
                    if (buckets[currindex - 1] != node1) {
                        curr = Reflection.value("Node_previous", node1);
                        return node1;
                    }
                }
            }
            return null;
        }
    }
}
