package org.vrclient.main.script.api.methods.interactive;

import java.util.ArrayList;
import java.util.List;

import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.query.NpcQuery;
import org.vrclient.main.script.api.wrappers.NPC;
import org.vrclient.main.script.api.wrappers.Tile;

public class Npcs {

	private static NpcQuery query = new NpcQuery();

    public static NpcQuery query() {
        return query;
    }
    public static NPC getNpcByName(String name)
    {
        for(NPC npc : getAll())
            if(npc.getName().equalsIgnoreCase(name))
                return npc;
        return null;
    }

    /**
     * @return Player[] : all npcs
     */
    public static NPC[] getAll() {
        return getAll(null);
    }

    /**
     * Get all NPCs that apply to that filter
     *
     * @param filter
     * @return NPC[]
     */
    public static NPC[] getAll(Filter<NPC> filter) {
        List<NPC> list = new ArrayList<NPC>();
        final Object[] objects = (Object[]) Reflection.value("client_npcs", null);
        for (Object npc : objects) {
            if (npc != null) {
                NPC wrapper = new NPC(npc);
                if ((filter == null || filter.accept(wrapper))) {
                    list.add(wrapper);
                }
            }
        }
        return list.toArray(new NPC[list.size()]);
    }

    /**
     * get Nearest Specific NPC to Specific Location
     *
     * @param location startLocation
     * @param filter   Player Filter
     * @return NPC
     */
    public static NPC getNearest(Tile location, Filter<NPC> filter) {
        NPC closet = new NPC(null);
        int distance = 9999;
        for (NPC npc : getAll(filter)) {
            if (npc.isValid() && distance > npc.distanceTo(location)) {
                closet = npc;
            }
        }
        return closet;
    }

    /**
     * get Nearest Specific NPC to Specific Location
     *
     * @param filter NPC Filter
     * @return NPC
     */
    public static NPC getNearest(Filter<NPC> filter) {
        return getNearest(Players.getLocal().getLocation(), filter);
    }

	/**

	 * @return wrapper that have null in structure to avoid Null Pointer Exception and able to use Player#isValid instead
	 */
	public static NPC nil(){
		return new NPC(null);
	}
}
