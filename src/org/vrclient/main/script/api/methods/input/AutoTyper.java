package org.vrclient.main.script.api.methods.input;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Random;

import org.vrclient.main.Configuration;

/**
 * Class AutoTyper, allows the client to send variable text
 * 
 * Note: This class is currently in development, and will be changed to include
 * a delay.
 * 
 * @author Nicole <nicole@rune-server.org> This file is protected by The BSD
 *         License, You should have recieved a copy named "BSD License.txt"
 */

public class AutoTyper {

	private Robot robot;

	private String textsToSay = new String();

	private static boolean running = false;
 
	private long delay = 4000;

	private int index = 0;

	private boolean paused = false;
	
	private static Configuration config = Configuration.getInstance();

	public AutoTyper() {
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public class AutoTyperRunnable implements Runnable {
		@Override
		public void run() {
			if(!config.getCanvas().getCanvas().hasFocus())
			return;//config.getCanvas().getCanvas().requestFocus();
			if (running) {
				if (index >= 10 || textsToSay.length() == index
						|| textsToSay == null) {
					index = 0;
				}
				String s = textsToSay;
				for (char c : s.toCharArray()) {
					if(c == ':') {
						robot.keyPress(KeyEvent.VK_SHIFT);
						robot.keyPress(KeyEvent.VK_SEMICOLON);
						robot.keyRelease(KeyEvent.VK_SEMICOLON);
						robot.keyRelease(KeyEvent.VK_SHIFT);						
					} else if(c == '_') {
						robot.keyPress(KeyEvent.VK_SHIFT);
						robot.keyPress(KeyEvent.VK_MINUS);
						robot.keyRelease(KeyEvent.VK_MINUS);
						robot.keyRelease(KeyEvent.VK_SHIFT);
					} else {
					robot.keyPress(Character.toUpperCase(c));
					robot.keyRelease(Character.toUpperCase(c));
					}
	
				}			
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
			}
		}

	}

	public void start(String string, boolean junk) {
		this.textsToSay = string;
		running = true;
		new Thread(new AutoTyperRunnable()).start();
	}

	public void stop() {
		running = false;
	}

	public void pause(boolean b) {
		paused = b;
	}
	public void type(String string, boolean junk) {
		this.textsToSay = string;
		running = true;
	}
	public boolean isRunning() {
		return running;
	}

	public boolean isPaused() {
		return paused;
	}

	public String getTexts() {
		return textsToSay;
	}

	public void setDelay(long l) {
		this.delay = l;
	}

	public long getDelay() {
		return delay;
	}
}