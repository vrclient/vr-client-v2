package org.vrclient.main.script.api.interfaces;

import org.vrclient.main.script.api.wrappers.Tile;

import java.awt.*;

/**
 * Created by VR on 7/29/2014.
 */
public interface Locatable {

    public boolean isOnScreen();

    public Point getPointOnScreen();

    public int distanceTo();

    public int distanceTo(Locatable locatable);

    public int distanceTo(Tile tile);

    public Tile getLocation();

    public static interface Query<Q> {
        public Q within(final int radius);

        public Q at(final Tile tile);

        public Q nearest();
    }
    

}
