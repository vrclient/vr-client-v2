package org.vrclient.main.script.api.interfaces;

/**
 * Created with IntelliJ IDEA.
 * User: VR
 * Date: 11/12/13
 * Time: 7:33 PM
 */
public interface Condition {

    public boolean active();
}
