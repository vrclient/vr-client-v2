package org.vrclient.main.script.api.interfaces;

/**
 * Created by VR on 7/30/2014.
 */
public interface Filter<E> {

    public boolean accept(E e);


}
