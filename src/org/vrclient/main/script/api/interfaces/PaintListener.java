package org.vrclient.main.script.api.interfaces;

import java.awt.*;
import java.awt.event.MouseAdapter;

/**
 * Created by VR on 7/30/2014.
 */
public interface PaintListener {

    public void render(Graphics2D graphics);
    
    
}
