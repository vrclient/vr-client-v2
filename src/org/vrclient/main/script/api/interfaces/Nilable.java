package org.vrclient.main.script.api.interfaces;

/**
 * Created by VR on 8/10/2014.
 */
public interface Nilable<E> {

    public E nil();

}
