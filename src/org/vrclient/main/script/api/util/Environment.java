package org.vrclient.main.script.api.util;

import org.vrclient.main.Configuration;


/*
 * Created by VR on 8/3/14
 */
public class Environment {

    public static int getUserId() {
        return Configuration.getInstance().getUser().getUserId();
    }

    public static String getDisplayName() {
        return Configuration.getInstance().getUser().getDisplayName();
    }

}
