package org.vrclient.main;

import org.vrclient.component.RSCanvas;
import org.vrclient.main.client.patterns.Pattern;
import org.vrclient.main.script.api.listeners.SoundMonitor;
import org.vrclient.main.client.security.encryption.AES;
import org.vrclient.main.script.api.methods.input.AutoTyper;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.ui.Console;
import org.vrclient.main.ui.Frame;
import org.vrclient.main.ui.login.misc.User;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Logger;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.*;

public class Configuration {

	private static final Logger logger = new Logger(Configuration.class);
	private AutoTyper autotyper;
	private RSCanvas canvas;
	private SoundMonitor soundMonitor;
	private Frame botFrame;
	private Console console;
	private User user;
	private AES encryption;
	private Pattern pattern;
	public Rectangle Recordingrect;

	

	private boolean enableSidebar = true;
	private boolean enableonTop = false;
	private boolean enablerecord = false;
	private boolean drawPlayers = false;
	private boolean drawTextDebugger = false;
	private boolean drawPlayerLocation = false;
	private boolean drawGroundItems = false;
	private boolean drawGameObjects = false;
	private boolean drawSettings = false;
	private boolean drawCanvas = true;
	private boolean drawGameState = false;
	private boolean drawMenu = false;
    private boolean drawWidgets = false;
	private boolean drawMiniMap = false;
	private boolean drawCallerInfo = true;
	private boolean drawBinder = false;
	private boolean writeBinderNames = false;
	private boolean drawMenuPlayers = false;
	private boolean drawEnemyLeaders = false;
	private boolean drawNightMareZone = false;
	private boolean drawExperienceTracker = true;
	private boolean drawCCInfo = false;
	private boolean drawBinderTimer = false;
	private boolean drawAntiTk = false;
	private boolean drawWorldMap = false;
	
	private boolean drawSpecBar;
	private boolean enablepilehelper;
	private boolean pilehelpercombat;
	private boolean drawfriends;
	private boolean drawOpponentInfo;
	private boolean drawFPS;
	private boolean rememberme;
	private boolean drawTilecaller1;
	private boolean drawoverhead;
	private boolean drawpilehelperprays;
	private boolean ssautoupload;
	public String recordLocation;
	public int recordHotKey;
	private boolean overloadAlert;
	private boolean absorbAlert;
	private boolean drawRapidHeal;
	private boolean autoSpammer;
	private boolean drawToolTip;
	private boolean drawBoosts;
	private boolean drawJewelry;
	public int overlayOpacity;	
	private boolean drawDeath;
	private boolean drawDeathTimer;
	private boolean drawBossTimer;
	private boolean drawStaminaTimer;
	private boolean drawTbTimer;
	private boolean drawAfTimer;
	private boolean drawSafTimer;
	private boolean drawHomeTimer;
	private boolean drawImbueTimer;
	private boolean drawVengTimer;
	private boolean drawOverloadTimer;
	private boolean drawImbheartTimer;
	
	


	private static Configuration instance = new Configuration();

	public boolean drawExperienceTracker() {
		return drawExperienceTracker;
	}
	public void setExperienceTracker(boolean experienceTracker) {
		this.drawExperienceTracker = experienceTracker;
	}
	public boolean drawSpecBar() {
		return drawSpecBar;
	}
	public void setSpecBar(boolean drawSpecBar) {
		this.drawSpecBar = drawSpecBar;
	}
	
	public boolean drawWorldMap() {
		return drawWorldMap;
	}
	public void drawWorldMap(boolean drawWorldMap) {
		this.drawWorldMap = drawWorldMap;
	}


	

	public boolean drawMenu() {
		return drawMenu;
	}

	public void drawMenu(boolean drawMenu) {
		this.drawMenu = drawMenu;
	}

	public boolean drawGameState() {
		return drawGameState;
	}
	public boolean drawWidgets() {
		return drawWidgets;
	}

	public void drawWidgets(boolean drawWidgets) {
		this.drawWidgets = drawWidgets;
	}
	public void drawGameState(boolean drawGameState) {
		this.drawGameState = drawGameState;
	}


	public void drawSettings(boolean drawSettings) {
		this.drawSettings = drawSettings;
	}

	public boolean drawSettings() {
		return this.drawSettings;
	}

	public static Configuration getInstance() {
		return instance;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Pattern pattern() {
		if (pattern == null)
			pattern = new Pattern();
		return pattern;
	}


	public AES getEncryption() {
		if (encryption == null)
			logger.error("Encryption isn't set!");
		return encryption;
	}

	public void setEncryption(AES encryption) {
		this.encryption = encryption;
	}

	public Console getConsole() {
		if (console == null)
			logger.error("Console isn't set!");
		return console;
	}

	public void setConsole(Console console) {
		this.console = console;
	}
	public AutoTyper getAutoTyper() {
		if(autotyper == null)
			logger.error("Autotyper isn't set!");
		return autotyper;
	}
	public void setAutoTyper(AutoTyper typer) {
		this.autotyper = typer;
	}
	

	public RSCanvas getCanvas() {
		if (canvas == null)
			logger.error("canvas isn't set!");
		return canvas;
	}

	public void setCanvas(RSCanvas canvas) {
		this.canvas = canvas;
	}

	public Frame getBotFrame() {
		if (botFrame == null)
			logger.error("botFrame isn't set!");
		return botFrame;
	}

	public void setBotFrame(Frame botFrame) {
		this.botFrame = botFrame;
	}

	
	public boolean enableSidebar() {
		return enableSidebar;
	}

	public void enableSidebar(boolean enableSidebar) {
		this.enableSidebar = enableSidebar;
	}

	public boolean enableonTop() {
		return enableonTop;
	}

	public void enableonTop(boolean enableonTop) {
		this.enableonTop = enableonTop;
	}
	
	public boolean enablerecord() {
		return enablerecord;
	}

	public void enablerecord(boolean enablerecord) {
		this.enablerecord = enablerecord;
	}

	public boolean drawPlayers() {
		return drawPlayers;
	}

	public void drawPlayers(boolean drawPlayers) {
		this.drawPlayers = drawPlayers;
	}
	public void drawBinderTimer(boolean drawBinderTimer) {
		this.drawBinderTimer = drawBinderTimer;
	}
	public boolean drawBinderTimer() {
		return drawBinderTimer;
	}
	public void drawAntiTk(boolean drawAntiTk) {
		this.drawAntiTk = drawAntiTk;
	}
	public boolean drawAntiTk() {
		return drawAntiTk;
	}

	public boolean drawGameObjects() {
		return drawGameObjects;
	}

	public boolean drawPlayerLocation() {
		return drawPlayerLocation;
	}

	public void drawPlayerLocation(boolean drawPlayerLocation) {
		this.drawPlayerLocation = drawPlayerLocation;
	}

	public void drawGameObjects(boolean drawGameObjects) {
		this.drawGameObjects = drawGameObjects;
	}

	public boolean drawGroundItems() {
		return drawGroundItems;
	}

	public void drawGroundItems(final boolean drawGroundItems) {
	
		this.drawGroundItems = drawGroundItems;
	}

	public boolean drawCanvas() {
		return drawCanvas;
	}

	public void drawCanvas(boolean drawCanvas) {
		this.drawCanvas = drawCanvas;
	}

	public boolean drawMiniMap() {
		return drawMiniMap;
	}
	public void drawMiniMap(boolean drawMiniMap) {
		this.drawMiniMap = drawMiniMap;
	}
    public boolean drawCallerInfo() {
		return this.drawCallerInfo;
	}
	public void drawCallerInfo(boolean drawCallerInfo) {
		this.drawCallerInfo = drawCallerInfo;
	}
	public void drawMenuPlayers(boolean drawMenuPlayers) {
		this.drawMenuPlayers = drawMenuPlayers;
	}
	public boolean drawMenuPlayers() {
		return this.drawMenuPlayers;
	}
	public boolean drawBinder() {
		return this.drawBinder;
	}
	public void drawBinder(boolean drawBinder) {
		this.drawBinder = drawBinder;
	}
	public void writeBinderNames(boolean writeBinderNames) {
		this.writeBinderNames = writeBinderNames;
	}
	public boolean writeBinderNames() {
		return this.writeBinderNames;
	}
	public void drawEnemyLeaders(boolean drawEnemyLeaders) {
		this.drawEnemyLeaders = drawEnemyLeaders;
	}
	public boolean drawEnemyLeaders() {
		return this.drawEnemyLeaders;
	}
	public void drawNightMareZone(boolean drawNightMareZone) {
		this.drawNightMareZone = drawNightMareZone;
	}
	public boolean nightmareZone() {
		return this.drawNightMareZone;
	}
	public boolean drawCCInfo() {
		return this.drawCCInfo;
	}
	public void drawCCInfo(boolean drawCCInfo) {
		this.drawCCInfo = drawCCInfo;
	}
	
	
	//Updated Settings
	public boolean enablepilehelper() {
		return this.enablepilehelper;
	}
	public void enablepilehelper(boolean enablepilehelper) {
		this.enablepilehelper = enablepilehelper;
	}
	public boolean pilehelpercombat() {
		return this.pilehelpercombat;
	}
	public void pilehelpercombat(boolean pilehelpercombat) {
		this.pilehelpercombat = pilehelpercombat;
	}
	public boolean drawfriends() {
		return this.drawfriends;
	}
	public void drawfriends(boolean drawfriends) {
		this.drawfriends = drawfriends;
	}
	public boolean drawOpponentInfo() {
		return drawOpponentInfo;
	}
	public void drawOpponentInfo(boolean drawOpponentInfo) {
		this.drawOpponentInfo = drawOpponentInfo;
	}
	public boolean drawFPS() {
		return drawFPS;
	}
	public void drawFPS(boolean drawFPS) {
		this.drawFPS = drawFPS;
	}
	public boolean rememberme() {
		return rememberme;
	}
	public void rememberme(boolean rememberme) {
		this.rememberme = rememberme;
	}
	public boolean drawTilecaller1() {
		return drawTilecaller1;
	}
	public void drawTilecaller1(boolean drawTilecaller1) {
		this.drawTilecaller1 = drawTilecaller1;
	}
	public boolean drawoverhead() {
		return drawoverhead;
	}
	public void drawoverhead(boolean drawoverhead) {
		this.drawoverhead = drawoverhead;
	}
	public boolean drawpilehelperprays() {
		return drawpilehelperprays;
	}
	public void drawpilehelperprays(boolean drawpilehelperprays) {
		this.drawpilehelperprays = drawpilehelperprays;
	}
	public boolean ssautoupload() {
		return ssautoupload;
	}
	public void ssautoupload(boolean ssautoupload) {
		this.ssautoupload = ssautoupload;
	}
	public void drawTextDebugger(boolean drawText) {
		this.drawTextDebugger = drawText;
	}
	public boolean drawTextDebugger() {
		return drawTextDebugger;
	}
	public void setSoundManager(SoundMonitor soundMonitor) {
		this.soundMonitor = soundMonitor;	
	}
	public SoundMonitor getSoundMonitor() {
		if (soundMonitor == null)
		logger.error("soundMonitor isn't set!");
		return soundMonitor;
	}
	public Rectangle getRecordingrect() {
		return Recordingrect;
	}
	public void setRecordingrect(Rectangle recordingrect) {
		Recordingrect = recordingrect;
	}
	public void recordLocation(String load) {
		recordLocation = load;
	}
	public void recordHotKey(int load) {
		recordHotKey = load;
	}
	public void overloadAlert(boolean overloadAlert) {
		this.overloadAlert = overloadAlert;
	}
	public boolean overloadAlert() {
		return overloadAlert;
	}
	public void absorbAlert(boolean absorbAlert) {
		this.absorbAlert = absorbAlert;
	}
	public boolean absorbAlert() {
		return absorbAlert;
	}
	public void drawRapidHeal(boolean drawRapidHeal) {
		this.drawRapidHeal = drawRapidHeal;
	}
	public boolean drawRapidHeal() {
		return drawRapidHeal;
	}
	public void autoSpammer(boolean autoSpammer) {
		this.autoSpammer = autoSpammer;
	}
	public boolean autoSpammer() {
		return autoSpammer;
	}
	public void drawToolTip(boolean drawToolTip) {
		this.drawToolTip = drawToolTip;
	}
	public boolean drawToolTip() {
		return drawToolTip;
	}
	
	public void drawBoosts(boolean drawBoosts) {
		this.drawBoosts = drawBoosts;
	}
	public boolean drawBoosts() {
		return drawBoosts;
	}
	
	public void drawJewelry(boolean drawJewelry) {
		this.drawJewelry = drawJewelry;
	}
	public boolean drawJewelry() {
		return drawJewelry;
	}
	public void overlayOpacity(int load) {
		overlayOpacity = load;
	}
	
	public void drawDeath(boolean drawDeath) {
		this.drawDeath = drawDeath;
	}
	public boolean drawDeath() {
		return drawDeath;
	}
	
	public void drawDeathTimer(boolean drawDeathTimer) {
		this.drawDeathTimer = drawDeathTimer;
	}
	public boolean drawDeathTimer() {
		return drawDeathTimer;
	}
	
	public void drawBossTimer(boolean drawBossTimer) {
		this.drawBossTimer = drawBossTimer;
	}
	public boolean drawBossTimer() {
		return drawBossTimer;
	}
	public void drawStaminaTimer(boolean drawStaminaTimer) {
		this.drawStaminaTimer = drawStaminaTimer;
	}
	public boolean drawStaminaTimer() {
		return drawStaminaTimer;
	}
	public void drawTbTimer(boolean drawTbTimer) {
		this.drawTbTimer = drawTbTimer;
	}
	public boolean drawTbTimer() {
		return drawTbTimer;
	}
	public void drawAfTimer(boolean drawAfTimer) {
		this.drawAfTimer = drawAfTimer;
	}
	public boolean drawAfTimer() {
		return drawAfTimer;
	}
	public void drawSafTimer(boolean drawSafTimer) {
		this.drawSafTimer = drawSafTimer;
	}
	public boolean drawSafTimer() {
		return drawSafTimer;
	}
	public void drawHomeTimer(boolean drawHomeTimer) {
		this.drawHomeTimer = drawHomeTimer;
	}
	public boolean drawHomeTimer() {
		return drawHomeTimer;
	}
	public void drawImbueTimer(boolean drawImbueTimer) {
		this.drawImbueTimer = drawImbueTimer;
	}
	public boolean drawImbueTimer() {
		return drawImbueTimer;
	}
	public void drawVengTimer(boolean drawVengTimer) {
		this.drawVengTimer = drawVengTimer;
	}
	public boolean drawVengTimer() {
		return drawVengTimer;
	}
	public void drawOverloadTimer(boolean drawOverloadTimer) {
		this.drawOverloadTimer = drawOverloadTimer;
	}
	public boolean drawOverloadTimer() {
		return drawOverloadTimer;
	}
	public void drawImbheartTimer(boolean drawImbheartTimer) {
		this.drawImbheartTimer = drawImbheartTimer;
	}
	public boolean drawImbheartTimer() {
		return drawImbheartTimer;
	}
	
	
}
