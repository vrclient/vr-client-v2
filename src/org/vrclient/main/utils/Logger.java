package org.vrclient.main.utils;

import org.vrclient.main.Configuration;

import java.awt.*;

/**
 * Created by VR on 7/29/2014.
 */
public class Logger {

    private final Configuration config = Configuration.getInstance();

    private String prefix = "";

    public Logger(Class<?> c) {
        this.prefix = c.getSimpleName();
    }

    public void error(String message) {
        System.out.println("[" + prefix + "] - ERROR: " + message);
        if (config != null && config.getConsole() != null)
            config.getConsole().append("[" + prefix + "] - ERROR: " + message, Color.RED.brighter());
    }

    public void info(String message) {
        info(message, Color.WHITE);
    }

    public void info(String message, Color color) {
        System.out.println("[" + prefix + "] - INFO: " + message);
        if (config != null && config.getConsole() != null)
            config.getConsole().append("[" + prefix + "] - INFO: " + message, color);
    }
}
