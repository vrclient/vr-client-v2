package org.vrclient.main.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PriceLookup {

	private static URLConnection con;
	private static InputStream is;
	private static InputStreamReader isr;
	private static BufferedReader br;

	private static String[] getData(int itemID) {
		try {
			URL url = new URL(
					"https://api.rsbuddy.com/grandExchange?a=guidePrice&i="
							+ itemID);
			con = url.openConnection();
			is = con.getInputStream();
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);
			String line = br.readLine();
			if (line != null) {
				return line.split(",");
			}
		} catch (Exception e) {

		} 
		return null;
	}

	public static int getPrice(int itemID) {
		String[] data = getData(itemID);
		if (data != null && data.length == 5) {
			return Integer.parseInt(data[0].replaceAll("\\D", ""));
		}
		return 1;
	}

	public static int getAverageBuyOffer(int itemID) {
		String[] data = getData(itemID);
		if (data != null && data.length == 5) {
			return Integer.parseInt(data[1].replaceAll("\\D", ""));
		}
		return 0;
	}

	public static int getAverageSellOffer(int itemID) {
		String[] data = getData(itemID);
		if (data != null && data.length == 5) {
			return Integer.parseInt(data[2].replaceAll("\\D", ""));
		}
		return 0;
	}
	
	public static Map<Integer, Integer> getPrices(Collection<Integer> ids) {
		Map<Integer, Integer> prices = new HashMap<>();
		List<Integer> idsClone = new ArrayList<>(ids);

		idsClone.forEach(id -> {
			if (id == 995) {
				prices.put(id, 1);
			} else {
				prices.put(id, getAverageBuyOffer(id));
			}
		});

		idsClone.removeAll(prices.keySet());

		if (idsClone.size() == 0) {
			return prices;
		}

		return prices;
	}

	
} 