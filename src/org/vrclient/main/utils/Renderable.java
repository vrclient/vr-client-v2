package org.vrclient.main.utils;

import java.awt.*;

/**
 * @author Tyler Sedlar
 * @since 4/6/15
 */
public interface Renderable {

    void render(Graphics2D g);
}
