package org.vrclient.main.utils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.parser.FieldHook;

/**
 * Created by VR on 7/29/2014.
 */
public class NetUtils {
	/**
	 * Static Method read source code of specific url
	 *
	 * @param url
	 * @return source page of the link
	 * @throws IOException 
	 * @throws MalformedURLException 
	 */
	
	public static String[] readPage(String url) {
		url = url.replaceAll(" ", "%20");
		ArrayList<String> lines = new ArrayList<String>();
		try {
			final URLConnection con = createURLConnection(url);
			final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				lines.add(line);
			}
			in.close();
		} catch (Exception e) {
			System.out.println("Error reading page!");
		}
		return lines.toArray(new String[lines.size()]);
	}
	public static String readUrl(String urlString) throws IOException
	  {
	    BufferedReader reader = null;
	    URLConnection uc = null;
	    try
	    {
	    	
	      URL url = new URL(urlString);	  
	      uc = url.openConnection();
	      uc.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
	      uc.connect();	  
	      reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
	      StringBuffer buffer = new StringBuffer();
	      
	      char[] chars = new char['?'];
	      int read;
	      while ((read = reader.read(chars)) != -1) {
	        buffer.append(chars, 0, read);
	      }
	      return buffer.toString();
	    }
	    catch (Exception ex)
	    {
	      System.out.println(ex);
	    }
	    finally
	    {
	      if (reader != null) {
	        try
	        {
	          reader.close();
	        }
	        catch (IOException ex)
	        {
	          System.out.println(ex);
	        }
	      }
	    }
	    return "";
	  }
	/**
	 * Static method for creating a URLConnection
	 *
	 * @param url the complete web URL for the file
	 * @return The
	 */
	public static URLConnection createURLConnection(String url) {
		try {
			final URL address = new URL(url);
			final URLConnection connection = address.openConnection();
			connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:31.0) Gecko/20100101 Firefox/31.0");
			connection.setConnectTimeout(5000);
			connection.setRequestProperty("Content-Type", "image/png");
			return connection;
		} catch (IOException ex) {
			System.out.println("Error creating connection!");
			ex.printStackTrace();
		}
		return null;
	}

	/**
	 * Handy static method for downloading and saving files.
	 *
	 * @param url      the complete web URL for the file
	 * @param location the complete destination including extension for the file
	 * @return true if the file exists in the location, false if an exception is thrown or the file does not exist
	 */
	public static boolean downloadFile(String url, String location) {
		try {

			final URLConnection connection = createURLConnection(url);

			final int contentLength = connection.getContentLength();
			final File destination = new File(location);

			if (destination.exists()) {
				final URLConnection savedFileConnection = destination.toURI().toURL().openConnection();
				if (savedFileConnection.getContentLength() == contentLength) {
					return true;
				}
			} else {
				final File parent = destination.getParentFile();
				if (parent != null && !parent.exists()) parent.mkdirs();
			}

			final ReadableByteChannel rbc = Channels.newChannel(connection.getInputStream());

			final FileOutputStream fos = new FileOutputStream(destination);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();

		} catch (IOException exception) {
			exception.printStackTrace();
			return false;
		}

		System.out.println(url + "->" + location);
		return new File(location).exists();
	}

public static Map<String, String> txtReader(String name) {
	Hashtable<String, String> store = new Hashtable<>();
	 try
     {
     File file = new File(Utilities.getContentDirectory()+""+name);
     System.out.println(file.getAbsolutePath());
     if (!file.exists()) {
    	 System.out.println("File "+name+" doesn't exist!");
    	 return null;
     }
     BufferedReader reader = new BufferedReader(new FileReader(file));
     String line = "";
     while((line = reader.readLine()) != null)
         {
    	 store.put(line.split(" ")[0], line.split(" ")[1]);
         //oldtext += line + "\r\n";
     }
     reader.close();
     return store;  
 }
 catch (IOException ioe)
     {
     ioe.printStackTrace();
 }
	return null;
}

}
