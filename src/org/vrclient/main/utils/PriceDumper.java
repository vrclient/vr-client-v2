package org.vrclient.main.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.vrclient.main.Constants;

public class PriceDumper {
public static void main(String[] args) {
	dumpPrices();
}
public static void dumpPrices() {
	new Thread() {
		public void run() {
			try {
			DateFormat df = new SimpleDateFormat("MM-dd-yy");
			Date dateobj = new Date();
			String mydate = df.format(dateobj);
			for(int i = 21000; i > 18000; i--) {
				int price = PriceLookup.getPrice(i);
				if(price > 0)
				FileUtils.save(Constants.SETTING_PATH, "prices "+mydate+".txt", ""+i, ""+price);
				System.out.println(i+": "+price);
			}
			System.out.println("Done!");
			} catch (Exception e) {
			e.printStackTrace();
			}
		}
	}.start();
}
}
