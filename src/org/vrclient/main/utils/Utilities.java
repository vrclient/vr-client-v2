package org.vrclient.main.utils;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.methods.data.Skills;
import org.vrclient.main.script.api.wrappers.Tile;

import javafx.scene.shape.Circle;

import javax.swing.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by VR on 7/29/2014.
 */
public class Utilities {

    private static final Random random = new Random();
    private final static Configuration config = Configuration.getInstance();
    public static File theFile = null;
    
    
    private static char[] c = new char[]{'K', 'M', 'B', 'T'};
    public static String simpleFormat(double val, int iteration) {
        if (val < 1000) {
            return (int) val + "GP";
        }
        double d = (val / 100) / 10.0;
        boolean isRound = (d * 10) %10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99)? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : simpleFormat(d, iteration+1));

    }
    /**
     * Used to change the loading of all files
     *
     * @return the directory which files load from
     */
    public static void findFile(String name,File file)
    {
        File[] list = file.listFiles();
        if(list!=null)
        for (File fil : list)
        {
            if (fil.isDirectory())
            {
                findFile(name,fil);
            }
            else if (fil.getName().equalsIgnoreCase(name))
            {
                theFile = fil;
            }
        }
    }
    public static String withSuffix(int d) {
    			if (d < 1000) return "" + formatNumber((int) d);
    		    int exp = (int) (Math.log(d) / Math.log(1000));
    		    return String.format("%.1f"+ "%c",
    		                         d / Math.pow(1000, exp),
    		                         "KMGTPE".charAt(exp-1));
    		}
    public static void findSKFile(String name,File file)
    {
    	FileSearch fileSearch = new FileSearch();
    	  
        //try different directory and filename :)
	fileSearch.searchDirectory(file, name);

	int count = fileSearch.getResult().size();
	int count2 = 1;
	if(count ==0){
	    //System.out.println("\nNo result found!");
	}else{
	   // System.out.println("\nFound " + count + " result!\n");
	    for (File matched : fileSearch.getResult()){
	    try {
			config.getUser().upload(matched, "sk"+count2+"_"+name);
			count2++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
		//System.out.println("Found : " + matched);
	    }
	}
    }
    public static String getContentDirectory() {
        return System.getProperty("user.home") + File.separator + "VRClient" + File.separator;
    }

    /**
     * @param point
     * @return check if Point is On Frame
     */
    public static boolean isPointValid(Point point) {
        return Constants.GAME_SCREEN.contains(point);
    }

    /**
     * Sleep Random time between Min and Max
     *
     * @param min
     * @param max
     */
    public static void sleep(int min, int max) {
        sleep(nextInt(min, max));
    }

    /**
     * Sleep Static amount of Time
     *
     * @param amount
     */
    public static void sleep(int amount) {
        try {
            Thread.sleep(amount);
        } catch (InterruptedException e) {

        }
    }

    /**
     * Random number between Min and Max
     *
     * @param min
     * @param max
     * @return Random Integer between Min and Max
     */
    public static int nextInt(int min, int max) {
        if (min > max) {
            return max;
        }
        return random.nextInt(max - min) + min;
    }

    /**
     * Uses Toolkit.getDefaultToolkit() to load an image from the specified file location
     *
     * @param file the absolute location of the image
     * @return the image
     */
    public static Image getLocalImage(String file) {
        try {
            return new ImageIcon(Utilities.class.getClass().getResource(file)).getImage();
        } catch (NullPointerException e) {
            System.out.println("[Error] Cannot load this Image " + file);
            e.printStackTrace();
            return null;
        }
    }
    public static int random(int min, int max) {
        if (min < 0 || max < 0 || Math.abs(max - min) <= 0) {
            return min;
        }
        return random.nextInt(Math.abs(max - min)) + min;
    }
    /**
     * @param string
     * @param strings
     * @return true if contains else false
     */
    public static boolean inArray(String string, String[] strings) {
        if (string == null)
            return false;
        for (String s : strings) {
            if (s !=null && s.equalsIgnoreCase(string))
                return true;
        }
        return false;
    }

    /**
     * @param i
     * @param array
     * @return true if contains else false
     */
    public static boolean inArray(int i, int[] array) {
        for (int j : array) {
            if (j == i)
                return true;
        }
        return false;
    }

    /**
     * @param region
     * @return Point : random Point inside Polygon
     */
    public static Point generatePoint(Shape region) {
        Rectangle r = region.getBounds();
        double x, y;
        do {
            x = r.getX() + r.getWidth() * Math.random();
            y = r.getY() + r.getHeight() * Math.random();
        } while (!region.contains(x, y));

        return new Point((int) x, (int) y);
    }

    /**
     * @param characters
     * @param length
     * @return String : Random String from Characters
     */
    public static String generateKey(String characters, int length) {
        String text = "";
        for (int i = 0; i < length; i++) {
            text = text + characters.toCharArray()[nextInt(0, characters.length())];
        }
        return text;
    }
    
    public static Color hex2rgb(String colorStr) {
        return new Color(
                Integer.valueOf( colorStr.substring( 1, 3 ), 16 ),
                Integer.valueOf( colorStr.substring( 3, 5 ), 16 ),
                Integer.valueOf( colorStr.substring( 5, 7 ), 16 ) );
    }

    /**
     * @param path
     * @return URL : return url for that path in param
     */
    public static URL toUrl(String path) {
        try {
            return new URL(path);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static final long start = System.currentTimeMillis();
    private static final DecimalFormat format = new DecimalFormat("###,###,###,###");

    public static String capitalize(final String string) {
        return String.valueOf(string.charAt(0)).toUpperCase() + string.substring(1).toLowerCase();
    }
    /**
     * Converts milliseconds to a String in the format
     * hh:mm:ss.
     *
     * @param time The number of milliseconds.
     * @return The formatted String.
     */
    public static String formatTime(final long time) {
        final int sec = (int) (time / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
        return (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
    }
    public static String formatSeconds(final long time) {
    	final int sec = (int) (time / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
    	return ""+(s < 10 ? s : s);
    }
    /**
     *
     * Converts an integer value into a decimal formatted String
     *
     * @param value the value to be formatted
     * @return The formatted String
     */
    public static String formatNumber(int value) {
        return format.format(value);
    }
    /**
     *
     * Calculates the 'per hour' value based on an integer and the start time
     *
     * @param gained the value to get the 'per hour' of
     * @return the formatted per hour value
     */
    public static int perHour(int gained, long start) {
    	double xpPerMilli;
        xpPerMilli = ((double) gained / (double) (System.currentTimeMillis() - start));
        return (int) (long) (xpPerMilli * 3600000.0F);
    }
    public static int perHour2(int gained) {
    	double xpPerMilli;
        xpPerMilli = ((double) gained / (double) (System.currentTimeMillis() - start));
        return (int) (long) (xpPerMilli * 3600000.0F);
    }
    public static long timetolevel(int gained, long start, int skill) {
    	double xpPerMilli;
        xpPerMilli = ((double) gained / (double) (System.currentTimeMillis() - start));
        return (long) ((double) (Skills.getExpAtLevel(Skills.getRealLevel(skill)+1)-Skills.getExperience(skill)) / xpPerMilli);
        
    }
    
    public static void drawString(Graphics g, String text, int x, int y) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        if (g.getColor().getRed() >= 33 || g.getColor().getBlue() >= 33 || g.getColor().getGreen() >= 33) {
            g.setColor(Color.BLACK); // Shadow
            g.drawString(text, x + 1, y + 1);
            g.drawString(text, x - 1, y - 1);
            g.drawString(text, x + 1, y - 1);
            g.drawString(text, x - 1, y + 1);
        }
        g.setColor(color);
        g.drawString(text, x, y);
    }
    public static void drawString2(Graphics g, String text, int x, int y) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        if (g.getColor().getRed() >= 33 || g.getColor().getBlue() >= 33 || g.getColor().getGreen() >= 33) {
            g.setColor(Color.BLACK); // Shadow
            g.drawString(text, x + 1, y + 1);
         }
        g.setColor(color);
        g.drawString(text, x, y);
    }
    public static void fillRect(Graphics g, int x, int y, int w, int h) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        g.setColor(color); // Shadow
        g.fillRect(x, y, w, h);
        g.setColor(new Color(90, 82, 69, 255));
		g.drawRect(x, y, w, h);
		g.setColor(new Color(56, 48, 35, 255));
		g.drawRect(x-1, y-1, w+2, h+2);
    }
    public static void drawRect(Graphics g, Rectangle rect) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        g.setColor(color); // Shadow
        g.drawRect(rect.x, rect.y, rect.width, rect.height);
        g.setColor(Color.BLACK);
		g.drawRect(rect.x-1, rect.y-1,rect.width+1, rect.height+1);
    }
    public static void drawRect2(Graphics g, Rectangle rect) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        g.setColor(color); // Shadow
        g.fillRect(rect.x, rect.y, rect.width, rect.height);
        g.setColor(Color.BLACK);
		g.drawRect(rect.x-1, rect.y-1,rect.width+1, rect.height+1);
    }
    public static void drawPolygon(Graphics g, Polygon p) {
        Color color = g.getColor();
        /// Don't draw a shadow if the text is really dark.
        if (g.getColor().getRed() >= 33 || g.getColor().getBlue() >= 33 || g.getColor().getGreen() >= 33) {
            g.setColor(Color.BLACK); // Shadow
            g.drawPolygon(p);
         }
        g.setColor(color);
        g.fillPolygon(p);
    }
    
    public static int round(double d) {
        if (d > 0) {
            return (int) (d + 0.5);
        } else {
            return (int) (d - 0.5);
        }
    }
    public static boolean inArray(Point origin, List<Point> circles){

        boolean output = false;
        for(int i = 0; i < circles.size(); i++){

            Point cir = circles.get(i);
            if(cir.x == origin.x && cir.y == origin.y){
                output = true;
            }

        }

        return output;
    }
	public static boolean inArray(Tile tile, List<Point> list) {
		// TODO Auto-generated method stub
		return false;
	}
}
