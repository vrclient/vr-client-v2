package org.vrclient.main.ui;

import org.vrclient.main.utils.Utilities;

import javax.swing.*;

import java.awt.*;

/**
 * Created by VR on 7/29/2014.
 */
public class Button extends JButton {


    /**
	 * 
	 */
	private static final long serialVersionUID = 8254130076836375897L;
	private Image buttonIcon;
    private Image buttonRollOverIcon;
    private Image buttonDisabledIcon;
    private Image buttonHoveredIcon;

    public Button(String imageName) {
        setButtonIcon(imageName);
        setIcon(new ImageIcon(buttonIcon));
        setMinimumSize(new Dimension(16, 16));
        setBorder(null);
        setBorderPainted(false);
        setFocusPainted(false);
        setOpaque(false);
        setContentAreaFilled(false);
    }

    public void setButtonIcon(String imageName) {
        buttonIcon = Utilities.getLocalImage("/resources/" + imageName);
        setIcon(new ImageIcon(buttonIcon));
    }

    public void setButtonHoverIcon(String imageName) {
        buttonHoveredIcon = Utilities.getLocalImage("/resources/" + imageName);
        setRolloverIcon(new ImageIcon(buttonHoveredIcon));
    }

    public void setButtonDisabledIcon(String imageName) {
        buttonDisabledIcon = Utilities.getLocalImage("/resources/" + imageName);
        setDisabledIcon(new ImageIcon(buttonDisabledIcon));
    }

    public void setButtonRollOverIcon(String imageName) {
        buttonRollOverIcon = Utilities.getLocalImage("/resources/" + imageName);
        setRolloverIcon(new ImageIcon(buttonRollOverIcon));
    }

}
