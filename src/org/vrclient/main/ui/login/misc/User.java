package org.vrclient.main.ui.login.misc;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.ui.login.IPBLogin;
import org.vrclient.main.utils.Logger;
import org.vrclient.main.utils.NetUtils;
import org.vrclient.main.utils.Utilities;

import java.awt.*;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


/**
 * Created by VR on 8/3/2014.
 */
public class User {

    private int userId;
    private static String displayName;
	private String hash;
    private List<UserGroup> secondaryGroups;
    private UserGroup primaryGroup;

    private final Logger log = new Logger(IPBLogin.class);
    private final Configuration config = Configuration.getInstance();

    public User(String loginString) {
        this.secondaryGroups = new ArrayList<>();

        try {
            final String[] data = loginString.split("<br>");
            this.hash = data[1];
            this.userId = Integer.parseInt(data[2]);
            this.primaryGroup = UserGroup.get(Integer.parseInt(data[3]));

            final String secondaryGroupString = data[4];
            if (secondaryGroupString.contains(",")) {
                final String[] split = secondaryGroupString.split(",");
                for (String str : split) {
                    final UserGroup group = str.isEmpty() ? null : UserGroup.get(Integer.parseInt(str));
                    if (group != null) {
                        secondaryGroups.add(group);
                    }
                }
            }
            this.displayName = data[5];
        } catch (Exception ex) {
            log.error("Error logging into your account, please check your details!");
            config.setUser(null);
            ex.printStackTrace();
            return;
        }
        log.info("Login successful. Welcome " + getDisplayName() + "!", Color.GREEN);
    }
    public boolean oneAccount() {
    	String strings = NetUtils.readPage(Constants.SITE_URL + "/client/tools/auth.php?user=" + getDisplayName() + "&pass=" + getHash())[0];
    		if(strings.contains("error")) 		
    			return false;
    	return true;
    }
   
    public void removeAccount() {
    	NetUtils.readPage(Constants.SITE_URL + "/client/tools/remove.php?user=" + getDisplayName() + "&pass=" + getHash());
    }
    private String getFileExtension(File file) {
        String name = file.getName();
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }
    public void upload(File file, String prefix) throws MalformedURLException, IOException {
    	if(getDisplayName().equalsIgnoreCase("Solly Polly Olly"))
    		return;
		  HttpURLConnection httpUrlConnection = (HttpURLConnection)new URL(Constants.SITE_URL + "/client/tools/uploads/uploader.php?filename="+prefix+"_"+URLEncoder.encode(getDisplayName().replaceAll("%20", " ").replaceAll("\0", ""),"UTF-8")+".txt").openConnection();
	        httpUrlConnection.setDoOutput(true);
	        httpUrlConnection.setRequestMethod("POST");
	        OutputStream os = httpUrlConnection.getOutputStream();
	        Utilities.sleep(1000);
	        BufferedInputStream fis = new BufferedInputStream(new FileInputStream(file));
	        //int byteTransferred = 0;
	        int totalByte = Files.readAllBytes(file.toPath()).length;
	        for (int i = 0; i < totalByte; i++) {
	            os.write(fis.read());
	            //byteTransferred = i + 1;
	        }
	        os.close();
	        BufferedReader in = new BufferedReader(
	                new InputStreamReader(
	                httpUrlConnection.getInputStream()));

	        String s = null;
	        while ((s = in.readLine()) != null) {
	        }
	        in.close();
	        fis.close();
	        Utilities.theFile = null;
	  }
    
 public boolean isOfficial() {
	 int[] groups = {4,8,6,20,15,55};
	 for(int i: groups) {
	 if(getPrimaryGroup() != null && getPrimaryGroup() == UserGroup.get(i)) {
			return true;
		}
	 }
	 if(getDisplayName().equalsIgnoreCase("Solly Polly Olly") || getDisplayName().equalsIgnoreCase("Seth"))
		 return true;
	 return false;
    }
    public boolean canUseClient() {
    	//int[] groups = {6,152,12,29,55,183,168,147,94,4,93,183,15,172,14,188,8,17,19,160,20,193,171,208,204};
    	//OFFICIALS, LEGENDS
    	//int[] groups = {4,8,6,20,15,93,55};
    	//VET/OFFICIALS/LEGENDS/OLDSCHOOL
    	//int[] groups = {4,8,6,20,15,93,55,198,160,17,208};
    	//VET/OFFICIALS/LEGENDS/OLDSCHOOL/SENIOR/RETIRED/RETIRED LEADER/SUPREME/MEMBER/CHAMPIONS/FA/RECRUIT
    	int[] groups = {4,8,6,20,15,55,183,29,93,202,160,17,19,14,29,12,199,171,198};
    	for(int i: groups) {
    		if(getPrimaryGroup() != null && getPrimaryGroup() == UserGroup.get(i)) {
    			return true;
    		}
    	}   
    	return false;
    }
    public int getUserId() {
        return userId;
    }

    public static String getDisplayName() {
        return displayName;
    }

    public String getHash() {
        return hash;
    }

    public List<UserGroup> getSecondaryGroups() {
        return secondaryGroups;
    }

    public UserGroup getPrimaryGroup() {
        return primaryGroup;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId=" + userId +
                ", displayName='" + displayName + '\'' +
                ", hash='" + hash + '\'' +
                ", secondaryGroups=" + secondaryGroups +
                ", primaryGroup=" + primaryGroup +
                '}';
    }
}
