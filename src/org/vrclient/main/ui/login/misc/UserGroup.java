package org.vrclient.main.ui.login.misc;

/**
 * Created by VR on 8/3/2014.
 */
public enum UserGroup {
	
    Leader(4),Officer(8),Advisor(6),Warlord(20),Pk_Leader(55),Council(15),Champion(183),MVP(29),Legend(93),Immortal(202),Veteran(160),OldSchool(17),Senior(19),Member(14),Applicant(12),Founder(198),Retired(171), Recruit(199);

    private int id;

    private UserGroup(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public static UserGroup get(final int id) {
        for (UserGroup group : values()) {
            if (group.getId() == id) {
                return group;
            }
        }
        return null;
    }

}