/*
 * Created by JFormDesigner on Tue Mar 01 20:25:17 CST 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.ui.ColorPickersniper;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Logger;
import org.vrclient.main.utils.Utilities;

/**
 * @author Brainrain
 */
public class SniperPanel extends JPanel {
	private static final String[] args = null;
	private final Logger log = new Logger(getClass());
	private final Configuration config = Configuration.getInstance();
	
	public SniperPanel() {
		initComponents();
		
	}
	
	public ArrayList<String> enemyCallers() {
		ArrayList<String> names = new ArrayList<String>();
		if(sniper1 != null && sniperCheckbox1.isSelected() && !names.contains(sniper1.getText().toLowerCase())) 
			names.add(sniper1.getText().toLowerCase().toLowerCase());	
		if(sniper2 != null && sniperCheckbox2.isSelected() && !names.contains(sniper2.getText().toLowerCase())) 
			names.add(sniper2.getText().toLowerCase().toLowerCase());	
		if(sniper3 != null && sniperCheckbox3.isSelected() && !names.contains(sniper3.getText().toLowerCase())) 
			names.add(sniper3.getText().toLowerCase().toLowerCase());	
		if(sniper4 != null && sniperCheckbox4.isSelected() && !names.contains(sniper4.getText().toLowerCase())) 
			names.add(sniper4.getText().toLowerCase().toLowerCase());	
		if(sniper5 != null && sniperCheckbox5.isSelected() && !names.contains(sniper5.getText().toLowerCase())) 
			names.add(sniper5.getText().toLowerCase().toLowerCase());	
		if(sniper6 != null && sniperCheckbox6.isSelected() && !names.contains(sniper6.getText().toLowerCase())) 
			names.add(sniper6.getText().toLowerCase().toLowerCase());	
		if(sniper7 != null && sniperCheckbox7.isSelected() && !names.contains(sniper7.getText().toLowerCase())) 
			names.add(sniper7.getText().toLowerCase().toLowerCase());	
		if(sniper8 != null && sniperCheckbox8.isSelected() && !names.contains(sniper8.getText().toLowerCase())) 
			names.add(sniper8.getText().toLowerCase().toLowerCase());	
		return names;
	}
	
	private void toggleButton1ActionPerformed(ActionEvent e) {
		if(toggleButton1.isSelected()) {
			toggleButton1.setText("Enabled");
			toggleButton1.setBackground(new Color(76, 207, 75));
		} else {
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
		}
			
		config.drawEnemyLeaders(toggleButton1.isSelected());
		log.info(config.drawEnemyLeaders() ? "Enabled drawing enemy fall in leaders." : "Disabled drawing enemy fall in leaders.");
	}

	private void showSniperSettingsActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		panel4.setVisible(true);
	}

	

	private void saveSniperSettingsActionPerformed(ActionEvent e) {
		panel4.setVisible(false);
		panel1.setVisible(true);
	}

	private void clearSniperListActionPerformed(ActionEvent e) {
		sniper1.setText("");
		sniperCheckbox1.setSelected(false);
		sniper2.setText("");
		sniperCheckbox2.setSelected(false);
		sniper3.setText("");
		sniperCheckbox3.setSelected(false);
		sniper4.setText("");
		sniperCheckbox4.setSelected(false);
		sniper5.setText("");
		sniperCheckbox5.setSelected(false);
		sniper6.setText("");
		sniperCheckbox6.setSelected(false);
		sniper7.setText("");
		sniperCheckbox7.setSelected(false);
		sniper8.setText("");
		sniperCheckbox8.setSelected(false);
		
	}

	private void button1ActionPerformed(ActionEvent e) {
		ColorPickersniper.main(args);
	}

	private void button6ActionPerformed(ActionEvent e) {
		
		JFileChooser fileChooser = new JFileChooser();
		File file2 = new File(Constants.SNIPER_PATH);
		if (file2.exists()) {
		fileChooser.setCurrentDirectory(file2);
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			  String file = fileChooser.getSelectedFile().getName();
			  
			  
		    	  sniper1.setText(FileUtils.loadsnipers(file, "sniper1"));
		    	  sniper2.setText(FileUtils.loadsnipers(file, "sniper2"));
		    	  sniper3.setText(FileUtils.loadsnipers(file, "sniper3"));
		    	  sniper4.setText(FileUtils.loadsnipers(file, "sniper4"));
		    	  sniper5.setText(FileUtils.loadsnipers(file, "sniper5"));
		    	  sniper6.setText(FileUtils.loadsnipers(file, "sniper6"));
		    	  sniper7.setText(FileUtils.loadsnipers(file, "sniper7"));
		    	  sniper8.setText(FileUtils.loadsnipers(file, "sniper8"));
		      
			}  
	    } else {
	    	JOptionPane.showMessageDialog(this, "You have no list saved.");
		}
	}

	private void button7ActionPerformed(ActionEvent e) {
		String file = JOptionPane.showInputDialog(this, "Name of list? (ex. DF,DI,ROT)");
		
		File f = new File(Constants.SNIPER_PATH + File.separator + file + ".txt");
		
		if(file.isEmpty())
		{
			JOptionPane.showMessageDialog(this, "You need to name your list.");
		} else if(f.exists()) {
			int reply = JOptionPane.showConfirmDialog(null, "A list with this name already exists, OVERWRITE this list?", "File Already exists", JOptionPane.YES_NO_OPTION);
	        if (reply == JOptionPane.YES_OPTION) {
	        	FileUtils.savesnipers(file + ".txt", "sniper1", sniper1.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper2", sniper2.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper3", sniper3.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper4", sniper4.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper5", sniper5.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper6", sniper6.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper7", sniper7.getText());
	  		  FileUtils.savesnipers(file+ ".txt", "sniper8", sniper8.getText());
	        } else {
		           
	        }
		} else {
		  FileUtils.savesnipers(file + ".txt", "sniper1", sniper1.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper2", sniper2.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper3", sniper3.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper4", sniper4.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper5", sniper5.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper6", sniper6.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper7", sniper7.getText());
		  FileUtils.savesnipers(file+ ".txt", "sniper8", sniper8.getText());
		  
		}
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panel1 = new JPanel();
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel2 = new JPanel();
		toggleButton1 = new JToggleButton();
		button5 = new JButton();
		panel3 = new JPanel();
		sniperCheckbox1 = new JCheckBox();
		sniper1 = new JTextField();
		sniperCheckbox2 = new JCheckBox();
		sniper2 = new JTextField();
		sniperCheckbox3 = new JCheckBox();
		sniper3 = new JTextField();
		sniperCheckbox4 = new JCheckBox();
		sniper4 = new JTextField();
		sniperCheckbox5 = new JCheckBox();
		sniper5 = new JTextField();
		sniperCheckbox6 = new JCheckBox();
		sniper6 = new JTextField();
		sniperCheckbox7 = new JCheckBox();
		sniper7 = new JTextField();
		sniperCheckbox8 = new JCheckBox();
		sniper8 = new JTextField();
		button4 = new JButton();
		button7 = new JButton();
		button6 = new JButton();
		panel4 = new JPanel();
		label1 = new JLabel();
		separator1 = new JSeparator();
		label4 = new JLabel();
		button1 = new JButton();
		button3 = new JButton();

		//======== this ========
		setBackground(new Color(58, 58, 58));
		setForeground(new Color(70, 70, 70));
		setBorder(null);
		setRequestFocusEnabled(false);
		setMaximumSize(new Dimension(220, 32767));
		setMinimumSize(new Dimension(220, 397));
		setPreferredSize(new Dimension(220, 435));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		//======== panel1 ========
		{
			panel1.setBorder(null);
			panel1.setRequestFocusEnabled(false);
			panel1.setMinimumSize(new Dimension(220, 397));
			panel1.setPreferredSize(new Dimension(220, 435));
			panel1.setMaximumSize(new Dimension(220, 32767));
			panel1.setLayout(new FormLayout(
				"left:default:grow",
				"default, $lgap, fill:20dlu, $lgap, fill:195dlu:grow"));

			//======== panelheader ========
			{
				panelheader.setPreferredSize(new Dimension(56, 25));
				panelheader.setBackground(Color.black);
				panelheader.setMaximumSize(new Dimension(210, 2147483647));
				panelheader.setLayout(new FormLayout(
					"center:default:grow",
					"fill:default:grow"));

				//---- skilllabel ----
				skilllabel.setText("Sniper List");
				skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
				skilllabel.setForeground(Color.white);
				panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.FILL));
			}
			panel1.add(panelheader, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

			//======== panel2 ========
			{
				panel2.setBackground(new Color(0, 0, 0, 0));
				panel2.setBorder(null);
				panel2.setAlignmentX(0.0F);
				panel2.setAlignmentY(0.0F);
				panel2.setMaximumSize(new Dimension(220, 32767));
				panel2.setMinimumSize(new Dimension(220, 397));
				panel2.setPreferredSize(new Dimension(220, 435));
				panel2.setForeground(new Color(0, 0, 0, 0));
				panel2.setOpaque(false);
				panel2.setLayout(new FormLayout(
					"center:default:grow, $lcgap, 11dlu:grow, $lcgap, default:grow",
					"fill:0dlu, fill:12dlu:grow"));
				((FormLayout)panel2.getLayout()).setColumnGroups(new int[][] {{1, 5}});

				//---- toggleButton1 ----
				toggleButton1.setText("Disabled");
				toggleButton1.setBackground(new Color(194, 44, 44));
				toggleButton1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						toggleButton1ActionPerformed(e);
					}
				});
				panel2.add(toggleButton1, CC.xy(1, 2, CC.RIGHT, CC.CENTER));

				//---- button5 ----
				button5.setText("Settings");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showSniperSettingsActionPerformed(e);
					}
				});
				panel2.add(button5, CC.xy(5, 2, CC.LEFT, CC.CENTER));
			}
			panel1.add(panel2, CC.xy(1, 3));

			//======== panel3 ========
			{
				panel3.setAlignmentY(5.0F);
				panel3.setAlignmentX(5.0F);
				panel3.setMaximumSize(new Dimension(220, 32767));
				panel3.setMinimumSize(new Dimension(220, 397));
				panel3.setPreferredSize(new Dimension(220, 435));
				panel3.setLayout(new FormLayout(
					"center:13dlu, 2*(default:grow, $lcgap)",
					"8*(default, $lgap), 15dlu, $lgap, default"));
				((FormLayout)panel3.getLayout()).setRowGroups(new int[][] {{1, 3, 5, 7, 9, 11, 13, 15}});

				//---- sniperCheckbox1 ----
				sniperCheckbox1.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox1.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox1.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox1.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox1.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox1.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox1.setFocusPainted(false);
				sniperCheckbox1.setFocusable(false);
				sniperCheckbox1.setBorder(null);
				panel3.add(sniperCheckbox1, CC.xy(1, 1, CC.CENTER, CC.CENTER));

				//---- sniper1 ----
				sniper1.setBackground(Color.black);
				sniper1.setSelectedTextColor(Color.black);
				sniper1.setCaretColor(Color.lightGray);
				sniper1.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper1.setForeground(new Color(204, 204, 204));
				sniper1.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper1.setSelectionStart(9);
				sniper1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
				sniper1.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper1, CC.xywh(2, 1, 3, 1));

				//---- sniperCheckbox2 ----
				sniperCheckbox2.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox2.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox2.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox2.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox2.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox2.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox2.setFocusPainted(false);
				sniperCheckbox2.setFocusable(false);
				sniperCheckbox2.setBorder(null);
				panel3.add(sniperCheckbox2, CC.xy(1, 3, CC.CENTER, CC.CENTER));

				//---- sniper2 ----
				sniper2.setBackground(Color.black);
				sniper2.setSelectedTextColor(Color.black);
				sniper2.setCaretColor(Color.lightGray);
				sniper2.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper2.setForeground(new Color(204, 204, 204));
				sniper2.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper2.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper2, CC.xywh(2, 3, 3, 1));

				//---- sniperCheckbox3 ----
				sniperCheckbox3.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox3.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox3.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox3.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox3.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox3.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox3.setFocusPainted(false);
				sniperCheckbox3.setFocusable(false);
				sniperCheckbox3.setBorder(null);
				panel3.add(sniperCheckbox3, CC.xy(1, 5, CC.CENTER, CC.CENTER));

				//---- sniper3 ----
				sniper3.setBackground(Color.black);
				sniper3.setSelectedTextColor(Color.black);
				sniper3.setCaretColor(Color.lightGray);
				sniper3.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper3.setForeground(new Color(204, 204, 204));
				sniper3.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper3.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper3, CC.xywh(2, 5, 3, 1));

				//---- sniperCheckbox4 ----
				sniperCheckbox4.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox4.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox4.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox4.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox4.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox4.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox4.setFocusPainted(false);
				sniperCheckbox4.setFocusable(false);
				sniperCheckbox4.setBorder(null);
				panel3.add(sniperCheckbox4, CC.xy(1, 7, CC.CENTER, CC.CENTER));

				//---- sniper4 ----
				sniper4.setBackground(Color.black);
				sniper4.setSelectedTextColor(Color.black);
				sniper4.setCaretColor(Color.lightGray);
				sniper4.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper4.setForeground(new Color(204, 204, 204));
				sniper4.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper4.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper4, CC.xywh(2, 7, 3, 1));

				//---- sniperCheckbox5 ----
				sniperCheckbox5.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox5.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox5.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox5.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox5.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox5.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox5.setFocusPainted(false);
				sniperCheckbox5.setFocusable(false);
				sniperCheckbox5.setBorder(null);
				panel3.add(sniperCheckbox5, CC.xy(1, 9, CC.CENTER, CC.CENTER));

				//---- sniper5 ----
				sniper5.setBackground(Color.black);
				sniper5.setSelectedTextColor(Color.black);
				sniper5.setCaretColor(Color.lightGray);
				sniper5.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper5.setForeground(new Color(204, 204, 204));
				sniper5.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper5.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper5, CC.xywh(2, 9, 3, 1));

				//---- sniperCheckbox6 ----
				sniperCheckbox6.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox6.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox6.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox6.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox6.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox6.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox6.setFocusPainted(false);
				sniperCheckbox6.setFocusable(false);
				sniperCheckbox6.setBorder(null);
				panel3.add(sniperCheckbox6, CC.xy(1, 11, CC.CENTER, CC.CENTER));

				//---- sniper6 ----
				sniper6.setBackground(Color.black);
				sniper6.setSelectedTextColor(Color.black);
				sniper6.setCaretColor(Color.lightGray);
				sniper6.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper6.setForeground(new Color(204, 204, 204));
				sniper6.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper6.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper6, CC.xywh(2, 11, 3, 1));

				//---- sniperCheckbox7 ----
				sniperCheckbox7.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox7.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox7.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox7.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox7.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox7.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox7.setFocusPainted(false);
				sniperCheckbox7.setFocusable(false);
				sniperCheckbox7.setBorder(null);
				panel3.add(sniperCheckbox7, CC.xy(1, 13, CC.CENTER, CC.CENTER));

				//---- sniper7 ----
				sniper7.setBackground(Color.black);
				sniper7.setSelectedTextColor(Color.black);
				sniper7.setCaretColor(Color.lightGray);
				sniper7.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper7.setForeground(new Color(204, 204, 204));
				sniper7.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper7.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper7, CC.xywh(2, 13, 3, 1));

				//---- sniperCheckbox8 ----
				sniperCheckbox8.setHorizontalAlignment(SwingConstants.CENTER);
				sniperCheckbox8.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				sniperCheckbox8.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				sniperCheckbox8.setMaximumSize(new Dimension(21, 22));
				sniperCheckbox8.setMinimumSize(new Dimension(21, 22));
				sniperCheckbox8.setPreferredSize(new Dimension(21, 22));
				sniperCheckbox8.setFocusPainted(false);
				sniperCheckbox8.setFocusable(false);
				sniperCheckbox8.setBorder(null);
				panel3.add(sniperCheckbox8, CC.xy(1, 15, CC.CENTER, CC.CENTER));

				//---- sniper8 ----
				sniper8.setBackground(Color.black);
				sniper8.setSelectedTextColor(Color.black);
				sniper8.setCaretColor(Color.lightGray);
				sniper8.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				sniper8.setForeground(new Color(204, 204, 204));
				sniper8.setFont(new Font("Tahoma", Font.BOLD, 11));
				sniper8.setHorizontalAlignment(SwingConstants.LEFT);
				panel3.add(sniper8, CC.xywh(2, 15, 3, 1));

				//---- button4 ----
				button4.setText("Clear List");
				button4.setToolTipText("Uses a list set by Officials");
				button4.setBackground(new Color(102, 51, 255));
				button4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						clearSniperListActionPerformed(e);
					}
				});
				panel3.add(button4, CC.xywh(1, 17, 4, 1, CC.FILL, CC.DEFAULT));

				//---- button7 ----
				button7.setText("Save");
				button7.setToolTipText("Uses a list set by Officials");
				button7.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button7ActionPerformed(e);
					}
				});
				panel3.add(button7, CC.xywh(1, 19, 2, 1, CC.CENTER, CC.DEFAULT));

				//---- button6 ----
				button6.setText("Load");
				button6.setToolTipText("Uses a list set by Officials");
				button6.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button6ActionPerformed(e);
					}
				});
				panel3.add(button6, CC.xy(4, 19, CC.CENTER, CC.DEFAULT));
			}
			panel1.add(panel3, CC.xy(1, 5));
		}
		add(panel1);

		//======== panel4 ========
		{
			panel4.setMaximumSize(new Dimension(220, 32767));
			panel4.setMinimumSize(new Dimension(220, 397));
			panel4.setPreferredSize(new Dimension(220, 435));
			panel4.setVisible(false);
			panel4.setLayout(new FormLayout(
				"center:pref:grow, center:default:grow",
				"22dlu, default, $pgap, 2*(default, $lgap), default"));

			//---- label1 ----
			label1.setText("Sniper Settings");
			label1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			panel4.add(label1, CC.xywh(1, 1, 2, 1, CC.CENTER, CC.DEFAULT));
			panel4.add(separator1, CC.xywh(1, 2, 2, 1));

			//---- label4 ----
			label4.setText("Sniper Color");
			panel4.add(label4, CC.xy(1, 4, CC.RIGHT, CC.DEFAULT));

			//---- button1 ----
			button1.setText("Color");
			button1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button1ActionPerformed(e);
				}
			});
			panel4.add(button1, CC.xy(2, 4));

			//---- button3 ----
			button3.setText("Save & Close");
			button3.setBackground(new Color(51, 102, 255));
			button3.setMaximumSize(new Dimension(125, 23));
			button3.setMinimumSize(new Dimension(125, 23));
			button3.setPreferredSize(new Dimension(150, 23));
			button3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveSniperSettingsActionPerformed(e);
				}
			});
			panel4.add(button3, CC.xywh(1, 8, 2, 1, CC.CENTER, CC.DEFAULT));
		}
		add(panel4);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	      if (forumInfo.exists()) {
	    	  
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "sniper2color", "FF0000"); 
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color")));
	    	  } else {
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "sniper2color")));
	    	  }
	    	  
	    	  
	      }
	
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panel1;
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel2;
	private JToggleButton toggleButton1;
	private JButton button5;
	private JPanel panel3;
	private JCheckBox sniperCheckbox1;
	private JTextField sniper1;
	private JCheckBox sniperCheckbox2;
	private JTextField sniper2;
	private JCheckBox sniperCheckbox3;
	private JTextField sniper3;
	private JCheckBox sniperCheckbox4;
	private JTextField sniper4;
	private JCheckBox sniperCheckbox5;
	private JTextField sniper5;
	private JCheckBox sniperCheckbox6;
	private JTextField sniper6;
	private JCheckBox sniperCheckbox7;
	private JTextField sniper7;
	private JCheckBox sniperCheckbox8;
	private JTextField sniper8;
	private JButton button4;
	private JButton button7;
	private JButton button6;
	private JPanel panel4;
	private JLabel label1;
	private JSeparator separator1;
	private JLabel label4;
	public JButton button1;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables

	
}
