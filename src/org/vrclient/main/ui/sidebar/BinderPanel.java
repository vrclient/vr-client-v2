/*
 * Created by JFormDesigner on Tue Mar 01 20:28:23 CST 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.ui.ColorPickerbinder;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Logger;
import org.vrclient.main.utils.Utilities;

/**
 * @author Brainrain
 */
public class BinderPanel extends JPanel {
	
	private static final String[] args = null;
	private final Logger log = new Logger(getClass());
	private final Configuration config = Configuration.getInstance();
	public DefaultListModel<String> listModel = new DefaultListModel<String>();
	
	public BinderPanel() {
		initComponents();
		list1 = new JList<String>(listModel);
	}
	public void addBinder(final String name) {
		SwingUtilities.invokeLater (new Runnable () { 
			   public void run () { 
		scrollPane1.setViewportView(list1);			
		if(!listModel.contains(name))
		listModel.addElement(name);
			   	}
			   });
	}
	public void clearBinders() {
		listModel.clear();
	}
	
	private void toggleButton1ActionPerformed(ActionEvent e) {
		if(toggleButton1.isSelected()) {
			toggleButton1.setText("Enabled");
			toggleButton1.setBackground(new Color(76, 207, 75));
		} else {
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
		}
		
		config.drawBinder(toggleButton1.isSelected());
		log.info(config.drawBinder() ? "Enabled drawing binders." : "Disabled Drawing binders.");
	}

	private void showBinderSettingsActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		panel4.setVisible(true);
	}

	private void clearBinderListActionPerformed(ActionEvent e) {
		config.getBotFrame().getBinderPanel().clearBinders();
		list1.removeAll();
	}

	private void saveBinderSettingsActionPerformed(ActionEvent e) {
		
		panel4.setVisible(false);
		panel1.setVisible(true);
	}

	private void checkBox1ActionPerformed(ActionEvent e) {
		config.writeBinderNames(checkBox1.isSelected());
		log.info(config.writeBinderNames() ? "Enabled writing binders." : "Disabled writing binders.");
	}

	private void button1ActionPerformed(ActionEvent e) {
		ColorPickerbinder.main(args);
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panel1 = new JPanel();
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel2 = new JPanel();
		toggleButton1 = new JToggleButton();
		button5 = new JButton();
		panel3 = new JPanel();
		scrollPane1 = new JScrollPane();
		list1 = new JList();
		button4 = new JButton();
		panel4 = new JPanel();
		label1 = new JLabel();
		separator1 = new JSeparator();
		label4 = new JLabel();
		button1 = new JButton();
		label3 = new JLabel();
		checkBox1 = new JCheckBox();
		button3 = new JButton();

		//======== this ========
		setBackground(new Color(58, 58, 58));
		setForeground(new Color(70, 70, 70));
		setBorder(null);
		setRequestFocusEnabled(false);
		setMaximumSize(new Dimension(210, 32767));
		setPreferredSize(new Dimension(220, 383));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		//======== panel1 ========
		{
			panel1.setBorder(null);
			panel1.setRequestFocusEnabled(false);
			panel1.setMinimumSize(new Dimension(210, 420));
			panel1.setMaximumSize(new Dimension(220, 32767));
			panel1.setLayout(new FormLayout(
				"default:grow",
				"default, $lgap, fill:20dlu, $lgap, fill:195dlu:grow"));

			//======== panelheader ========
			{
				panelheader.setPreferredSize(new Dimension(56, 25));
				panelheader.setBackground(Color.black);
				panelheader.setMaximumSize(new Dimension(210, 2147483647));
				panelheader.setLayout(new FormLayout(
					"center:default:grow",
					"fill:default:grow"));

				//---- skilllabel ----
				skilllabel.setText("Binder List");
				skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
				skilllabel.setForeground(Color.white);
				panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.FILL));
			}
			panel1.add(panelheader, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

			//======== panel2 ========
			{
				panel2.setBackground(new Color(0, 0, 0, 0));
				panel2.setBorder(null);
				panel2.setAlignmentX(0.0F);
				panel2.setAlignmentY(0.0F);
				panel2.setMaximumSize(new Dimension(210, 20));
				panel2.setMinimumSize(new Dimension(210, 20));
				panel2.setPreferredSize(new Dimension(210, 20));
				panel2.setForeground(new Color(0, 0, 0, 0));
				panel2.setOpaque(false);
				panel2.setLayout(new FormLayout(
					"center:default:grow, $lcgap, 11dlu:grow, $lcgap, center:default:grow",
					"fill:0dlu, fill:default:grow"));
				((FormLayout)panel2.getLayout()).setColumnGroups(new int[][] {{1, 5}});

				//---- toggleButton1 ----
				toggleButton1.setText("Disabled");
				toggleButton1.setBackground(new Color(194, 44, 44));
				toggleButton1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						toggleButton1ActionPerformed(e);
					}
				});
				panel2.add(toggleButton1, CC.xy(1, 2, CC.RIGHT, CC.CENTER));

				//---- button5 ----
				button5.setText("Settings");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showBinderSettingsActionPerformed(e);
					}
				});
				panel2.add(button5, CC.xy(5, 2, CC.LEFT, CC.CENTER));
			}
			panel1.add(panel2, CC.xy(1, 3));

			//======== panel3 ========
			{
				panel3.setAlignmentY(5.0F);
				panel3.setAlignmentX(5.0F);
				panel3.setMaximumSize(new Dimension(210, 32767));
				panel3.setMinimumSize(new Dimension(210, 420));
				panel3.setPreferredSize(new Dimension(220, 440));
				panel3.setLayout(new FormLayout(
					"$lcgap, default:grow, $lcgap",
					"149dlu, $lgap, 15dlu, default"));

				//======== scrollPane1 ========
				{
					scrollPane1.setPreferredSize(new Dimension(150, 200));
					scrollPane1.setMaximumSize(new Dimension(220, 32767));

					//---- list1 ----
					list1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					list1.setPreferredSize(new Dimension(35, 2));
					list1.setMaximumSize(new Dimension(125, 300));
					list1.setMinimumSize(new Dimension(125, 200));
					list1.setFocusCycleRoot(true);
					scrollPane1.setViewportView(list1);
				}
				panel3.add(scrollPane1, CC.xy(2, 1, CC.FILL, CC.FILL));

				//---- button4 ----
				button4.setText("Clear list");
				button4.setToolTipText("Uses a list set by Officials");
				button4.setBackground(new Color(102, 51, 255));
				button4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						clearBinderListActionPerformed(e);
					}
				});
				panel3.add(button4, CC.xy(2, 4, CC.CENTER, CC.DEFAULT));
			}
			panel1.add(panel3, CC.xy(1, 5));
		}
		add(panel1);

		//======== panel4 ========
		{
			panel4.setMaximumSize(new Dimension(220, 32767));
			panel4.setMinimumSize(new Dimension(220, 420));
			panel4.setPreferredSize(new Dimension(220, 440));
			panel4.setVisible(false);
			panel4.setLayout(new FormLayout(
				"2*(default:grow)",
				"22dlu, default, $pgap, 3*(default, $lgap), default"));
			((FormLayout)panel4.getLayout()).setRowGroups(new int[][] {{4, 6}});

			//---- label1 ----
			label1.setText("Binder Settings");
			label1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			panel4.add(label1, CC.xywh(1, 1, 2, 1, CC.CENTER, CC.DEFAULT));
			panel4.add(separator1, CC.xywh(1, 2, 2, 1));

			//---- label4 ----
			label4.setText("Binder Color:");
			panel4.add(label4, CC.xy(1, 4, CC.RIGHT, CC.DEFAULT));

			//---- button1 ----
			button1.setText("Color");
			button1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button1ActionPerformed(e);
				}
			});
			panel4.add(button1, CC.xy(2, 4, CC.CENTER, CC.DEFAULT));

			//---- label3 ----
			label3.setText("Display Binder Name:");
			panel4.add(label3, CC.xy(1, 6, CC.RIGHT, CC.DEFAULT));

			//---- checkBox1 ----
			checkBox1.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox1.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox1ActionPerformed(e);
				}
			});
			panel4.add(checkBox1, CC.xy(2, 6, CC.CENTER, CC.DEFAULT));

			//---- button3 ----
			button3.setText("Save & Close");
			button3.setBackground(new Color(51, 102, 255));
			button3.setMaximumSize(new Dimension(125, 23));
			button3.setMinimumSize(new Dimension(125, 23));
			button3.setPreferredSize(new Dimension(150, 23));
			button3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveBinderSettingsActionPerformed(e);
				}
			});
			panel4.add(button3, CC.xywh(1, 10, 2, 1, CC.CENTER, CC.DEFAULT));
		}
		add(panel4);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		  File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	      if (forumInfo.exists()) {
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") == null){
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "binder2color", "15ff00");
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color")));
	    	  } else {
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color")));
	    		  //comboBox2.setSelectedIndex(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "calleroppcolor")));
	    	  }
	    	  
	    	  
	      }
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panel1;
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel2;
	private JToggleButton toggleButton1;
	private JButton button5;
	private JPanel panel3;
	private JScrollPane scrollPane1;
	private JList list1;
	private JButton button4;
	private JPanel panel4;
	private JLabel label1;
	private JSeparator separator1;
	private JLabel label4;
	public JButton button1;
	private JLabel label3;
	private JCheckBox checkBox1;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
