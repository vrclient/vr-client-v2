/*
 * Created by JFormDesigner on Fri Apr 08 19:57:17 PDT 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.swing.*;

import org.vrclient.main.Constants;
import org.vrclient.main.script.api.interfaces.Filter;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.script.api.wrappers.definitions.ItemDefinition;
import org.vrclient.main.utils.NetUtils;
import org.vrclient.main.utils.Utilities;

/**
 * @author Sol Crystal
 */
public class SplitterUI extends JPanel {
	public SplitterUI() {
		initComponents();
	}
	public String withSuffix(int d) {
		if (d < 1000) return "" + Utilities.formatNumber((int) d);
	    int exp = (int) (Math.log(d) / Math.log(1000));
	    return String.format("%.1f"+ "%c",
	                         d / Math.pow(1000, exp),
	                         "KMGTPE".charAt(exp-1));
	}
	public int getID() {
		if(textField1.getText().contains("ags")) {
			return 11802;			
		} else if(textField1.getText().contains("hasta")) {
			return 11889;
		} else if(textField1.getText().contains("spear")) {
			return 11824;
		} else if(textField1.getText().contains("bludgeon") || textField1.getText().contains("bludge")) {
			return 13263;
		} else if(textField1.getText().contains("sgs")) {
			return 11806;
		} else if(textField1.getText().contains("bgs")) {
			return 11804;
		} else if(textField1.getText().contains("dfs")) {
			return 11284;
		} else if(textField1.getText().contains("dwh") || textField1.getText().contains("warhammer")) {
			return 13576;
		} else if (textField1.getText().contains("stod")) {
			return 11791;
		} else if(textField1.getText().contains("zgs")) {
			return 11808;
		} else if(textField1.getText().contains("serp")) {
			return 12929;
		} else if(textField1.getText().contains("ballista")) {
			return 19481;
		} else if(textField1.getText().contains("acb")) {
			return 11785;
		} else {
			return -1;
		}
	}
	private Filter<Player> filter = new Filter<Player>() {
        @Override
        public boolean accept(Player player) {
            return player != null 
            		&& player.isValid()
            		&& Menu.clanList().contains(player.getName());
        }
    };
	private void button1ActionPerformed(ActionEvent e) {
		if(!Game.isLoggedIn())
			return;
		if(textField1.getText().isEmpty()) {
			JOptionPane.showMessageDialog(this, "You need to name the item you pked!", null,
			        JOptionPane.WARNING_MESSAGE);
			return;
		}
		String workingDirectory = System.getProperty("user.dir");
		DateFormat df = new SimpleDateFormat("MM-dd-yy");
		Date dateobj = new Date();
		String mydate = df.format(dateobj);
		File file = new File(workingDirectory+"/"+textField1.getText()+" "+mydate+".txt");
		ArrayList<String> lineList = new ArrayList<String>();
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			int count = 0;
			if(Menu.clanList().isEmpty() || Menu.clanList() == null) {
				for(Player player: Players.getAll()) {
					lineList.add(player.getName());
					count++;
				}
			} else {
			for(Player player: Players.getAll(filter)) {
			lineList.add(player.getName());		
			count++;
				}
			}
			Collections.sort(lineList);
			for(String name : lineList) {
				bw.write(name);
				bw.newLine();
			}
			bw.newLine();
			bw.newLine();
			bw.write("Total People: "+count);
			bw.newLine();
			bw.write("Pked Item: "+textField1.getText());
			bw.newLine();
			String price = NetUtils.readPage("http://api.rsbuddy.com/grandExchange?a=guidePrice&i="+getID())[0];
			String value = price.substring(price.indexOf(":"), price.indexOf(",")).split(":")[1];
			int value2 = Integer.parseInt(value);
			bw.write("Suggested Selling Price: "+withSuffix(value2));
			bw.newLine();
			bw.write("Approximate Split: "+withSuffix(value2/count));
			bw.newLine();
			if(!textField2.getText().isEmpty())
			bw.write("Picture for this item: "+textField2.getText());
			bw.close();
			JOptionPane.showMessageDialog(this, "Done! The file was generated at "+workingDirectory, null,
			        JOptionPane.INFORMATION_MESSAGE);
			if(Desktop.isDesktopSupported())
			Desktop.getDesktop().open(file);
			textField1.setText(null);
			textField2.setText(null);
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		label1 = new JLabel();
		label2 = new JLabel();
		textField1 = new JTextField();
		label3 = new JLabel();
		textField2 = new JTextField();
		button1 = new JButton();
		label4 = new JLabel();
		label5 = new JLabel();
		label6 = new JLabel();

		//======== this ========
		setPreferredSize(new Dimension(200, 400));
		setMinimumSize(new Dimension(200, 400));
		setInheritsPopupMenu(true);
		setMaximumSize(new Dimension(200, 400));

		//---- label1 ----
		label1.setText("Splitter");
		label1.setHorizontalAlignment(SwingConstants.CENTER);
		label1.setFont(new Font("Tahoma", Font.BOLD, 12));

		//---- label2 ----
		label2.setText("What is the Pked Item?");
		label2.setFont(new Font("Tahoma", Font.BOLD, 10));

		//---- label3 ----
		label3.setText("Insert picture link (optional)");
		label3.setFont(new Font("Segoe UI", Font.BOLD, 12));

		//---- button1 ----
		button1.setText("Generate File");
		button1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				button1ActionPerformed(e);
			}
		});

		//---- label4 ----
		label4.setText("(ags, hasta, dwh, dfs, bludgeon, sgs,");
		label4.setFont(new Font("Tahoma", Font.PLAIN, 10));

		//---- label5 ----
		label5.setText("stod, spear, bgs, zgs, serp, ballista, acb)");
		label5.setFont(new Font("Tahoma", Font.PLAIN, 10));

		//---- label6 ----
		label6.setText("(put only one major item)");
		label6.setFont(new Font("Tahoma", Font.PLAIN, 10));

		GroupLayout layout = new GroupLayout(this);
		setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup()
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addComponent(label1, GroupLayout.PREFERRED_SIZE, 53, GroupLayout.PREFERRED_SIZE)
					.addGap(70, 70, 70))
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createParallelGroup()
					.addComponent(textField2, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addComponent(textField1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
					.addComponent(label3, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup()
						.addComponent(label5, GroupLayout.Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup()
							.addGap(24, 24, 24)
							.addGroup(layout.createParallelGroup()
								.addComponent(label4)
								.addGroup(layout.createSequentialGroup()
									.addGap(12, 12, 12)
									.addGroup(layout.createParallelGroup()
										.addComponent(label2)
										.addComponent(label6, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))))))
					.addGap(10, 10, 10))
				.addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
					.addComponent(button1)
					.addGap(54, 54, 54))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup()
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(label1)
					.addGap(8, 8, 8)
					.addComponent(label2)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(label6)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(label4)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(label5)
					.addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
					.addComponent(textField1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(label3)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(textField2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18, 18, 18)
					.addComponent(button1)
					.addContainerGap(176, Short.MAX_VALUE))
		);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JLabel label1;
	private JLabel label2;
	private JTextField textField1;
	private JLabel label3;
	private JTextField textField2;
	private JButton button1;
	private JLabel label4;
	private JLabel label5;
	private JLabel label6;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
