/*
 * Created by JFormDesigner on Tue Mar 01 19:49:35 CST 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.*;
import javax.swing.border.*;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
//import com.jformdesigner.model.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.methods.data.Menu;
import org.vrclient.main.script.api.methods.interactive.Players;
import org.vrclient.main.script.api.wrappers.Player;
import org.vrclient.main.ui.ColorPickercaller;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Logger;
import org.vrclient.main.utils.NetUtils;
import org.vrclient.main.utils.Utilities;


/**
 * @author Brainrain
 */
public class CallerPanel extends JPanel {
	private static final long serialVersionUID = 6298080221708150682L;
	private static final String[] args = null;
	private final Configuration config = Configuration.getInstance();
	private final Logger log = new Logger(getClass());
	public ListMultimap<String, String> rsns = ArrayListMultimap.create();
	public CallerPanel() {
		initComponents();
		rsns.clear();
		callerName1.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[0]);
		callerName2.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[1]);
		callerName3.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[2]);
		callerName4.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[3]);
		callerName5.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[4]);
		callerName6.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[5]);
		callerName7.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[6]);
		callerName8.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[7]);
		String[] caller1 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=1");
		String[] caller2 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=2");
		String[] caller3 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=3");
		String[] caller4 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=4");
		String[] caller5 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=5");
		String[] caller6 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=6");
		String[] caller7 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=7");
		String[] caller8 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=8");
		for(int i = 0; i < caller1.length; i++) {
		rsns.put(callerName1.getText(), caller1[i]);
		}
		for(int i = 0; i < caller2.length; i++) {
			rsns.put(callerName2.getText(), caller2[i]);
			}
		for(int i = 0; i < caller3.length; i++) {
			rsns.put(callerName3.getText(), caller3[i]);
			}
		for(int i = 0; i < caller4.length; i++) {
			rsns.put(callerName4.getText(), caller4[i]);
			}
		for(int i = 0; i < caller5.length; i++) {
			rsns.put(callerName5.getText(), caller5[i]);
			}
		for(int i = 0; i < caller6.length; i++) {
			rsns.put(callerName6.getText(), caller6[i]);
			}
		for(int i = 0; i < caller7.length; i++) {
			rsns.put(callerName7.getText(), caller7[i]);
			}
		for(int i = 0; i < caller8.length; i++) {
			rsns.put(callerName8.getText(), caller8[i]);
			}
	}
	public String getRSN(String name) {
		if(rsns.containsKey(name)) {
			return rsns.get(name).toString();
		}
		return name;
	}
	public ArrayList<String> getRSNs(String name) {
		ArrayList<String> names = new ArrayList<String>();
		for(Entry<String, String> entry : rsns.entries()) {
			if(entry.getKey().equalsIgnoreCase(name.toLowerCase())) {
				names.add(entry.getValue().toLowerCase());
			}
		}
		return names;
	}
	public ArrayList<String> callers() {
		ArrayList<String> names = new ArrayList<String>();
		if(!callerName1.getText().isEmpty() && callerBox1.isSelected()) {
			if(rsns.containsKey(callerName1.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName1.getText().toLowerCase()));
			} else {
				names.add(callerName1.getText().toLowerCase());
			}
		}
		if(!callerName2.getText().isEmpty() && callerBox2.isSelected()) {
			if(rsns.containsKey(callerName2.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName2.getText().toLowerCase()));
			} else {
				names.add(callerName2.getText().toLowerCase());
			}
		}
		if(!callerName3.getText().isEmpty() && callerBox3.isSelected()) {
			if(rsns.containsKey(callerName3.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName3.getText().toLowerCase()));
			} else {
				names.add(callerName3.getText().toLowerCase());
			}
		}
		if(!callerName4.getText().isEmpty() && callerBox4.isSelected()) {
			if(rsns.containsKey(callerName4.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName4.getText().toLowerCase()));
			} else {
				names.add(callerName4.getText().toLowerCase());
			}
		}
		if(!callerName5.getText().isEmpty() && callerBox5.isSelected()) {
			if(rsns.containsKey(callerName5.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName5.getText().toLowerCase()));
			} else {
				names.add(callerName5.getText().toLowerCase());
			}
		}
		if(!callerName6.getText().isEmpty() && callerBox6.isSelected()) {
			if(rsns.containsKey(callerName6.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName6.getText().toLowerCase()));
			} else {
				names.add(callerName6.getText().toLowerCase());
			}
		}
		if(!callerName7.getText().isEmpty() && callerBox7.isSelected()) {
			if(rsns.containsKey(callerName7.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName7.getText().toLowerCase()));
			} else {
				names.add(callerName7.getText().toLowerCase());
			}
		}
		if(!callerName8.getText().isEmpty() && callerBox8.isSelected()) {
			if(rsns.containsKey(callerName8.getText().toLowerCase())) {
				names.addAll(getRSNs(callerName8.getText().toLowerCase()));
			} else {
				names.add(callerName8.getText().toLowerCase());
			}
		}
		return names;
	}
	private void getCallersActionPerformed(ActionEvent e) {
			rsns.clear();
			new Thread(new Runnable() {
				public void run() {
					callerName1.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[0]);
					callerName2.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[1]);
					callerName3.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[2]);
					callerName4.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[3]);
					callerName5.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[4]);
					callerName6.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[5]);
					callerName7.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[6]);
					callerName8.setText(NetUtils.readPage(Constants.SITE_URL + "/client/tools/callernames.txt")[7]);
					String[] caller1 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=1");
					String[] caller2 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=2");
					String[] caller3 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=3");
					String[] caller4 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=4");
					String[] caller5 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=5");
					String[] caller6 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=6");
					String[] caller7 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=7");
					String[] caller8 = NetUtils.readPage(Constants.SITE_URL + "/client/tools/callers.php?34craf&id=8");
					for(int i = 0; i < caller1.length; i++) {
					rsns.put(callerName1.getText(), caller1[i]);
					}
					for(int i = 0; i < caller2.length; i++) {
						rsns.put(callerName2.getText(), caller2[i]);
						}
					for(int i = 0; i < caller3.length; i++) {
						rsns.put(callerName3.getText(), caller3[i]);
						}
					for(int i = 0; i < caller4.length; i++) {
						rsns.put(callerName4.getText(), caller4[i]);
						}
					for(int i = 0; i < caller5.length; i++) {
						rsns.put(callerName5.getText(), caller5[i]);
						}
					for(int i = 0; i < caller6.length; i++) {
						rsns.put(callerName6.getText(), caller6[i]);
						}
					for(int i = 0; i < caller7.length; i++) {
						rsns.put(callerName7.getText(), caller7[i]);
						}
					for(int i = 0; i < caller8.length; i++) {
						rsns.put(callerName8.getText(), caller8[i]);
						}
				}
			}).start();
			
		}
	
	private void showCallerSettingsActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		panel4.setVisible(true);
	}

	private void saveCallerSettingsActionPerformed(ActionEvent e) {
		if(checkBox1.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawTilecaller1", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawTilecaller1", "false");
		}
		panel4.setVisible(false);
		panel1.setVisible(true);
	}

	

	private void toggleButton1ActionPerformed(ActionEvent e) {
		if(toggleButton1.isSelected()) {
			toggleButton1.setText("Enabled");
			toggleButton1.setBackground(new Color(76, 207, 75));
		} else {
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
		}
			
		config.drawCallerInfo(toggleButton1.isSelected());
		log.info(config.drawCallerInfo() ? "Enabled Caller names." : "Disabled Caller names.");
		config.drawMenuPlayers(toggleButton1.isSelected());
		log.info(config.drawMenuPlayers() ? "Enabled draw Pile names." : "Disabled draw Pile names.");
	}

	private void clearCallerListActionPerformed(ActionEvent e) {
		callerName1.setText("");
		callerBox1.setSelected(false);
		callerName2.setText("");
		callerBox2.setSelected(false);
		callerName3.setText("");
		callerBox3.setSelected(false);
		callerName4.setText("");
		callerBox4.setSelected(false);
		callerName5.setText("");
		callerBox5.setSelected(false);
		callerName6.setText("");
		callerBox6.setSelected(false);
		callerName7.setText("");
		callerBox7.setSelected(false);
		callerName8.setText("");
		callerBox8.setSelected(false);
		callerName1.setVisible(true);
		callerName2.setVisible(true);
		callerName3.setVisible(true);
		callerName4.setVisible(true);
		callerName5.setVisible(true);
		callerName6.setVisible(true);
		callerName7.setVisible(true);
		callerName8.setVisible(true);
		callerName1.setBackground(new Color(15, 15, 15));
		callerName2.setBackground(new Color(15, 15, 15));
		callerName3.setBackground(new Color(15, 15, 15));
		callerName4.setBackground(new Color(15, 15, 15));
		callerName5.setBackground(new Color(15, 15, 15));
		callerName6.setBackground(new Color(15, 15, 15));
		callerName7.setBackground(new Color(15, 15, 15));
		callerName8.setBackground(new Color(15, 15, 15));

	}

	private void button1ActionPerformed(ActionEvent e) {
		ColorPickercaller.main(args);
	}

	private void button17ActionPerformed(ActionEvent e) {
		String file = JOptionPane.showInputDialog(this, "Name of list? (ex. DF,DI,ROT)");
		File f = new File(Constants.CALLER_PATH + File.separator + file + ".txt");
		
		if(file.isEmpty())
		{
			JOptionPane.showMessageDialog(this, "You need to name your list.");
		} else if(f.exists()) {
			int reply = JOptionPane.showConfirmDialog(null, "A list with this name already exists, OVERWRITE this list?", "File Already exists", JOptionPane.YES_NO_OPTION);
	        if (reply == JOptionPane.YES_OPTION) {
	        	FileUtils.savecallers(file + ".txt", "caller1", callerName1.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller2", callerName2.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller3", callerName3.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller4", callerName4.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller5", callerName5.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller6", callerName6.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller7", callerName7.getText());
	  		  FileUtils.savecallers(file+ ".txt", "caller8", callerName8.getText());
	        }
	        else {
	           
	        }
		} else {
		  FileUtils.savecallers(file + ".txt", "caller1", callerName1.getText());
		  FileUtils.savecallers(file+ ".txt", "caller2", callerName2.getText());
		  FileUtils.savecallers(file+ ".txt", "caller3", callerName3.getText());
		  FileUtils.savecallers(file+ ".txt", "caller4", callerName4.getText());
		  FileUtils.savecallers(file+ ".txt", "caller5", callerName5.getText());
		  FileUtils.savecallers(file+ ".txt", "caller6", callerName6.getText());
		  FileUtils.savecallers(file+ ".txt", "caller7", callerName7.getText());
		  FileUtils.savecallers(file+ ".txt", "caller8", callerName8.getText());
		}
	}

	private void button6ActionPerformed(ActionEvent e) {
		JFileChooser fileChooser = new JFileChooser();
		File file2 = new File(Constants.CALLER_PATH);
		fileChooser.setCurrentDirectory(file2);
		if (file2.exists()) {
			if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			  String file = fileChooser.getSelectedFile().getName();
			  
			  
		    	  callerName1.setText(FileUtils.loadcallers(file, "caller1"));
		    	  callerName2.setText(FileUtils.loadcallers(file, "caller2"));
		    	  callerName3.setText(FileUtils.loadcallers(file, "caller3"));
		    	  callerName4.setText(FileUtils.loadcallers(file, "caller4"));
		    	  callerName5.setText(FileUtils.loadcallers(file, "caller5"));
		    	  callerName6.setText(FileUtils.loadcallers(file, "caller6"));
		    	  callerName7.setText(FileUtils.loadcallers(file, "caller7"));
		    	  callerName8.setText(FileUtils.loadcallers(file, "caller8"));
		   }
		} else {
			JOptionPane.showMessageDialog(this, "You have no list saved.");
		}
	}

	private void checkBox1ActionPerformed(ActionEvent e) {
		config.drawTilecaller1(checkBox1.isSelected());
	}

	private void callerBox1ActionPerformed(ActionEvent e) {
		if(callerBox1.isSelected()) {
			callerName1.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName1.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox2ActionPerformed(ActionEvent e) {
		if(callerBox2.isSelected()) {
			callerName2.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName2.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox3ActionPerformed(ActionEvent e) {
		if(callerBox3.isSelected()) {
			callerName3.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName3.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox4ActionPerformed(ActionEvent e) {
		if(callerBox4.isSelected()) {
			callerName4.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName4.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox5ActionPerformed(ActionEvent e) {
		if(callerBox5.isSelected()) {
			callerName5.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName5.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox6ActionPerformed(ActionEvent e) {
		if(callerBox6.isSelected()) {
			callerName6.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName6.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox7ActionPerformed(ActionEvent e) {
		if(callerBox7.isSelected()) {
			callerName7.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName7.setBackground(new Color(15, 15, 15));
		}
	}

	private void callerBox8ActionPerformed(ActionEvent e) {
		if(callerBox8.isSelected()) {
			callerName8.setBackground(new Color(114, 199, 108));
		
		} else {
			callerName8.setBackground(new Color(15, 15, 15));
		}
	}

	

	
	
	

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panel1 = new JPanel();
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel2 = new JPanel();
		toggleButton1 = new JToggleButton();
		button5 = new JButton();
		panel3 = new JPanel();
		callerBox1 = new JCheckBox();
		callerName1 = new JTextField();
		callerBox2 = new JCheckBox();
		callerName2 = new JTextField();
		callerBox3 = new JCheckBox();
		callerName3 = new JTextField();
		callerBox4 = new JCheckBox();
		callerName4 = new JTextField();
		callerBox5 = new JCheckBox();
		callerName5 = new JTextField();
		callerBox6 = new JCheckBox();
		callerName6 = new JTextField();
		callerBox7 = new JCheckBox();
		callerName7 = new JTextField();
		callerBox8 = new JCheckBox();
		callerName8 = new JTextField();
		button4 = new JButton();
		button6 = new JButton();
		panel4 = new JPanel();
		label1 = new JLabel();
		separator1 = new JSeparator();
		label5 = new JLabel();
		button1 = new JButton();
		label3 = new JLabel();
		checkBox1 = new JCheckBox();
		button3 = new JButton();

		//======== this ========
		setBackground(new Color(58, 58, 58));
		setForeground(new Color(70, 70, 70));
		setBorder(null);
		setRequestFocusEnabled(false);
		setMinimumSize(new Dimension(220, 420));
		setMaximumSize(new Dimension(220, 32767));
		setPreferredSize(new Dimension(220, 440));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		//======== panel1 ========
		{
			panel1.setBorder(null);
			panel1.setRequestFocusEnabled(false);
			panel1.setMinimumSize(new Dimension(220, 420));
			panel1.setPreferredSize(new Dimension(220, 440));
			panel1.setMaximumSize(new Dimension(220, 32767));
			panel1.setLayout(new FormLayout(
				"default:grow",
				"default, $lgap, fill:20dlu, $lgap, fill:199dlu:grow"));

			//======== panelheader ========
			{
				panelheader.setPreferredSize(new Dimension(56, 25));
				panelheader.setBackground(Color.black);
				panelheader.setMaximumSize(new Dimension(210, 2147483647));
				panelheader.setFocusable(false);
				panelheader.setDoubleBuffered(false);
				panelheader.setLayout(new FormLayout(
					"default, center:default:grow, default",
					"fill:default:grow"));

				//---- skilllabel ----
				skilllabel.setText("Caller List");
				skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
				skilllabel.setForeground(Color.white);
				panelheader.add(skilllabel, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.FILL));
			}
			panel1.add(panelheader, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

			//======== panel2 ========
			{
				panel2.setBackground(Color.black);
				panel2.setBorder(null);
				panel2.setAlignmentX(0.0F);
				panel2.setAlignmentY(0.0F);
				panel2.setMaximumSize(new Dimension(220, 2147483647));
				panel2.setMinimumSize(new Dimension(220, 14));
				panel2.setPreferredSize(new Dimension(220, 14));
				panel2.setOpaque(false);
				panel2.setLayout(new FormLayout(
					"left:default:grow, $lcgap, 11dlu:grow, $lcgap, center:default:grow",
					"fill:0dlu, fill:default:grow"));
				((FormLayout)panel2.getLayout()).setColumnGroups(new int[][] {{1, 5}});

				//---- toggleButton1 ----
				toggleButton1.setText("Enabled");
				toggleButton1.setBackground(new Color(76, 207, 75));
				toggleButton1.setSelected(true);
				toggleButton1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						toggleButton1ActionPerformed(e);
					}
				});
				panel2.add(toggleButton1, CC.xy(1, 2, CC.RIGHT, CC.CENTER));

				//---- button5 ----
				button5.setText("Settings");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						showCallerSettingsActionPerformed(e);
					}
				});
				panel2.add(button5, CC.xy(5, 2, CC.LEFT, CC.CENTER));
			}
			panel1.add(panel2, CC.xy(1, 3));

			//======== panel3 ========
			{
				panel3.setAlignmentY(5.0F);
				panel3.setAlignmentX(5.0F);
				panel3.setMaximumSize(new Dimension(220, 2147483647));
				panel3.setMinimumSize(new Dimension(220, 14));
				panel3.setPreferredSize(new Dimension(220, 14));
				panel3.setLayout(new FormLayout(
					"center:13dlu, right:default:grow, $lcgap, left:default:grow, $lcgap",
					"10*(default, $lgap), default"));
				((FormLayout)panel3.getLayout()).setRowGroups(new int[][] {{1, 3, 5, 7, 9, 11, 13, 15}});

				//---- callerBox1 ----
				callerBox1.setBorder(null);
				callerBox1.setHorizontalAlignment(SwingConstants.CENTER);
				callerBox1.setFocusable(false);
				callerBox1.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox1.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox1.setFocusPainted(false);
				callerBox1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox1ActionPerformed(e);
					}
				});
				panel3.add(callerBox1, CC.xy(1, 1, CC.CENTER, CC.CENTER));

				//---- callerName1 ----
				callerName1.setBackground(new Color(15, 15, 15));
				callerName1.setSelectedTextColor(Color.black);
				callerName1.setCaretColor(Color.lightGray);
				callerName1.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName1.setForeground(new Color(204, 204, 204));
				callerName1.setSelectionStart(9);
				callerName1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
				callerName1.setHorizontalAlignment(SwingConstants.LEFT);
				callerName1.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName1, CC.xywh(2, 1, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox2 ----
				callerBox2.setBorder(null);
				callerBox2.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox2.setFocusPainted(false);
				callerBox2.setFocusable(false);
				callerBox2.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox2.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox2ActionPerformed(e);
					}
				});
				panel3.add(callerBox2, CC.xy(1, 3, CC.CENTER, CC.CENTER));

				//---- callerName2 ----
				callerName2.setBackground(new Color(15, 15, 15));
				callerName2.setSelectedTextColor(Color.black);
				callerName2.setCaretColor(Color.lightGray);
				callerName2.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName2.setForeground(new Color(204, 204, 204));
				callerName2.setHorizontalAlignment(SwingConstants.LEFT);
				callerName2.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName2, CC.xywh(2, 3, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox3 ----
				callerBox3.setBorder(null);
				callerBox3.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox3.setFocusPainted(false);
				callerBox3.setFocusable(false);
				callerBox3.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox3.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox3.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox3ActionPerformed(e);
					}
				});
				panel3.add(callerBox3, CC.xy(1, 5, CC.CENTER, CC.CENTER));

				//---- callerName3 ----
				callerName3.setBackground(new Color(15, 15, 15));
				callerName3.setSelectedTextColor(Color.black);
				callerName3.setCaretColor(Color.lightGray);
				callerName3.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName3.setForeground(new Color(204, 204, 204));
				callerName3.setHorizontalAlignment(SwingConstants.LEFT);
				callerName3.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName3, CC.xywh(2, 5, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox4 ----
				callerBox4.setBorder(null);
				callerBox4.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox4.setFocusPainted(false);
				callerBox4.setFocusable(false);
				callerBox4.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox4.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox4ActionPerformed(e);
					}
				});
				panel3.add(callerBox4, CC.xy(1, 7, CC.CENTER, CC.CENTER));

				//---- callerName4 ----
				callerName4.setBackground(new Color(15, 15, 15));
				callerName4.setSelectedTextColor(Color.black);
				callerName4.setCaretColor(Color.lightGray);
				callerName4.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName4.setForeground(new Color(204, 204, 204));
				callerName4.setHorizontalAlignment(SwingConstants.LEFT);
				callerName4.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName4, CC.xywh(2, 7, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox5 ----
				callerBox5.setBorder(null);
				callerBox5.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox5.setFocusPainted(false);
				callerBox5.setFocusable(false);
				callerBox5.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox5.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox5ActionPerformed(e);
					}
				});
				panel3.add(callerBox5, CC.xy(1, 9, CC.CENTER, CC.CENTER));

				//---- callerName5 ----
				callerName5.setBackground(new Color(15, 15, 15));
				callerName5.setSelectedTextColor(Color.black);
				callerName5.setCaretColor(Color.lightGray);
				callerName5.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName5.setForeground(new Color(204, 204, 204));
				callerName5.setHorizontalAlignment(SwingConstants.LEFT);
				callerName5.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName5, CC.xywh(2, 9, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox6 ----
				callerBox6.setBorder(null);
				callerBox6.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox6.setFocusPainted(false);
				callerBox6.setFocusable(false);
				callerBox6.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox6.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox6.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox6ActionPerformed(e);
					}
				});
				panel3.add(callerBox6, CC.xy(1, 11, CC.CENTER, CC.CENTER));

				//---- callerName6 ----
				callerName6.setBackground(new Color(15, 15, 15));
				callerName6.setSelectedTextColor(Color.black);
				callerName6.setCaretColor(Color.lightGray);
				callerName6.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName6.setForeground(new Color(204, 204, 204));
				callerName6.setHorizontalAlignment(SwingConstants.LEFT);
				callerName6.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName6, CC.xywh(2, 11, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox7 ----
				callerBox7.setBorder(null);
				callerBox7.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox7.setFocusPainted(false);
				callerBox7.setFocusable(false);
				callerBox7.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox7.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox7.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox7ActionPerformed(e);
					}
				});
				panel3.add(callerBox7, CC.xy(1, 13, CC.CENTER, CC.CENTER));

				//---- callerName7 ----
				callerName7.setBackground(new Color(15, 15, 15));
				callerName7.setSelectedTextColor(Color.black);
				callerName7.setCaretColor(Color.lightGray);
				callerName7.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName7.setForeground(new Color(204, 204, 204));
				callerName7.setHorizontalAlignment(SwingConstants.LEFT);
				callerName7.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName7, CC.xywh(2, 13, 3, 1, CC.FILL, CC.FILL));

				//---- callerBox8 ----
				callerBox8.setBorder(null);
				callerBox8.setHorizontalAlignment(SwingConstants.LEFT);
				callerBox8.setFocusPainted(false);
				callerBox8.setFocusable(false);
				callerBox8.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
				callerBox8.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
				callerBox8.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						callerBox8ActionPerformed(e);
					}
				});
				panel3.add(callerBox8, CC.xy(1, 15, CC.CENTER, CC.CENTER));

				//---- callerName8 ----
				callerName8.setBackground(new Color(15, 15, 15));
				callerName8.setSelectedTextColor(Color.black);
				callerName8.setCaretColor(Color.lightGray);
				callerName8.setBorder(new CompoundBorder(
					new LineBorder(new Color(46, 46, 46)),
					new EmptyBorder(3, 10, 3, 10)));
				callerName8.setForeground(new Color(204, 204, 204));
				callerName8.setHorizontalAlignment(SwingConstants.LEFT);
				callerName8.setFont(new Font("Tahoma", Font.BOLD, 11));
				panel3.add(callerName8, CC.xywh(2, 15, 3, 1, CC.FILL, CC.FILL));

				//---- button4 ----
				button4.setText("Update Callers");
				button4.setToolTipText("Uses a list set by Officials");
				button4.setBackground(new Color(102, 51, 255));
				button4.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						getCallersActionPerformed(e);
					}
				});
				panel3.add(button4, CC.xywh(1, 17, 4, 1));

				//---- button6 ----
				button6.setText("Clear List");
				button6.setToolTipText("Uses a list set by Officials");
				button6.setPreferredSize(new Dimension(81, 23));
				button6.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						clearCallerListActionPerformed(e);
					}
				});
				panel3.add(button6, CC.xywh(1, 19, 4, 1, CC.CENTER, CC.DEFAULT));
			}
			panel1.add(panel3, CC.xy(1, 5));
		}
		add(panel1);

		//======== panel4 ========
		{
			panel4.setMinimumSize(new Dimension(220, 269));
			panel4.setMaximumSize(new Dimension(220, 2147483647));
			panel4.setPreferredSize(new Dimension(220, 269));
			panel4.setVisible(false);
			panel4.setLayout(new FormLayout(
				"right:default:grow, default:grow",
				"22dlu, 4*(default, $lgap), default"));
			((FormLayout)panel4.getLayout()).setRowGroups(new int[][] {{4, 6}});

			//---- label1 ----
			label1.setText("Caller Settings");
			label1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			panel4.add(label1, CC.xywh(1, 1, 2, 1, CC.CENTER, CC.DEFAULT));
			panel4.add(separator1, CC.xywh(1, 2, 2, 1));

			//---- label5 ----
			label5.setText("Caller Opponent Color:");
			panel4.add(label5, CC.xy(1, 4, CC.RIGHT, CC.DEFAULT));

			//---- button1 ----
			button1.setBackground(new Color(255, 102, 255));
			button1.setText("Color");
			button1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button1ActionPerformed(e);
				}
			});
			panel4.add(button1, CC.xy(2, 4, CC.CENTER, CC.DEFAULT));

			//---- label3 ----
			label3.setText("Draw Tile?");
			panel4.add(label3, CC.xy(1, 6));

			//---- checkBox1 ----
			checkBox1.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox1.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox1ActionPerformed(e);
				}
			});
			panel4.add(checkBox1, CC.xy(2, 6, CC.CENTER, CC.DEFAULT));

			//---- button3 ----
			button3.setText("Save & Close");
			button3.setBackground(new Color(51, 102, 255));
			button3.setMaximumSize(new Dimension(125, 23));
			button3.setMinimumSize(new Dimension(125, 23));
			button3.setPreferredSize(new Dimension(150, 23));
			button3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveCallerSettingsActionPerformed(e);
				}
			});
			panel4.add(button3, CC.xywh(1, 10, 2, 1, CC.CENTER, CC.DEFAULT));
		}
		add(panel4);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	      if (forumInfo.exists()) {
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "displaycaller") != null){
	    		  //checkBox3.setSelected(Boolean.valueOf(FileUtils.load(Constants.SETTING_FILE_NAME, "displaycaller")));
	    	  }
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "callercolor") != null){
	    		  
	    		  //comboBox1.setSelectedIndex(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "callercolor")));
	    	  }
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color") == null){
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "calleropp2color", "00FFFF");
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color")));
	    	  } else {
	    		  button1.setBackground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "calleropp2color")));
	    		  //comboBox2.setSelectedIndex(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "calleroppcolor")));
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawTilecaller1") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawTilecaller1", "true");
	    	  } else {
	    		  Boolean a = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawTilecaller1"));
	    		  config.drawTilecaller1(a);
	    		  checkBox1.setSelected(a);
	    	  }
	    	  
	      }
	      
	      
		
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panel1;
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel2;
	private JToggleButton toggleButton1;
	private JButton button5;
	private JPanel panel3;
	private JCheckBox callerBox1;
	private JTextField callerName1;
	private JCheckBox callerBox2;
	private JTextField callerName2;
	private JCheckBox callerBox3;
	private JTextField callerName3;
	private JCheckBox callerBox4;
	private JTextField callerName4;
	private JCheckBox callerBox5;
	private JTextField callerName5;
	private JCheckBox callerBox6;
	private JTextField callerName6;
	private JCheckBox callerBox7;
	private JTextField callerName7;
	private JCheckBox callerBox8;
	private JTextField callerName8;
	private JButton button4;
	private JButton button6;
	private JPanel panel4;
	private JLabel label1;
	private JSeparator separator1;
	private JLabel label5;
	public JButton button1;
	private JLabel label3;
	private JCheckBox checkBox1;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}