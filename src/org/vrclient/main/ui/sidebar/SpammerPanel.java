/*
 * Created by JFormDesigner on Tue Mar 15 11:22:45 PDT 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.utils.Logger;

/**
 * @author Sol Crystal
 */
public class SpammerPanel extends JPanel {
	private final Configuration config = Configuration.getInstance();
	private final Logger log = new Logger(getClass());
	public SpammerPanel() {
		initComponents();
	}

	private void toggleButton1ActionPerformed(ActionEvent e) {
		if(toggleButton1.isSelected()) {
			toggleButton1.setText("Enabled");
			toggleButton1.setBackground(new Color(76, 207, 75));
		} else {
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
			config.autoSpammer(toggleButton1.isSelected());
		}
		
		config.autoSpammer(toggleButton1.isSelected());
		log.info(config.autoSpammer() ? "Enabled drawing spammer." : "Disabled Drawing spammer.");
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel1 = new JPanel();
		toggleButton1 = new JToggleButton();
		label2 = new JLabel();
		comboBox3 = new JComboBox<>();
		comboBox1 = new JComboBox<>();
		comboBox2 = new JComboBox<>();

		//======== this ========
		setPreferredSize(new Dimension(220, 440));
		setMaximumSize(new Dimension(220, 32767));
		setLayout(new FormLayout(
			"default:grow",
			"default, $lgap, fill:default:grow, $lgap, default"));

		//======== panelheader ========
		{
			panelheader.setPreferredSize(new Dimension(56, 25));
			panelheader.setBackground(Color.black);
			panelheader.setMaximumSize(new Dimension(210, 2147483647));
			panelheader.setLayout(new FormLayout(
				"center:default:grow",
				"fill:default:grow"));

			//---- skilllabel ----
			skilllabel.setText("AutoSpammer");
			skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
			skilllabel.setForeground(Color.white);
			panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.FILL));
		}
		add(panelheader, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

		//======== panel1 ========
		{
			panel1.setLayout(new FormLayout(
				"$lcgap, default:grow, $lcgap",
				"5*(default, $lgap), default"));

			//---- toggleButton1 ----
			toggleButton1.setText("Disabled");
			toggleButton1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					toggleButton1ActionPerformed(e);
				}
			});
			panel1.add(toggleButton1, CC.xy(2, 1));

			//---- label2 ----
			label2.setText("Effects");
			label2.setFont(new Font("Tahoma", Font.BOLD, 12));
			panel1.add(label2, CC.xy(2, 3, CC.CENTER, CC.DEFAULT));

			//---- comboBox3 ----
			comboBox3.setModel(new DefaultComboBoxModel<>(new String[] {
				"none",
				"flash2"
			}));
			panel1.add(comboBox3, CC.xy(2, 5));

			//---- comboBox1 ----
			comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
				"Spam Partial Name",
				"Spam Full Name"
			}));
			panel1.add(comboBox1, CC.xy(2, 7));

			//---- comboBox2 ----
			comboBox2.setModel(new DefaultComboBoxModel<>(new String[] {
				"none",
				"<html><font color=lightblue>cyan</font></html>",
				"<html><font color=green>green</font></html>",
				"<html><font color=red>red</font></html>",
				"<html><font color=purple>purple</font></html>",
				"<html><font color=white>white</font></html>"
			}));
			comboBox2.setVisible(false);
			panel1.add(comboBox2, CC.xy(2, 9));
		}
		add(panel1, CC.xy(1, 3));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel1;
	private JToggleButton toggleButton1;
	private JLabel label2;
	public JComboBox<String> comboBox3;
	public JComboBox<String> comboBox1;
	private JComboBox<String> comboBox2;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
