/*
 * Created by JFormDesigner on Sat Nov 26 04:17:27 CST 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.time.Duration;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.vrclient.main.Configuration;
import org.vrclient.main.script.api.listeners.ExperienceMonitor;
import org.vrclient.main.utils.Utilities;

import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;

/**
 * @author Seth Troll
 */
public class SkillPanel extends JPanel {
	
	public Configuration config = Configuration.getInstance();
	public int attacklvl,attackgained,attackleft,defenselvl,defensegained,defenseleft,strengthlvl,strengthgained,strengthleft,hplvl,hpgained,hpleft,rangelvl,rangegained,rangeleft,prayerlvl,prayergained,prayerleft,magiclvl,magicgained,magicleft,cooklvl,cookgained,cookleft,wclvl,wcgained,wcleft,fletchlvl,fletchgained,fletchleft,fishlvl,fishgained,fishleft,fmlvl,fmgained,fmleft,craftlvl,craftgained,craftleft,smithlvl,smithgained,smithleft,mininglvl,mininggained,miningleft,herblvl,herbgained,herbleft,agilitylvl,agilitygained,agilityleft,thievelvl,thievegained,thieveleft,slayerlvl,slayergained,slayerleft,farmlvl,farmgained,farmleft,rclvl,rcgained,rcleft,hunterlvl,huntergained,hunterleft,conlvl,congained,conleft,attackxphr,defensexphr,strengthxphr,hpxphr,prayerxphr,rangexphr,magicxphr,cookxphr,wcxphr,fletchxphr,fishxphr,fmxphr,craftxphr,smithxphr,miningxphr,herbxphr,agilityxphr,thievexphr,slayerxphr,farmxphr,rcxphr,hunterxphr,conxphr;
	public double attackprogress,defenseprogress,strengthprogress,hpprogress,prayerprogress,rangeprogress,magicprogress,cookprogress,wcprogress,fletchprogress,fishprogress,fmprogress,craftprogress,smithprogress,miningprogress,herbprogress,agilityprogress,thieveprogress,slayerprogress,farmprogress,rcprogress,hunterprogress,conprogress;
	public long attackttl,defensettl,strengthttl,hpttl,prayerttl,rangettl,magicttl,cookttl,wcttl,fletchttl,fishttl,fmttl,craftttl,smithttl,miningttl,herbttl,agilityttl,thievettl,slayerttl,farmttl,rcttl,hunterttl,conttl;
	String sleft = "Left: ";
	String sgain = "Gained: ";
	public SkillPanel() {
		initComponents();
	}

	private void panel0MouseClicked(MouseEvent e) {
		// TODO add your code here
	}

	private void panel0MouseEntered(MouseEvent e) {
		// TODO add your code here
	}

	private void button1ActionPerformed(ActionEvent e) {
		ExperienceMonitor.stop();
		panel0.setVisible(false);
		panel1.setVisible(false);
		panel2.setVisible(false);
		panel3.setVisible(false);
		panel4.setVisible(false);
		panel5.setVisible(false);
		panel6.setVisible(false);
		panel7.setVisible(false);
		panel8.setVisible(false);
		panel9.setVisible(false);
		panel10.setVisible(false);
		panel11.setVisible(false);
		panel12.setVisible(false);
		panel13.setVisible(false);
		panel14.setVisible(false);
		panel15.setVisible(false);
		panel16.setVisible(false);
		panel17.setVisible(false);
		panel18.setVisible(false);
		panel19.setVisible(false);
		panel20.setVisible(false);
		panel21.setVisible(false);
		panel22.setVisible(false);
		
	}

	private void panel0MouseReleased(MouseEvent e) {
		// TODO add your code here
	}

	

	

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panelheader = new JPanel();
		skilllabel = new JLabel();
		scrollPane1 = new JScrollPane();
		panelskills = new JPanel();
		panel0 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Attack.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = attacklvl;
				double skillProgress = attackprogress;
				int skillGained = attackgained;
				int skillLeft = attackleft;
				long skillttl = attackttl;
				int skillxphr = attackxphr;
				Color skillcolor = new Color(165, 42, 42, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel1 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Defence.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = defenselvl;
				double skillProgress = defenseprogress;
				int skillGained = defensegained;
				int skillLeft = defenseleft;
				long skillttl = defensettl;
				int skillxphr = defensexphr;
				Color skillcolor = new Color(95, 178, 236, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel2 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Strength.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = strengthlvl;
				double skillProgress = strengthprogress;
				int skillGained = strengthgained;
				int skillLeft = strengthleft;
				long skillttl = strengthttl;
				int skillxphr = strengthxphr;
				Color skillcolor = new Color(41, 135, 90, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel3 = 	new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/HP.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = hplvl;
				double skillProgress = hpprogress;
				int skillGained = hpgained;
				int skillLeft = hpleft;
				long skillttl = hpttl;
				int skillxphr = hpxphr;
				Color skillcolor = new Color(255, 88, 66, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel4 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Range.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = rangelvl;
				double skillProgress = rangeprogress;
				int skillGained = rangegained;
				int skillLeft = rangeleft;
				long skillttl = rangettl;
				int skillxphr = rangexphr;
				Color skillcolor = new Color(23, 101, 12, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel5 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Prayer.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = prayerlvl;
				double skillProgress = prayerprogress;
				int skillGained = prayergained;
				int skillLeft = prayerleft;
				long skillttl = prayerttl;
				int skillxphr = prayerxphr;
				Color skillcolor = new Color(255, 255, 255, 225);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel6 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Magic.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = magiclvl;
				double skillProgress = magicprogress;
				int skillGained = magicgained;
				int skillLeft = magicleft;
				long skillttl = magicttl;
				int skillxphr = magicxphr;
				Color skillcolor = new Color(40, 87, 224, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel7 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Cooking.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = cooklvl;
				double skillProgress = cookprogress;
				int skillGained = cookgained;
				int skillLeft = cookleft;
				long skillttl = cookttl;
				int skillxphr = cookxphr;
				Color skillcolor = new Color(140, 34, 159, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel8 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/woodcutting.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = wclvl;
				double skillProgress = wcprogress;
				int skillGained = wcgained;
				int skillLeft = wcleft;
				long skillttl = wcttl;
				int skillxphr = wcxphr;
				Color skillcolor = new Color(39, 137, 43, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel9 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Fletching.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = fletchlvl;
				double skillProgress = fletchprogress;
				int skillGained = fletchgained;
				int skillLeft = fletchleft;
				long skillttl = fletchttl;
				int skillxphr = fletchxphr;
				Color skillcolor = new Color(3, 99, 6, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel10 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Fishing.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = fishlvl;
				double skillProgress = fishprogress;
				int skillGained = fishgained;
				int skillLeft = fishleft;
				long skillttl = fishttl;
				int skillxphr = fishxphr;
				Color skillcolor = new Color(32, 153, 255, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel11 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Firemaking.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = fmlvl;
				double skillProgress = fmprogress;
				int skillGained = fmgained;
				int skillLeft = fmleft;
				long skillttl = fmttl;
				int skillxphr = fmxphr;
				Color skillcolor = new Color(255, 114, 2, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel12 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Crafting.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = craftlvl;
				double skillProgress = craftprogress;
				int skillGained = craftgained;
				int skillLeft = craftleft;
				long skillttl = craftttl;
				int skillxphr = craftxphr;
				Color skillcolor = new Color(88, 46, 13, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};


		panel13 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Smithing.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = smithlvl;
				double skillProgress = smithprogress;
				int skillGained = smithgained;
				int skillLeft = smithleft;
				long skillttl = smithttl;
				int skillxphr = smithxphr;
				Color skillcolor = new Color(72, 72, 72, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel14 = new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Mining.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = mininglvl;
				double skillProgress = miningprogress;
				int skillGained = mininggained;
				int skillLeft = miningleft;
				long skillttl = miningttl;
				int skillxphr = miningxphr;
				Color skillcolor = new Color(46, 46, 46, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel15 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Herblore.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = herblvl;
				double skillProgress = herbprogress;
				int skillGained = herbgained;
				int skillLeft = herbleft;
				long skillttl = herbttl;
				int skillxphr = herbxphr;
				Color skillcolor = new Color(27, 155, 21, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel16 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Agility.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = agilitylvl;
				double skillProgress = agilityprogress;
				int skillGained = agilitygained;
				int skillLeft = agilityleft;
				long skillttl = agilityttl;
				int skillxphr = agilityxphr;
				Color skillcolor = new Color(53, 21, 251, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel17 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Thieving.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = thievelvl;
				double skillProgress = thieveprogress;
				int skillGained = thievegained;
				int skillLeft = thieveleft;
				long skillttl = thievettl;
				int skillxphr = thievexphr;
				Color skillcolor = new Color(156, 21, 251, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel18 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Slayer.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = slayerlvl;
				double skillProgress = slayerprogress;
				int skillGained = slayergained;
				int skillLeft = slayerleft;
				long skillttl = slayerttl;
				int skillxphr = slayerxphr;
				Color skillcolor = new Color(36, 36, 36, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};
		panel19 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Farming.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = farmlvl;
				double skillProgress = farmprogress;
				int skillGained = farmgained;
				int skillLeft = farmleft;
				long skillttl = farmttl;
				int skillxphr = farmxphr;
				Color skillcolor = new Color(56, 182, 76, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel20 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Runecrafting.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = rclvl;
				double skillProgress = rcprogress;
				int skillGained = rcgained;
				int skillLeft = rcleft;
				long skillttl = rcttl;
				int skillxphr = rcxphr;
				Color skillcolor = new Color(255, 105, 46, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel21 = 
			new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Hunter.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = hunterlvl;
				double skillProgress = hunterprogress;
				int skillGained = huntergained;
				int skillLeft = hunterleft;
				long skillttl = hunterttl;
				int skillxphr = hunterxphr;
				Color skillcolor = new Color(113, 73, 22, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};

		panel22 = 	new JPanel(){
		    private static final long serialVersionUID = 1L;
				@Override
				public void paintComponent(final Graphics g){
				super.paintComponent(g);

					BufferedImage image = null;
				try {
					image = ImageIO.read(getClass().getResource("/resources/skills/Construction.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				int skillLevel = conlvl;
				double skillProgress = conprogress;
				int skillGained = congained;
				int skillLeft = conleft;
				long skillttl = conttl;
				int skillxphr = conxphr;
				Color skillcolor = new Color(97, 56, 2, 200);

				g.setFont(new Font("Verdana", Font.PLAIN, 9));
				FontMetrics fm = g.getFontMetrics();
				//Background
				g.setColor(new Color(58, 50, 41, 125));
				g.fillRect(0, 0, panelheader.getWidth()-1, 50);
				g.setColor(new Color(25, 25, 25, 200));
				g.drawRect(0, 0, panelheader.getWidth()-1, 50);

				//image
				int ix = (40 - image.getWidth()) / 2;
		    	int iy = 5;
				g.drawImage(image, ix, iy, null);

				//Level
				int lvlx = (40 - fm.stringWidth(skillLevel+"")) / 2;
				int lvly = ((image.getHeight()-fm.getHeight())/2)+5+fm.getAscent();
				g.setColor(Color.white);
				Utilities.drawString(g,skillLevel+"", lvlx, lvly);

				//
				Utilities.drawString2(g,sgain + Utilities.withSuffix(skillGained),40, 12);

				//
				int lx2 = fm.stringWidth("XP/Hr: 999.9k");
				Utilities.drawString2(g,"XP/Hr: "+Utilities.withSuffix(skillxphr), 210-lx2, 12);

				//
				Utilities.drawString2(g,sleft + Utilities.withSuffix(skillLeft), 40, 26);

				//
				Duration duration = Duration.ofMillis(skillttl);
		        String s = String.format("TTL: "+"%02d:%02d:%02d%n", duration.toHours(),
		                    duration.minusHours(duration.toHours()).toMinutes(),
		                    duration.minusMinutes(duration.toMinutes()).getSeconds());
				int lx4 = fm.stringWidth(s);
				Utilities.drawString2(g, s, 210-lx2, 26);

				//Progress Bar
				g.setColor(new Color(0, 0, 0));
		        g.fillRect(3, 32, panelheader.getWidth()-6, 16);
			    g.setColor(skillcolor);
			    g.fillRect(4, 33, ((int)skillProgress*(panelheader.getWidth()-8))/100, 14);

				//Progress Text

				int px = ((panelheader.getWidth()-6) - fm.stringWidth(skillProgress+"%"))/2;
				int py = ((18 - fm.getHeight()) / 2) + fm.getAscent();
				g.setColor(Color.white);	
				g.drawString(skillProgress + "%", px+3, py+31);
				}
			};


		panel24 = new JPanel();
		panel23 = new JPanel();
		button1 = new JButton();

		//======== this ========
		setMinimumSize(new Dimension(220, 200));
		setPreferredSize(new Dimension(220, 200));
		setOpaque(false);
		setFont(new Font("Tahoma", Font.BOLD, 11));
		setAlignmentX(0.0F);
		setAlignmentY(0.0F);
		setLayout(new FormLayout(
			"default:grow",
			"2*(top:default, $lgap), bottom:default:grow"));

		//======== panelheader ========
		{
			panelheader.setPreferredSize(new Dimension(56, 25));
			panelheader.setBackground(Color.black);
			panelheader.setLayout(new FormLayout(
				"center:default:grow",
				"fill:default:grow"));

			//---- skilllabel ----
			skilllabel.setText("Skill Tracker");
			skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
			skilllabel.setForeground(Color.white);
			panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.DEFAULT));
		}
		add(panelheader, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

		//======== scrollPane1 ========
		{
			scrollPane1.setBorder(null);
			scrollPane1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane1.setPreferredSize(null);

			//======== panelskills ========
			{
				panelskills.setPreferredSize(null);
				panelskills.setLayout(new BoxLayout(panelskills, BoxLayout.Y_AXIS));

				//======== panel0 ========
				{
					panel0.setPreferredSize(new Dimension(210, 52));
					panel0.setBorder(null);
					panel0.setVisible(false);

					GroupLayout panel0Layout = new GroupLayout(panel0);
					panel0.setLayout(panel0Layout);
					panel0Layout.setHorizontalGroup(
						panel0Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel0Layout.setVerticalGroup(
						panel0Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel0);

				//======== panel1 ========
				{
					panel1.setPreferredSize(new Dimension(210, 52));
					panel1.setBorder(null);
					panel1.setVisible(false);

					GroupLayout panel1Layout = new GroupLayout(panel1);
					panel1.setLayout(panel1Layout);
					panel1Layout.setHorizontalGroup(
						panel1Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel1Layout.setVerticalGroup(
						panel1Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel1);

				//======== panel2 ========
				{
					panel2.setPreferredSize(new Dimension(210, 52));
					panel2.setBorder(null);
					panel2.setVisible(false);

					GroupLayout panel2Layout = new GroupLayout(panel2);
					panel2.setLayout(panel2Layout);
					panel2Layout.setHorizontalGroup(
						panel2Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel2Layout.setVerticalGroup(
						panel2Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel2);

				//======== panel3 ========
				{
					panel3.setPreferredSize(new Dimension(210, 52));
					panel3.setBorder(null);
					panel3.setVisible(false);

					GroupLayout panel3Layout = new GroupLayout(panel3);
					panel3.setLayout(panel3Layout);
					panel3Layout.setHorizontalGroup(
						panel3Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel3Layout.setVerticalGroup(
						panel3Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel3);

				//======== panel4 ========
				{
					panel4.setPreferredSize(new Dimension(210, 52));
					panel4.setBorder(null);
					panel4.setVisible(false);

					GroupLayout panel4Layout = new GroupLayout(panel4);
					panel4.setLayout(panel4Layout);
					panel4Layout.setHorizontalGroup(
						panel4Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel4Layout.setVerticalGroup(
						panel4Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel4);

				//======== panel5 ========
				{
					panel5.setPreferredSize(new Dimension(210, 52));
					panel5.setBorder(null);
					panel5.setVisible(false);

					GroupLayout panel5Layout = new GroupLayout(panel5);
					panel5.setLayout(panel5Layout);
					panel5Layout.setHorizontalGroup(
						panel5Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel5Layout.setVerticalGroup(
						panel5Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel5);

				//======== panel6 ========
				{
					panel6.setPreferredSize(new Dimension(210, 52));
					panel6.setBorder(null);
					panel6.setVisible(false);

					GroupLayout panel6Layout = new GroupLayout(panel6);
					panel6.setLayout(panel6Layout);
					panel6Layout.setHorizontalGroup(
						panel6Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel6Layout.setVerticalGroup(
						panel6Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel6);

				//======== panel7 ========
				{
					panel7.setPreferredSize(new Dimension(210, 52));
					panel7.setBorder(null);
					panel7.setVisible(false);

					GroupLayout panel7Layout = new GroupLayout(panel7);
					panel7.setLayout(panel7Layout);
					panel7Layout.setHorizontalGroup(
						panel7Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel7Layout.setVerticalGroup(
						panel7Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel7);

				//======== panel8 ========
				{
					panel8.setPreferredSize(new Dimension(210, 52));
					panel8.setBorder(null);
					panel8.setVisible(false);

					GroupLayout panel8Layout = new GroupLayout(panel8);
					panel8.setLayout(panel8Layout);
					panel8Layout.setHorizontalGroup(
						panel8Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel8Layout.setVerticalGroup(
						panel8Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel8);

				//======== panel9 ========
				{
					panel9.setPreferredSize(new Dimension(210, 52));
					panel9.setBorder(null);
					panel9.setVisible(false);

					GroupLayout panel9Layout = new GroupLayout(panel9);
					panel9.setLayout(panel9Layout);
					panel9Layout.setHorizontalGroup(
						panel9Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel9Layout.setVerticalGroup(
						panel9Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel9);

				//======== panel10 ========
				{
					panel10.setPreferredSize(new Dimension(210, 52));
					panel10.setBorder(null);
					panel10.setVisible(false);

					GroupLayout panel10Layout = new GroupLayout(panel10);
					panel10.setLayout(panel10Layout);
					panel10Layout.setHorizontalGroup(
						panel10Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel10Layout.setVerticalGroup(
						panel10Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel10);

				//======== panel11 ========
				{
					panel11.setPreferredSize(new Dimension(210, 52));
					panel11.setBorder(null);
					panel11.setVisible(false);

					GroupLayout panel11Layout = new GroupLayout(panel11);
					panel11.setLayout(panel11Layout);
					panel11Layout.setHorizontalGroup(
						panel11Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel11Layout.setVerticalGroup(
						panel11Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel11);

				//======== panel12 ========
				{
					panel12.setPreferredSize(new Dimension(210, 52));
					panel12.setBorder(null);
					panel12.setVisible(false);

					GroupLayout panel12Layout = new GroupLayout(panel12);
					panel12.setLayout(panel12Layout);
					panel12Layout.setHorizontalGroup(
						panel12Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel12Layout.setVerticalGroup(
						panel12Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel12);

				//======== panel13 ========
				{
					panel13.setPreferredSize(new Dimension(210, 52));
					panel13.setBorder(null);
					panel13.setVisible(false);

					GroupLayout panel13Layout = new GroupLayout(panel13);
					panel13.setLayout(panel13Layout);
					panel13Layout.setHorizontalGroup(
						panel13Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel13Layout.setVerticalGroup(
						panel13Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel13);

				//======== panel14 ========
				{
					panel14.setPreferredSize(new Dimension(210, 52));
					panel14.setBorder(null);
					panel14.setVisible(false);

					GroupLayout panel14Layout = new GroupLayout(panel14);
					panel14.setLayout(panel14Layout);
					panel14Layout.setHorizontalGroup(
						panel14Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel14Layout.setVerticalGroup(
						panel14Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel14);

				//======== panel15 ========
				{
					panel15.setPreferredSize(new Dimension(210, 52));
					panel15.setBorder(null);
					panel15.setVisible(false);

					GroupLayout panel15Layout = new GroupLayout(panel15);
					panel15.setLayout(panel15Layout);
					panel15Layout.setHorizontalGroup(
						panel15Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel15Layout.setVerticalGroup(
						panel15Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel15);

				//======== panel16 ========
				{
					panel16.setPreferredSize(new Dimension(210, 52));
					panel16.setBorder(null);
					panel16.setVisible(false);

					GroupLayout panel16Layout = new GroupLayout(panel16);
					panel16.setLayout(panel16Layout);
					panel16Layout.setHorizontalGroup(
						panel16Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel16Layout.setVerticalGroup(
						panel16Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel16);

				//======== panel17 ========
				{
					panel17.setPreferredSize(new Dimension(210, 52));
					panel17.setBorder(null);
					panel17.setVisible(false);

					GroupLayout panel17Layout = new GroupLayout(panel17);
					panel17.setLayout(panel17Layout);
					panel17Layout.setHorizontalGroup(
						panel17Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel17Layout.setVerticalGroup(
						panel17Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel17);

				//======== panel18 ========
				{
					panel18.setPreferredSize(new Dimension(210, 52));
					panel18.setBorder(null);
					panel18.setVisible(false);

					GroupLayout panel18Layout = new GroupLayout(panel18);
					panel18.setLayout(panel18Layout);
					panel18Layout.setHorizontalGroup(
						panel18Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel18Layout.setVerticalGroup(
						panel18Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel18);

				//======== panel19 ========
				{
					panel19.setPreferredSize(new Dimension(210, 52));
					panel19.setBorder(null);
					panel19.setVisible(false);

					GroupLayout panel19Layout = new GroupLayout(panel19);
					panel19.setLayout(panel19Layout);
					panel19Layout.setHorizontalGroup(
						panel19Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel19Layout.setVerticalGroup(
						panel19Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel19);

				//======== panel20 ========
				{
					panel20.setPreferredSize(new Dimension(210, 52));
					panel20.setBorder(null);
					panel20.setVisible(false);

					GroupLayout panel20Layout = new GroupLayout(panel20);
					panel20.setLayout(panel20Layout);
					panel20Layout.setHorizontalGroup(
						panel20Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel20Layout.setVerticalGroup(
						panel20Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel20);

				//======== panel21 ========
				{
					panel21.setPreferredSize(new Dimension(210, 52));
					panel21.setBorder(null);
					panel21.setVisible(false);

					GroupLayout panel21Layout = new GroupLayout(panel21);
					panel21.setLayout(panel21Layout);
					panel21Layout.setHorizontalGroup(
						panel21Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel21Layout.setVerticalGroup(
						panel21Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel21);

				//======== panel22 ========
				{
					panel22.setPreferredSize(new Dimension(210, 52));
					panel22.setBorder(null);
					panel22.setVisible(false);

					GroupLayout panel22Layout = new GroupLayout(panel22);
					panel22.setLayout(panel22Layout);
					panel22Layout.setHorizontalGroup(
						panel22Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
					panel22Layout.setVerticalGroup(
						panel22Layout.createParallelGroup()
							.addGap(0, 0, Short.MAX_VALUE)
					);
				}
				panelskills.add(panel22);
			}
			scrollPane1.setViewportView(panelskills);
		}
		add(scrollPane1, CC.xy(1, 3, CC.DEFAULT, CC.TOP));

		//======== panel24 ========
		{
			panel24.setLayout(new FormLayout(
				"default:grow",
				"bottom:default"));

			//======== panel23 ========
			{
				panel23.setLayout(new FormLayout(
					"default:grow",
					"default"));

				//---- button1 ----
				button1.setText("Reset");
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
					}
				});
				panel23.add(button1, CC.xy(1, 1, CC.CENTER, CC.BOTTOM));
			}
			panel24.add(panel23, CC.xy(1, 1));
		}
		add(panel24, CC.xy(1, 5));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		scrollPane1.getVerticalScrollBar().setPreferredSize(new Dimension(0,0));
		scrollPane1.getVerticalScrollBar().setUnitIncrement(20);
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panelheader;
	private JLabel skilllabel;
	private JScrollPane scrollPane1;
	public JPanel panelskills;
	public JPanel panel0;
	public JPanel panel1;
	public JPanel panel2;
	public JPanel panel3;
	public JPanel panel4;
	public JPanel panel5;
	public JPanel panel6;
	public JPanel panel7;
	public JPanel panel8;
	public JPanel panel9;
	public JPanel panel10;
	public JPanel panel11;
	public JPanel panel12;
	public JPanel panel13;
	public JPanel panel14;
	public JPanel panel15;
	public JPanel panel16;
	public JPanel panel17;
	public JPanel panel18;
	public JPanel panel19;
	public JPanel panel20;
	public JPanel panel21;
	public JPanel panel22;
	private JPanel panel24;
	private JPanel panel23;
	private JButton button1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	
	
	
}