/*
 * Created by JFormDesigner on Fri Mar 11 19:47:56 PST 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.*;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.util.Timer;
import org.vrclient.main.utils.Logger;

/**
 * @author Sol Crystal
 */
public class CCPanel extends JPanel {
	private final Logger log = new Logger(getClass());
	private final Configuration config = Configuration.getInstance();
	public static DefaultListModel<String> mageModel = new DefaultListModel<String>();
	public static DefaultListModel<String> rangeModel = new DefaultListModel<String>();
	public static DefaultListModel<String> meleeModel = new DefaultListModel<String>();
	public static DefaultListModel<String> missingModel = new DefaultListModel<String>();
	public Timer timer = new Timer(0);
	public CCPanel() {
		initComponents();
		if(!Constants.FAILED && Configuration.getInstance().getUser().isOfficial())
		this.setVisible(true);
		
		list1.setModel(mageModel);
		list2.setModel(rangeModel);
		list5.setModel(meleeModel);
		list4.setModel(missingModel);
	}
	public boolean hasMage(final String name) {
		return mageModel.contains(name);
	}
	public boolean hasRanger(final String name) {
		return rangeModel.contains(name);
	}
	public boolean hasMelee(final String name) {
		return meleeModel.contains(name);
	}
	public void addMage(final String name) {
		if(!mageModel.contains(name))
			mageModel.addElement(name);
		SwingUtilities.invokeLater (new Runnable () { 
			   public void run () { 
		scrollPane1.setViewportView(list1);
			   	}
			   });
	}
	public void addRanger(final String name) {
		if(!rangeModel.contains(name))
			rangeModel.addElement(name);
		SwingUtilities.invokeLater (new Runnable () { 
			   public void run () { 
		scrollPane2.setViewportView(list2);			
			   	}
			   });
	}
	public void addMelee(final String name) {
		if(!meleeModel.contains(name))
			meleeModel.addElement(name);
		SwingUtilities.invokeLater (new Runnable () { 
			   public void run () { 
		scrollPane4.setViewportView(list5);			
			   	}
			   });
	}
	public void addMissing(final String name) {
		if(!missingModel.contains(name))
			missingModel.addElement(name);
		SwingUtilities.invokeLater (new Runnable () { 
		public void run () { 
		scrollPane3.setViewportView(list4);			
			   	}
			   });
	}
	public void clearNames() {
		mageModel.clear();
		rangeModel.clear();
		meleeModel.clear();
		missingModel.clear();
	}
	public void clearAll() {
		mageModel.clear();
		rangeModel.clear();
		meleeModel.clear();
		missingModel.clear();
		timer = new Timer(0);
	}
	public void updateText() {
		label2.setText("Magic: "+mageModel.size());
		label3.setText("Rangers: "+rangeModel.size());
		label4.setText("Meleers: "+meleeModel.size());
		label6.setText("Missing people: "+missingModel.size());
	}
	public void setRangeText() {
		label3.setText("Rangers: "+rangeModel.size());
	}
	public void setMeleeText() {
		label4.setText("Meleers: "+meleeModel.size());
	}

	public void setTotal(String string) {
		label5.setText("Total in-Game: "+string);
	}
	

	private void button2ActionPerformed(ActionEvent e) {
		clearAll();
	}

	private void toggleButton1ActionPerformed(ActionEvent e) {
		if(toggleButton1.isSelected()) {
			toggleButton1.setText("Enabled");
			toggleButton1.setBackground(new Color(76, 207, 75));
		} else {
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
			config.drawCCInfo(toggleButton1.isSelected());
			clearAll();
		}
		
		config.drawCCInfo(toggleButton1.isSelected());
		log.info(config.drawCCInfo() ? "Enabled drawing cc." : "Disabled Drawing cc.");
	}

	private void showCallerSettingsActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void callersActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void saveCallerSettingsActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void getCallersActionPerformed(ActionEvent e) {
		// TODO add your code here
	}

	private void clearCallerListActionPerformed(ActionEvent e) {
		// TODO add your code here
	}
	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel2 = new JPanel();
		label8 = new JLabel();
		scrollPane3 = new JScrollPane();
		list4 = new JList();
		toggleButton1 = new JToggleButton();
		button2 = new JButton();
		panel1 = new JPanel();
		label2 = new JLabel();
		label5 = new JLabel();
		label6 = new JLabel();
		scrollPane1 = new JScrollPane();
		list1 = new JList();
		scrollPane2 = new JScrollPane();
		list2 = new JList();
		label3 = new JLabel();
		label4 = new JLabel();
		scrollPane4 = new JScrollPane();
		list5 = new JList();

		//======== this ========
		setMaximumSize(new Dimension(220, 440));
		setMinimumSize(new Dimension(200, 440));
		setPreferredSize(new Dimension(220, 440));
		setVisible(false);
		setLayout(new FormLayout(
			"default:grow",
			"2*(default, $lgap), default"));

		//======== panelheader ========
		{
			panelheader.setPreferredSize(new Dimension(56, 25));
			panelheader.setBackground(Color.black);
			panelheader.setMaximumSize(new Dimension(210, 2147483647));
			panelheader.setLayout(new FormLayout(
				"center:default:grow",
				"fill:default:grow"));

			//---- skilllabel ----
			skilllabel.setText("Clan Chat");
			skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
			skilllabel.setForeground(Color.white);
			panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.FILL));
		}
		add(panelheader, CC.xy(1, 1));

		//======== panel2 ========
		{
			panel2.setLayout(new FormLayout(
				"$lcgap, default:grow, $ugap, default:grow, $lcgap",
				"default, 15dlu, default:grow, 3*($lgap, default)"));

			//---- label8 ----
			label8.setText("Missing People");
			label8.setFont(new Font("Tahoma", Font.BOLD, 12));
			panel2.add(label8, CC.xywh(2, 3, 3, 1, CC.CENTER, CC.DEFAULT));

			//======== scrollPane3 ========
			{

				//---- list4 ----
				list4.setBorder(new LineBorder(new Color(77, 77, 77), 1, true));
				scrollPane3.setViewportView(list4);
			}
			panel2.add(scrollPane3, CC.xywh(2, 5, 3, 1));

			//---- toggleButton1 ----
			toggleButton1.setText("Disabled");
			toggleButton1.setBackground(new Color(194, 44, 44));
			toggleButton1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					toggleButton1ActionPerformed(e);
				}
			});
			panel2.add(toggleButton1, CC.xy(4, 1));

			//---- button2 ----
			button2.setText("Clear lists");
			button2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button2ActionPerformed(e);
				}
			});
			panel2.add(button2, CC.xy(2, 1));
		}
		add(panel2, CC.xy(1, 3));

		//======== panel1 ========
		{
			panel1.setVisible(false);
			panel1.setLayout(new FormLayout(
				"6*(default, $lcgap)",
				"default"));

			//---- label2 ----
			label2.setText("Mages");
			label2.setHorizontalAlignment(SwingConstants.CENTER);
			label2.setFont(new Font("Tahoma", Font.BOLD, 10));
			label2.setForeground(new Color(204, 204, 204));
			label2.setIcon(new ImageIcon(getClass().getResource("/resources/skills/Magic.png")));
			panel1.add(label2, CC.xy(2, 1));

			//---- label5 ----
			label5.setText("Total in-game:");
			label5.setFont(new Font("Tahoma", Font.BOLD, 10));
			label5.setForeground(new Color(204, 204, 204));
			panel1.add(label5, CC.xy(3, 1));

			//---- label6 ----
			label6.setText("Missing people:");
			label6.setFont(new Font("Tahoma", Font.BOLD, 10));
			label6.setForeground(new Color(204, 204, 204));
			panel1.add(label6, CC.xy(4, 1));

			//======== scrollPane1 ========
			{

				//---- list1 ----
				list1.setBorder(new LineBorder(new Color(77, 77, 77), 1, true));
				list1.setBackground(null);
				scrollPane1.setViewportView(list1);
			}
			panel1.add(scrollPane1, CC.xy(5, 1));

			//======== scrollPane2 ========
			{

				//---- list2 ----
				list2.setBorder(new LineBorder(new Color(77, 77, 77), 1, true));
				scrollPane2.setViewportView(list2);
			}
			panel1.add(scrollPane2, CC.xy(6, 1));

			//---- label3 ----
			label3.setText("Rangers");
			label3.setHorizontalAlignment(SwingConstants.CENTER);
			label3.setFont(new Font("Tahoma", Font.BOLD, 10));
			label3.setForeground(new Color(204, 204, 204));
			label3.setIcon(new ImageIcon(getClass().getResource("/resources/skills/Range.png")));
			panel1.add(label3, CC.xy(7, 1));

			//---- label4 ----
			label4.setText("Meleers");
			label4.setHorizontalAlignment(SwingConstants.CENTER);
			label4.setFont(new Font("Tahoma", Font.BOLD, 10));
			label4.setForeground(new Color(204, 204, 204));
			label4.setIcon(new ImageIcon(getClass().getResource("/resources/skills/Attack.png")));
			panel1.add(label4, CC.xy(8, 1));

			//======== scrollPane4 ========
			{

				//---- list5 ----
				list5.setBorder(new LineBorder(new Color(77, 77, 77), 1, true));
				scrollPane4.setViewportView(list5);
			}
			panel1.add(scrollPane4, CC.xy(11, 1));
		}
		add(panel1, CC.xy(1, 5));
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel2;
	private JLabel label8;
	private JScrollPane scrollPane3;
	private JList list4;
	private JToggleButton toggleButton1;
	private JButton button2;
	private JPanel panel1;
	private JLabel label2;
	private JLabel label5;
	private JLabel label6;
	private JScrollPane scrollPane1;
	private JList list1;
	private JScrollPane scrollPane2;
	private JList list2;
	private JLabel label3;
	private JLabel label4;
	private JScrollPane scrollPane4;
	private JList list5;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
