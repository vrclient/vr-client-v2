/*
 * Created by JFormDesigner on Thu Jul 07 18:36:17 CDT 2016
 */

package org.vrclient.main.ui.sidebar;

import java.awt.*;
import java.awt.event.*;
import java.io.File;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.filechooser.FileSystemView;

import org.vrclient.component.plugins.timerOverlays;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.methods.interactive.GroundItems;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

/**
 * @author Seth
 */
public class SettingsPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Configuration config = Configuration.getInstance();
	
	public SettingsPanel() {
		initComponents();
	}

	private void button2ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		pilehelper.setVisible(true);
	}

	private void saveCallerSettingsActionPerformed(ActionEvent e) {
		if(checkBox2.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "enablepilehelper", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "enablepilehelper", "false");
		}
		if(checkBox1.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "pilehelpercombat", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "pilehelpercombat", "false");
		}
		if(checkBox9.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawpilehelperprays", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawpilehelperprays", "false");
		}
		
		pilehelper.setVisible(false);
		panel1.setVisible(true);
	}

	

	private void checkBox1ActionPerformed(ActionEvent e) {
		config.pilehelpercombat(checkBox1.isSelected());
	}

	private void checkBox2ActionPerformed(ActionEvent e) {
		config.enablepilehelper(checkBox2.isSelected());
	}

	private void checkBox4ActionPerformed(ActionEvent e) {
		if(checkBox4.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawJewelry", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawJewelry", "false");
		}
		config.drawJewelry(checkBox4.isSelected());
	}

	private void button4ActionPerformed(ActionEvent e) {
		if(checkBox6.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawFPS", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawFPS", "false");
		}
		if(checkBox7.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "rememberme", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "rememberme", "false");
		}
		FileUtils.save(Constants.SETTING_FILE_NAME, "defaultworld", (String)comboBox1.getSelectedItem());
		FileUtils.save(Constants.SETTING_FILE_NAME, "defaultworldindex", comboBox1.getSelectedIndex() + "");
		general.setVisible(false);
		panel1.setVisible(true);
	}

	private void button1ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		general.setVisible(true);
	}

	private void button5ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		combat.setVisible(true);
	}

	private void checkBox3ActionPerformed(ActionEvent e) {
		config.drawOpponentInfo(checkBox3.isSelected());
	}

	private void checkBox5ActionPerformed(ActionEvent e) {
		config.setSpecBar(checkBox5.isSelected());
	}

	private void button6ActionPerformed(ActionEvent e) {
		if(checkBox3.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawOpponentInfo", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawOpponentInfo", "false");
		}
		if(checkBox8.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawoverhead", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawoverhead", "false");
		}
		if(checkBox5.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "setSpecBar", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "setSpecBar", "false");
		}
		combat.setVisible(false);
		panel1.setVisible(true);
	}

	private void checkBox6ActionPerformed(ActionEvent e) {
		config.drawFPS(checkBox6.isSelected());
	}

	private void checkBox7ActionPerformed(ActionEvent e) {
		config.rememberme(checkBox7.isSelected());
	}

	private void checkBox10ActionPerformed(ActionEvent e) {
		config.drawGroundItems(checkBox10.isSelected());
	}

	private void button8ActionPerformed(ActionEvent e) {
		if(checkBox10.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawGroundItems", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawGroundItems", "false");
		}
		if(checkBox4.isSelected()) {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawfriends", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawfriends", "false");
		}
		textoverlay.setVisible(false);
		panel1.setVisible(true);
	}

	private void button7ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		textoverlay.setVisible(true);
	}

	private void checkBox8ActionPerformed(ActionEvent e) {
		config.drawoverhead(checkBox8.isSelected());
	}

	private void checkBox9ActionPerformed(ActionEvent e) {
		config.drawpilehelperprays(checkBox9.isSelected());
	}

	private void button9ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		recording.setVisible(true);
	}

	private void button11ActionPerformed(ActionEvent e) {
		JFileChooser f = new JFileChooser();
        f.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY); 
        f.showSaveDialog(null);
        FileUtils.save(Constants.SETTING_FILE_NAME, "recordLocation", f.getSelectedFile().getPath());
        config.recordLocation(f.getSelectedFile().getPath());
        button11.setText(config.recordLocation);
        
        
	}

	private void textField1MouseClicked(MouseEvent e) {
		
		
		textField1.addKeyListener( new KeyListener() {

	        @Override
	        public void keyTyped( KeyEvent evt ) {
	        }

	        @Override
	        public void keyPressed( KeyEvent evt ) {
	        }

	        @Override
	        public void keyReleased( KeyEvent evt ) {
	        	textField1.setText(KeyEvent.getKeyText(evt.getKeyCode()));
	        	FileUtils.save(Constants.SETTING_FILE_NAME, "recordHotKey", ""+evt.getKeyCode());
	        	config.recordHotKey(evt.getKeyCode());
	        }
	    } );
	}

	private void button10ActionPerformed(ActionEvent e) {
		recording.setVisible(false);
		panel1.setVisible(true);
	}

	private void textField1FocusGained(FocusEvent e) {
		label20.setVisible(true);
	}
	private void textField1FocusLost(FocusEvent e) {
		label20.setVisible(false);
	}

	private void checkBox11ActionPerformed(ActionEvent e) {
		if(checkBox11.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "overloadAlert", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "overloadAlert", "false");
		}
		config.overloadAlert(checkBox11.isSelected());
	}

	private void checkBox12ActionPerformed(ActionEvent e) {
		if(checkBox12.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "absorbAlert", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "absorbAlert", "false");
		}
		config.absorbAlert(checkBox12.isSelected());
	}

	private void checkBox13ActionPerformed(ActionEvent e) {
		if(checkBox13.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawRapidHeal", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawRapidHeal", "false");
		}
		config.drawRapidHeal(checkBox13.isSelected());
	}

	private void button12ActionPerformed(ActionEvent e) {
		nmz.setVisible(false);
		panel1.setVisible(true);
	}

	private void button13ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		nmz.setVisible(true);
		
	}

	private void checkBox14ActionPerformed(ActionEvent e) {
		if(checkBox14.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawToolTip", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawToolTip", "false");
		}
		config.drawToolTip(checkBox14.isSelected());
	}

	private void checkBox15ActionPerformed(ActionEvent e) {
		if(checkBox15.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawNightMareZone", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawNightMareZone", "false");
		}
		config.drawNightMareZone(checkBox15.isSelected());
	}

	private void checkBox16ActionPerformed(ActionEvent e) {
		if(checkBox16.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawBoosts", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawBoosts", "false");
		}
		config.drawBoosts(checkBox16.isSelected());
	}

	private void slider1StateChanged(ChangeEvent e) {
		FileUtils.save(Constants.SETTING_FILE_NAME, "overlayOpacity", slider1.getValue()+"");
		config.overlayOpacity(slider1.getValue());
	}

	private void checkBox17ActionPerformed(ActionEvent e) {
		if(checkBox17.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeath", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeath", "false");
		}
		config.drawDeath(checkBox17.isSelected());
	}

	private void checkBox18ActionPerformed(ActionEvent e) {
		if(checkBox18.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeathTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeathTimer", "false");
		}
		if(timerOverlays.findTimerFor("death") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("death"));
		config.drawDeathTimer(checkBox18.isSelected());
	}

	private void checkBox19ActionPerformed(ActionEvent e) {
		if(checkBox19.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawBossTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawBossTimer", "false");
		}
		config.drawBossTimer(checkBox19.isSelected());
	}

	private void checkBox20ActionPerformed(ActionEvent e) {
		if(checkBox20.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawStaminaTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawStaminaTimer", "false");
		}
		if(timerOverlays.findTimerFor("stam") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("stam"));
		config.drawStaminaTimer(checkBox20.isSelected());
	}

	private void checkBox21ActionPerformed(ActionEvent e) {
		if(checkBox21.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawTbTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawTbTimer", "false");
		}
		if(timerOverlays.findTimerFor("tb") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("tb"));
		config.drawTbTimer(checkBox21.isSelected());
	}

	private void checkBox22ActionPerformed(ActionEvent e) {
		if(checkBox22.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawAfTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawAfTimer", "false");
		}
		if(timerOverlays.findTimerFor("af") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("af"));
		config.drawAfTimer(checkBox22.isSelected());
	}

	private void checkBox23ActionPerformed(ActionEvent e) {
		if(checkBox23.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawSafTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawSafTimer", "false");
		}
		if(timerOverlays.findTimerFor("saf") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("saf"));
		config.drawSafTimer(checkBox23.isSelected());
	}

	private void checkBox24ActionPerformed(ActionEvent e) {
		if(checkBox24.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawHomeTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawHomeTimer", "false");
		}
		if(timerOverlays.findTimerFor("hometele") != null)
	        	timerOverlays.timers.remove(timerOverlays.findTimerFor("hometele"));
		config.drawHomeTimer(checkBox24.isSelected());
	}

	private void checkBox25ActionPerformed(ActionEvent e) {
		if(checkBox25.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbueTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbueTimer", "false");
		}
		if(timerOverlays.findTimerFor("magicimbue") != null)
        	timerOverlays.timers.remove(timerOverlays.findTimerFor("magicimbue"));
		config.drawImbueTimer(checkBox25.isSelected());
	}

	private void checkBox26ActionPerformed(ActionEvent e) {
		if(checkBox26.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawVengTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawVengTimer", "false");
		}
		if(timerOverlays.findTimerFor("veng") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("veng"));
		config.drawVengTimer(checkBox26.isSelected());
	}

	private void checkBox27ActionPerformed(ActionEvent e) {
		if(checkBox27.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawOverloadTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawOverloadTimer", "false");
		}
		if(timerOverlays.findTimerFor("overload") != null)
    		timerOverlays.timers.remove(timerOverlays.findTimerFor("overload"));
		config.drawOverloadTimer(checkBox27.isSelected());
	}

	private void checkBox28ActionPerformed(ActionEvent e) {
		if(checkBox28.isSelected()) {		
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbheartTimer", "true");
		} else {
			FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbheartTimer", "false");
		}
		if(timerOverlays.findTimerFor("imbheart") != null)
        	timerOverlays.timers.remove(timerOverlays.findTimerFor("imbheart"));
		config.drawImbheartTimer(checkBox28.isSelected());
	}

	private void button15ActionPerformed(ActionEvent e) {
		panel1.setVisible(false);
		timeroverlay.setVisible(true);
	}

	private void button14ActionPerformed(ActionEvent e) {
		timeroverlay.setVisible(false);
		panel1.setVisible(true);
	}

	

	
	

	

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		panel1 = new JPanel();
		panelheader = new JPanel();
		skilllabel = new JLabel();
		panel3 = new JPanel();
		button1 = new JButton();
		button5 = new JButton();
		button15 = new JButton();
		button7 = new JButton();
		button2 = new JButton();
		button9 = new JButton();
		button13 = new JButton();
		general = new JPanel();
		label4 = new JLabel();
		separator2 = new JSeparator();
		label6 = new JLabel();
		comboBox1 = new JComboBox<>();
		label12 = new JLabel();
		checkBox7 = new JCheckBox();
		label27 = new JLabel();
		slider1 = new JSlider();
		button4 = new JButton();
		combat = new JPanel();
		label8 = new JLabel();
		separator3 = new JSeparator();
		label9 = new JLabel();
		checkBox3 = new JCheckBox();
		label14 = new JLabel();
		checkBox8 = new JCheckBox();
		label10 = new JLabel();
		checkBox5 = new JCheckBox();
		label26 = new JLabel();
		checkBox16 = new JCheckBox();
		label28 = new JLabel();
		checkBox17 = new JCheckBox();
		button6 = new JButton();
		timeroverlay = new JPanel();
		label29 = new JLabel();
		separator7 = new JSeparator();
		label30 = new JLabel();
		checkBox18 = new JCheckBox();
		label31 = new JLabel();
		checkBox19 = new JCheckBox();
		label32 = new JLabel();
		checkBox20 = new JCheckBox();
		label33 = new JLabel();
		checkBox21 = new JCheckBox();
		label34 = new JLabel();
		checkBox22 = new JCheckBox();
		label35 = new JLabel();
		checkBox23 = new JCheckBox();
		label36 = new JLabel();
		checkBox24 = new JCheckBox();
		label37 = new JLabel();
		checkBox25 = new JCheckBox();
		label38 = new JLabel();
		checkBox26 = new JCheckBox();
		label39 = new JLabel();
		checkBox27 = new JCheckBox();
		label40 = new JLabel();
		button14 = new JButton();
		checkBox28 = new JCheckBox();
		textoverlay = new JPanel();
		label13 = new JLabel();
		separator4 = new JSeparator();
		label11 = new JLabel();
		checkBox6 = new JCheckBox();
		label17 = new JLabel();
		checkBox10 = new JCheckBox();
		label2 = new JLabel();
		checkBox14 = new JCheckBox();
		label7 = new JLabel();
		checkBox4 = new JCheckBox();
		button8 = new JButton();
		pilehelper = new JPanel();
		label1 = new JLabel();
		separator1 = new JSeparator();
		label3 = new JLabel();
		checkBox2 = new JCheckBox();
		label5 = new JLabel();
		checkBox1 = new JCheckBox();
		label15 = new JLabel();
		checkBox9 = new JCheckBox();
		button3 = new JButton();
		recording = new JPanel();
		label16 = new JLabel();
		separator5 = new JSeparator();
		label18 = new JLabel();
		button11 = new JButton();
		label19 = new JLabel();
		textField1 = new JTextField();
		label20 = new JLabel();
		button10 = new JButton();
		nmz = new JPanel();
		label21 = new JLabel();
		separator6 = new JSeparator();
		label25 = new JLabel();
		checkBox15 = new JCheckBox();
		label22 = new JLabel();
		checkBox11 = new JCheckBox();
		label23 = new JLabel();
		checkBox12 = new JCheckBox();
		label24 = new JLabel();
		checkBox13 = new JCheckBox();
		button12 = new JButton();

		//======== this ========
		setBackground(new Color(58, 58, 58));
		setForeground(new Color(70, 70, 70));
		setBorder(null);
		setRequestFocusEnabled(false);
		setMinimumSize(new Dimension(220, 420));
		setMaximumSize(new Dimension(220, 32767));
		setPreferredSize(new Dimension(220, 440));
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));

		//======== panel1 ========
		{
			panel1.setBorder(null);
			panel1.setRequestFocusEnabled(false);
			panel1.setMinimumSize(new Dimension(220, 420));
			panel1.setPreferredSize(new Dimension(220, 440));
			panel1.setMaximumSize(new Dimension(220, 32767));
			panel1.setVisible(false);
			panel1.setLayout(new FormLayout(
				"default:grow",
				"fill:default, $lgap, 15dlu, $lgap, fill:199dlu:grow"));

			//======== panelheader ========
			{
				panelheader.setPreferredSize(new Dimension(56, 25));
				panelheader.setBackground(Color.black);
				panelheader.setMaximumSize(new Dimension(210, 2147483647));
				panelheader.setLayout(new FormLayout(
					"center:default:grow",
					"fill:default:grow"));

				//---- skilllabel ----
				skilllabel.setText("Settings");
				skilllabel.setFont(new Font("Tahoma", Font.BOLD, 14));
				skilllabel.setForeground(Color.white);
				panelheader.add(skilllabel, CC.xy(1, 1, CC.CENTER, CC.FILL));
			}
			panel1.add(panelheader, CC.xy(1, 1));

			//======== panel3 ========
			{
				panel3.setAlignmentY(5.0F);
				panel3.setAlignmentX(5.0F);
				panel3.setMaximumSize(new Dimension(220, 2147483647));
				panel3.setMinimumSize(new Dimension(220, 14));
				panel3.setPreferredSize(new Dimension(220, 14));
				panel3.setLayout(new FormLayout(
					"right:default:grow, 2dlu",
					"7*(default)"));
				((FormLayout)panel3.getLayout()).setRowGroups(new int[][] {{1, 2, 3, 4, 5, 6, 7}});

				//---- button1 ----
				button1.setText("General Settings");
				button1.setHorizontalTextPosition(SwingConstants.CENTER);
				button1.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button1ActionPerformed(e);
					}
				});
				panel3.add(button1, CC.xy(1, 1, CC.FILL, CC.DEFAULT));

				//---- button5 ----
				button5.setText("Combat Settings");
				button5.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button5ActionPerformed(e);
					}
				});
				panel3.add(button5, CC.xy(1, 2, CC.FILL, CC.DEFAULT));

				//---- button15 ----
				button15.setText("Timer Settings");
				button15.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button15ActionPerformed(e);
					}
				});
				panel3.add(button15, CC.xy(1, 3, CC.FILL, CC.CENTER));

				//---- button7 ----
				button7.setText("Text overlays");
				button7.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button7ActionPerformed(e);
					}
				});
				panel3.add(button7, CC.xy(1, 4, CC.FILL, CC.DEFAULT));

				//---- button2 ----
				button2.setText("Pile Helper");
				button2.setHorizontalTextPosition(SwingConstants.CENTER);
				button2.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button2ActionPerformed(e);
					}
				});
				panel3.add(button2, CC.xy(1, 5, CC.FILL, CC.DEFAULT));

				//---- button9 ----
				button9.setText("Video Record Settings");
				button9.setHorizontalTextPosition(SwingConstants.CENTER);
				button9.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button9ActionPerformed(e);
					}
				});
				panel3.add(button9, CC.xy(1, 6, CC.FILL, CC.DEFAULT));

				//---- button13 ----
				button13.setText("Nightmare Zone Settings");
				button13.setHorizontalTextPosition(SwingConstants.CENTER);
				button13.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						button13ActionPerformed(e);
					}
				});
				panel3.add(button13, CC.xy(1, 7, CC.FILL, CC.DEFAULT));
			}
			panel1.add(panel3, CC.xy(1, 5));
		}
		add(panel1);

		//======== general ========
		{
			general.setToolTipText("1");
			general.setComponentPopupMenu(null);
			general.setPreferredSize(new Dimension(220, 440));
			general.setMaximumSize(new Dimension(220, 32767));
			general.setMinimumSize(new Dimension(220, 420));
			general.setVisible(false);
			general.setLayout(new FormLayout(
				"right:default:grow, $glue, center:default:grow",
				"22dlu, 5*(default, $lgap), default"));
			((FormLayout)general.getLayout()).setRowGroups(new int[][] {{4, 6, 8}});

			//---- label4 ----
			label4.setText("General Settings");
			label4.setFont(new Font("Tahoma", Font.PLAIN, 14));
			general.add(label4, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			general.add(separator2, CC.xywh(1, 2, 3, 1));

			//---- label6 ----
			label6.setText("Default World:");
			label6.setFont(label6.getFont().deriveFont(label6.getFont().getSize() + 1f));
			general.add(label6, CC.xy(1, 4));

			//---- comboBox1 ----
			comboBox1.setMinimumSize(new Dimension(50, 20));
			comboBox1.setPreferredSize(new Dimension(50, 20));
			comboBox1.setModel(new DefaultComboBoxModel<>(new String[] {
				"301",
				"302",
				"303",
				"304",
				"305",
				"306",
				"308",
				"309",
				"310",
				"311",
				"312",
				"313",
				"314",
				"316",
				"317",
				"318",
				"319",
				"320",
				"321",
				"322",
				"325",
				"326",
				"327",
				"328",
				"329",
				"330",
				"333",
				"334",
				"335",
				"336",
				"337",
				"338",
				"341",
				"342",
				"343",
				"345",
				"346",
				"349",
				"350",
				"351",
				"352",
				"353",
				"354",
				"357",
				"358",
				"359",
				"360",
				"361",
				"362",
				"365",
				"366",
				"367",
				"368",
				"369",
				"370",
				"373",
				"374",
				"375",
				"376",
				"377",
				"378",
				"381",
				"382",
				"383",
				"384",
				"385",
				"386",
				"393",
				"394"
			}));
			comboBox1.setSelectedIndex(62);
			general.add(comboBox1, CC.xy(3, 4, CC.CENTER, CC.DEFAULT));

			//---- label12 ----
			label12.setText("Remember Username?");
			label12.setFont(label12.getFont().deriveFont(label12.getFont().getSize() + 1f));
			general.add(label12, CC.xy(1, 6));

			//---- checkBox7 ----
			checkBox7.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox7.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox7.setFocusable(false);
			checkBox7.setFocusPainted(false);
			checkBox7.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox7ActionPerformed(e);
				}
			});
			general.add(checkBox7, CC.xy(3, 6));

			//---- label27 ----
			label27.setText("Overlay Opacity");
			general.add(label27, CC.xy(1, 8, CC.CENTER, CC.DEFAULT));

			//---- slider1 ----
			slider1.setFocusable(false);
			slider1.setMinimum(100);
			slider1.setMaximum(255);
			slider1.setValue(240);
			slider1.setPaintLabels(true);
			slider1.addChangeListener(new ChangeListener() {
				@Override
				public void stateChanged(ChangeEvent e) {
					slider1StateChanged(e);
				}
			});
			general.add(slider1, CC.xy(3, 8));

			//---- button4 ----
			button4.setText("Save & Close");
			button4.setBackground(new Color(51, 102, 255));
			button4.setMaximumSize(new Dimension(125, 23));
			button4.setMinimumSize(new Dimension(125, 23));
			button4.setPreferredSize(new Dimension(150, 23));
			button4.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button4ActionPerformed(e);
				}
			});
			general.add(button4, CC.xywh(1, 12, 3, 1, CC.CENTER, CC.DEFAULT));
		}
		add(general);

		//======== combat ========
		{
			combat.setToolTipText("1");
			combat.setComponentPopupMenu(null);
			combat.setPreferredSize(new Dimension(220, 440));
			combat.setMaximumSize(new Dimension(220, 32767));
			combat.setMinimumSize(new Dimension(220, 420));
			combat.setVisible(false);
			combat.setLayout(new FormLayout(
				"right:default:grow, $lcgap, center:default:grow",
				"16dlu, 8dlu, 8*($lgap, default)"));
			((FormLayout)combat.getLayout()).setRowGroups(new int[][] {{4, 6, 8, 10, 12}});

			//---- label8 ----
			label8.setText("Combat Settings");
			label8.setFont(new Font("Tahoma", Font.PLAIN, 14));
			combat.add(label8, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			combat.add(separator3, CC.xywh(1, 2, 3, 1));

			//---- label9 ----
			label9.setText("Display Opponent Info");
			label9.setFont(label9.getFont().deriveFont(label9.getFont().getSize() + 1f));
			combat.add(label9, CC.xy(1, 4, CC.RIGHT, CC.DEFAULT));

			//---- checkBox3 ----
			checkBox3.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox3.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox3.setFocusable(false);
			checkBox3.setFocusPainted(false);
			checkBox3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox3ActionPerformed(e);
				}
			});
			combat.add(checkBox3, CC.xy(3, 4));

			//---- label14 ----
			label14.setText("Show Prayer style");
			label14.setFont(label14.getFont().deriveFont(label14.getFont().getSize() + 1f));
			combat.add(label14, CC.xy(1, 6));

			//---- checkBox8 ----
			checkBox8.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox8.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox8.setFocusable(false);
			checkBox8.setFocusPainted(false);
			checkBox8.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox8ActionPerformed(e);
				}
			});
			combat.add(checkBox8, CC.xy(3, 6));

			//---- label10 ----
			label10.setText("Special Attack Orb");
			label10.setFont(label10.getFont().deriveFont(label10.getFont().getSize() + 1f));
			combat.add(label10, CC.xy(1, 8, CC.RIGHT, CC.DEFAULT));

			//---- checkBox5 ----
			checkBox5.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox5.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox5.setFocusable(false);
			checkBox5.setFocusPainted(false);
			checkBox5.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox5ActionPerformed(e);
				}
			});
			combat.add(checkBox5, CC.xy(3, 8));

			//---- label26 ----
			label26.setText("Draw Combat Boosts");
			combat.add(label26, CC.xy(1, 10));

			//---- checkBox16 ----
			checkBox16.setFocusable(false);
			checkBox16.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox16.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox16.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox16ActionPerformed(e);
				}
			});
			combat.add(checkBox16, CC.xy(3, 10));

			//---- label28 ----
			label28.setText("Draw Death Marker");
			combat.add(label28, CC.xy(1, 12));

			//---- checkBox17 ----
			checkBox17.setFocusable(false);
			checkBox17.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox17.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox17.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox17ActionPerformed(e);
				}
			});
			combat.add(checkBox17, CC.xy(3, 12));

			//---- button6 ----
			button6.setText("Save & Close");
			button6.setBackground(new Color(51, 102, 255));
			button6.setMaximumSize(new Dimension(125, 23));
			button6.setMinimumSize(new Dimension(125, 23));
			button6.setPreferredSize(new Dimension(150, 23));
			button6.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button6ActionPerformed(e);
				}
			});
			combat.add(button6, CC.xywh(1, 16, 3, 1, CC.CENTER, CC.DEFAULT));
		}
		add(combat);

		//======== timeroverlay ========
		{
			timeroverlay.setToolTipText("1");
			timeroverlay.setComponentPopupMenu(null);
			timeroverlay.setPreferredSize(new Dimension(220, 440));
			timeroverlay.setMaximumSize(new Dimension(220, 32767));
			timeroverlay.setMinimumSize(new Dimension(220, 420));
			timeroverlay.setLayout(new FormLayout(
				"right:default:grow, $lcgap, center:default:grow",
				"16dlu, 8dlu, 14*($lgap, default)"));
			((FormLayout)timeroverlay.getLayout()).setRowGroups(new int[][] {{4, 6, 8, 10, 12, 14, 16, 18, 20, 22}});

			//---- label29 ----
			label29.setText("Timer Overlays");
			label29.setFont(new Font("Tahoma", Font.PLAIN, 14));
			timeroverlay.add(label29, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			timeroverlay.add(separator7, CC.xywh(1, 2, 3, 1));

			//---- label30 ----
			label30.setText("Death");
			label30.setFont(label30.getFont().deriveFont(label30.getFont().getSize() + 1f));
			timeroverlay.add(label30, CC.xy(1, 4, CC.RIGHT, CC.DEFAULT));

			//---- checkBox18 ----
			checkBox18.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox18.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox18.setFocusable(false);
			checkBox18.setFocusPainted(false);
			checkBox18.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox18ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox18, CC.xy(3, 4, CC.CENTER, CC.CENTER));

			//---- label31 ----
			label31.setText("Bosses");
			label31.setFont(label31.getFont().deriveFont(label31.getFont().getSize() + 1f));
			timeroverlay.add(label31, CC.xy(1, 6));

			//---- checkBox19 ----
			checkBox19.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox19.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox19.setFocusable(false);
			checkBox19.setFocusPainted(false);
			checkBox19.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox19ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox19, CC.xy(3, 6));

			//---- label32 ----
			label32.setText("Stamina");
			label32.setFont(label32.getFont().deriveFont(label32.getFont().getSize() + 1f));
			timeroverlay.add(label32, CC.xy(1, 8, CC.RIGHT, CC.DEFAULT));

			//---- checkBox20 ----
			checkBox20.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox20.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox20.setFocusable(false);
			checkBox20.setFocusPainted(false);
			checkBox20.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox20ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox20, CC.xy(3, 8));

			//---- label33 ----
			label33.setText("Teleblock");
			timeroverlay.add(label33, CC.xy(1, 10));

			//---- checkBox21 ----
			checkBox21.setFocusable(false);
			checkBox21.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox21.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox21.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox21ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox21, CC.xy(3, 10));

			//---- label34 ----
			label34.setText("Antifire");
			timeroverlay.add(label34, CC.xy(1, 12));

			//---- checkBox22 ----
			checkBox22.setFocusable(false);
			checkBox22.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox22.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox22.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox22ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox22, CC.xy(3, 12));

			//---- label35 ----
			label35.setText("Extended Antifire");
			timeroverlay.add(label35, CC.xy(1, 14));

			//---- checkBox23 ----
			checkBox23.setFocusable(false);
			checkBox23.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox23.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox23.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox23ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox23, CC.xy(3, 14));

			//---- label36 ----
			label36.setText("Home Teleport");
			timeroverlay.add(label36, CC.xy(1, 16));

			//---- checkBox24 ----
			checkBox24.setFocusable(false);
			checkBox24.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox24.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox24.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox24ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox24, CC.xy(3, 16));

			//---- label37 ----
			label37.setText("Magic Imbue");
			timeroverlay.add(label37, CC.xy(1, 18));

			//---- checkBox25 ----
			checkBox25.setFocusable(false);
			checkBox25.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox25.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox25.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox25ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox25, CC.xy(3, 18));

			//---- label38 ----
			label38.setText("Vengeance");
			timeroverlay.add(label38, CC.xy(1, 20));

			//---- checkBox26 ----
			checkBox26.setFocusable(false);
			checkBox26.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox26.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox26.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox26ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox26, CC.xy(3, 20));

			//---- label39 ----
			label39.setText("Overload");
			timeroverlay.add(label39, CC.xy(1, 22));

			//---- checkBox27 ----
			checkBox27.setFocusable(false);
			checkBox27.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox27.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox27.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox27ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox27, CC.xy(3, 22));

			//---- label40 ----
			label40.setText("Imbued Heart");
			timeroverlay.add(label40, CC.xy(1, 24));

			//---- button14 ----
			button14.setText("Save & Close");
			button14.setBackground(new Color(51, 102, 255));
			button14.setMaximumSize(new Dimension(125, 23));
			button14.setMinimumSize(new Dimension(125, 23));
			button14.setPreferredSize(new Dimension(150, 23));
			button14.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button14ActionPerformed(e);
				}
			});
			timeroverlay.add(button14, CC.xywh(1, 28, 3, 1, CC.CENTER, CC.DEFAULT));

			//---- checkBox28 ----
			checkBox28.setFocusable(false);
			checkBox28.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected2.png")));
			checkBox28.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox2.png")));
			checkBox28.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox28ActionPerformed(e);
				}
			});
			timeroverlay.add(checkBox28, CC.xy(3, 24));
		}
		add(timeroverlay);

		//======== textoverlay ========
		{
			textoverlay.setToolTipText("1");
			textoverlay.setComponentPopupMenu(null);
			textoverlay.setPreferredSize(new Dimension(220, 440));
			textoverlay.setMaximumSize(new Dimension(220, 32767));
			textoverlay.setMinimumSize(new Dimension(220, 420));
			textoverlay.setVisible(false);
			textoverlay.setLayout(new FormLayout(
				"right:default:grow, $glue, center:default:grow",
				"22dlu, 6*(default, $lgap), default"));
			((FormLayout)textoverlay.getLayout()).setRowGroups(new int[][] {{4, 6, 8, 10}});

			//---- label13 ----
			label13.setText("Text Overlays");
			label13.setFont(new Font("Tahoma", Font.PLAIN, 14));
			textoverlay.add(label13, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			textoverlay.add(separator4, CC.xywh(1, 2, 3, 1));

			//---- label11 ----
			label11.setText("Show FPS");
			label11.setFont(label11.getFont().deriveFont(label11.getFont().getSize() + 1f));
			textoverlay.add(label11, CC.xy(1, 4));

			//---- checkBox6 ----
			checkBox6.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox6.setFocusable(false);
			checkBox6.setFocusPainted(false);
			checkBox6.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox6.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox6ActionPerformed(e);
				}
			});
			textoverlay.add(checkBox6, CC.xy(3, 4));

			//---- label17 ----
			label17.setText("Display items on ground");
			label17.setFont(label17.getFont().deriveFont(label17.getFont().getSize() + 1f));
			textoverlay.add(label17, CC.xy(1, 6));

			//---- checkBox10 ----
			checkBox10.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox10.setFocusable(false);
			checkBox10.setFocusPainted(false);
			checkBox10.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox10.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox10ActionPerformed(e);
				}
			});
			textoverlay.add(checkBox10, CC.xy(3, 6));

			//---- label2 ----
			label2.setText("Mouse-over Tool tip");
			label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 1f));
			textoverlay.add(label2, CC.xy(1, 8));

			//---- checkBox14 ----
			checkBox14.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox14.setFocusable(false);
			checkBox14.setFocusPainted(false);
			checkBox14.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox14.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox14ActionPerformed(e);
				}
			});
			textoverlay.add(checkBox14, CC.xy(3, 8));

			//---- label7 ----
			label7.setText("Show Jewelry Charges:");
			textoverlay.add(label7, CC.xy(1, 10));

			//---- checkBox4 ----
			checkBox4.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox4.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox4.setFocusable(false);
			checkBox4.setFocusPainted(false);
			checkBox4.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox4ActionPerformed(e);
				}
			});
			textoverlay.add(checkBox4, CC.xy(3, 10));

			//---- button8 ----
			button8.setText("Save & Close");
			button8.setBackground(new Color(51, 102, 255));
			button8.setMaximumSize(new Dimension(125, 23));
			button8.setMinimumSize(new Dimension(125, 23));
			button8.setPreferredSize(new Dimension(150, 23));
			button8.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button8ActionPerformed(e);
				}
			});
			textoverlay.add(button8, CC.xywh(1, 14, 3, 1, CC.CENTER, CC.DEFAULT));
		}
		add(textoverlay);

		//======== pilehelper ========
		{
			pilehelper.setToolTipText("1");
			pilehelper.setComponentPopupMenu(null);
			pilehelper.setPreferredSize(new Dimension(220, 440));
			pilehelper.setMaximumSize(new Dimension(220, 32767));
			pilehelper.setMinimumSize(new Dimension(220, 420));
			pilehelper.setVisible(false);
			pilehelper.setLayout(new FormLayout(
				"right:default:grow, $glue, center:default:grow",
				"22dlu, 6dlu, 5*($lgap, default)"));
			((FormLayout)pilehelper.getLayout()).setRowGroups(new int[][] {{4, 6, 8}});

			//---- label1 ----
			label1.setText("Pile Helper Settings");
			label1.setFont(new Font("Tahoma", Font.PLAIN, 14));
			pilehelper.add(label1, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			pilehelper.add(separator1, CC.xywh(1, 2, 3, 1));

			//---- label3 ----
			label3.setText("Enabled?");
			label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 1f));
			pilehelper.add(label3, CC.xy(1, 4));

			//---- checkBox2 ----
			checkBox2.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox2.setFocusable(false);
			checkBox2.setFocusPainted(false);
			checkBox2.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox2ActionPerformed(e);
				}
			});
			pilehelper.add(checkBox2, CC.xy(3, 4));

			//---- label5 ----
			label5.setText("Display only when targetted?");
			label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() & ~Font.BOLD, label5.getFont().getSize() + 1f));
			pilehelper.add(label5, CC.xy(1, 6));

			//---- checkBox1 ----
			checkBox1.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox1.setFocusable(false);
			checkBox1.setFocusPainted(false);
			checkBox1.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox1ActionPerformed(e);
				}
			});
			pilehelper.add(checkBox1, CC.xy(3, 6));

			//---- label15 ----
			label15.setText("Suggest pray to use?");
			label15.setFont(label15.getFont().deriveFont(label15.getFont().getSize() + 1f));
			pilehelper.add(label15, CC.xy(1, 8));

			//---- checkBox9 ----
			checkBox9.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox9.setFocusable(false);
			checkBox9.setFocusPainted(false);
			checkBox9.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox9.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox9ActionPerformed(e);
				}
			});
			pilehelper.add(checkBox9, CC.xy(3, 8));

			//---- button3 ----
			button3.setText("Save & Close");
			button3.setBackground(new Color(51, 102, 255));
			button3.setMaximumSize(new Dimension(125, 23));
			button3.setMinimumSize(new Dimension(125, 23));
			button3.setPreferredSize(new Dimension(150, 23));
			button3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					saveCallerSettingsActionPerformed(e);
				}
			});
			pilehelper.add(button3, CC.xywh(1, 12, 3, 1, CC.CENTER, CC.DEFAULT));
		}
		add(pilehelper);

		//======== recording ========
		{
			recording.setToolTipText("1");
			recording.setComponentPopupMenu(null);
			recording.setPreferredSize(new Dimension(220, 440));
			recording.setMaximumSize(new Dimension(220, 32767));
			recording.setMinimumSize(new Dimension(220, 420));
			recording.setVisible(false);
			recording.setLayout(new FormLayout(
				"right:default:grow, 10dlu, center:default:grow, $ugap",
				"22dlu, 6dlu, 2*($lgap, default), $lgap, 10dlu, $lgap, default"));

			//---- label16 ----
			label16.setText("Video Record Settings");
			label16.setFont(new Font("Tahoma", Font.PLAIN, 14));
			recording.add(label16, CC.xywh(1, 1, 3, 1, CC.CENTER, CC.DEFAULT));
			recording.add(separator5, CC.xywh(1, 2, 3, 1));

			//---- label18 ----
			label18.setText("Save Location:");
			recording.add(label18, CC.xy(1, 4));

			//---- button11 ----
			button11.setText("Folder");
			button11.setMaximumSize(new Dimension(80, 23));
			button11.setMinimumSize(new Dimension(80, 23));
			button11.setPreferredSize(new Dimension(80, 23));
			button11.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button11ActionPerformed(e);
				}
			});
			recording.add(button11, CC.xy(3, 4));

			//---- label19 ----
			label19.setText("Record Hotkey:");
			recording.add(label19, CC.xy(1, 6));

			//---- textField1 ----
			textField1.setEditable(false);
			textField1.setHorizontalAlignment(SwingConstants.CENTER);
			textField1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					textField1MouseClicked(e);
					textField1MouseClicked(e);
				}
			});
			textField1.addFocusListener(new FocusAdapter() {
				@Override
				public void focusGained(FocusEvent e) {
					textField1FocusGained(e);
				}
				@Override
				public void focusLost(FocusEvent e) {
					textField1FocusLost(e);
				}
			});
			recording.add(textField1, CC.xy(3, 6, CC.FILL, CC.DEFAULT));

			//---- label20 ----
			label20.setText("Press any key to set hotkey");
			label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.ITALIC));
			label20.setVisible(false);
			recording.add(label20, CC.xywh(1, 8, 3, 1, CC.RIGHT, CC.DEFAULT));

			//---- button10 ----
			button10.setText("Save & Close");
			button10.setBackground(new Color(51, 102, 255));
			button10.setMaximumSize(new Dimension(125, 23));
			button10.setMinimumSize(new Dimension(125, 23));
			button10.setPreferredSize(new Dimension(150, 23));
			button10.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button10ActionPerformed(e);
				}
			});
			recording.add(button10, CC.xywh(1, 10, 3, 1, CC.CENTER, CC.DEFAULT));
		}
		add(recording);

		//======== nmz ========
		{
			nmz.setToolTipText("1");
			nmz.setComponentPopupMenu(null);
			nmz.setPreferredSize(new Dimension(220, 440));
			nmz.setMaximumSize(new Dimension(220, 32767));
			nmz.setMinimumSize(new Dimension(220, 420));
			nmz.setVisible(false);
			nmz.setLayout(new FormLayout(
				"right:default:grow, center:default:grow",
				"16dlu, 8dlu, 6*($lgap, default)"));
			((FormLayout)nmz.getLayout()).setRowGroups(new int[][] {{4, 6, 8, 10}});

			//---- label21 ----
			label21.setText("Nightmare Zone Settings");
			label21.setFont(new Font("Tahoma", Font.PLAIN, 14));
			nmz.add(label21, CC.xywh(1, 1, 2, 1, CC.CENTER, CC.DEFAULT));
			nmz.add(separator6, CC.xywh(1, 2, 2, 1));

			//---- label25 ----
			label25.setText("Enabled?");
			nmz.add(label25, CC.xy(1, 4));

			//---- checkBox15 ----
			checkBox15.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox15.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox15.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox15ActionPerformed(e);
				}
			});
			nmz.add(checkBox15, CC.xy(2, 4));

			//---- label22 ----
			label22.setText("Overload Alert:");
			label22.setFont(label22.getFont().deriveFont(label22.getFont().getSize() + 1f));
			nmz.add(label22, CC.xy(1, 6, CC.RIGHT, CC.DEFAULT));

			//---- checkBox11 ----
			checkBox11.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox11.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox11.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox11ActionPerformed(e);
				}
			});
			nmz.add(checkBox11, CC.xy(2, 6));

			//---- label23 ----
			label23.setText("Absorb Alert:");
			label23.setFont(label23.getFont().deriveFont(label23.getFont().getSize() + 1f));
			nmz.add(label23, CC.xy(1, 8));

			//---- checkBox12 ----
			checkBox12.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox12.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox12.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox12ActionPerformed(e);
				}
			});
			nmz.add(checkBox12, CC.xy(2, 8));

			//---- label24 ----
			label24.setText("Rapid Heal Timer:");
			label24.setFont(label24.getFont().deriveFont(label24.getFont().getSize() + 1f));
			nmz.add(label24, CC.xy(1, 10, CC.RIGHT, CC.DEFAULT));

			//---- checkBox13 ----
			checkBox13.setIcon(new ImageIcon(getClass().getResource("/resources/checkbox.png")));
			checkBox13.setSelectedIcon(new ImageIcon(getClass().getResource("/resources/checkbox-selected.png")));
			checkBox13.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					checkBox13ActionPerformed(e);
				}
			});
			nmz.add(checkBox13, CC.xy(2, 10));

			//---- button12 ----
			button12.setText("Save & Close");
			button12.setBackground(new Color(51, 102, 255));
			button12.setMaximumSize(new Dimension(125, 23));
			button12.setMinimumSize(new Dimension(125, 23));
			button12.setPreferredSize(new Dimension(150, 23));
			button12.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button12ActionPerformed(e);
				}
			});
			nmz.add(button12, CC.xywh(1, 14, 2, 1, CC.CENTER, CC.DEFAULT));
		}
		add(nmz);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		  File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	      if (forumInfo.exists()) {
	    	  if (FileUtils.load(Constants.SETTING_FILE_NAME, "enablepilehelper") == null || FileUtils.load(Constants.SETTING_FILE_NAME, "pilehelpercombat") == null){
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "enablepilehelper", "true");
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "pilehelpercombat", "true");
	    	  } else {
	    		  Boolean b = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "enablepilehelper"));
	    		  Boolean a = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "pilehelpercombat"));
	    		  config.enablepilehelper(b);
	    		  checkBox2.setSelected(b);
	    		  config.pilehelpercombat(a);
	    		  checkBox1.setSelected(a);
	    	  }
	    	  
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawfriends") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawfriends", "false");
	    	  } else {
	    		  Boolean c = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawfriends"));
	    		  config.drawfriends(c);
	    		  checkBox4.setSelected(c);
	    	  }
	    	  
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "defaultworldindex") == null) {
	    		  comboBox1.setSelectedIndex(48);
	    	  } else {
	    		  comboBox1.setSelectedIndex(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "defaultworldindex")));
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawOpponentInfo") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawOpponentInfo", "true");
	    	  } else {
	    		  Boolean d = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawOpponentInfo"));
	    		  config.drawOpponentInfo(d);
	    		  checkBox3.setSelected(d);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "setSpecBar") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "setSpecBar", "false");
	    	  } else {
	    		  Boolean f = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "setSpecBar"));
	    		  config.setSpecBar(f);
	    		  checkBox5.setSelected(f);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawFPS") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawFPS", "false");
	    	  } else {
	    		  Boolean g = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawFPS"));
	    		  config.drawFPS(g);
	    		  checkBox6.setSelected(g);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "rememberme") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "rememberme", "true");
	    	  } else {
	    		  Boolean h = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "rememberme"));
	    		  config.rememberme(h);
	    		  checkBox7.setSelected(h);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawGroundItems") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawGroundItems", "false");
	    	  } else {
	    		  Boolean i = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawGroundItems"));
	    		  config.drawGroundItems(i);
	    		  checkBox10.setSelected(i);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawoverhead") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawoverhead", "true");
	    	  } else {
	    		  Boolean j = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawoverhead"));
	    		  config.drawoverhead(j);
	    		  checkBox8.setSelected(j);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawpilehelperprays") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawpilehelperprays", "true");
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawpilehelperprays"));
	    		  config.drawpilehelperprays(k);
	    		  checkBox8.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "recordLocation") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "recordLocation", FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\VR Videos");
	    	      config.recordLocation(FileSystemView.getFileSystemView().getDefaultDirectory().getPath() + "\\VR Videos");
	    	      button11.setText(config.recordLocation);
	    	  } else {
	    		  config.recordLocation(FileUtils.load(Constants.SETTING_FILE_NAME, "recordLocation"));
	    		  button11.setText(config.recordLocation);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "recordHotKey") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "recordHotKey", "117");
	    	      config.recordHotKey(117);
	    	      textField1.setText(KeyEvent.getKeyText(117));
	    	  } else {
	    		  config.recordHotKey(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "recordHotKey")));
	    		  textField1.setText(KeyEvent.getKeyText(config.recordHotKey));
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawNightMareZone") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawNightMareZone", "true");
	    		  checkBox15.setSelected(true);
	    		  config.drawNightMareZone(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawNightMareZone"));
	    		  config.drawNightMareZone(k);
	    		  checkBox15.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "overloadAlert") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "overloadAlert", "true");
	    		  checkBox11.setSelected(true);
	    		  config.overloadAlert(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "overloadAlert"));
	    		  config.overloadAlert(k);
	    		  checkBox11.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "absorbAlert") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "absorbAlert", "true");
	    		  checkBox12.setSelected(true);
	    		  config.absorbAlert(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "absorbAlert"));
	    		  config.absorbAlert(k);
	    		  checkBox12.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawRapidHeal") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawRapidHeal", "true");
	    		  checkBox13.setSelected(true);
	    		  config.drawRapidHeal(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawRapidHeal"));
	    		  config.drawRapidHeal(k);
	    		  checkBox13.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawToolTip") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawToolTip", "false");
	    		  checkBox14.setSelected(false);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawToolTip"));
	    		  config.drawToolTip(k);
	    		  checkBox14.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawBoosts") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawBoosts", "true");
	    		  checkBox16.setSelected(true);
	    		  config.drawBoosts(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawBoosts"));
	    		  config.drawBoosts(k);
	    		  checkBox16.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawJewelry") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawJewelry", "true");
	    		  checkBox4.setSelected(true);
	    		  config.drawJewelry(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawJewelry"));
	    		  config.drawJewelry(k);
	    		  checkBox4.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "overlayOpacity") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "overlayOpacity", "240");
	    	      config.overlayOpacity(240);
	    	      slider1.setValue(240);
	    	  } else {
	    		  config.overlayOpacity(Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "overlayOpacity")));
	    		  slider1.setValue(config.overlayOpacity);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawDeath") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeath", "true");
	    		  checkBox17.setSelected(true);
	    		  config.drawDeath(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawDeath"));
	    		  config.drawDeath(k);
	    		  checkBox17.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawDeathTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawDeathTimer", "true");
	    		  checkBox18.setSelected(true);
	    		  config.drawDeathTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawDeathTimer"));
	    		  config.drawDeathTimer(k);
	    		  checkBox18.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawBossTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawBossTimer", "true");
	    		  checkBox19.setSelected(true);
	    		  config.drawBossTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawBossTimer"));
	    		  config.drawBossTimer(k);
	    		  checkBox19.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawStaminaTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawStaminaTimer", "true");
	    		  checkBox20.setSelected(true);
	    		  config.drawStaminaTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawStaminaTimer"));
	    		  config.drawStaminaTimer(k);
	    		  checkBox20.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawTbTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawTbTimer", "true");
	    		  checkBox21.setSelected(true);
	    		  config.drawTbTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawTbTimer"));
	    		  config.drawTbTimer(k);
	    		  checkBox21.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawAfTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawAfTimer", "true");
	    		  checkBox22.setSelected(true);
	    		  config.drawAfTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawAfTimer"));
	    		  config.drawAfTimer(k);
	    		  checkBox22.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawSafTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawSafTimer", "true");
	    		  checkBox23.setSelected(true);
	    		  config.drawSafTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawSafTimer"));
	    		  config.drawSafTimer(k);
	    		  checkBox23.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawHomeTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawHomeTimer", "true");
	    		  checkBox24.setSelected(true);
	    		  config.drawHomeTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawHomeTimer"));
	    		  config.drawHomeTimer(k);
	    		  checkBox24.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawImbueTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbueTimer", "true");
	    		  checkBox25.setSelected(true);
	    		  config.drawImbueTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawImbueTimer"));
	    		  config.drawImbueTimer(k);
	    		  checkBox25.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawVengTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawVengTimer", "true");
	    		  checkBox26.setSelected(true);
	    		  config.drawVengTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawVengTimer"));
	    		  config.drawVengTimer(k);
	    		  checkBox26.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawOverloadTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawOverloadTimer", "true");
	    		  checkBox27.setSelected(true);
	    		  config.drawOverloadTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawOverloadTimer"));
	    		  config.drawOverloadTimer(k);
	    		  checkBox27.setSelected(k);
	    	  }
	    	  if(FileUtils.load(Constants.SETTING_FILE_NAME, "drawImbheartTimer") == null) {
	    		  FileUtils.save(Constants.SETTING_FILE_NAME, "drawImbheartTimer", "true");
	    		  checkBox28.setSelected(true);
	    		  config.drawImbheartTimer(true);
	    	  } else {
	    		  Boolean k = Boolean.parseBoolean(FileUtils.load(Constants.SETTING_FILE_NAME, "drawImbheartTimer"));
	    		  config.drawImbheartTimer(k);
	    		  checkBox28.setSelected(k);
	    	  }
	      }
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	private JPanel panel1;
	private JPanel panelheader;
	private JLabel skilllabel;
	private JPanel panel3;
	private JButton button1;
	private JButton button5;
	private JButton button15;
	private JButton button7;
	private JButton button2;
	private JButton button9;
	private JButton button13;
	private JPanel general;
	private JLabel label4;
	private JSeparator separator2;
	private JLabel label6;
	private JComboBox<String> comboBox1;
	private JLabel label12;
	private JCheckBox checkBox7;
	private JLabel label27;
	private JSlider slider1;
	private JButton button4;
	private JPanel combat;
	private JLabel label8;
	private JSeparator separator3;
	private JLabel label9;
	private JCheckBox checkBox3;
	private JLabel label14;
	private JCheckBox checkBox8;
	private JLabel label10;
	private JCheckBox checkBox5;
	private JLabel label26;
	private JCheckBox checkBox16;
	private JLabel label28;
	private JCheckBox checkBox17;
	private JButton button6;
	private JPanel timeroverlay;
	private JLabel label29;
	private JSeparator separator7;
	private JLabel label30;
	private JCheckBox checkBox18;
	private JLabel label31;
	private JCheckBox checkBox19;
	private JLabel label32;
	private JCheckBox checkBox20;
	private JLabel label33;
	private JCheckBox checkBox21;
	private JLabel label34;
	private JCheckBox checkBox22;
	private JLabel label35;
	private JCheckBox checkBox23;
	private JLabel label36;
	private JCheckBox checkBox24;
	private JLabel label37;
	private JCheckBox checkBox25;
	private JLabel label38;
	private JCheckBox checkBox26;
	private JLabel label39;
	private JCheckBox checkBox27;
	private JLabel label40;
	private JButton button14;
	private JCheckBox checkBox28;
	private JPanel textoverlay;
	private JLabel label13;
	private JSeparator separator4;
	private JLabel label11;
	private JCheckBox checkBox6;
	private JLabel label17;
	private JCheckBox checkBox10;
	private JLabel label2;
	private JCheckBox checkBox14;
	private JLabel label7;
	private JCheckBox checkBox4;
	private JButton button8;
	private JPanel pilehelper;
	private JLabel label1;
	private JSeparator separator1;
	private JLabel label3;
	private JCheckBox checkBox2;
	private JLabel label5;
	private JCheckBox checkBox1;
	private JLabel label15;
	private JCheckBox checkBox9;
	private JButton button3;
	private JPanel recording;
	private JLabel label16;
	private JSeparator separator5;
	private JLabel label18;
	private JButton button11;
	private JLabel label19;
	private JTextField textField1;
	private JLabel label20;
	private JButton button10;
	private JPanel nmz;
	private JLabel label21;
	private JSeparator separator6;
	private JLabel label25;
	private JCheckBox checkBox15;
	private JLabel label22;
	private JCheckBox checkBox11;
	private JLabel label23;
	private JCheckBox checkBox12;
	private JLabel label24;
	private JCheckBox checkBox13;
	private JButton button12;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
}
