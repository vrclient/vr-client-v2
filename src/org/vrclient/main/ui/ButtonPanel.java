package org.vrclient.main.ui;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.text.SimpleDateFormat;


import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.vrclient.main.Configuration;
import org.vrclient.main.utils.ScreenRecorderSettings;

/**
 * Created by VR on 7/29/2014.
 */
public class ButtonPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3147770371892729345L;
	private final Color color = new Color(77, 77, 77);
	

	private Button screenshotButton, settingsButton, sidebarButton, onTopButton, galleryButton;
	
	JButton recordButton;
	JButton configButton;
	private PopupMenu menu;
	ScreenRecorder screenRecorder;
	public Rectangle captureSize;
	SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	public DateTime start;
	DateTime now;
	Timer SimpleTimer;
	String title;
	//private Main main;
	
	
	private final Configuration config = Configuration.getInstance();

	public ButtonPanel() {
		
		this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		this.setBorder(new EmptyBorder(5,0,5,0));
		menu = new PopupMenu();
		screenshotButton = new Button("camera.png");
		screenshotButton.setButtonHoverIcon("camera_hover.png");
		screenshotButton.setToolTipText("Take a Screenshot");
		screenshotButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Screenshotmanager.takeScreeny();
			}
		});
		add(screenshotButton);
		
		onTopButton = new Button("ontop.png");
		onTopButton.setButtonHoverIcon("ontop_hover.png");
		onTopButton.setToolTipText("Put the client onTop.");
		onTopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				config.enableonTop(!config.enableonTop());
				config.getBotFrame().setAlwaysOnTop(config.enableonTop());
				onTopButton.setButtonIcon(config.enableonTop() ? "ontop-selected.png" : "ontop.png");
				onTopButton.setButtonHoverIcon(config.enableonTop() ? "ontop-selected_hover.png" : "ontop_hover.png");
				onTopButton.revalidate();
				
			}
		});
		add(onTopButton);
		
		
		galleryButton = new Button("gallery.png");
		galleryButton.setButtonHoverIcon("gallery_hover.png");
		galleryButton.setToolTipText("Screenshot Manager");
		galleryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				config.enableonTop(false);
				config.getBotFrame().setAlwaysOnTop(config.enableonTop());
				onTopButton.setButtonIcon(config.enableonTop() ? "ontop-selected.png" : "ontop.png");
				onTopButton.revalidate();
				new Screenshotmanager();
				Screenshotmanager.frame1.setVisible(true);
				
			}
		});
		add(galleryButton);
		
		
		recordButton = new JButton("Record");
		recordButton.setContentAreaFilled(false);
		recordButton.setBorderPainted(false);
		recordButton.setOpaque(false);
		recordButton.setIcon(new ImageIcon(getClass().getResource("/resources/record.png")));
		recordButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				recordButton.setContentAreaFilled(true);
				recordButton.setBorderPainted(true);
				recordButton.setOpaque(true);
				
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				recordButton.setContentAreaFilled(false);
				recordButton.setBorderPainted(false);
				recordButton.setOpaque(false);
				
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		recordButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RecordVideo();
			}
		});
		add(recordButton);
		
		
		add(Box.createHorizontalGlue());
		
		
		settingsButton = new Button("settings.png");
		settingsButton.setButtonHoverIcon("settings_hover.png");
		settingsButton.setToolTipText("Display the client settings.");
		settingsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JButton button = (JButton) e.getSource();
				menu.show(ButtonPanel.this, button.getX(), button.getY());
			}
		});
		add(settingsButton);
		
		sidebarButton = new Button("expand.png");
		sidebarButton.setButtonHoverIcon("expand_hover.png");
		sidebarButton.setToolTipText("Expand/Collapse Sidebar");
		sidebarButton.addActionListener(new ActionListener() {
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean maximized = (Configuration.getInstance().getBotFrame().getExtendedState() & Frame.MAXIMIZED_HORIZ) != 0;
                Configuration.getInstance().getBotFrame().getSidePanel().setVisible(!Configuration.getInstance().getBotFrame().getSidePanel().isVisible());
                //Configuration.getInstance().getBotFrame().resize();
                
                int newWidth = Configuration.getInstance().getBotFrame().getWidth();
                if (Configuration.getInstance().getBotFrame().getSidePanel().isVisible()) {
                	if (!maximized) {
                        newWidth += Configuration.getInstance().getBotFrame().getSidePanel().getWidth();
                    }
                    
                } else {
                	
                    if (!maximized) {
                    	
                        newWidth -= Configuration.getInstance().getBotFrame().getSidePanel().getWidth();
                    } else {
                    	
                    }
                    
                    
                }
                
                Configuration.getInstance().getBotFrame().setSize(new Dimension(newWidth, Configuration.getInstance().getBotFrame().getHeight()));
                //Configuration.getInstance().getBotFrame().resize();
                //config.getBotFrame().resizemap();
				sidebarButton.setButtonIcon(config.enableSidebar() ? "expand.png" : "collapse.png");
				sidebarButton.setButtonHoverIcon(config.enableSidebar() ? "expand_hover.png" : "collapse_hover.png");
				sidebarButton.revalidate();
			}
		});
		add(sidebarButton);
	}

	@Override
	public void paintComponent(Graphics g) {
		final Graphics2D graphics2D = (Graphics2D) g;
		graphics2D.setPaint(color);
		g.fillRect(0, 0, getWidth(), getHeight());
	}
	
	
	public void startRecording() throws Exception
    {    
		File file;
		
		if(config.recordLocation == null) {
			file = new File(config.recordLocation.replace("C\\:", "C:"));
		} else {
			file = new File(config.recordLocation.replace("C\\:", "C:"));
		}
		
        
        Dimension screenSize = Configuration.getInstance().getBotFrame().loader.getSize();
        int width = screenSize.width;
        int height = screenSize.height;
                      
        captureSize = new Rectangle(Configuration.getInstance().getBotFrame().loader.getLocationOnScreen().x+1,Configuration.getInstance().getBotFrame().loader.getLocationOnScreen().y+1, width-2, height-2);
                      
      GraphicsConfiguration gc = GraphicsEnvironment
         .getLocalGraphicsEnvironment()
         .getDefaultScreenDevice()
         .getDefaultConfiguration();

     this.screenRecorder = new ScreenRecorderSettings(gc, captureSize,
         new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
         new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
              CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
              DepthKey, 24, FrameRateKey, Rational.valueOf(60),
              QualityKey, 1.0f,
              KeyFrameIntervalKey, 50 * 60),
        null,
         null, file, "VR-Video");

    this.screenRecorder.start();
    }

    public void stopRecording() throws Exception
    {
      this.screenRecorder.stop();
    }
    
   
    
    public void RecordVideo() {
    	if(!config.enablerecord()) {
			try {
				start = new DateTime();
				SimpleTimer = new Timer(1000, new ActionListener(){
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	DateTime now = new DateTime();
				    	Period period = new Period(start, now);
						PeriodFormatter HHMMSSFormater = new PeriodFormatterBuilder()
						        .printZeroAlways()
						        .minimumPrintedDigits(2)
						        .appendHours().appendSeparator(":")
						        .appendMinutes().appendSeparator(":")
						        .appendSeconds()
						        .toFormatter(); // produce thread-safe formatter
						
				        recordButton.setText(String.valueOf(HHMMSSFormater.print(period)));
				       
				    }
				});
				SimpleTimer.start();
				startRecording();
				config.enablerecord(true);
				config.setRecordingrect(Configuration.getInstance().getBotFrame().loader.getBounds());
				recordButton.setBackground(Color.RED);
			} catch (Exception e1) {
				
				e1.printStackTrace();
			} 
		} else {
			try {
				stopRecording();
				config.enablerecord(false);
				recordButton.setBackground(null);
				SimpleTimer.stop();
				recordButton.setText("Record");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
		}
    }
	
	
}