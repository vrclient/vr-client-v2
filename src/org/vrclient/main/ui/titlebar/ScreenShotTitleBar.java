package org.vrclient.main.ui.titlebar;

import javax.swing.*;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.ui.Button;
import org.vrclient.main.ui.PopupMenu;
import org.vrclient.main.ui.Screenshotmanager;
import org.vrclient.main.ui.login.misc.User;
import org.vrclient.main.utils.ComponentMover;
import org.vrclient.main.utils.ScreenRecorderSettings;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;

/**
 */
@SuppressWarnings("serial")
public class ScreenShotTitleBar extends JPanel {

    public static final Color BACKGROUND_COLOR = new Color(57, 57, 57);
    JButton close;
	public JButton maximize;
	JButton minimize;
    
    ScreenRecorder screenRecorder;
	public Rectangle captureSize;
	SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	public DateTime start;
	DateTime now;
	Timer SimpleTimer;
	
    public ScreenShotTitleBar(JFrame parent) {
        
        setPreferredSize(new Dimension(parent.getWidth(), 30));
        setBackground(BACKGROUND_COLOR);
        
        setLayout(new BorderLayout());
        
        JPanel rButtons = new JPanel();
        FlowLayout layout = new FlowLayout();
        layout.setVgap(5);
        layout.setHgap(5);
        rButtons.setLayout(layout);
        rButtons.setBorder(null);
        rButtons.getInsets().set(0, 0, 0, 0);
        rButtons.setBackground(BACKGROUND_COLOR);
        
       
       
        JLabel title = new JLabel("ScreenShots");
        title.setIcon(new ImageIcon(getClass().getResource("/resources/icon.png")));
        title.setFont(title.getFont().deriveFont(Font.BOLD).deriveFont(12f));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setForeground(Color.WHITE);
        title.setOpaque(false);
        rButtons.add(title);
        
        add(rButtons, BorderLayout.WEST);
        
        FlowLayout layout2 = new FlowLayout();
        layout2.setVgap(5);
        layout2.setHgap(0);
        
        JPanel allbuttons = new JPanel();
        allbuttons.setLayout(new BoxLayout(allbuttons, BoxLayout.X_AXIS));
        allbuttons.setBorder(null);
        allbuttons.getInsets().set(0, 0, 0, 0);
        
        JPanel lButtons1 = new JPanel();
        lButtons1.setLayout(layout2);
        lButtons1.setBorder(null);
        lButtons1.getInsets().set(0, 0, 0, 0);
        lButtons1.setOpaque(false);
        lButtons1.setBackground(BACKGROUND_COLOR);
        
        JPanel lButtons2 = new JPanel();
        lButtons2.setLayout(layout2);
        lButtons2.setBorder(null);
        lButtons2.getInsets().set(0, 0, 0, 0);
        lButtons2.setOpaque(false);
        lButtons2.setBackground(BACKGROUND_COLOR);
        
        minimize = new JButton();
        minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize.png")));
        minimize.setContentAreaFilled(false);
        minimize.setBorderPainted(false);
        minimize.setOpaque(false);
        minimize.setFocusable(false);
        minimize.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize.png")));
			}
        });
        minimize.addActionListener((evt) -> Screenshotmanager.frame1.setState(Frame.ICONIFIED));
        lButtons2.add(minimize);

        maximize = new JButton();
        maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize.png")));
        maximize.setContentAreaFilled(false);
        maximize.setBorderPainted(false);
        maximize.setOpaque(false);
        maximize.setFocusable(false);
        maximize.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize.png")));
			}
        });
        maximize.addActionListener((evt) -> Screenshotmanager.toggleMaximized());
        lButtons2.add(maximize);
       
        close = new JButton();
        close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close.png")));
        close.setContentAreaFilled(false);
        close.setBorderPainted(false);
        close.setOpaque(false);
        close.setFocusable(false);
        close.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close.png")));
			}
        });
        close.addActionListener((evt) -> Screenshotmanager.frame1.dispose());
        lButtons2.add(close);

        allbuttons.add(lButtons1);
        allbuttons.add(Box.createHorizontalStrut(25));
        allbuttons.add(lButtons2);
        add(allbuttons, BorderLayout.EAST);
        addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==2){
                	Screenshotmanager.toggleMaximized();
                }
            }
        });
        ComponentMover cm = new ComponentMover(Screenshotmanager.frame1, this);
        
    }
    
   
    
   

}
