package org.vrclient.main.ui.titlebar;

import javax.swing.*;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.monte.media.Format;
import org.monte.media.FormatKeys.MediaType;
import org.monte.media.math.Rational;
import org.monte.screenrecorder.ScreenRecorder;
import org.vrclient.component.RSCanvas;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.ui.Button;
import org.vrclient.main.ui.PopupMenu;
import org.vrclient.main.ui.Screenshotmanager;
import org.vrclient.main.ui.login.misc.User;
import org.vrclient.main.utils.ComponentMover;
import org.vrclient.main.utils.ScreenRecorderSettings;

import static org.monte.media.FormatKeys.EncodingKey;
import static org.monte.media.FormatKeys.FrameRateKey;
import static org.monte.media.FormatKeys.KeyFrameIntervalKey;
import static org.monte.media.FormatKeys.MIME_AVI;
import static org.monte.media.FormatKeys.MediaTypeKey;
import static org.monte.media.FormatKeys.MimeTypeKey;
import static org.monte.media.VideoFormatKeys.CompressorNameKey;
import static org.monte.media.VideoFormatKeys.DepthKey;
import static org.monte.media.VideoFormatKeys.ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE;
import static org.monte.media.VideoFormatKeys.QualityKey;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;

/**
 */
@SuppressWarnings("serial")
public class FrameTitleBar extends JPanel {

    public static final Color BACKGROUND_COLOR = new Color(57, 57, 57);
    JButton close;
	public JButton maximize;
	JButton minimize;
    private Button screenshotButton, settingsButton, sidebarButton, onTopButton, galleryButton, recordButton;
    private Configuration config = Configuration.getInstance();
    private PopupMenu menu;
    ScreenRecorder screenRecorder;
	public Rectangle captureSize;
	SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
	public DateTime start;
	DateTime now;
	Timer SimpleTimer;
	private static RSCanvas canvas;
	
    public FrameTitleBar(JFrame parent) {
        
        setPreferredSize(new Dimension(parent.getWidth(), 30));
        setBackground(BACKGROUND_COLOR);
        
        setLayout(new BorderLayout());
        menu = new PopupMenu();
        JPanel rButtons = new JPanel();
        FlowLayout layout = new FlowLayout();
        layout.setVgap(5);
        layout.setHgap(5);
        rButtons.setLayout(layout);
        rButtons.setBorder(null);
        rButtons.getInsets().set(0, 0, 0, 0);
        rButtons.setBackground(BACKGROUND_COLOR);
        
       
       
        JLabel title = new JLabel(Constants.CLIENT_TITLE + " [ " + User.getDisplayName() + " ]");
        title.setIcon(new ImageIcon(getClass().getResource("/resources/icon.png")));
        title.setFont(title.getFont().deriveFont(Font.BOLD).deriveFont(12f));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setForeground(Color.WHITE);
        title.setOpaque(false);
        rButtons.add(title);
        
        JLabel version = new JLabel("v. " + Constants.CLIENT_VERSION);
        version.setFont(title.getFont().deriveFont(Font.BOLD).deriveFont(11f));
        rButtons.add(version);
        add(rButtons, BorderLayout.WEST);
        
        FlowLayout layout2 = new FlowLayout();
        layout2.setVgap(5);
        layout2.setHgap(0);
        
        JPanel allbuttons = new JPanel();
        allbuttons.setLayout(new BoxLayout(allbuttons, BoxLayout.X_AXIS));
        allbuttons.setBorder(null);
        allbuttons.getInsets().set(0, 0, 0, 0);
        
        JPanel lButtons1 = new JPanel();
        lButtons1.setLayout(layout2);
        lButtons1.setBorder(null);
        lButtons1.getInsets().set(0, 0, 0, 0);
        lButtons1.setOpaque(false);
        lButtons1.setBackground(BACKGROUND_COLOR);
        
        JPanel lButtons2 = new JPanel();
        lButtons2.setLayout(layout2);
        lButtons2.setBorder(null);
        lButtons2.getInsets().set(0, 0, 0, 0);
        lButtons2.setOpaque(false);
        lButtons2.setBackground(BACKGROUND_COLOR);
        
        
        
        screenshotButton = new Button("camera.png");
		screenshotButton.setButtonHoverIcon("camera_hover.png");
		screenshotButton.setToolTipText("Take a Screenshot");
		screenshotButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Screenshotmanager.takeScreeny();
			}
		});
		lButtons1.add(screenshotButton);
		galleryButton = new Button("gallery.png");
		galleryButton.setButtonHoverIcon("gallery_hover.png");
		galleryButton.setToolTipText("Screenshot Manager");
		galleryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				config.enableonTop(false);
				config.getBotFrame().setAlwaysOnTop(config.enableonTop());
				onTopButton.setButtonIcon(config.enableonTop() ? "ontop-selected.png" : "ontop.png");
				onTopButton.revalidate();
				new Screenshotmanager();
				Screenshotmanager.frame1.setVisible(true);
				
			}
		});
		lButtons1.add(galleryButton);
		recordButton = new Button("record.png");
		recordButton.setButtonHoverIcon("record_hover.png");
		recordButton.setToolTipText("Record Video");		recordButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RecordVideo();
			}
		});
		lButtons1.add(recordButton);
		onTopButton = new Button("ontop.png");
		onTopButton.setButtonHoverIcon("ontop_hover.png");
		onTopButton.setToolTipText("Put the client onTop.");
		onTopButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				config.enableonTop(!config.enableonTop());
				config.getBotFrame().setAlwaysOnTop(config.enableonTop());
				onTopButton.setButtonIcon(config.enableonTop() ? "ontop-selected.png" : "ontop.png");
				onTopButton.setButtonHoverIcon(config.enableonTop() ? "ontop-selected_hover.png" : "ontop_hover.png");
				onTopButton.revalidate();
				
			}
		});
		lButtons1.add(onTopButton);
		settingsButton = new Button("settings.png");
		settingsButton.setButtonHoverIcon("settings_hover.png");
		settingsButton.setToolTipText("Display the client settings.");
		settingsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JButton button = (JButton) e.getSource();
				menu.show(settingsButton, button.getX(), button.getY());
			}
		});
		lButtons1.add(settingsButton);
		
		sidebarButton = new Button("expand.png");
		sidebarButton.setButtonHoverIcon("expand_hover.png");
		sidebarButton.setToolTipText("Expand/Collapse Sidebar");
		sidebarButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				moveSideBar();
			}
		});
		lButtons1.add(sidebarButton);
		
		
		
        minimize = new JButton();
        minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize.png")));
        minimize.setContentAreaFilled(false);
        minimize.setBorderPainted(false);
        minimize.setOpaque(false);
        minimize.setFocusable(false);
        minimize.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				minimize.setIcon(new ImageIcon(getClass().getResource("/resources/title/minimize.png")));
			}
        });
        minimize.addActionListener((evt) -> config.getBotFrame().setState(Frame.ICONIFIED));
        lButtons2.add(minimize);

        maximize = new JButton();
        maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize.png")));
        maximize.setContentAreaFilled(false);
        maximize.setBorderPainted(false);
        maximize.setOpaque(false);
        maximize.setFocusable(false);
        maximize.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				maximize.setIcon(new ImageIcon(getClass().getResource("/resources/title/maximize.png")));
			}
        });
        maximize.addActionListener((evt) -> config.getBotFrame().toggleMaximized());
        lButtons2.add(maximize);
       
        close = new JButton();
        close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close.png")));
        close.setContentAreaFilled(false);
        close.setBorderPainted(false);
        close.setOpaque(false);
        close.setFocusable(false);
        close.addMouseListener(new MouseAdapter() {
        	@Override
            public void mouseEntered(MouseEvent arg) {
        		close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close-hover.png")));
        	}

			@Override
			public void mouseExited(MouseEvent arg0) {
				close.setIcon(new ImageIcon(getClass().getResource("/resources/title/close.png")));
			}
        });
        close.addActionListener((evt) -> config.getBotFrame().SystemClose());
        lButtons2.add(close);

        allbuttons.add(lButtons1);
        allbuttons.add(Box.createHorizontalStrut(25));
        allbuttons.add(lButtons2);
        add(allbuttons, BorderLayout.EAST);
        addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){
                if(e.getClickCount()==2){
                	config.getBotFrame().toggleMaximized();
                }
            }
        });
        ComponentMover cm = new ComponentMover(parent, this);
        
    }
    public void startRecording() throws Exception
    {    
		File file;
		
		if(config.recordLocation == null) {
			file = new File(config.recordLocation.replace("C\\:", "C:"));
		} else {
			file = new File(config.recordLocation.replace("C\\:", "C:"));
		}
		
		
        Dimension screenSize = config.getCanvas().getSize();
        int width = screenSize.width;
        int height = screenSize.height;
                      
        captureSize = new Rectangle(Configuration.getInstance().getBotFrame().loader.getLocationOnScreen().x+1+((config.getBotFrame().loader.getWidth() - config.getCanvas().getWidth())/2),Configuration.getInstance().getBotFrame().loader.getLocationOnScreen().y+1, width-2, height-2);
                      
      GraphicsConfiguration gc = GraphicsEnvironment
         .getLocalGraphicsEnvironment()
         .getDefaultScreenDevice()
         .getDefaultConfiguration();

     this.screenRecorder = new ScreenRecorderSettings(gc, captureSize,
         new Format(MediaTypeKey, MediaType.FILE, MimeTypeKey, MIME_AVI),
         new Format(MediaTypeKey, MediaType.VIDEO, EncodingKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
              CompressorNameKey, ENCODING_AVI_TECHSMITH_SCREEN_CAPTURE,
              DepthKey, 24, FrameRateKey, Rational.valueOf(60),
              QualityKey, 1.0f,
              KeyFrameIntervalKey, 50 * 60),
         null,
         null, file, "VR-Video");
    start = new DateTime();
    this.screenRecorder.start();
    }

    public void stopRecording() throws Exception
    {
      this.screenRecorder.stop();
    }
    
   
    
    public void RecordVideo() {
    	if(!config.enablerecord()) {
			try {
				start = new DateTime();
				SimpleTimer = new Timer(1000, new ActionListener(){
				    @Override
				    public void actionPerformed(ActionEvent e) {
				    	DateTime now = new DateTime();
				    	Period period = new Period(start, now);
						PeriodFormatter HHMMSSFormater = new PeriodFormatterBuilder()
						        .printZeroAlways()
						        .minimumPrintedDigits(2)
						        .appendHours().appendSeparator(":")
						        .appendMinutes().appendSeparator(":")
						        .appendSeconds()
						        .toFormatter(); // produce thread-safe formatter
						
				        recordButton.setText(String.valueOf(HHMMSSFormater.print(period)));
				       
				    }
				});
				SimpleTimer.start();
				startRecording();
				config.enablerecord(true);
				recordButton.setForeground(Color.RED);
				recordButton.setIcon(new ImageIcon(getClass().getResource("/resources/record_on.png")));
				recordButton.setButtonHoverIcon("record_on_hover.png");
			} catch (Exception e1) {
				
				e1.printStackTrace();
			} 
		} else {
			try {
				stopRecording();
				config.enablerecord(false);
				recordButton.setForeground(null);
				recordButton.setText("");
				recordButton.setIcon(new ImageIcon(getClass().getResource("/resources/record.png")));
				recordButton.setButtonHoverIcon("record_hover.png");
				SimpleTimer.stop();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
		}
    }
   
    public void moveSideBar() {
    	boolean maximized = (Configuration.getInstance().getBotFrame().getExtendedState() & Frame.MAXIMIZED_HORIZ) != 0;
        Configuration.getInstance().getBotFrame().getSidePanel().setVisible(!Configuration.getInstance().getBotFrame().getSidePanel().isVisible());
        
        int newWidth = Configuration.getInstance().getBotFrame().getWidth();
        if (Configuration.getInstance().getBotFrame().getSidePanel().isVisible()) {
        	if (!maximized) {
                newWidth += Configuration.getInstance().getBotFrame().getSidePanel().getWidth();
            }
        	Configuration.getInstance().getBotFrame().setMinimumSize(new Dimension(1023, 535));
        } else {
        	
            if (!maximized) {
            	
                newWidth -= Configuration.getInstance().getBotFrame().getSidePanel().getWidth();
            } else {
            	
            }
            Configuration.getInstance().getBotFrame().setMinimumSize(new Dimension(769, 535));
            
        }
        
        Configuration.getInstance().getBotFrame().setSize(new Dimension(newWidth, Configuration.getInstance().getBotFrame().getHeight()));
        
        if(!config.getBotFrame().getSidePanel().isVisible()){
        	sidebarButton.setButtonIcon("collapse.png");
        	sidebarButton.setButtonHoverIcon("collapse_hover.png");
        } else {
        	sidebarButton.setButtonIcon("expand.png");
        	sidebarButton.setButtonHoverIcon("expand_hover.png");
        }
       sidebarButton.revalidate();
    }
   

}
