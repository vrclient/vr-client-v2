package org.vrclient.main.ui;
 
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.colorchooser.*;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;
 
/* ColorChooserDemo.java requires no other files. */
public class ColorPickerbinder extends JPanel
                              implements ChangeListener {
 
    protected JColorChooser tcc;
    protected JLabel banner;
    protected JButton save;
 
    public ColorPickerbinder() {
    	super(new BorderLayout());
        if (FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color") == null) {
    		FileUtils.save(Constants.SETTING_FILE_NAME, "binder2color", "15ff00");
    	}
        
        
        
        //Set up the banner at the top of the window
        
        banner = new JLabel("Violent Resolution",
                            JLabel.CENTER);
        banner.setForeground(Utilities.hex2rgb("#" + FileUtils.load(Constants.SETTING_FILE_NAME, "binder2color")));
        banner.setBackground(Color.white);
        banner.setOpaque(true);
        banner.setFont(new Font("SansSerif", Font.BOLD, 24));
        banner.setPreferredSize(new Dimension(100, 65));
 
        JPanel bannerPanel = new JPanel(new BorderLayout());
        bannerPanel.add(banner, BorderLayout.CENTER);
        
        bannerPanel.setBorder(BorderFactory.createTitledBorder("Settings"));
 
        //Set up color chooser for setting text color
        tcc = new JColorChooser(banner.getForeground());
        tcc.getSelectionModel().addChangeListener(this);
        tcc.setBorder(BorderFactory.createTitledBorder(
                                             "Choose Text Color"));
        
        add(bannerPanel, BorderLayout.CENTER);
        add(tcc, BorderLayout.PAGE_END);
    }
 
    public void stateChanged(ChangeEvent e) {
        Color newColor = tcc.getColor();
        banner.setForeground(newColor);
        FileUtils.save(Constants.SETTING_FILE_NAME, "binder2color", String.format("%02X%02X%02X", newColor.getRed(), newColor.getGreen(), newColor.getBlue()));
        Configuration.getInstance().getBotFrame().getBinderPanel().button1.setBackground(newColor);
    }
    
    
 
    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    
    private static void createAndShowGUI() {
    	
    	
    	
        //Create and set up the window.
        final JFrame frame = new JFrame("BinderList Color Chooser");
        frame.setLocationRelativeTo(null);
        frame.addWindowListener(new WindowListener() {
            //I skipped unused callbacks for readability

            @Override
            public void windowClosing(WindowEvent e) {
                
                    frame.setVisible(false);
                    frame.dispose();
                
            }

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
        });
 
        //Create and set up the content pane.
        JComponent newContentPane = new ColorPickerbinder();
        newContentPane.setOpaque(true); //content panes must be opaque
        frame.setContentPane(newContentPane);
        frame.setLocationRelativeTo(null);
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
 
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}