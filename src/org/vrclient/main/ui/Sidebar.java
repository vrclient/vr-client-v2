package org.vrclient.main.ui;

import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.JPanel;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.ui.sidebar.BinderPanel;
import org.vrclient.main.ui.sidebar.CCPanel;
import org.vrclient.main.ui.sidebar.CallerPanel;
import org.vrclient.main.ui.sidebar.SettingsPanel;
import org.vrclient.main.ui.sidebar.SkillPanel;
import org.vrclient.main.ui.sidebar.SniperPanel;
import org.vrclient.main.ui.sidebar.SpammerPanel;
import org.vrclient.main.ui.sidebar.SplitterUI;


public class Sidebar extends JTabbedPane {
	private final CallerPanel CallerPanel = new CallerPanel();
    private final SniperPanel SniperPanel = new SniperPanel();
    private final BinderPanel BinderPanel = new BinderPanel();
    private final SkillPanel SkillPanel = new SkillPanel();
  
    private final CCPanel CCPanel = new CCPanel();
    private final SpammerPanel SpammerPanel = new SpammerPanel();
    private final SettingsPanel SettingsPanel = new SettingsPanel();
	public Sidebar() {
		setTabPlacement(SwingConstants.RIGHT);
		setBorder(null);
		setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/mic.png")), 270, null), CallerPanel);
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/mic2.png")), 270, null), SniperPanel);
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/binders.png")), 270, null), BinderPanel);
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/spammer.png")), 270, null), SpammerPanel);
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/skilltab.png")), 270, null), SkillPanel);
      
       if(!Constants.FAILED && Configuration.getInstance().getUser().isOfficial()) {
	        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/clanchat.png")), 270, null), CCPanel);
	    }
        addTab("", rotateIcon(new ImageIcon(getClass().getResource("/resources/setting-tab.png")), 270, null), SettingsPanel);
        
    }
	 private ImageIcon rotateIcon(ImageIcon icon, int angle, ImageObserver label) {
	        int w = icon.getIconWidth();
	        int h = icon.getIconHeight();
	        int type = BufferedImage.BITMASK;  // other options, see api
	        BufferedImage image = new BufferedImage(h, w, type);
	        Graphics2D g2 = image.createGraphics();
	        double x = (h - w)/2.0;
	        double y = (w - h)/2.0;
	        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
	        at.rotate(Math.toRadians(angle), w/2.0, h/2.0);
	        g2.drawImage(icon.getImage(), at, label);
	        g2.dispose();
	        icon = new ImageIcon(image);
	        return icon;
	    }
	
	public Sidebar getSideBar() {
		return this;
	}
	public CallerPanel getCallerPanel() {
    	return CallerPanel;
    }
    public SniperPanel getSniperPanel() {
    	return SniperPanel;
    }
    public BinderPanel getBinderPanel() {
    	return BinderPanel;
    }
    public SkillPanel getSkillPanel() {
    	return SkillPanel;
    }
    public CCPanel getCCPanel() {
    	return CCPanel;
    }
    public SpammerPanel getSpammerPanel() {
    	return SpammerPanel;
    }
    public SettingsPanel getSettingsPanel() {
    	return SettingsPanel;
    }
}
