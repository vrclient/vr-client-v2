package org.vrclient.main.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;

/**
 * Created by VR on 7/29/2014.
 */
public class PopupMenu extends JPopupMenu implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5639231076995913638L;
	private final JMenu view;
	private final JCheckBoxMenuItem displayBinderTimer, players, miniMap, gameState, debugger, gameObject, settings;
	private final JMenuItem widgets;
	private final Configuration config = Configuration.getInstance();

	

	public PopupMenu() {
		view = new JMenu("View");

		gameState = new JCheckBoxMenuItem("Game State");
		gameState.addActionListener(this);
		
		miniMap = new JCheckBoxMenuItem("1's Item Track");
		miniMap.addActionListener(this);
		
		players = new JCheckBoxMenuItem("Display Player names for everyone");
		players.addActionListener(this);
		


		if(Configuration.getInstance().getUser().isOfficial()) {
						debugger = new JCheckBoxMenuItem("Dev Text Plugins");
						debugger.addActionListener(this);
						gameObject = new JCheckBoxMenuItem("Dev GameObject Plugins");
						gameObject.addActionListener(this);
						settings = new JCheckBoxMenuItem("Game Settings");
						settings.addActionListener(this);
					} else {
						debugger = null;
						gameObject = null;
						settings = null;
					}
		

		
		
		displayBinderTimer = new JCheckBoxMenuItem("Display BinderList Timers");
		displayBinderTimer.addActionListener(this);
		
		
		widgets = new JMenuItem("Widget Explorer");
		widgets.addActionListener(this);
		
		


	
		//add(gameState);
		add(new JSeparator());
		add(players);
		add(miniMap);
		add(displayBinderTimer);
		
		add(new JSeparator());
		if(!Constants.FAILED && Configuration.getInstance().getUser().isOfficial()) {
			add(widgets);
			add(debugger);
			add(gameObject);
			add(settings);
		}
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == displayBinderTimer) {
			config.drawBinderTimer(!config.drawBinderTimer());
		} else if (e.getSource() == players) {
			config.drawPlayers(!config.drawPlayers());
		} else if (e.getSource() == widgets) {
			new WidgetViewer();
			config.drawWidgets(!config.drawWidgets());
		} else if (e.getSource() == miniMap) {
			config.drawMiniMap(!config.drawMiniMap());
		} else if (e.getSource() == gameState) {
			config.drawGameState(!config.drawGameState());
		} else if (e.getSource() == debugger) {
					config.drawTextDebugger(!config.drawTextDebugger());
		} else if (e.getSource() == gameObject) {
			config.drawGameObjects(!config.drawGameObjects());
		} else if (e.getSource() == settings) {
			config.drawSettings(!config.drawSettings());
		}
	}
}
