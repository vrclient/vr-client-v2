package org.vrclient.main.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Insets;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.RSLoader;
import org.vrclient.main.ui.sidebar.BinderPanel;
import org.vrclient.main.ui.sidebar.CallerPanel;
import org.vrclient.main.ui.sidebar.SkillPanel;
import org.vrclient.main.ui.sidebar.SniperPanel;
import org.vrclient.main.ui.titlebar.FrameTitleBar;
import org.vrclient.main.utils.ComponentResizer;
import org.vrclient.main.utils.Utilities;


/**
 * Created by VR on 7/29/2014.
 */
public class Frame extends JFrame implements WindowListener,ComponentListener{

    /**
	 * 
	 */
	private static final long serialVersionUID = 5752868874839930988L;

	public final RSLoader loader;
	public final ButtonPanel buttonPanel;
    public final ComponentResizer cr;
    private FrameTitleBar titleBar;
    private Configuration configuration = Configuration.getInstance();
    private Sidebar sideBar;
    JLayeredPane layeredPane;
    public JLabel mapButton;
    public static int STANDARD_WIDTH;
    public static int STANDARD_HEIGHT;
   
    public Frame() {
    	this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setResizable(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.getRootPane().setBorder(BorderFactory.createMatteBorder(0, 2, 2, 2, new Color(77,77,77)));
        
        
        cr = new ComponentResizer();
        cr.setDragInsets(new Insets(4, 4, 4, 4));
        cr.registerComponent(this);
        
        this.titleBar = new FrameTitleBar(this);
        this.buttonPanel = new ButtonPanel();
        this.sideBar = new Sidebar();
        
        
        this.getContentPane().add(titleBar, BorderLayout.NORTH);
        this.getContentPane().add(sideBar, BorderLayout.EAST);
        
        
        
        //Rs
        this.loader = new RSLoader(configuration);
        this.loader.setBounds(0,0,0,0);
        this.loader.setOpaque(true);
        this.loader.setBackground(Color.BLACK);
        
        
       this.addComponentListener(
            	new ComponentListener(){
            		@Override
                	public void componentResized(ComponentEvent e) {
            			if(titleBar!=null && configuration.enablerecord())
            			try {
            				titleBar.stopRecording();
							titleBar.startRecording();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
            		}
            		@Override
					public void componentMoved(ComponentEvent e) {
            			if(titleBar.captureSize != null && loader!= null) {
	            			titleBar.captureSize.setLocation(loader.getLocationOnScreen().x+1+((loader.getWidth() - configuration.getCanvas().getWidth())/2),loader.getLocationOnScreen().y+1);
	            			
	            		}
					}
					@Override
					public void componentShown(ComponentEvent e) {
					}
					@Override
					public void componentHidden(ComponentEvent e) {
					}
            	});
        
        //
        this.getContentPane().add(loader, BorderLayout.CENTER);

        this.addWindowListener(this);
        this.setLocationRelativeTo(getParent());
        KeyboardFocusManager keyManager;

        keyManager=KeyboardFocusManager.getCurrentKeyboardFocusManager();
        keyManager.addKeyEventDispatcher(new KeyEventDispatcher() {

          @Override
          public boolean dispatchKeyEvent(KeyEvent e) {
            if(e.getID()==KeyEvent.KEY_RELEASED && e.getKeyCode() == KeyEvent.VK_PRINTSCREEN){
            	Screenshotmanager.takeScreeny();
            	return true;
            }
            
            
            if(configuration.recordHotKey > 0) {
            	if(e.getID()==KeyEvent.KEY_RELEASED && e.getKeyCode() == configuration.recordHotKey){
                	configuration.getBotFrame().titleBar.RecordVideo();
                	return true;
                }
            } else {
            	if(e.getID()==KeyEvent.KEY_RELEASED && e.getKeyCode() == KeyEvent.VK_F6){
                	configuration.getBotFrame().titleBar.RecordVideo();
                	return true;
                }
            }
            
            return false;
          }

        });
        
        

        ArrayList<Image> icons = new ArrayList<Image>();
        icons.add(new ImageIcon(Utilities.getLocalImage("/resources/vricon_16x16.png")).getImage());
        icons.add(new ImageIcon(Utilities.getLocalImage("/resources/vricon_32x32.png")).getImage());
        icons.add(new ImageIcon(Utilities.getLocalImage("/resources/vricon_64x64.png")).getImage());
        this.setIconImages(icons);
        this.setUndecorated(true);
        this.setMinimumSize(new Dimension(1023, 535));
        this.pack();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) ((dimension.getWidth() - this.getWidth()) / 2);
        int y = (int) ((dimension.getHeight() - this.getHeight()) / 2);
        this.setLocation(x, y);
        
    }
  
   public ButtonPanel getButtonPanel() {
	   return buttonPanel;
   }
    public RSLoader loader() {
        return loader;
    }
    public Sidebar getSidePanel() {
        return sideBar;
    }
    public FrameTitleBar getTitleBar() {
        return titleBar;
    }
    
    public CallerPanel getCallerPanel() {
    	return getSidePanel().getCallerPanel();
    }
    public SniperPanel getSniperPanel() {
    	return getSidePanel().getSniperPanel();
    }
    public BinderPanel getBinderPanel() {
    	return getSidePanel().getBinderPanel();
    }
    public SkillPanel getSkillPanel() {
    	return getSidePanel().getSkillPanel();
    }
    @Override
    public void windowOpened(WindowEvent e) {

    }
    
    @Override
    public void windowClosing(WindowEvent e) {
    	this.setAlwaysOnTop(false);
    	this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    	 String ObjButtons[] = {"Yes","No"};
         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to exit?","Close Client?",JOptionPane.WARNING_MESSAGE,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
         if(PromptResult==JOptionPane.YES_OPTION)
         {
        	 //IPBLogin.logout();
        	 if(!Constants.FAILED)
        	 Configuration.getInstance().getUser().removeAccount();
             System.exit(0);
         } else {
        	 if(configuration.enableonTop())
        		 this.setAlwaysOnTop(true);
        	 else
        		 this.setAlwaysOnTop(false);
         }
    }
    @Override
    public void componentResized(ComponentEvent e)
    {
    	
    }
    @Override
    public void windowClosed(WindowEvent e) {
    	
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }


	@Override
	public void componentMoved(ComponentEvent paramComponentEvent) {
	}


	@Override
	public void componentShown(ComponentEvent paramComponentEvent) {
	}


	@Override
	public void componentHidden(ComponentEvent paramComponentEvent) {
	}
	

	public void closeClient() {
		SwingUtilities.invokeLater(new Runnable() { 
			   public void run() { 
		JOptionPane.showMessageDialog(null, "The client requires to be updated! Check Forums if there is a newer version", null,
		        JOptionPane.WARNING_MESSAGE);
		Configuration.getInstance().getUser().removeAccount();
		System.exit(0);
		}
	});
	}
	public void SystemClose() {
		this.setAlwaysOnTop(false);
    	this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    	 String ObjButtons[] = {"Yes","No"};
         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to exit?","Close Client?",JOptionPane.WARNING_MESSAGE,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
         if(PromptResult==JOptionPane.YES_OPTION)
         {
        	 //IPBLogin.logout();
        	 if(!Constants.FAILED)
        	 Configuration.getInstance().getUser().removeAccount();
             System.exit(0);
         } else {
        	 if(configuration.enableonTop())
        		 this.setAlwaysOnTop(true);
        	 else
        		 this.setAlwaysOnTop(false);
         }
	}
	
	public Dimension getDefaultSize() {
        return new Dimension(STANDARD_WIDTH, STANDARD_HEIGHT);
    }
	public void toggleMaximized() {
        if (this.getExtendedState() != JFrame.MAXIMIZED_BOTH) {
        	STANDARD_WIDTH = this.getWidth();
        	STANDARD_HEIGHT = this.getHeight();
        	this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else {
        	this.setExtendedState(0);
        	this.setMinimumSize(new Dimension(1023, 535));
        	this.setMaximumSize(getDefaultSize());
        	this.setPreferredSize(getDefaultSize());
        	this.setSize(getDefaultSize());
        }
    }

	

	
}