/*
 * Created by JFormDesigner on Sat Jul 23 15:39:34 CDT 2016
 */

package org.vrclient.main.ui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.BorderFactory;

import org.apache.commons.codec.binary.Base64;
import org.vrclient.component.RSCanvas;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.ui.titlebar.FrameTitleBar;
import org.vrclient.main.ui.titlebar.ScreenShotTitleBar;
import org.vrclient.main.utils.ComponentMover;
import org.vrclient.main.utils.ComponentResizer;
import org.vrclient.main.utils.FileUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jgoodies.forms.factories.CC;
import com.jgoodies.forms.layout.FormLayout;


public class Screenshotmanager extends JFrame {
	
	public static RSCanvas canvas;
	private ScreenShotTitleBar titleBar;
	public ComponentResizer cr;
	public static int STANDARD_WIDTH;
    public static int STANDARD_HEIGHT;
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Screenshotmanager() {
		
		initComponents();
		list1.setSelectedIndex(list1.getModel().getSize()-1);
		
	}

	

	private void button1ActionPerformed(ActionEvent e) {
		button1.setEnabled(false);
		button2.setEnabled(false);
		textField1.setText("Uploading..");
		new Thread(new Runnable() {
			public void run() {
				if(list1.getSelectedValuesList() != null) {
					try {
						textField1.setText(null);
						for(Object item: list1.getSelectedValuesList()) {	
						File file = new File(Constants.SCREENSHOT_PATH + File.separator + item);
						BufferedImage image = ImageIO.read(file);
						String imgur = upload(image);
						if(imgur == null) {
							
						} else {
							if(list1.getSelectedValuesList().indexOf(item) == 0)
								textField1.setText(imgur);
							else
							textField1.setText(textField1.getText()+"\n"+imgur);
							FileUtils.savescreeny("Screenshots.ini", file+"", imgur);
						}
						}
						Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
						StringSelection stringSelection = new StringSelection(textField1.getText());
						clpbrd.setContents(stringSelection, null);
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
						JOptionPane.showMessageDialog(null, "Copied to clipboard!", "Screenshot Manager",
			                    JOptionPane.INFORMATION_MESSAGE);
								}
						});
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
	}).start();
	}

	private void textField1MouseClicked(MouseEvent e) {
		if(list1.getSelectedValue() != null && !textField1.getText().isEmpty()) {
			if (e.getClickCount() == 1) {
				StringSelection stringSelection = new StringSelection (textField1.getText());
				Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
				clpbrd.setContents (stringSelection, null);
				textField1.selectAll();
			}
			if (e.getClickCount() == 2) {
				try {
					Desktop.getDesktop().browse(new URI(textField1.getText()));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	

	private void button2ActionPerformed(ActionEvent e) {
		if(list1.getSelectedValue() != null) {
			try {
				String file = Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValue();
				Runtime.getRuntime().exec(new String[] { "C:\\WINDOWS\\system32\\mspaint.exe", file});
			} catch (IOException e1) {
			}
		}
			
		
	}

	private void button3ActionPerformed(ActionEvent e) {
		if(list1.getSelectedValue() != null) {
			if(list1.getSelectedValuesList().size() > 1) {
				String ObjButtons[] = {"Yes","No"};
		         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to delete this image?",null,JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
		         if(PromptResult==JOptionPane.YES_OPTION)
		         {
		        	 for (int i = 0; i < list1.getSelectedValuesList().size(); i++) {
		        		File file = new File(Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValuesList().get(i));
			        	file.delete();
		        	 }
		        	 label1.setIcon(null);
			 		 textField1.setText("");
		        	 list1.setSelectedIndex(1);
		        	 updateList();
		         }
				
			} else {
				String ObjButtons[] = {"Yes","No"};
		         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to delete this image?",null,JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
		         if(PromptResult==JOptionPane.YES_OPTION)
		         {
		        	
		        	int index = list1.getSelectedIndex(); 
		        	File file = new File(Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValue());
		        	file.delete();
		        	label1.setIcon(null);
		 			textField1.setText("");
		 			DefaultListModel<String> listModel = (DefaultListModel<String>) list1.getModel();
		            listModel.removeElementAt(index);
		            list1.setSelectedIndex(index - 1);
		 		}
			}
		}
	}

	private void list1ValueChanged(ListSelectionEvent e) {
		if(list1.getSelectedValuesList().size() > 1) {
			button1.setEnabled(false);
  		  	button2.setEnabled(false);
		} else {
			button1.setEnabled(true);
  		  	button2.setEnabled(true);
		}
		ImageIcon image;
		String file = Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValue();
		image = new ImageIcon(file);
        label1.setIcon(image);
        File screenyInfo = new File(Constants.SETTING_PATH + File.separator + "Screenshots.ini");
        if (screenyInfo.exists()) {
	    	  if (FileUtils.load("Screenshots.ini", list1.getSelectedValue()+"") != null) {
	    		  textField1.setText(FileUtils.load("Screenshots.ini", list1.getSelectedValue()+""));
	    		  button1.setEnabled(false);
	    		  button2.setEnabled(false);
	    	  } else {
	    		  textField1.setText("");
	    		  button1.setEnabled(true);
	    		  button2.setEnabled(true);
	    	  }
        }
	}

	private void list1KeyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_DELETE) {
			if(list1.getSelectedValue() != null) {
				if(list1.getSelectedValuesList().size() > 1) {
					String ObjButtons[] = {"Yes","No"};
			         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to delete this image?",null,JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
			         if(PromptResult==JOptionPane.YES_OPTION)
			         {
			        	 for (int i = 0; i < list1.getSelectedValuesList().size(); i++) {
			        		File file = new File(Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValuesList().get(i));
				        	file.delete();
			        	 }
			        	 label1.setIcon(null);
				 		 textField1.setText("");
			        	 list1.setSelectedIndex(1);
			        	 updateList();
			         }
					
				} else {
					String ObjButtons[] = {"Yes","No"};
			         int PromptResult = JOptionPane.showOptionDialog(null,"Are you sure you want to delete this image?",null,JOptionPane.DEFAULT_OPTION,JOptionPane.WARNING_MESSAGE,null,ObjButtons,ObjButtons[1]);
			         if(PromptResult==JOptionPane.YES_OPTION)
			         {
			        	
			        	int index = list1.getSelectedIndex(); 
			        	File file = new File(Constants.SCREENSHOT_PATH + File.separator + list1.getSelectedValue());
			        	file.delete();
			        	label1.setIcon(null);
			 			textField1.setText("");
			 			DefaultListModel<String> listModel = (DefaultListModel<String>) list1.getModel();
			            listModel.removeElementAt(index);
			            list1.setSelectedIndex(index - 1);
			 		}
				}
			}
		}
	}

	

	private void initComponents() {
		
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		frame1 = new JFrame();
		splitPane1 = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT){     private final int location = 170;     {         setDividerLocation( location );     }     @Override     public int getDividerLocation() {         return location ;     }     @Override     public int getLastDividerLocation() {         return location ;     } };
		scrollPane3 = new JScrollPane();
		list1 = new JList();
		scrollPane2 = new JScrollPane();
		panel1 = new JPanel();
		label1 = new JLabel();
		button1 = new JButton();
		button2 = new JButton();
		textField1 = new JTextField();
		button3 = new JButton();

		//======== frame1 ========
		{
			frame1.setTitle("Screenshots");
			frame1.setMinimumSize(new Dimension(1100, 600));
			frame1.setUndecorated(true);
			Container frame1ContentPane = frame1.getContentPane();
			frame1ContentPane.setLayout(new FormLayout(
				"$lcgap, 100px, 70px, default:grow, default, $lcgap",
				"default, default:grow, default, $lgap, default"));
			titleBar = new ScreenShotTitleBar(this);
			frame1.getContentPane().add(titleBar, CC.xywh(1, 1, 6, 1, CC.DEFAULT, CC.FILL));

			getRootPane().setBorder(BorderFactory.createMatteBorder(0, 2, 2, 2, new Color(77,77,77)));
			cr = new ComponentResizer();
			cr.setDragInsets(new Insets(4, 4, 4, 4));
			cr.registerComponent(frame1);


			//======== splitPane1 ========
			{
				splitPane1.setLastDividerLocation(-1);
				splitPane1.setMinimumSize(new Dimension(800, 400));
				splitPane1.setDividerLocation(160);
				splitPane1.setDividerSize(10);

				//======== scrollPane3 ========
				{

					//---- list1 ----
					list1.addListSelectionListener(new ListSelectionListener() {
						@Override
						public void valueChanged(ListSelectionEvent e) {
							list1ValueChanged(e);
						}
					});
					list1.addKeyListener(new KeyAdapter() {
						@Override
						public void keyPressed(KeyEvent e) {
							list1KeyPressed(e);
						}
					});
					scrollPane3.setViewportView(list1);
				}
				splitPane1.setLeftComponent(scrollPane3);

				//======== scrollPane2 ========
				{
					scrollPane2.setMinimumSize(new Dimension(765, 503));
					scrollPane2.setBorder(null);

					//======== panel1 ========
					{
						panel1.setLayout(new GridBagLayout());
						((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {1.0};
						((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {1.0, 0.0};

						//---- label1 ----
						label1.setVerticalAlignment(SwingConstants.TOP);
						label1.setHorizontalTextPosition(SwingConstants.LEADING);
						label1.setVerticalTextPosition(SwingConstants.TOP);
						panel1.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
							GridBagConstraints.CENTER, GridBagConstraints.NONE,
							new Insets(0, 0, 0, 0), 0, 0));
					}
					scrollPane2.setViewportView(panel1);
				}
				splitPane1.setRightComponent(scrollPane2);
			}
			frame1ContentPane.add(splitPane1, CC.xywh(2, 2, 4, 1, CC.DEFAULT, CC.FILL));

			//---- button1 ----
			button1.setText("Upload");
			button1.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button1ActionPerformed(e);
				}
			});
			frame1ContentPane.add(button1, CC.xy(2, 3, CC.FILL, CC.FILL));

			//---- button2 ----
			button2.setText("Edit");
			button2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button2ActionPerformed(e);
				}
			});
			frame1ContentPane.add(button2, CC.xy(3, 3, CC.FILL, CC.FILL));

			//---- textField1 ----
			textField1.setEditable(false);
			textField1.setForeground(Color.white);
			textField1.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					textField1MouseClicked(e);
				}
			});
			frame1ContentPane.add(textField1, CC.xy(4, 3));

			//---- button3 ----
			button3.setText("Delete");
			button3.setBackground(new Color(255, 0, 0, 63));
			button3.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					button3ActionPerformed(e);
				}
			});
			frame1ContentPane.add(button3, CC.xy(5, 3));
			frame1.pack();
			frame1.setLocationRelativeTo(frame1.getOwner());
		}
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
		
		updateList();
	 }

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	public static JFrame frame1;
	private JSplitPane splitPane1;
	private JScrollPane scrollPane3;
	public static JList list1;
	private JScrollPane scrollPane2;
	private JPanel panel1;
	public JLabel label1;
	private JButton button1;
	private JButton button2;
	public static JTextField textField1;
	private JButton button3;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	
	
	
	
		
	public String upload(BufferedImage image) {
	    String IMGUR_POST_URI = "https://api.imgur.com/3/upload";
	    String IMGUR_API_KEY = "4253799ee3fc7ca";
	    try {
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        //System.out.println("Writing image...");
	        ImageIO.write(image, "png", baos);
	        URL url = new URL(IMGUR_POST_URI);

	        //System.out.println("Encoding...");
	        String data = URLEncoder.encode("image", "UTF-8") + "=" + URLEncoder.encode(Base64.encodeBase64String(baos.toByteArray()).toString(), "UTF-8");
	        data += "&" + URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode(IMGUR_API_KEY, "UTF-8");

	        //System.out.println("Connecting...");
	       
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        conn.setDoInput(true);
	        conn.setRequestProperty("Authorization", "Client-ID " + IMGUR_API_KEY);
	        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
	       
	        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

	        //System.out.println("Sending data...");
	        wr.write(data);
	        wr.flush();

	        //System.out.println("Finished.");

	        //just display the raw response
	        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        
	        String line;
	        while ((line = in.readLine()) != null) {
	        	JsonElement jelement = new JsonParser().parse(line);
	            JsonObject  jobject = jelement.getAsJsonObject();
	            jobject = jobject.getAsJsonObject("data");
	            String result = jobject.get("link").toString().replace("\"", "");
	            return result;
	        }
	        
	        in.close();	  
	    } catch (Exception e) {
	        System.out.println("Error: " + e.getMessage());
	        e.printStackTrace();
	    }
		return null;
	}
	public static void updateList(){
		DefaultListModel<String> model1 = new DefaultListModel<String>();
	    File o = new File(Constants.SCREENSHOT_PATH);

	    File[] yourFileList = o.listFiles();
	    Arrays.sort(yourFileList, new Comparator<File>(){
	        public int compare(File f1, File f2)
	        {
	            return Long.valueOf(f1.lastModified()).compareTo(f2.lastModified());
	        } });
	    //Collections.reverse(Arrays.asList(yourFileList));
	    for(File f : yourFileList) {
	        model1.addElement(f.getName());
	    }	    
	    list1.setModel(model1);
	    int size = list1.getModel().getSize();
	    list1.setSelectedIndex(size-1);
    }
	
	public static void takeScreeny() {
		if(Configuration.getInstance().getSoundMonitor() == null || Configuration.getInstance().getCanvas() == null)
			return;
		 new Thread() {
	        	public void run() {
	        try {
				Configuration.getInstance().getSoundMonitor().playClip(getClass().getResource("/resources/screenshot.wav"));
			} catch (IOException | UnsupportedAudioFileException
					| LineUnavailableException | InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        	}
	        }.start();
		try {
			if(Configuration.getInstance().getCanvas().getGameBuffer() != null) {
				File forumInfo = new File(Constants.SCREENSHOT_PATH);
			      if (!forumInfo.exists()) {
			    	forumInfo.mkdirs();
			      }
			    Date date = new Date() ;
			    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy_HH.mm.ss");
				ImageIO.write(Configuration.getInstance().getCanvas().getGameBuffer(), "png", new File(Constants.SCREENSHOT_PATH + File.separator + dateFormat.format(date) + ".png"));
				if(Screenshotmanager.frame1 == null) {
					//System.out.println("not visible");
				} else {
					Screenshotmanager.updateList();
				}
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static void toggleMaximized() {
        if (frame1.getExtendedState() != JFrame.MAXIMIZED_BOTH) {
        	STANDARD_WIDTH = frame1.getWidth();
        	STANDARD_HEIGHT = frame1.getHeight();
        	frame1.setExtendedState(JFrame.MAXIMIZED_BOTH);
        } else {
        	frame1.setExtendedState(0);
        	frame1.setMinimumSize(new Dimension(1100, 600));
        	frame1.setMaximumSize(getDefaultSize());
        	frame1.setPreferredSize(getDefaultSize());
        	frame1.setSize(getDefaultSize());
        }
    }
	
	public static Dimension getDefaultSize() {
        return new Dimension(STANDARD_WIDTH, STANDARD_HEIGHT);
    }
	
	
}
