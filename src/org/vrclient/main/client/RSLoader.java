package org.vrclient.main.client;

import org.vrclient.component.RSCanvas;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;
import org.vrclient.main.client.parser.FieldHook;
import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.client.reflection.Reflection;
import org.vrclient.main.script.api.listeners.ExperienceMonitor;
import org.vrclient.main.script.api.listeners.SoundMonitor;
import org.vrclient.main.client.security.encryption.AES;
import org.vrclient.main.script.api.methods.data.Game;
import org.vrclient.main.script.api.methods.input.AutoTyper;
import org.vrclient.main.script.api.util.Random;
import org.vrclient.main.utils.FileDownloader;
import org.vrclient.main.utils.FileUtils;
import org.vrclient.main.utils.Utilities;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.applet.Applet;
import java.applet.AppletContext;
import java.applet.AppletStub;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;

public class RSLoader extends JPanel implements AppletStub {

	private static final long serialVersionUID = -8902296630509146560L;
	private boolean isAppletLoaded = false;
	private final Font font = new Font("Tahoma", Font.BOLD, 10);
	BufferedImage logo;
	private final AES encryption = new AES("Nf6JBwAn\\YGX,J,g", "dh'Tv3X(PX;fx`+u");
	private RsJarLoader classLoader = null;
	private FileDownloader downloader;
	private static Applet applet;
	
	private final Parameters params;

	public RSLoader(final Configuration configuration) {
		setLayout(new BorderLayout(0, 0));
		int World = 62;
		File forumInfo = new File(Constants.SETTING_PATH + File.separator + Constants.SETTING_FILE_NAME);
	     if (forumInfo.exists()) {
			if(FileUtils.load(Constants.SETTING_FILE_NAME, "defaultworld") != null) {
				World = (Integer.parseInt(FileUtils.load(Constants.SETTING_FILE_NAME, "defaultworld")) - 300);
	  	  	} else {
	  	  		World = 62;
	  	  	}
	     }
  	  	params = new Parameters(World);
		setSize(new Dimension(Constants.APPLET_WIDTH, Constants.APPLET_HEIGHT));
		setMinimumSize(new Dimension(Constants.APPLET_WIDTH, Constants.APPLET_HEIGHT));
		setPreferredSize(new Dimension(Constants.APPLET_WIDTH, Constants.APPLET_HEIGHT));

		if (HookReader.VERSION <= Constants.CLIENT_VERSION) {
			final Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {

					downloader = new FileDownloader(params.get("codebase") + params.get("initial_jar"),
							Utilities.getContentDirectory() + "game/os-gamepack.jar");
					downloader.run();
				
					final File jar = new File(Utilities.getContentDirectory() + "game/os-gamepack.jar");

					try {
						classLoader = new RsJarLoader(jar.toURI().toURL());
					} catch (MalformedURLException e) {
						e.printStackTrace();
					}

					final String mainClass = params.get("initial_class").replaceAll(".class", "");
					try {
						applet = (Applet) classLoader.loadClass(mainClass).newInstance();
					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException a) {
						a.printStackTrace();
					}


					applet.setStub(RSLoader.this);
					applet.resize(new Dimension(Constants.APPLET_WIDTH, Constants.APPLET_HEIGHT));
					applet.init();
					applet.start();
					applet.setLocation(0, 0);
					applet.setLayout(null);
					isAppletLoaded = true;
					RSLoader.this.add(applet, BorderLayout.CENTER);
					RSLoader.this.revalidate();					
					while (applet.getComponents().length == 0) {
						Utilities.sleep(1000, 1500);
					}
					try {
					Reflection.init();
					} catch(Exception e) {
						Reflection.init();
						SwingUtilities.invokeLater(new Runnable() {
							public void run() {
						JOptionPane.showMessageDialog(null, "Please restart the client and contact Sol. It appears something crashed!", null,
						        JOptionPane.ERROR_MESSAGE);
							}
						});
					}
					while (Game.getGameState() < 10) {
						Utilities.sleep(1000, 2000);
					}
					
					 if (FileUtils.load(Constants.RS_FILE_NAME, "rsn") != null && configuration.rememberme()){
						 try {
							  	FieldHook fieldHook = HookReader.fields.get("client_username");
					    		Class<?> clazz = Configuration.getInstance().getBotFrame().loader().loadClass(fieldHook.getClassName());
					    		Field field = clazz.getDeclaredField(fieldHook.getFieldName());
					    		field.setAccessible(true);
					    		field.set(null, encryption.decrypt(FileUtils.load(Constants.RS_FILE_NAME, "rsn")));
					    		Reflection.value(field, null);
					    		Game.updatedRSN = true;					    						  
					        	} catch (Exception e) {
					        	}
							
			    	  }
					Utilities.sleep(2000, 3000);
					RSCanvas canvas = new RSCanvas();
					//configuration.setCanvas(canvas);
					canvas.loadAll();
					configuration.setSoundManager(new SoundMonitor());
					//canvas.set();			
					configuration.setAutoTyper(new AutoTyper());
					ExperienceMonitor.start();
				}
			});
			thread.start();
		}
	}

	@Override
	public void paintComponent(Graphics graphics) {
		if (!isAppletLoaded) {
			final Graphics2D graphics2D = (Graphics2D) graphics;
			graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

			graphics2D.setColor(Color.BLACK);
			graphics2D.fillRect(0, 0, getWidth(), getHeight());
			graphics2D.setFont(font);

			if (HookReader.VERSION > Constants.CLIENT_VERSION) {
				graphics.setColor(Color.GREEN.darker());
				graphics2D.drawString("There is a newer version of the client , Please download it from http://www.vr-rs.com/client", 150, 280);
			} else {
				if (downloader != null || downloader.isFinished()) {
					final int width = downloader.getPercentage() * 300 / 100;
					try {
						 logo = ImageIO.read(getClass().getResource("/resources/logo.png"));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					FontMetrics fm = graphics.getFontMetrics();
					graphics.setColor(new Color(0, 150, 255));
					graphics2D.fillRect((Configuration.getInstance().getBotFrame().loader().getWidth()-300)/2, (Configuration.getInstance().getBotFrame().loader().getHeight()-20)/2, width, 20);
					graphics.setColor(new Color(0, 150, 255));
					graphics2D.drawRect((Configuration.getInstance().getBotFrame().loader().getWidth()-300)/2, (Configuration.getInstance().getBotFrame().loader().getHeight()-20)/2, 300, 20);

					graphics2D.setColor(Color.WHITE);
					graphics2D.drawString("Downloading OSRS Client - Please wait", (Configuration.getInstance().getBotFrame().loader().getWidth()-fm.stringWidth("Downloading OSRS Client - Please wait"))/2, (Configuration.getInstance().getBotFrame().loader().getHeight()-fm.getHeight())/2 - fm.getAscent() - 5);
					graphics2D.drawString(downloader.getPercentage() + "%",(Configuration.getInstance().getBotFrame().loader().getWidth()-fm.stringWidth(downloader.getPercentage() + "%"))/2, (Configuration.getInstance().getBotFrame().loader().getHeight()-fm.getHeight())/2 + fm.getAscent());
				}
				graphics.drawImage(logo,(Configuration.getInstance().getBotFrame().loader().getWidth()-logo.getWidth())/2,Configuration.getInstance().getBotFrame().loader().getHeight()-logo.getHeight()-30,null);
				
			}
			repaint(600);
		}
	}
	public static Applet getApplet(){
        return applet;
    }
	public Class<?> loadClass(final String className) {
		if (classLoader == null) {
			System.out.println("Error Null Class Loader");
			return null;
		}
		if(!classLoader.classes().containsKey(className)){
			try {
				return classLoader.loadClass(className);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return classLoader.classes().get(className);
	}
	
	@Override
	public boolean isActive() {
		return false;
	}

	@Override
	public URL getDocumentBase() {
		return getCodeBase();
	}

	@Override
	public URL getCodeBase() {
		try {
			return new URL(params.get("codebase"));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getParameter(String name) {
		return params.get(name);
	}


	@Override
	public AppletContext getAppletContext() {
		return null;  //To change body of implemented methods use File | Settings | File Templates.
	}
	@Override
	public void appletResize(int width, int height) {
		final Dimension size = new Dimension(Constants.APPLET_WIDTH, Constants.APPLET_WIDTH);
		applet.setSize(size);
		applet.setPreferredSize(size);
	}
}
