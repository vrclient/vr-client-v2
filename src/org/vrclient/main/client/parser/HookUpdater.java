package org.vrclient.main.client.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class HookUpdater {
	  public static String client_player;
	  public static String client_players;
	  public static String client_npcs;
	  public static String client_friends;
	  public static String client_clanMembers;
	  public static String client_experiences;
	  public static String client_current_levels;
	  public static String client_real_levels;
	  public static String client_settings;
	  public static String client_varps;
	  public static String client_login_index;
	  public static String client_login_index_Multiplier;
	  public static String client_game_state;
	  public static String client_game_state_Multiplier;
	  public static String client_baseX;
	  public static String client_baseX_Multiplier;
	  public static String client_baseY;
	  public static String client_baseY_Multiplier;
	  public static String client_minimapScale;
	  public static String client_minimapScale_Multiplier;
	  public static String client_minimapAngle;
	  public static String client_minimapAngle_Multiplier;
	  public static String client_minimapOffset;
	  public static String client_minimapOffset_Multiplier;
	  public static String client_plane;
	  public static String client_plane_Multiplier;
	  public static String client_cameraX;
	  public static String client_cameraX_Multiplier;
	  public static String client_cameraY;
	  public static String client_cameraY_Multiplier;
	  public static String client_cameraZ;
	  public static String client_cameraZ_Multiplier;
	  public static String client_cameraPitch;
	  public static String client_cameraPitch_Multiplier;
	  public static String client_cameraYaw;
	  public static String client_cameraYaw_Multiplier;
	  public static String client_scale;
	  public static String client_scale_Multiplier;
	  public static String client_tileSettings;
	  public static String client_tileHeights;
	  public static String client_widgets;
	  public static String client_world;
	  public static String client_world_Multiplier;
	  public static String client_groundItems;
	  public static String client_username;
	  public static String client_password;
	  public static String client_menuVisible;
	  public static String client_menuCount;
	  public static String client_menuCount_Multiplier;
	  public static String client_menuActions;
	  public static String client_menuActionNames;
	  public static String client_menuX;
	  public static String client_menuX_Multiplier;
	  public static String client_menuY;
	  public static String client_menuY_Multiplier;
	  public static String client_menuWidth;
	  public static String client_menuWidth_Multiplier;
	  public static String client_menuHeight;
	  public static String client_menuHeight_Multiplier;
	  public static String client_region;
	  public static String Actor;
	  public static String Actor_combatInfoList;
	  public static String Actor_localX;
	  public static String Actor_localX_Multiplier;
	  public static String Actor_localY;
	  public static String Actor_localY_Multiplier;
	  public static String Actor_interacting;
	  public static String Actor_interacting_Multiplier;
	  public static String Actor_spellAnimationId;
	  public static String Actor_spellAnimationId_Multiplier;
	  public static String Player;
	  public static String Player_name;
	  public static String Player_team;
	  public static String Player_team_Multiplier;
	  public static String Player_level;
	  public static String Player_level_Multiplier;
	  public static String Player_skullIcon;
	  public static String Player_skullIcon_Multiplier;
	  public static String Player_overheadIcon;
	  public static String Player_overheadIcon_Multiplier;
	  public static String Player_playerDefinition;
	  public static String PlayerDefinition;
	  public static String PlayerDefinition_equipmentIds;
	  public static String Npc;
	  public static String Npc_definition;
	  public static String NpcDefinition;
	  public static String NpcDefinition_name;
	  public static String NpcDefinition_id;
	  public static String NpcDefinition_id_Multiplier;
	  public static String Friend;
	  public static String Friend_name;
	  public static String Friend_prevName;
	  public static String Widget;
	  public static String Widget_children;
	  public static String Widget_text;
	  public static String Widget_name;
	  public static String Widget_itemId;
	  public static String Widget_itemId_Multiplier;
	  public static String Widget_itemQuantity;
	  public static String Widget_itemQuantity_Multiplier;
	  public static String Widget_itemIds;
	  public static String Widget_itemQuantities;
	  public static String Widget_boundsIndex;
	  public static String Widget_boundsIndex_Multiplier;
	  public static String Widget_X;
	  public static String Widget_X_Multiplier;
	  public static String Widget_Y;
	  public static String Widget_Y_Multiplier;
	  public static String Widget_Width;
	  public static String Widget_Width_Multiplier;
	  public static String Widget_Height;
	  public static String Widget_Height_Multiplier;
	  public static String Widget_ScrollX;
	  public static String Widget_ScrollX_Multiplier;
	  public static String Widget_ScrollY;
	  public static String Widget_ScrollY_Multiplier;
	  public static String Widget_hidden;
	  public static String ItemDefinition;
	  public static String ItemDefinition_name;
	  public static String Node;
	  public static String Node_next;
	  public static String Node_previous;
	  public static String Node_uid;
	  public static String LinkedList;
	  public static String LinkedList_head;
	  public static String LinkedList_current;
	  public static String CombatInfo1;
	  public static String CombatInfo1_healthRatio;
	  public static String CombatInfo1_healthRatio_Multiplier;
	  public static String CombatInfo2;
      public static String CombatInfo2_healthScale;
	  public static String CombatInfo2_healthScale_Multiplier;
	  public static String CombatInfoListHolder;
	  public static String CombatInfoListHolder_combatInfo1;
	  public static String CombatInfoListHolder_combatInfo2;
	  public static String CombatInfoList;
	  public static String CombatInfoList_node;
	  public static String GroundItem;
	  public static String GroundItem_id;
	  public static String GroundItem_id_Multiplier;
	  public static String GroundItem_quantity;
	  public static String GroundItem_quantity_Multiplier;
	  public static String ClanMember;
	  public static String ClanMember_name;
	  public static String GameObject;
	  public static String GameObject_id;
	  public static String GameObject_id_Multiplier;
	  public static String getItemDefinition;
	  public static String getItemDefinition_ParamType;
	  public static String getItemDefinition_ParamValue;
	static String readUrl(String urlString) throws IOException
	  {
	    BufferedReader reader = null;
	    URLConnection uc = null;
	    try
	    {
	    	
	      URL url = new URL(urlString);	  
	      uc = url.openConnection();
	      uc.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0)");
	      uc.connect();
	   
	      reader = new BufferedReader(new InputStreamReader(uc.getInputStream()));
	      StringBuffer buffer = new StringBuffer();
	      
	      char[] chars = new char['?'];
	      int read;
	      while ((read = reader.read(chars)) != -1) {
	        buffer.append(chars, 0, read);
	      }
	      return buffer.toString();
	    }
	    catch (Exception ex)
	    {
	      System.out.println(ex);
	    }
	    finally
	    {
	      if (reader != null) {
	        try
	        {
	          reader.close();
	        }
	        catch (IOException ex)
	        {
	          System.out.println(ex);
	        }
	      }
	    }
	    return "";
	  }
	public static String findClassName(String name) throws IllegalArgumentException, IllegalAccessException {
		Field[] arrayOfField = HookUpdater.class.getFields();
		Object value = null;
		int j = HookUpdater.class.getDeclaredFields().length;
	      for (int i = 0; i < j; i++)
	      {
	    	  if(arrayOfField[i].getName().equalsIgnoreCase(name)) {
	    		  value = arrayOfField[i];
	    		  return String.valueOf(arrayOfField[i].get(value));
	    	  }
	      }
		return null;
	}
	 public static void updateHooks() {
	    String json;
	    try
	    {
	    	json = readUrl("https://vr-rs.com/client/hooks.html");
	    }
	    catch (Exception ex)
	    {
	      json = "";
	      System.out.println(ex);
	    }
	    
	    Field localObject1 = null;
	    try
	    {
	      Field[] arrayOfField = HookUpdater.class.getFields();
	      int j = HookUpdater.class.getDeclaredFields().length;
	      for (int i = 0; i < j; i++)
	      {
	    	localObject1 = arrayOfField[i];
	    	localObject1.setAccessible(true);
	    	Object localObject3 = localObject1.getName();

	        Pattern localPattern = Pattern.compile(localObject3.toString());
	        Matcher matcher = localPattern.matcher(json);
	        
	        Pattern localPattern2 = Pattern.compile("(?<= \")[0-9a-z.-]+");
	        Matcher matcher2 = localPattern2.matcher(json);
	        try {
	        	int count = 0;
	        	if(matcher.find()) {
	        		while (count != i) {
	        			count++;
	        			matcher2.find();
	        		}
	        		localObject1.set(localObject1, matcher2.find() ? matcher2.group() : "x.x");
	        	}              
	        
	              
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
	      }
	      return;
	    }
	    catch (IllegalArgumentException localIllegalArgumentException)
	    {
	    }
	  }
}