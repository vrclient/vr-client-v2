package org.vrclient.main.client.parser;

import org.vrclient.main.Constants;
import org.vrclient.main.client.parser.HookUpdater;
import org.vrclient.main.utils.NetUtils;

import java.lang.reflect.Field;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by VR on 7/29/2014.
 */
public class HookReader {

	public static Hashtable<String, FieldHook> fields = new Hashtable<>();
	public static Hashtable<String, MethodHook> methods = new Hashtable<>();
	public static Map<String, String> types = new Hashtable<>();
	public static int broken = 0;


	public static double VERSION = 0.0;

	/**
	 * Parse Html File to get hooks info
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static void init() throws InstantiationException, IllegalAccessException {
		 FieldHook fieldHook = null;
		 MethodHook methodHook = null;
		types = NetUtils.txtReader("types.txt");
		for (Field source : HookUpdater.class.getFields()) {
			source.setAccessible(true);
			if(String.valueOf(source.getName()).contains("Multiplier") 
					|| !String.valueOf(source.getName()).contains("_")
					|| String.valueOf(source.getName()).contains("setGameState_ParamType")
					|| String.valueOf(source.getName()).contains("setGameState_ParamValue")
					|| String.valueOf(source.getName()).contains("loadWorlds_ParamType")
					|| String.valueOf(source.getName()).contains("loadWorlds_ParamValue")
					|| String.valueOf(source.getName()).contains("getItemDefinition_ParamType")
					|| String.valueOf(source.getName()).contains("getItemDefinition_ParamValue")
					|| String.valueOf(source.getName()).contains("setWorld_ParamType")
					|| String.valueOf(source.getName()).contains("setWorld_ParamValue")
					|| String.valueOf(source.getName()).contains("client_worldDomain")
					|| String.valueOf(source.getName()).contains("client_worlds")
					|| String.valueOf(source.getName()).contains("client_chatBoxChannels")
					|| String.valueOf(source.getName()).contains("client_exchangeOffers")
					|| String.valueOf(source.getName()).contains("client_worldSelectorOpen")
					|| String.valueOf(source.getName()).contains("GameObject")
					//^^
					|| String.valueOf(source.getName()).contains("ExchangeOffer_status")) continue;	
			String name = String.valueOf(source.getName());
			String fieldValue = HookUpdater.findClassName(name);
			if(fieldValue.equalsIgnoreCase("x.xx")) {
				System.out.println("BROKEN HOOK: "+name);
				broken++;
				continue;
			}
				String multiplier = HookUpdater.findClassName(name+"_Multiplier");
				if(multiplier != null) {	
				if(!fieldValue.contains(".")) {
				String findClassName = HookUpdater.findClassName(name.split("_")[0]);	
				System.out.println(source.getName().toString()+" "+findClassName+"#"+fieldValue+" I "+multiplier);
				fieldHook = new FieldHook(source.getName().toString()+" "+findClassName+"#"+fieldValue+" I "+multiplier);
				fields.put(fieldHook.getFieldKey(), fieldHook);
				} else {
				System.out.println(source.getName().toString()+" "+fieldValue.replace('.', '#')+" I "+multiplier);
				fieldHook = new FieldHook(source.getName().toString()+" "+fieldValue.replace('.', '#')+" I "+multiplier);
				fields.put(fieldHook.getFieldKey(), fieldHook);
				}
				} else {	
					String findClassName = HookUpdater.findClassName(name.split("_")[0]);
					if(findClassName != null) {
					System.out.println(name+" "+findClassName+"#"+fieldValue+" "+types.get(name));
					fieldHook = new FieldHook(name+" "+findClassName+"#"+fieldValue+" "+types.get(name));
					fields.put(fieldHook.getFieldKey(), fieldHook);	
					} else {
					System.out.println(source.getName().toString()+" "+fieldValue.replace('.', '#')+" "+types.get(name));
					fieldHook = new FieldHook(source.getName().toString()+" "+fieldValue.replace('.', '#')+" "+types.get(name));
					fields.put(fieldHook.getFieldKey(), fieldHook);	
					}
				}
		}
		final String[] sourceCode = NetUtils.readPage("http://vr-rs.com/client/myhooks.html");
		for (String source : sourceCode) {
			if (!source.contains("#"))
				continue;
			String type = source.split(" ")[0];
			if (type.contains("Client#getGameObjectComposite()") || type.contains("GameObjectComposite#getChildComposite()") 
					|| type.contains("getItemComposite()") || type.contains("NpcDefinition#getChildComposite()")) {
				methodHook = new MethodHook(source);
				methods.put(methodHook.getMethodKey(), methodHook);
				System.out.println(methodHook.getMethodKey()+" "+methodHook.getClassName()+"#"+methodHook.getMethodName()+" "+methodHook.getType()+" "+methodHook.getCorrectParam());
			} else {
				fieldHook = new FieldHook(source);
				if(fieldHook.getType().equalsIgnoreCase("I"))
				System.out.println(fieldHook.getFieldKey()+" "+fieldHook.getClassName()+"#"+fieldHook.getFieldName()+" "+fieldHook.getType()+" "+fieldHook.getMultiplier());
				else
					System.out.println(fieldHook.getFieldKey()+" "+fieldHook.getClassName()+"#"+fieldHook.getFieldName()+" "+fieldHook.getType());	
				fields.put(fieldHook.getFieldKey(), fieldHook);
			}
		}
		/*fieldHook = new FieldHook("Client#getCanvas() fi#qo Ljava/awt/Canvas;");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Client#getWidgetNodes() client#iz Lgi;");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Widget#getParentId() fa#al I -117382683");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Actor#animationId ax#by I 9827261");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("WidgetNode#getId() c#a I 1547999453");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Widget#getId() fa#z I 2003393859");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Client#getPlayerIndex() client#gf I -2046181907");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("NodeHashTable#getBuckets() gs#w L[ha;");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		methodHook = new MethodHook("Client#getItemComposite() x#a int 0");
		methods.put(methodHook.getMethodKey(), methodHook);*/
		/********************************TEMPORARY VARIABLES UNTIL UPDATED ONLINE**************************************/
		/*fieldHook = new FieldHook("Actor#getSpokenMessage() az#ad Ljava/lang/String;");
		fields.put(fieldHook.getFieldKey(), fieldHook);	
							
		fieldHook = new FieldHook("Client#getLoopCycle() client#f I 1141184381");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Actor#getHitCycles() az#aa I -1302720403");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Renderable#getModelHeight() cw#cm I -1118017801");
		fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("Actor#getQueueSize() az#ci I -1975992535");
		fields.put(fieldHook.getFieldKey(), fieldHook);
				
		//fieldHook = new FieldHook("NpcDefinition ar#j Lao;");
		//fields.put(fieldHook.getFieldKey(), fieldHook);
		
		fieldHook = new FieldHook("NPCComposite#getActions() ag#k [Ljava/lang/String;");
		fields.put(fieldHook.getFieldKey(), fieldHook);
					
					
		fieldHook = new FieldHook("NodeHashTable#getBuckets() gt#x L[hl;");
		fields.put(fieldHook.getFieldKey(), fieldHook);

		fieldHook = new FieldHook("Widget#getSlotContentIds() fw#eh [I");
		fields.put(fieldHook.getFieldKey(), fieldHook);
			
			
		methodHook = new MethodHook("Client#getItemComposite() aa#w byte 2");
		methods.put(methodHook.getMethodKey(), methodHook);*/
		/*****************************************************************************************/
		System.out.println("Total fields: "+fields.size()+" Methods: "+methods.size()+" BrokenHooks: "+broken);
		
}


}