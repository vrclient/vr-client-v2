package org.vrclient.main.client.injection;

import java.util.List;

import jdk.internal.org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.vrclient.component.RSCanvas;


public class CanvasInjector implements Injector {
	private static String CANVAS_EXT = RSCanvas.class
            .getCanonicalName().replaceAll("\\.", "/");
    @Override
	public boolean canRun(ClassNode node) {
    	 return node.superName.equals("java/awt/Canvas");
    }
	

	@Override
	public void run(ClassNode classNode) {
		try {
		// System.out.println("Canvas is: " + classNode.superName);
		 classNode.superName = CANVAS_EXT;
	        //System.out.println("Canvas is now: " + classNode.superName);
	        List<MethodNode> methods = (List<MethodNode>) classNode.methods;
			for (int i = 0; i < methods.size(); i++) {
				final MethodNode mn = methods.get(i);
				if (!mn.name.equals("<init>")) {
	                continue;
	            }
	            for (final AbstractInsnNode ain : mn.instructions.toArray()) {
	                if (ain.getOpcode() == Opcodes.INVOKESPECIAL
	                        && ((MethodInsnNode) ain).owner.equals("java/awt/Canvas")) {
	                    ((MethodInsnNode) ain).owner = CANVAS_EXT;
	                }
	            }
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
