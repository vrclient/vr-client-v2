package org.vrclient.main.client.injection;

import java.util.ListIterator;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.InsnList;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.VarInsnNode;
import org.vrclient.main.client.injection.callback.ObjectDefinitionCallBack;
import org.vrclient.main.client.parser.HookReader;

/**
 * Created by Hiasat on 8/24/14.
 */
public class ObjectDefinitionInjector implements Injector {
	@Override
	public boolean canRun(ClassNode classNode) {
		return  HookReader.fields.size() > 0 &&  HookReader.methods.get("Client#getGameObjectComposite()").getClassName().equals(classNode.name);
	}

	@Override
	public void run(ClassNode classNode) {
		ListIterator<MethodNode> mnIt = classNode.methods.listIterator();
		while (mnIt.hasNext()) {
			MethodNode mn = mnIt.next();
			if (mn.desc.equals(HookReader.methods.get("Client#getGameObjectComposite()").getType())) {
				callBack(mn);
			}
		}
	}


	private static void callBack(MethodNode mn) {
		InsnList nl = new InsnList();
		AbstractInsnNode[] mnNodes = mn.instructions.toArray();
		for (AbstractInsnNode abstractInsnNode : mnNodes) {
			if (abstractInsnNode.getOpcode() == Opcodes.ARETURN) {
				nl.add(new VarInsnNode(Opcodes.ILOAD, 0));
				nl.add(new MethodInsnNode(Opcodes.INVOKESTATIC, ObjectDefinitionCallBack.class.getCanonicalName().replace('.','/'), "add", "(" + "Ljava/lang/Object;I" + ")V"));
				nl.add(new VarInsnNode(Opcodes.ALOAD, 2));

			}
			nl.add(abstractInsnNode);
		}
		mn.instructions = nl;
		mn.visitMaxs(0, 0);
		mn.visitEnd();
	}



}