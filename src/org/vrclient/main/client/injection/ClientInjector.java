package org.vrclient.main.client.injection;

import java.util.List;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.*;
import org.vrclient.main.Configuration;
import org.vrclient.main.Constants;


public class ClientInjector implements Injector {
	private int revision;
	@Override
	public boolean canRun(ClassNode classNode) {
		  return classNode.name.equals("client");
		  //return classNode.name.equals("client") /*|| classNode.name.equals(HookReader.fields.get("client_renderArea").getClassName())*/;
	}
	  private IntInsnNode nextInt(AbstractInsnNode node) {
	        if (node == null) return null;
	        AbstractInsnNode current = node;
	        while ((current = current.getNext()) != null) {
	            if (current.getOpcode() == Opcodes.SIPUSH || current.getOpcode() == Opcodes.BIPUSH)
	                return ((IntInsnNode) current);
	        }
	        return null;
	    }
	    @SuppressWarnings("unchecked")
		private int getRevision(ClassNode client){
	        MethodNode init = null;
	        MethodNode fn = null;
	        for (MethodNode mn : (List<MethodNode>) client.methods) {
	            if (mn.name.equals("init"))
	                init = mn;
	            
	        }
	        if (init == null)
	            throw new RuntimeException("init() is null, this shouldn't happen!");
	        AbstractInsnNode current = init.instructions.getFirst();
	        while (true) {
	            try {
	                current = current.getNext();
	                IntInsnNode nextInt;
	                if ((nextInt = nextInt(current)).operand != 765)
	                    continue;
	                if ((nextInt = nextInt(nextInt)).operand != 503)
	                    continue;
	                revision = nextInt(nextInt).operand;
	                return nextInt(nextInt).operand;
	            } catch (NullPointerException e) {
	                throw new RuntimeException("Couldn't identify revision, no more instructions");
	            }
	        }
	    }
	    private static void callBack(MethodNode mn) {
			/*InsnList nl = new InsnList();
			AbstractInsnNode[] mnNodes = mn.instructions.toArray();
			for (AbstractInsnNode abstractInsnNode : mnNodes) {
				//if ((((localObject2 = (AbstractInsnNode)((ListIterator)localObject1).next()) instanceof FieldInsnNode)) && ((localObject2 = (FieldInsnNode)localObject2).getOpcode() == 178) && (((FieldInsnNode)localObject2).owner.equals(Hooks.client_scale.split("\\.")[0])) && (((FieldInsnNode)localObject2).name.equals(Hooks.client_scale.split("\\.")[1])))
				if (abstractInsnNode instanceof FieldInsnNode && abstractInsnNode.getOpcode() == 178 
						&& ((FieldInsnNode) abstractInsnNode).owner.equals(HookReader.fields.get("client_scale").getClassName())
						&& ((FieldInsnNode) abstractInsnNode).name.equals(HookReader.fields.get("client_scale").getFieldName())) {
					((FieldInsnNode) abstractInsnNode).owner = "injection/Injected";
					((FieldInsnNode) abstractInsnNode).name = "scale";
					int multiplier = Integer.valueOf(HookReader.fields.get("client_scale").getMultiplier());
				      for (int i = Integer.MIN_VALUE; i < Integer.MAX_VALUE; i) {
				        if (multiplier * i == 1)
				        {
				        	Injected.scale = i;
				        	//HookReader.fields.put("client_scale_Multiplier", i);
				          System.out.println("client_scale_Multiplier inverse set to: "  i);
				          break;
				        }
				      }
					}else
					if (abstractInsnNode instanceof FieldInsnNode && abstractInsnNode.getOpcode() == 178
							&& ((FieldInsnNode) abstractInsnNode).owner.equalsIgnoreCase(HookReader.fields.get("client_renderArea").getClassName())
							&& ((FieldInsnNode) abstractInsnNode).name.equalsIgnoreCase(HookReader.fields.get("client_renderArea").getFieldName())) {
						//((FieldInsnNode)abstractInsnNode).owner = "injection/Injected";
			           //((FieldInsnNode)abstractInsnNode).name = "renderArea";
			          // System.out.println(((FieldInsnNode)abstractInsnNode).owner"."((FieldInsnNode)abstractInsnNode).name);
			           for (int i = 0; i < Injected.renderArea.length; i) {
			               for (int j = 0; j < Injected.renderArea[i].length; j) {
			                 Injected.renderArea[i][j] = true;
			               }
			             }
					}
				}*/
	    }
	    @Override
	 	public void run(ClassNode classNode) {
	 		if(getRevision(classNode) != Integer.parseInt(Constants.REVISION)) {
	 			System.out.println(getRevision(classNode) + " does not match " + Integer.parseInt(Constants.REVISION));
	 			Configuration.getInstance().getBotFrame().closeClient();
	 		}
			
		}
			/*ListIterator<MethodNode> mnIt = classNode.methods.listIterator();
			while (mnIt.hasNext()) {
				MethodNode mn = mnIt.next();
					callBack(mn);
				}*/

}
