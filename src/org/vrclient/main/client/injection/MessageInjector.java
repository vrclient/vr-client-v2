package org.vrclient.main.client.injection;

import org.vrclient.main.client.parser.HookReader;
import org.vrclient.main.script.api.events.Event;
import org.objectweb.asm.tree.*;

import java.util.ListIterator;

public class MessageInjector implements Injector {
	String var1 = "v";
    String var2 = "e";
	@Override
	public boolean canRun(ClassNode classNode) {
		return var1.equals(classNode.name);
	}

	@Override
	public void run(ClassNode classNode) {
		  ListIterator<MethodNode> var7 = classNode.methods.listIterator();
	      while(var7.hasNext()) {
	    	 MethodNode var5;
	         if((var5 = var7.next()).name.equals(var2) && var5.desc.startsWith("(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String")) {
	        	InsnList var3;
	            (var3 = new InsnList()).add(new VarInsnNode(21, 0));
	            var3.add(new VarInsnNode(25, 1));
	            var3.add(new VarInsnNode(25, 2));
	            var3.add(new MethodInsnNode(184, Event.class.getCanonicalName().replaceAll("\\.", "/"), "submitMessageEvent", "(ILjava/lang/String;Ljava/lang/String;)V"));
	            InsnList var6;
	            AbstractInsnNode var4 = (var6 = var5.instructions).get(0);
	            var6.insertBefore(var4, var3);
	         }
	      }

	}
	
	


}
