package org.vrclient.main;

import java.awt.*;
import java.io.File;
import java.net.URI;

import org.vrclient.main.utils.NetUtils;

public class Constants {

	public static final double CLIENT_VERSION = 1.53;
	public static final String REVISION = NetUtils.readPage(Constants.SITE_URL + "/client/tools/revision.txt")[0];

	public static final String CLIENT_TITLE = "Violent Resolution";

	public static int APPLET_WIDTH = 760;
	public static int APPLET_HEIGHT = 503;


	public static Rectangle GAME_SCREEN = new Rectangle(0, 0, APPLET_WIDTH, APPLET_HEIGHT);
	public static Rectangle VIEWPORT = new Rectangle(5, 5, APPLET_WIDTH, APPLET_HEIGHT);

	public static final String HOME_PATH = (new File(System.getProperty("user.home")).exists() ? System.getProperty("user.home") : "/root") + "/VRClient";
	public static final String SETTING_PATH = HOME_PATH + File.separator + "settings";
	public static final String SCREENSHOT_PATH = HOME_PATH + File.separator + "screenshots";
	public static final String CALLER_PATH = HOME_PATH + File.separator + "settings" + File.separator + "callers";
	public static final String SNIPER_PATH = HOME_PATH + File.separator + "settings" + File.separator + "snipers";
	public static final String ACCOUNT_FILE_NAME = "Accounts.json";
	public static final String FORUM_FILE_NAME = "Forum.ini";
	public static final String SETTING_FILE_NAME = "Settings.ini";
	public static final String RS_FILE_NAME = " Login.ini";
    public static final String PATTERN_FILE_NAME = "Patterns.ini";

	public static final String SITE_URL = "http://www.vr-rs.com";
	public static final String BACKUP_SITE_URL = "http://www.SOME-SITE.com";
	public static boolean FAILED = false;

	public static final int[] WORLDS = {362};

	public static final String KEYBOARD_KEYS = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*~";

	public static final String PRELOGIN_URL = "http://vr-rs.com/forums/index.php?app=core&module=global&section=login";
	public static final String LOGIN_URL = "http://vr-rs.com/forums/index.php?app=core&module=global&section=login&do=process";
}
