package org.vrclient;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.*;
import java.net.URLConnection;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.vrclient.Update;
import org.vrclient.main.Constants;
import org.vrclient.main.utils.NetUtils;

/**
 * Created by VR on 7/29/2014.
 */
public class Update implements Runnable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String source, destination;
    private int percentage = 0;
    private int length, written;
    private final static String version = NetUtils.readPage(Constants.SITE_URL + "/client/tools/version.txt")[0];
    public static String workingDirectory = System.getProperty("user.dir");
    
    public Update(String source, String destination) {
        this.source = source;
        this.destination = destination;
    }

    @Override
    public void run() {
    	
        OutputStream output;
        InputStream input;
        URLConnection connection;
        try {
        	
        	 final JProgressBar jProgressBar = new JProgressBar();
        	 jProgressBar.setFont(new Font("Tahoma", Font.BOLD, 14));
             jProgressBar.setMaximum(100);
             jProgressBar.setStringPainted(true);
             jProgressBar.setBackground(new Color(50,50,50,255));
             jProgressBar.setForeground(new Color(0,144,255,255));
             JFrame frame = new JFrame("Client Update");
             frame.setContentPane(jProgressBar);
             frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
             frame.setResizable(false);
             frame.setSize(300, 70);
             Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
             int x = (int) ((dimension.getWidth() - frame.getWidth()) / 2);
             int y = (int) ((dimension.getHeight() - frame.getHeight()) / 2);
             frame.setLocation(x, y);
             
             frame.setVisible(true);
            connection = NetUtils.createURLConnection(source);
            length = connection.getContentLength();
            final File dest = new File(destination);

            if (dest.exists()) {
                final URLConnection savedFileConnection = dest.toURI().toURL().openConnection();
                if (savedFileConnection.getContentLength() == length) {
                    return;
                }
            } else {
                final File parent = dest.getParentFile();
                if (!parent.exists()) parent.mkdirs();
            }

            output = new FileOutputStream(dest);
            input = connection.getInputStream();

            final byte[] data = new byte[1024];
            
            int read;

            while ((read = input.read(data)) != -1) {
            	output.write(data, 0, read);
            	written += read;
                percentage = (int) (((double) written / (double) length) * 100D);
                        jProgressBar.setValue(percentage);
                        jProgressBar.setString(percentage + "%");
                    }
            

            output.flush();
            output.close();
            input.close();

        } catch (IOException a) {
            System.out.println("Error downloading file!");
            a.printStackTrace();
        }
    }

    public boolean isFinished() {
        return written == 0 || length == written;
    }

    public int getPercentage() {
        return percentage;
    }
    public static void main(String[] args) {
    	new Update(Constants.SITE_URL + "/client/download/vrclientv"+version+".exe",workingDirectory + "/vrclientv"+version+".exe");
    }
}
